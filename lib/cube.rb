
# 分析用のキューブ

class Cube
  # 引数
  #     dim 次元 (1以上)
  def initialize dim
    raise ArgumentError if dim < 1
    @dim = dim
    @axes = []
    (0 .. dim - 1).each {|n| @axes[n] = {} }
    @data = []
  end

  # add [m, n, ...], val
  # valは整数でなければならない
  def add keys, val
    raise ArgumentError if keys.size != @dim
    raise TypeError if !val.is_a?(Integer)
    
    ary = @data
    keys.each_with_index {|key, i|
      axis_map = @axes[i]
      axis_map[key] = axis_map.size if !axis_map[key]
      if i != @dim - 1
        ary[axis_map[key]] = [] if !ary[axis_map[key]]
        ary = ary[axis_map[key]]
      else
        ary[axis_map[key]] = ary[axis_map[key]].to_i + val
      end
    }
  end
  
  # 値、あるいは値の合計
  # keysのいずれかがラムダ式の場合は適宜計算する。
  def [](*keys)
    raise ArgumentError if keys.size != @dim
    keys.each_with_index {|k, i|
      if k.is_a?(Proc)
        pkeys = keys.dup
        pkeys[i] = "__placeholder__"
        return k.call(self, *pkeys)
      elsif k.is_a?(String) && k == "__placeholder__"
        raise ArgumentError
      end
    }
    return sum_or_nil(0, @data, keys)
  end
  
  # 軸のキーの配列
  #     n   0から始まる軸番号
  def axis n
    @axes[n].keys.sort {|x, y| x <=> y}
  end

  private
  # 再帰的に値を求める
  def sum_or_nil i, data_ary, keys
    raise "i = #{i}, keys = #{keys.inspect}, ary = #{data_ary.inspect}" if keys.size + i != @dim
    return data_ary.to_i if keys.empty?
    
    data_ary = [] if !data_ary
    axis = @axes[i]
    keys = keys.dup
    key = keys.shift
    if !key
      val = 0
      data_ary.each {|x|
        val += sum_or_nil(i + 1, x, keys)
      }
      return val
    else
      return sum_or_nil(i + 1, (axis[key] ? data_ary[axis[key]] : nil), keys)
    end
  end
end


if __FILE__ == $0
  c = Cube.new 3
  c.add [:x, 1, 'a'], 10
  c.add [:x, 1, 'hoge'], 20
  c.add [:z, 1, 'a'], 30
  p c[:x, 1, 'hoge']  #=> 20
  p c[:z, 1, 'hoge']  #=> nil
  p c[nil, nil, 'a']  #=> 40
end
