# -*- coding:utf-8 -*-

# キューブを表示するためのテーブル
# 低い次元をy軸にする

class MapTable
  class Cell
    def initialize value, opt_hash
      @value = value
      @th = opt_hash[:th]
      (@opt = opt_hash.dup).delete(:th)
    end
    attr_reader :value, :th, :opt
  end

  def initialize
    @rows = []
  end

  def map x, y, value, opt_hash = {}
    raise TypeError if !x.is_a?(Integer)
    raise TypeError if !y.is_a?(Integer)
    
    @rows[y] = [] if !@rows[y]
    @rows[y][x] = Cell.new(value, opt_hash)
  end
  
  def to_html
    s = '<table border="1" style="border-collapse:collapse">'
    @rows.each {|row|
      s << "<tr>" + to_html_row(row || []) + "</tr>"
    }
    s << "</table>"
    return s
  end

  private
  def to_html_row row
    raise TypeError, "row expected an Array, but #{row.class}" if !row.is_a?(Array)
    s = ""
    i = 0
    while i < row.size 
      cell = row[i]
      if cell
        s << (cell.th ? "<th" : "<td")
        cell.opt.each {|k, v|
          s << " #{k}=\"#{v}\""
        }
        s << ">"
        s << cell.value.to_s  if cell.value
      else
        s << "<td>"
      end
      i += (cell && cell.opt[:colspan] ? cell.opt[:colspan] : 1)
    end
    return s
  end
end

