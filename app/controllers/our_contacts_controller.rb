# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 請求書発行者マスタ
class OurContactsController < ApplicationController
  before_filter :login_required
  
  before_filter :role_keiri_required,
                :only => [:new, :create, :edit, :update, :destroy]

  before_filter :set_our_contact, :only => [:show, :edit, :update, :destroy]

  
  # GET /our_contacts
  # GET /our_contacts.xml
  def index
    @our_contacts = OurContact.order("company_setting_id")
  end

  
  # GET /our_contacts/1
  # GET /our_contacts/1.xml
  def show
    #@our_contact = OurContact.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @our_contact }
    end
  end

  # GET /our_contacts/new
  # GET /our_contacts/new.xml
  def new
    @our_contact = OurContact.new
  end

  
  # POST /our_contacts
  # POST /our_contacts.xml
  def create
    @our_contact = OurContact.new(
                        our_contact_params.permit(:company_setting_id, # newのみ
                                                  :lang, :zip, :addr1, :addr2,
                                                  :addr3, :name, :tel) )
    if @our_contact.save
      flash[:notice] = 'OurContact was successfully created.'
      redirect_to :action => "show", :id => @our_contact
    else
      render :action => "new"
    end
  end

  
  # GET /our_contacts/1/edit
  def edit
    @company = @our_contact.company_setting
  end
  

  # PUT /our_contacts/1
  # PUT /our_contacts/1.xml
  def update
    @company = @our_contact.company_setting

    if @our_contact.update_attributes(
                        our_contact_params.permit(:lang, :zip, :addr1, :addr2,
                                                  :addr3, :name, :tel) )
      flash[:notice] = 'OurContact was successfully updated.'
      redirect_to :action => "show", :id => @our_contact
    else
      render :action => "edit" 
    end
  end

  # DELETE /our_contacts/1
  # DELETE /our_contacts/1.xml
  def destroy
    @our_contact.destroy
    redirect_to :action => "index"
  end

  
  ###########################################
  private
  def set_our_contact
    @our_contact = OurContact.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def our_contact_params
    params.fetch(:our_contact, {})
  end

end # OurContactsController
