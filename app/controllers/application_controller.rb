# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011-2013 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Acts as Authenticatedプラグインを使用する。
#require "rails_sup/authenticated_system/controller_helper"
require "nkf"


class ApplicationController < ActionController::Base
  # Pagination
  include Pagy::Backend

  # Only allow modern browsers supporting webp images, web push, badges, import maps, CSS nesting, and CSS :has.
  allow_browser versions: :modern

  # ポカ避けのため、ここに仕込む。不要なクラスでは `skip_before_action` すること
  before_action :require_login

  
private

  # Sorcery: 未ログインの状態で要ログインのページにアクセスした時に呼び出される
  def not_authenticated
    flash[:alert] = "ログインしてください。"
    redirect_to sign_in_account_path() #"/account/sign_in"
  end

  
  # for `before_action`
  def role_keiri_required
    if !current_user.has_role?(:keiri)
      redirect_to '/account/sign_in'
    end
  end

  
  def role_hrd_required
    if !current_user.has_role?(:hrd)
      redirect_to '/account/sign_in'
    end
  end

  
  def role_apar_required
    if !current_user.has_role?(:apar)
      redirect_to '/account/sign_in'
    end
  end

end  # of ApplicationController


#################################################################
# 共通ルーチン

# 10^n倍して四捨五入
def nominal_amount real, exp
  (real.to_f * (10 ** exp) + 0.5).floor
end


def date_from_select(s, name)
  return Date.new(s["#{name}(1i)"].to_i, s["#{name}(2i)"].to_i, s["#{name}(3i)"].to_i)
end


################ 文字コード関係

# UTF-8 to Shift_JIS (cp932)
def utf2sjis s
  if s
    NKF.nkf('-m0 -s -W', s)
  else
    nil
  end
end

def sjis2utf s
  if s
    NKF.nkf('-w -S -m0', s)
  else
    nil
  end
end


# 外貨建の金額を円建てに換算
# @deprecated   Use get_gl_amount().
# @return [Array] 配列[円建ての金額, 為替レート]
def get_yen_amount entry_day, amount, currency
  raise TypeError if !currency.is_a?(Currency)

  # 直近の日付を探して
  cr = CurrencyRate.find :first,
                           :select => 'MAX(date) as date',
                           :conditions => ['currency_id = ? AND date <= ?',
                                           currency.id, entry_day]
  raise "not found currency rate: currency = #{currency.name}, entry_day = #{entry_day}" if !cr || !cr.date

  # 適切なレコードを取得する
  cr = CurrencyRate.find :first,
                           :conditions => ['currency_id = ? AND date = ?',
                                           currency.id, cr.date]
  return [(Float(amount) * cr.rate / 10 ** currency.exponent + 0.5).floor, 
          cr.rate]
end


# 外貨建の金額を機能通貨建てに換算
# @return [Array] 配列[機能通貨建ての金額 (正規化された金額), 為替レート]
def get_gl_amount company, entry_day, amount, amt_currency
  raise TypeError if !company.is_a?(Company)
  raise TypeError if !amt_currency.is_a?(Currency)

  # 直近の日付を探して
  cr = CurrencyRate.where('currency_id = ? AND date <= ?',
                                              amt_currency.id, entry_day) \
                   .select('MAX(date) as date') \
                   .order('').first
  raise "not found currency rate: currency = #{amt_currency.code}, entry_day = #{entry_day}" if !cr || !cr.date

  # 適切なレコードを取得する
  cr = CurrencyRate.find :first,
                         :conditions => ['currency_id = ? AND date = ?',
                                           amt_currency.id, cr.date]
  fr = CurrencyRate.find :first,
                         :conditions => ["currency_id = ? AND date = ?",
                                       company.functional_currency.id, cr.date]

  return [ ((BigDecimal.new(amount.to_s) * cr.rate / fr.rate) / 
                  10 ** amt_currency.exponent + BigDecimal.new("0.5")).floor, 
           (cr.rate / fr.rate) ]
end


def currency_rate entry_day, currency1, currency2
  # 直近の日付を探して
  cr = CurrencyRate.find :first,
                           :select => 'MAX(date) as date',
                           :conditions => ['currency_id = ? AND date <= ?',
                                           currency1.id, entry_day]
  raise "not found currency rate: currency = #{currency1.name}, entry_day = #{entry_day}" if !cr || !cr.date

  # 適切なレコードを取得する
  cr1 = CurrencyRate.find :first,
                          :conditions => ['currency_id = ? AND date = ?',
                                           currency1.id, cr.date]
  cr2 = CurrencyRate.find :first,
                          :conditions => ['currency_id = ? AND date = ?',
                                           currency2.id, cr.date]

  return cr1.rate / cr2.rate
end


# 3桁ごとにカンマを挿入
def format_amount(n, exp)
  raise TypeError, "n must be Numeric, but #{n.class}" if !n.is_a?(Numeric)
  raise TypeError if !exp.is_a?(Integer)
  raise ArgumentError if exp < 0

  n = BigDecimal.new(n.to_s) / (BigDecimal.new("10.0") ** exp) if exp != 0
  intpart, exppart = n.to_s.split('.', 2)
  # raise "exp is #{exp}, but n = #{n}" if exp != 0 && exppart && exppart.length > exp
  intstr = intpart.reverse.split(/([0-9]{1,3})/).collect {|d| d == "" ? ',' : d}.join('').reverse.chop
  if exp == 0
    return intstr
  end

  if exp > exppart.length
    sprintf("%s.%s%s", intstr, exppart, "0" * (exp - exppart.length) )
  elsif exp == exppart.length
    sprintf("%s.%s", intstr, exppart)
  else
    sprintf "%s.%s", intstr, exppart[0..2]
  end
end

