# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 資格マスタ
class QualificationsController < ApplicationController
  before_filter :login_required

  before_filter :role_hrd_required,
                :only => [:new, :create, :edit, :update, :destroy]
  
  before_filter :set_qualification, :only => [:show, :edit, :update, :destroy]


  # GET /qualifications
  # GET /qualifications.xml
  def index
    @qualifications = Qualification.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @qualifications }
    end
  end

  # GET /qualifications/1
  # GET /qualifications/1.xml
  def show
    #@qualification = Qualification.find(params[:id])
  end

  
  # GET /qualifications/new
  # GET /qualifications/new.xml
  def new
    @qualification = Qualification.new
    @qualification.active = true
  end

  
  # GET /qualifications/1/edit
  def edit
    #@qualification = Qualification.find(params[:id])
  end

  # POST /qualifications
  # POST /qualifications.xml
  def create
    @qualification = Qualification.new(
                        qualification_params.permit(:active, :name, :kana_name))
    @qualification.create_user_id = current_user.id

    respond_to do |format|
      if @qualification.save
        flash[:notice] = 'Qualification was successfully created.'
        format.html { redirect_to :action => 'show', :id => @qualification }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @qualification.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /qualifications/1
  # PUT /qualifications/1.xml
  def update
    #@qualification = Qualification.find(params[:id])

    if @qualification.update_attributes(
                        qualification_params.permit(:active, :name, :kana_name))
      flash[:notice] = 'Qualification was successfully updated.'
      redirect_to :action => "show", :id => @qualification
    else
      render :action => "edit"
    end
  end

  
  # DELETE /qualifications/1
  # DELETE /qualifications/1.xml
  def destroy
    #@qualification = Qualification.find(params[:id])
    @qualification.destroy

    respond_to do |format|
      flash[:notice] = '資格を削除しました。'
      format.html { redirect_to :action => 'index' }
      format.xml  { head :ok }
    end
  end
  
  # 資格 - 社員リスト
  def all
    @qualifications = Qualification.find(:all)
  end


  ###########################################
  private
  def set_qualification
    @qualification = Qualification.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def qualification_params
    params.fetch(:qualification, {})
  end

end # class QualificationsController
