# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# カンパニー設定
class CompaniesController < ApplicationController

  before_action :check_currency_count, :only => [:index, :new]
  before_action :set_company,
                :only => [:show, :edit, :update, :destroy, :gl_fix, :gl_fix2]
  
  
  # for before_action
  def check_currency_count
    if Currency.count == 0
      flash[:alert] = "通貨が一つもありません。先に通貨マスタ画面で通貨を登録してください。" 
      redirect_to :controller => "currencies", :action => "index"
    end
  end
  private :check_currency_count


  def index
    @pagy, @companies = pagy(Company.all)
  end


  def show
    #@company = Company.find params[:id]
  end

  def new
    @company = Company.new
    @company.active = true
  end


  # 作成
  def create
    @company = Company.new(company_params.permit(:active,
                                                 :name, :name_en, :country,
                                                 :currency_id, :si_counter,
                                                 :si_footer, :tax_reg_number))
    today = Date.today
    @company.attributes = {
      :fixed_day => today,
      :fixed_gl_day => today,
    }
    @company.si_counter = 1 if @company.si_counter.to_i == 0
    @company.si_footer = "" if !@company.si_footer
    if !company_params[:logo_image].blank?
      @company.logo_image = company_params[:logo_image].read
    end
    
    begin
      @company.save!
    rescue ActiveRecord::RecordInvalid
      render 'new', status: :unprocessable_entity
      return
    end

    flash[:notice] = 'カンパニーを作成しました。'
    redirect_to :action => 'show', :id => @company
  end

  
  # マスタ系の編集 (画面)
  def edit
    #@company = Company.find params[:id]
    @divisions = Division.where("end_day IS NULL AND company_id = ?", @company.id)
                         .order("division_code")
    if @divisions.count == 0
      flash[:alert] = "このカンパニーには会計部門が一つもありません。まず会計部門/コストセンタを登録してください。"
      redirect_to :controller => "control_divisions", :action => "index"
    end
  end
  

  def update
    #@company = Company.find params[:id]
    image = company_params[:logo_image]
    @company.attributes = company_params.permit(:active,
                                                :name, :name_en, :country,
                                                :currency_id, :si_counter,
                                                :si_footer, :tax_reg_number)
    @company.logo_image = image.read if !image.blank?

    begin
      Company.transaction do 
        if !@company.company_setting
          cs = CompanySetting.new :company_id => @company.id,
                       :bank_division_id => params[:bank_division_id],
                       :fallback_division_id => params[:fallback_division_id]
          cs.save!
        else
          @company.company_setting.attributes = {
            :bank_division_id => params[:bank_division_id],
            :fallback_division_id => params[:fallback_division_id]
          }
          @company.company_setting.save!
        end
        @company.save!
      end
    rescue ActiveRecord::RecordInvalid
      @divisions = Division.find :all, 
           :conditions => ["end_day IS NULL AND company_id = ?", @company.id],
           :order => "division_code"
      render 'edit', status: :unprocessable_entity
      return
    end

    flash[:notice] = 'カンパニーを更新しました。'
    redirect_to :action => 'show', :id => @company
  end
  

  # 伝票表示日, 仕訳生成確定日の編集 (画面)
  def gl_fix
    #@company = Company.find params[:id]
  end

  
  def gl_fix2
    #@company = Company.find params[:id]
    if @company.update_attributes(
                       company_params.permit(:fixed_day, :fixed_gl_day))
      flash[:notice] = 'company_setting was successfully updated.'
      redirect_to :action => 'show', :id => @company
    else
      render :action => 'gl_fix'
    end
  end

  
  def destroy
    begin
      Company.transaction do
        #company = Company.find params[:id]
        @company.company_setting.destroy if @company.company_setting
        @company.destroy
      end
    rescue ActiveRecord::InvalidForeignKey => @error
      render :action => 'show'
      return
    end
    flash[:notice] = 'カンパニーを削除しました。'
    redirect_to :action => 'index'
  end
  
  # 画像データ
  def logo_image
    r = Company.find params[:id]
    send_data r.logo_image, :type => "image/png"
  end

  
private ###########################################
  
  def set_company
    @company = Company.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def company_params
    params.fetch(:company, {})
  end

end # class CompaniesController
