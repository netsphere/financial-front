# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 入金・出金伝票
class PaymentSlipsController < ApplicationController
  before_filter :login_required
  before_filter :role_keiri_required

  before_filter :set_payment_slip, :only => [:show, :edit, :update, :destroy,
                                             :print, :edit_cash, :remain ]

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post,
         :only => [ :destroy, :create, :sign_off, :add_lines, 
                    :destroy_line, :update_cash, :cancel_slip ],
         :redirect_to => { :controller => "receivings", :action => :index }

  verify :method => :patch, :only => [ :update ],
         :redirect_to => { :controller => "receivings", :action => :index }

=begin
  # GET /payment_slips
  # GET /payment_slips.xml
  def index
    @payment_slips = PaymentSlip.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @payment_slips }
    end
  end
=end


  def set_partner_count
    x_= Partner.find_by_sql [<<EOF, @payment_slip.id]
SELECT DISTINCT sales_invoices.partner_id AS id
    FROM receivings
    LEFT JOIN sales_invoices ON receivings.sales_invoice_id = sales_invoices.id
    WHERE cash_slip_id = ?
EOF
    @ar_partner_count = x_ ? x_.size : 0

    x_ = Partner.find_by_sql [<<EOF, @payment_slip.id]
SELECT DISTINCT invoices.partner_id AS id
    FROM payments
    JOIN invoices ON payments.invoice_id = invoices.id
    WHERE payment_slip_id = ?
EOF
    @ap_partner_count = x_ ? x_.size : 0
  end
  private :set_partner_count


  # 詳細表示
  def show
    #@payment_slip = PaymentSlip.find params[:id]
    set_partner_count
    @cash_records = @payment_slip.payment_banks
  end


  # 伝票印刷
  def print
    #@payment_slip = PaymentSlip.find params[:id]
    @petty_pays = PettyPay.find :all, 
                       :conditions => ["cash_slip_id = ?", @payment_slip.id],
                       :order => "user_id, id"
  end


  # 新しい入金/出金伝票
  def new
    @company = Company.find params[:company]
    @payment_slip = PaymentSlip.new
    @payment_slip.slip_type = params[:slip_type]

    # 小口精算の支払い
    @petty_pay = params[:petty_pay]

    # 小口精算の会社への返金
    if !params[:petty_payback].blank?
      @petty_payback = PettyExpense.find params[:petty_payback]
    end
  end


  # GET /payment_slips/1/edit
  def edit
    #@payment_slip = PaymentSlip.find params[:id]
  end


  def edit_cash
    #@payment_slip = PaymentSlip.find params[:id]
    @cash_records = @payment_slip.payment_banks
  end


  # 新規作成
  def create
    @company = Company.find params[:company]
    @payment_slip = PaymentSlip.new(
                             payment_slip_params.permit(:slip_type, :date) )
    @payment_slip.attributes = {
      :state => 0,
      :company_id => params[:company]
    }
    @payment_slip.create_user_id = current_user.id

    begin
      PaymentSlip.transaction do 
        @payment_slip.save!

        # 小口精算の支払い
        @petty_pay = params[:petty_pay]
        if !@petty_pay.blank?
          pays = PettyPay.find(:all, 
                   :conditions => ["demand_batch_id = ? AND transaction_type = 'PAYM'", @petty_pay])
          pays.each do |pay|
            pay.cash_slip_id = @payment_slip.id
            pay.save!
          end
        end

        # 小口精算の会社への返金
        if !params[:petty_payback].blank?
          @petty_payback = PettyExpense.find params[:petty_payback]
          
          receiving = Receiving.new :cash_slip_id => @payment_slip.id,
                             :petty_expense_id => @petty_payback.id,
                             :amount => @petty_payback.receivable_amount_total,
                             :currency_id => @petty_payback.reimb_currency_id,
                             :charge => 0
          receiving.save!
        end
      end # transaction
    rescue ActiveRecord::RecordInvalid => @error
      render :action => "new"
      return
    end

    redirect_to( {:action => "show", :id => @payment_slip}, 
                 :notice => 'PaymentSlip was successfully created.' )
  end


  # PUT /payment_slips/1
  # PUT /payment_slips/1.xml
  def update
    #@payment_slip = PaymentSlip.find params[:id]

    if @payment_slip.update_attributes(payment_slip_params.permit("date(1i)", "date(2i)", "date(3i)") )
      redirect_to( {:action => "show", :id => @payment_slip}, 
                  :notice => 'PaymentSlip was successfully updated.' )
    else
      render :action => "edit"
    end
  end


  # 伝票の削除 (実行)
  def destroy
    #@payment_slip = PaymentSlip.find params[:id]

    if @payment_slip.state == 20 || @payment_slip.state == 30
      flash[:alert] = "会計システム連動済みまたはキャンセルされた伝票は, 削除できません。"
      redirect_to :action => "show", :id => @payment_slip
      return
    end
      
    PaymentSlip.transaction do
      cond = ["payment_slip_id = ?", @payment_slip.id]
      Payment.delete_all cond
      PaymentBank.delete_all cond
      Receiving.delete_all ["cash_slip_id = ?", @payment_slip.id]
      RsLog.delete_all cond
      @payment_slip.destroy
    end

    flash[:notice] = "出金伝票を削除しました。"
    redirect_to :controller => "receiving_slips", :action => "list", 
                                        :company => @payment_slip.company
  end


  # 支払う債務を選ぶ
  def remain
    #@payment_slip = PaymentSlip.find params[:id]
    @currency = Currency.find params[:currency]
    @company = @payment_slip.company

    @due_day = params[:due] ? date_from_select(params[:due], 'day') : 
                              Date.today + 5
    # 相殺の場合は取引先限定.
    @ar_partner = params[:ar_partner] ? Partner.find(params[:ar_partner]) : nil
      
    # 輸入消費税の考慮を忘れずに
    cond = [<<EOF, @due_day, @company.id, @currency.id]
        (invoices.bank_account_id IS NULL) AND 
        (state <= 20) AND payment_state <> 20 AND 
        (due_day <= ?) AND 
        company_setting_id = ? AND
        currency_id = ? AND
        amount_total <> (SELECT coalesce(sum(payments.amount), 0) FROM payments 
                     LEFT JOIN payment_slips ON payments.payment_slip_id = payment_slips.id
                     WHERE payment_slips.state <> 30 AND invoices.id = payments.invoice_id)
EOF
    # 相殺と, 出金が複数口座から出ている場合は, 別の取引先は不可
    if @ar_partner ||
       (@payment_slip.payment_banks.size > 1 && !@payment_slip.payments.empty?)
      cond[0] << " AND partner_id = ?"
      cond << (!params[:ar_partner].blank? ? params[:ar_partner] : 
                              @payment_slip.payments.first.invoice.partner.id)
    end

    @invoices = Invoice.find :all,
                       :conditions => cond,
                       :order => "partner_id, buy_day"
  end


  # 債務の消し込み行を追加(実行)
  def add_lines
    @payment_slip = PaymentSlip.find params[:id]

    if params[:inv].blank?
      flash[:notice] = "行は追加されませんでした。"
      redirect_to :action => "show", :id => @payment_slip
      return
    end
    
    payments = []
    currency = nil
    params[:inv].each do |key, r|
      next if r[:amount].blank?
      currency = Invoice.find(key).currency

      # 請求書単位での金額
      amount = nominal_amount r[:amount], currency.exponent

      # 科目単位に分解する
      remain_amounts = Invoice.find(key).remain_ac_amounts
      remain_amounts.each do |crac_id, remain|
        # クレジットの場合はremainがマイナスになっている
        if remain != 0
          pay_amount = remain >= 0 ? [remain, amount].min : 
                                       [remain, amount].max
          payments << (Payment.new :payment_slip_id => @payment_slip.id,
                                 :invoice_id => key,
                                 :account_title_id => crac_id,
                                 :amount => pay_amount,
                                 :currency_id => currency.id )
          amount -= pay_amount
          break if amount == 0
        end
      end
    end

    Payment.transaction do 
      amount_total = 0
      payments.each do |r|
        r.save!
        amount_total += r.amount
      end
=begin
債務の通貨の口座がないことがある。無理筋
      if @payment_slip.payment_banks.empty?
        bank = nil
        OurBank.find(:all).each do |b|
          if b.currency.code == currency.code
            bank = b
            break
          end
        end
        r = PaymentBank.new :payment_slip => @payment_slip,
                        :our_bank => bank,
                        :cash_amount => amount_total,
                        :inv_amount => amount_total
        r.save!
      end
=end
    end # transaction

    flash[:notice] = "債務の消し込み行を追加しました。"
    redirect_to :action => "show", :id => @payment_slip
  end # add_lines()


  # 債務の消し込み行を1行削除
  def destroy_line
    @payment_slip = PaymentSlip.find params[:id]
    count = @payment_slip.payments.size + @payment_slip.receivings.size
    line = Payment.find params[:line]

    Payment.transaction do 
      line.destroy
      # 入出金だけ、という伝票を作らない
      if count == 1
        PaymentBank.delete_all ["payment_slip_id = ?", @payment_slip.id]
      end
    end # transaction

    flash[:notice] = "債務の消し込み行を削除しました。"
    redirect_to :action => "show", :id => @payment_slip
  end


  # 伝票のうち, 出金を更新
  def update_cash
    @payment_slip = PaymentSlip.find params[:id]

    if !@payment_slip.payments.empty?
      inv_currency = @payment_slip.payments.first.currency
    elsif !@payment_slip.petty_pays.empty?
      inv_currency = @payment_slip.petty_pays.first.currency
    elsif !@payment_slip.receivings.empty?
      inv_currency = @payment_slip.receivings.first.currency
    else
      raise "internal error"
    end

    lines = params[:cash_lines].to_i
    @cash_records = []
    (0..(lines - 1)).each do |lineno|
      pa = params["payment_bank#{lineno + 1}"]
      if pa[:cash_amount].to_f != 0.0
        bank = OurBank.find pa[:our_bank]
        pb = PaymentBank.new :payment_slip => @payment_slip,
                   :our_bank => bank,
                   :cash_amount => nominal_amount(pa[:cash_amount], 
                                                  bank.currency.exponent)

        if bank.currency.id == inv_currency.id
          pb[:inv_amount] = nominal_amount(pa[:cash_amount], 
                                             inv_currency.exponent)
        else
          pb[:inv_amount] = nominal_amount(pa[:inv_amount], 
                                             inv_currency.exponent)
        end

        if @payment_slip.slip_type == 2 # 出金
          pb.cash_amount = -pb.cash_amount
        end
        @cash_records << pb
      end
    end

    begin
      PaymentSlip.transaction do
        PaymentBank.delete_all ["payment_slip_id = ?", @payment_slip.id]

        @cash_records.each do |cash_record|
          cash_record.save!
        end
      end # transaction
    rescue ActiveRecord::RecordInvalid
      set_partner_count
      render :action => "show"
      return
    end 

    flash[:notice] = "出金を更新しました。"
    redirect_to :action => "show", :id => @payment_slip
  end


  # 経理承認 (実行)
  def sign_off
    @payment_slip = PaymentSlip.find params[:id]
    PaymentSlip.transaction do 
      if @payment_slip.state < 10
        log = RsLog.new :payment_slip => @payment_slip,
                        :kind => RsLog::SIGN_OFF,
                        :remarks => "",
                        :done_by => current_user,
                        :done_at => Time.now
        log.save!
        @payment_slip.state = 10
        @payment_slip.save!
      end
    end # transaction
    flash[:notice] = "出金伝票を承認しました。"
    redirect_to :action => "show", :id => @payment_slip
  end


  def cancel_slip
    payment_slip = PaymentSlip.find params[:id]
    company = payment_slip.company
    PaymentSlip.transaction do
      payment_slip.state = 30
      payment_slip.save!

      log = RsLog.new :payment_slip => payment_slip,
                      :kind => RsLog::CANCEL,
                      :remarks => "",
                      :done_by => current_user,
                      :done_at => Time.now
      log.save!
    end # of transaction
    flash[:notice] = "出金伝票をvoidしました。"
    redirect_to :controller => "receiving_slips",
                :action => "list", :company => company
  end

  
  ###########################################
  private
  def set_payment_slip
    @payment_slip = PaymentSlip.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def payment_slip_params
    params.fetch(:payment_slip, {})
  end

end # class PaymentSlipsController
