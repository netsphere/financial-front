# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 添付ファイル
class AttachFilesController < ApplicationController
  before_filter :login_required
  
  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }
  
  # （自部門の）一覧表示
  def list
    div = current_user.default_division
    raise if !div
    @cdiv = div.default_controller
    raise if !@cdiv

    @attach_files = AttachFile.find :all,
        :select => 'id, original_filename, content_type, attach_kind_id, remarks, division_id, created_by, created_at',
        :conditions => ['division_id IN (SELECT division_id FROM controllers_divisions WHERE control_division_id = ?)', @cdiv]
  end

  
=begin
  def show
    @attach_file = AttachFile.find(params[:id])
  end
=end

  
  # 新規アップロード画面
  def new
    @attach_file = AttachFile.new
    @partner = Partner.find(params[:partner]) if params[:partner]
    @invoice = Invoice.find(params[:invoice]) if params[:invoice]
  end


  # 内部で @partner, @invoice を設定する
  # @return キャンセルで戻る先.
  def set_document
    if params[:invoice]
      @invoice = Invoice.find params[:invoice]
      to_opt = {:controller => 'invoices', :action => 'show', :id => @invoice}
    elsif params[:partner]
      @partner = Partner.find params[:partner]
      to_opt = {:controller => 'partners', :action => 'show', :id => @partner}
    else
      to_opt = {:action => 'list'}
    end

    return to_opt
  end
  private :set_document

  
  # ファイルをアップロードする
  def create
    up = params[:attach_file]

    # 内部で @partner, @invoice が設定される.
    to_return = set_document

    @attach_file = AttachFile.new(
                  :attach_kind_id => up[:attach_kind_id],
                  :remarks => up[:remarks],
                  :created_by => current_user,
                  :division => current_user.default_division)
    if !up[:file]
      @error = 'ファイルを選択してください。'
      render :action => 'new'
      return
    end
    @attach_file.data = up[:file].read
    @attach_file.original_filename = up[:file].original_filename
    @attach_file.content_type = up[:file].content_type

    AttachFile.transaction {
      @attach_file.save!
      ref = AttachFileRef.new(
                  :attach_file => @attach_file,
                  :partner => @partner,
                  :invoice => @invoice)
      ref.save!
    }
    flash[:notice] = "ファイルをアップロードしました。"
    redirect_to to_return
  end


  # コメントの編集
  def edit
    @attach_file = AttachFile.find(params[:id])
    @partner = Partner.find(params[:partner]) if params[:partner]
    @invoice = Invoice.find(params[:invoice]) if params[:invoice]
  end

  
  def update
    @attach_file = AttachFile.find(params[:id])

    # 内部で @partner, @invoice が設定される.
    to_return = set_document
    
    if @attach_file.update_attributes(
                      params[:attach_file].permit(:attach_kind_id, :remarks) )
      flash[:notice] = 'AttachFile was successfully updated.'
      redirect_to to_return
    else
      render :action => 'edit'
    end
  end

  
  # ファイル内容を出力する
  def download
    file = AttachFile.find params[:id]
    send_data file.data, :filename => file.original_filename, :type => file.content_type
  end
  
  # エントリを削除する
  def destroy
    AttachFile.transaction {
      file = AttachFile.find(params[:id])
      AttachFileRef.delete_all ['attach_file_id = ?', file.id]
      file.destroy
    }
    flash[:notice] = "添付ファイルを削除しました。"
    redirect_to :action => 'list'
  end

end
