# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 添付ファイルの種別
class AttachKindsController < ApplicationController
  before_filter :login_required
  before_filter :role_keiri_required, 
                :only => [:new, :create, :edit, :update, :destroy]

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :create, :update ],
         :redirect_to => { :action => :index }
  verify :method => :delete, :only => [ :destroy ],
         :redirect_to => { :action => :index }

  before_filter :set_attach_kind, :only => [:show, :edit, :update, :destroy]

  
  def index
    @attach_kinds = AttachKind.paginate(:page => params[:page], :per_page => 20) \
                              .order('sort')
  end

  
  def show
    #@attach_kind = AttachKind.find(params[:id])
  end

  def new
    @attach_kind = AttachKind.new
  end

  
  def create
    @attach_kind = AttachKind.new(
                     attach_kind_params.permit(:name, :sort, :enable_invoice) )
    if !@attach_kind.sort
      max_sort = AttachKind.select("max(sort) AS max_sort").order("").first
      @attach_kind.sort = max_sort.max_sort.to_i + 1
    end
    
    if @attach_kind.save
      flash[:notice] = 'AttachKind was successfully created.'
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    #@attach_kind = AttachKind.find(params[:id])
  end

  
  def update
    #@attach_kind = AttachKind.find(params[:id])
    if @attach_kind.update_attributes(
                  attach_kind_params.permit(:name, :sort, :enable_invoice) )
      flash[:notice] = 'AttachKind was successfully updated.'
      redirect_to :action => 'show', :id => @attach_kind
    else
      render :action => 'edit'
    end
  end

  
  def destroy
    @attach_kind.destroy
    redirect_to :action => 'index'
  end

  
  ###########################################
  private
  def set_attach_kind
    @attach_kind = AttachKind.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def attach_kind_params
    params.fetch(:attach_kind, {})
  end

end
