# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require "ostruct"


# 入金伝票
class ReceivingSlipsController < ApplicationController
  #before_filter :login_required
  before_action :role_keiri_required


  # カンパニーを選択
  def index
    @companies = current_user.accessible_companies
  end

  
  # 入金・出金の両方をリスト表示
  def list
    @company = Company.find(params[:company])
    @our_bank = OurBank.find(params[:our_bank]) if !params[:our_bank].blank?
    
    if !params[:term].blank?
      @term = OpenStruct.new({
        :st => date_from_select(params[:term], "st"),
        :ed => date_from_select(params[:term], "ed")
      })
    end

    if @our_bank
      cond = ['our_bank_id = ?', @our_bank.id]
      if @term
        cond[0] << " AND date BETWEEN ? AND ?"
        cond << @term.st << @term.ed
      end
=begin
      @receiving_slips = ReceivingSlip.paginate :page => params[:page], 
                :per_page => 25,
                :conditions => cond,
                :order => 'date DESC, id'
=end
      # 銀行口座がないときがありうる
      @cash_slips = PaymentSlip.paginate :page => params[:page],
                :select => "payment_slips.*, payment_banks.cash_amount AS cash_amount, payment_banks.our_bank_id AS our_bank_id",
                :per_page => 25,
                :joins => "LEFT JOIN payment_banks ON payment_slips.id = payment_banks.payment_slip_id",
                :conditions => cond,
                :order => "date DESC, id"
    else
      #r_cond = ['our_banks.company_setting_id = ?', @company.id]
      p_cond = ["company_id = ?", @company.id]
      if @term
        #r_cond[0] << " AND date BETWEEN ? AND ?"
        #r_cond << @term.st << @term.ed
        p_cond[0] << " AND date BETWEEN ? AND ?"
        p_cond << @term.st << @term.ed
      end
=begin
      @receiving_slips = ReceivingSlip.paginate :page => params[:page], 
              :per_page => 25,
              :select => 'receiving_slips.*',
              :joins => 'LEFT JOIN our_banks ON receiving_slips.our_bank_id = our_banks.id',
              :conditions => r_cond,
              :order => 'date DESC, id'
=end
      @cash_slips = PaymentSlip.paginate :page => params[:page],
                :select => "payment_slips.*, payment_banks.cash_amount AS cash_amount, payment_banks.our_bank_id AS our_bank_id",
                :joins => "LEFT JOIN payment_banks ON payment_slips.id = payment_banks.payment_slip_id",
                :per_page => 25,
                :conditions => p_cond,
                :order => "date DESC, id"
    end
  end


=begin
  # 伝票を表示
  def show
    @receiving_slip = PaymentSlip.find params[:id]
  end
=end


=begin
  # 新しい入金伝票
  def new
    @company = Company.find params[:company]
    if !params[:petty_expense].blank?
      @petty_expense = PettyExpense.find params[:petty_expense] 
    end
    @cash_slip = PaymentSlip.new

    @our_banks = OurBank.grouped_list @company
    if @our_banks.empty?
      flash[:alert] = "自社銀行口座がありません。まずマスタ設定してください。"
      redirect_to :controller => "our_banks", :action => "index"
    end
  end


  # GET /receiving_slips/1/edit
  def edit
    @receiving_slip = ReceivingSlip.find(params[:id])
    @our_bank = @receiving_slip.our_bank
  end


  # 伝票の作成
  def create
    @company = Company.find params[:company]
    if !params[:petty_expense].blank?
      @petty_expense = PettyExpense.find params[:petty_expense] 
    end

    @receiving_slip = ReceivingSlip.new params[:receiving_slip]
    @receiving_slip.attributes = {
      :created_by => current_user.id,
      :state => 0,
      :amount => 0
    }

    if @petty_expense
      @receiving_slip.receivings << 
             ( Receiving.new :petty_expense_id => @petty_expense.id,
                             :amount => @petty_expense.receivable_amount_total,
                             :charge => 0,
                             :currency_id => @petty_expense.reimb_currency_id )
    end

    if @receiving_slip.save
      flash[:notice] = 'ReceivingSlip was successfully created.'
      redirect_to :action => 'show', :id => @receiving_slip
    else
      @our_banks = OurBank.grouped_list @company
      render :action => "new"
    end
  end


  # PUT /receiving_slips/1
  # PUT /receiving_slips/1.xml
  def update
    @receiving_slip = ReceivingSlip.find params[:id]
    @receiving_slip.attributes = params[:receiving_slip]

    begin
      ReceivingSlip.transaction do 
        r = Receiving.find :first, :conditions => ["receiving_slip_id = ?", @receiving_slip.id]
        if r 
          recalc_total_amount
        end

        @receiving_slip.save!
      end
    rescue ActiveRecord::RecordInvalid
      render :action => "edit"
      return
    end

    flash[:notice] = 'ReceivingSlip was successfully updated.'
    redirect_to :action => 'show', :id => @receiving_slip
  end


  # 伝票削除
  def destroy
    @receiving_slip = ReceivingSlip.find params[:id]
    company = @receiving_slip.our_bank.company_setting

    ReceivingSlip.transaction do 
      Receiving.delete_all ["receiving_slip_id = ?", @receiving_slip.id]
      RsLog.delete_all ["receiving_slip_id = ?", @receiving_slip.id]
      @receiving_slip.destroy
    end

    flash[:notice] = "入金伝票を削除しました。"
    redirect_to :action => 'list', :company => company
  end
=end


  # 入出金伝票から、債権の消し込み行を1行削除
  def destroy_receiving
    @payment_slip = PaymentSlip.find params[:id]
    count = @payment_slip.payments.size + @payment_slip.receivings.size
    receiving = Receiving.find params[:receiving]

=begin    
    # @receiving_slip.receivings.delete @receiving
    Receiving.transaction do 
      if receiving.sales_invoice.currency.code == 
                                       @receiving_slip.our_bank.currency.code
        @receiving_slip.amount -= receiving.amount
        @receiving_slip.save!
        receiving.destroy
      else
=end
    Receiving.transaction do
      receiving.destroy
      # 入出金だけ、という伝票を作らない
      if count == 1
        PaymentBank.delete_all ["payment_slip_id = ?", @payment_slip.id]
      end
    end # transaction

    flash[:notice] = "入金行を削除しました。"
    redirect_to :controller => "payment_slips", :action => 'show', 
                :id => @payment_slip
  end


  # 経理承認
  def sign_off
    @receiving_slip = ReceivingSlip.find params[:id]
    ReceivingSlip.transaction do 
      if @receiving_slip.state < ReceivingSlip::SIGNED_OFF
        log = RsLog.new :receiving_slip => @receiving_slip,
                        :kind => RsLog::SIGN_OFF,
                        :remarks => "",
                        :done_by => current_user,
                        :done_at => Time.now
        log.save!
        @receiving_slip.state = ReceivingSlip::SIGNED_OFF
        @receiving_slip.save!
      end
    end # transaction      
    flash[:notice] = "入金伝票を承認しました。"
    redirect_to :action => "show", :id => @receiving_slip
  end


  # 未入金一覧 (表示)
  def remain
    today = Date.today
    @cash_slip = PaymentSlip.find params[:id]
    @currency = Currency.find params[:currency]

    @company = @cash_slip.company
    @due_day = params['due'] ? date_from_select(params['due'], 'day') : 
                               today + 5
    # 相殺の場合は取引先限定.
    @ap_partner = params[:ap_partner] ? Partner.find(params[:ap_partner]) : nil

    cond = [<<EOF, @due_day, @company.id, @currency.id]
(state = 10 OR state = 20) AND (due_day <= ?) AND company_setting_id = ? AND
    currency_id = ? AND
amount_total <> (SELECT coalesce(sum(receivings.amount), 0) FROM receivings 
                     LEFT JOIN payment_slips 
                                ON receivings.cash_slip_id = payment_slips.id
                     WHERE payment_slips.state <> 30 AND 
                              sales_invoices.id = receivings.sales_invoice_id)
EOF
    # 相殺の場合は, 別の取引先は不可
    if @ap_partner
      cond[0] << " AND partner_id = ?"
      cond << @ap_partner.id
    end
    
    @invoices = SalesInvoice.where(*cond).order('partner_id, sell_day')
  end

  
  # 入金消し込み: 債権を追加(実行)
  def add_receivings
    @receiving_slip = PaymentSlip.find params[:id]
    #our_bank = @receiving_slip.our_bank

    if params[:inv].blank?
      flash[:notice] = "行は追加されませんでした。"
      redirect_to :controller => "payment_slips", :action => 'show', 
                  :id => @receiving_slip
      return
    end

    currency = nil
    # amount_total = 0
    received = []
    params[:inv].each {|key, r|
      if !r[:amount].blank? || !r[:charge].blank?
        currency = SalesInvoice.find(key).currency if !currency

        amount = nominal_amount(r[:amount], currency.exponent)
        # amount_total += amount
        received << Receiving.new(
                        :cash_slip_id => @receiving_slip.id,
                        :amount => amount,
                        :charge => (r[:charge].blank? ? 0 : r[:charge]),
                        :sales_invoice_id => key,
                        :currency_id => currency.id
                    )
      end
    }

    PaymentSlip.transaction do
      received.each {|r|
        r.save!
      }
    end
=begin
      # 追加した行だけを加算するのは不味い。全部計算しなおす
      if currency == @receiving_slip.our_bank.currency
        recalc_total_amount
      end
      @receiving_slip.save!
    end # transaction
=end

    flash[:notice] = 'Receiving was successfully created.'
    redirect_to :controller => "payment_slips", :action => 'show', 
                :id => @receiving_slip
  end


=begin
  def recalc_total_amount
    a = Receiving.find :first, :select => "sum(amount) as amount", 
                  :conditions => ["receiving_slip_id = ?", @receiving_slip.id]
    @receiving_slip.amount = a.amount
  end
  private :recalc_total_amount
=end


  # 未払いの債務一覧
  def remain_payables
    @receiving_slip = ReceivingSlip.find params[:id]

    # 自分の銀行は円口座でも相手口座が外貨はありえる。
    @invoices = Invoice.find :all,
         :conditions => [],
         :order => "partner_id, buy_day"
  end


  # 印刷用画面
  def print
    @receiving_slip = ReceivingSlip.find(params[:id])
    @our_bank = @receiving_slip.our_bank
    raise TypeError if !@our_bank.is_a?(OurBank) # DEBUG
  end


  # 入金額を更新
  def update_amount
    @receiving_slip = ReceivingSlip.find params[:id]
    @receiving_slip.amount = 
            nominal_amount(params[:receiving_slip][:amount], 
                           @receiving_slip.our_bank.currency.exponent)
    @receiving_slip.save!

    flash[:notice] = 'Receiving was successfully updated.'
    redirect_to :action => 'show', :id => @receiving_slip
  end


  def cancel_slip
    receiving_slip = ReceivingSlip.find params[:id]
    company = receiving_slip.our_bank.company_setting

    ReceivingSlip.transaction do
      receiving_slip.state = 30
      receiving_slip.save!

      log = RsLog.new :receiving_slip => receiving_slip,
                      :kind => RsLog::CANCEL,
                      :remarks => "",
                      :done_by => current_user,
                      :done_at => Time.now
      log.save!
    end # of transaction
    flash[:notice] = "入金伝票をvoidしました。"
    redirect_to :action => "list", :company => company
  end
end
