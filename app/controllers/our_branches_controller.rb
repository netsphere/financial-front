# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 自社店舗
class OurBranchesController < ApplicationController
  # GET /our_branches.xml
  def index
    if params[:branch_code]
      @our_branches = OurBranch.find :all,
                  :conditions => ['branch_code = ?', params[:branch_code]],
                  :limit => 1
    else
      @our_branches = OurBranch.find(:all)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @our_branches }
    end
  end

  # GET /our_branches/1
  # GET /our_branches/1.xml
  def show
    @our_branch = OurBranch.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @our_branch }
    end
  end

  # GET /our_branches/new
  # GET /our_branches/new.xml
  def new
    @our_branch = OurBranch.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @our_branch }
    end
  end

  # GET /our_branches/1/edit
  def edit
    @our_branch = OurBranch.find(params[:id])
  end

  # POST /our_branches
  # POST /our_branches.xml
  def create
    @our_branch = OurBranch.new(params[:our_branch])

    respond_to do |format|
      if @our_branch.save
        flash[:notice] = 'OurBranch was successfully created.'
        format.html { redirect_to(@our_branch) }
        format.xml  { render :xml => @our_branch, :status => :created, :location => @our_branch }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @our_branch.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /our_branches/1
  # PUT /our_branches/1.xml
  def update
    @our_branch = OurBranch.find(params[:id])

    respond_to do |format|
      if @our_branch.update_attributes(params[:our_branch])
        flash[:notice] = 'OurBranch was successfully updated.'
        format.html { redirect_to(@our_branch) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @our_branch.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /our_branches/1
  # DELETE /our_branches/1.xml
  def destroy
    @our_branch = OurBranch.find(params[:id])
    @our_branch.destroy

    respond_to do |format|
      format.html { redirect_to(our_branches_url) }
      format.xml  { head :ok }
    end
  end
end
