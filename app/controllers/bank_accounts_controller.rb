# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require 'partner_search_box'


# 取引先に紐づける（振込先）銀行口座
class BankAccountsController < ApplicationController
  before_filter :login_required
  before_filter :role_keiri_required, :only => [:new, :create, :edit, :update]

  before_filter :set_bank_account, :only => [:show, :edit, :update, :destroy]

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => 'index' }

  
  # 一覧画面
  def index
    @bank_accounts = BankAccount.paginate :page => params[:page], 
                                 :per_page => 20
  end


  # 取引先を特定した一覧
  def list
    @partner = Partner.find params[:partner]
    @bank_accounts = BankAccount.paginate :page => params[:page],
                               :per_page => 25,
                               :conditions => ["partner_id = ?", @partner.id],
                               :order => "zaimu_ac"
    @en_bank_accounts = EnBankAccount.find :all, 
                    :conditions => ["partner_id = ?", @partner.id]
  end


  def show
    #@bank_account = BankAccount.find(params[:id])
  end


  # 新規作成 (画面)
  def new
    @partner = Partner.find params[:partner]
    @bank_branch = BankBranch.new
    @bank_account = BankAccount.new
    @bank_account.active = true
  end


  # 銀行・支店を選択するダイアログを表示
  def open_sel_bank
    render :layout => false, :format => "js"
  end


  def list_banks
    @bank_text = params[:bank]
    @branch_text = params[:branch]

    text_br = '%' + Bank.zen2han(@branch_text) + '%'
    br_zen = '%' + normalize_kc(@branch_text) + '%'
    
    if @bank_text.to_s.strip == ""
      # 支店名のみ
      @branches = BankBranch.find :all,
                         :conditions => ['name LIKE ? OR kana_name LIKE ?', 
                                         br_zen, text_br],
                         :limit => 31
    else
      # 両方
      text_b = '%' + Bank.zen2han(@bank_text) + '%'
      b_zen = '%' + normalize_kc(@bank_text) + '%'

      @branches = BankBranch.find :all,
             :select => 'bank_branches.*', 
             :joins => 'JOIN banks ON bank_branches.bank_id = banks.id',
             :conditions => ['(bank_branches.name LIKE ? OR bank_branches.kana_name LIKE ?) AND (banks.name LIKE ? OR banks.kana_name LIKE ?)', 
                             br_zen, text_br, b_zen, text_b],
             :limit => 31
    end

    render :layout => false, :format => "js"
  end


  # フォームの銀行・支店を更新
  def update_bank
    @branch = BankBranch.find params[:branch]
    render :layout => false, :format => "js"
  end


  # 作成
  def create
    @partner = Partner.find params[:partner]
    @bank_account = BankAccount.new(
                        bank_account_params.permit(:active, :zaimu_ac,
                                                   :ac_type, :ac_number,
                                                   :kana_name) )
    @bank_account.bank_branch_id = params[:branch_id]
    @bank_account.partner_id = @partner.id
    @bank_branch = @bank_account.bank_branch

    begin
      BankAccount.transaction do 
        @bank_account.save!
        flash[:notice] = 'BankAccount was successfully created.'
        redirect_to :action => 'list', :partner => @partner
      end # transaction
    rescue ActiveRecord::RecordNotUnique => e
      @error = e
      render :action => 'new'
    rescue ActiveRecord::RecordInvalid
      render :action => 'new'      
    rescue => e # その他
      @error = e
      render :action => 'new'
    end
  end


  # 編集画面
  def edit
    #@bank_account = BankAccount.find params[:id]
    @partner = @bank_account.partner
    @bank_branch = @bank_account.bank_branch
  end


  def update
    #@bank_account = BankAccount.find params[:id]
    @partner = @bank_account.partner
    @bank_branch = BankBranch.find params[:branch_id]

    @bank_account.bank_branch_id = @bank_branch.id

    begin
      BankAccount.transaction do 
        now = Time.now

        @bank_account.attributes =
                        bank_account_params.permit(:active, :zaimu_ac,
                                                   :ac_type, :ac_number,
                                                   :kana_name)
        @bank_account.updated_at = now
        @bank_account.update_user_id = current_user.id
        @bank_account.save!

        @partner.updated_at = now
        @partner.updated_by = current_user
        @partner.save! # 更新日時を進める
      end # transaction
    rescue ActiveRecord::RecordNotUnique => e
      @error = e
      render :action => 'edit'
      return
    rescue ActiveRecord::RecordInvalid
      render :action => 'edit'
      return
    end

    flash[:notice] = 'BankAccount was successfully updated.'
    redirect_to :action => 'show', :id => @bank_account
  end


  def destroy
    partner = @bank_account.partner
    @bank_account.destroy
    redirect_to :controller => 'partners', :action => 'show', :id => partner
  end


  # 紐づける取引先を変更する画面
  def change_partner
    @bank_account = BankAccount.find params[:id]
  end
  
  def change_partner2
    @bank_account = BankAccount.find params[:id]

    @search = PartnerSearchBox.new(params[:search])
    @partners = @search.find_by_text
  end
  

  # 紐づけの変更 (実行)
  def change_partner_do
    @bank_account = BankAccount.find params[:id]
    @partner = Partner.find params[:partner]
    @bank_account.partner = @partner
    
    begin
      @bank_account.save!
    rescue ActiveRecord::RecordInvalid => e
      @bank_account = BankAccount.find params[:id] # 元に戻す

      @error = e
      @search = PartnerSearchBox.new
      @partners = []
      render :action => "change_partner2"
      return
    end

    flash[:notice] = '銀行口座は正常に更新されました。'
    redirect_to :controller => 'partners', :action => 'show', :id => @partner
  end


  ###########################################
  private
  def set_bank_account
    @bank_account = BankAccount.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def bank_account_params
    params.fetch(:bank_account, {})
  end

end # class BankAccountsController
