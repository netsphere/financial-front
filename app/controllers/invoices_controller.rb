# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require 'upload_module'
require 'invoice_search_box'
require "ostruct"
require 'will_paginate/array'


# (購買)請求書
class InvoicesController < ApplicationController
  include UploadModule
  before_filter :login_required
  before_filter :role_keiri_required, :only => [:sign_off_list, :sign_off]
  before_filter :role_apar_required

  before_filter :set_invoice, :only => [:show, :edit, :update, :destroy,
                                        :copy_new ]

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post,
         :only => [ :destroy, :create, :update, :print_out,
                    :change_partner_do, :cancel_invoice, :sign_off, 
                    :related_import3, :remove_import ],
         :redirect_to => { :action => :list }


  # （自部門の）請求書の一覧
  def list
    #初期表示…ログインユーザの部門IDから所属するコストセンタIDを取得
    div = current_user.default_division
    raise "current_user has not any division." if !div
    @cdiv = div.default_controller
    raise if !@cdiv
    #勘定科目、会計部門コンボボックスに表示する値を取得
    @cost_centers = current_user.viewable_cost_centers
    @ac_collection = AccountTitleTree.addableTree(0)
    
    if !params[:search]  #条件検索の入力値が含まれていない…トップ画面からの遷移・コストセンタ検索時
      #paramsにコストセンタ値を含む場合、その検索結果と置き換える
      @cdiv = ControlDivision.find params[:cost_center] if params[:cost_center]  
      @search = flash[:search]
      @invoices = Invoice.paginate :page => params[:page], :per_page => Invoice::PAGINATE_LIST,
                 :conditions => ["division_id IN (SELECT division_id
                                  FROM controllers_divisions
                                  WHERE control_division_id = ?)", @cdiv.id],
                 :order => "buy_day DESC, id"
    else  #検索フォームの入力値が含まれている場合…条件検索実行時
      session[:search] = params[:search]
      session[:search].store("path", Rails.application.routes.recognize_path(request.referer))
      @selected = session[:search]["account_titles"].to_i
      @search = InvoiceSearchBox.new session[:search]
      if !@search.valid?
        flash[:search] = @search
        redirect_to :action => 'list'
        return
      end
      @invoices = Invoice.find_by_sql(@search.find_by_text)
      @invoices = @invoices.paginate(:page => params[:page], :per_page => Invoice::PAGINATE_LIST)
    end
  end

  # 取引先別カンパニー別
  def listp
    @ac_collection = AccountTitleTree.addableTree(0)
    @partner = Partner.find params[:partner]
    @company = Company.find params[:company]

    if !params[:search]  #検索フォームの入力値が含まれていない…取引先画面からの遷移時
      @search = flash[:search]
      @invoices = Invoice.paginate :page => params[:page], :per_page => Invoice::PAGINATE_LIST,
                     :conditions => ["partner_id = ? AND company_setting_id = ?",
                                     @partner.id, @company.id],
                 :order => "buy_day DESC, id"
    else  #検索フォームの入力値が含まれている…検索実行時
      session[:search] = params[:search]
      session[:search].store("partner", @partner.id.to_s)
      session[:search].store("company", @company.id.to_s)
      session[:search].store("path", Rails.application.routes.recognize_path(request.referer))
      @selected = session[:search]["account_titles"].to_i
      @search = InvoiceSearchBox.new session[:search]
      if !@search.valid?
        flash[:search] = @search
        redirect_to :action => 'listp', params: {"partner" => params[:partner], "company" => params[:company]}
        return
      end
      @invoices = Invoice.find_by_sql(@search.find_by_text)
      @invoices = @invoices.paginate(:page => params[:page], :per_page => Invoice::PAGINATE_LIST)
    end
  end


  # 承認を要するリスト（表示）
  def sign_off_list
    @invoices = Invoice.paginate :page => params[:page], :per_page => 40,
             :conditions => ['state < ?', Invoice::SIGNED_OFF],
             :order => 'due_day'
  end
  
  # 承認
  def sign_off
    @invoice = Invoice.find(params[:id])
    Invoice.transaction {
      do_sign_off
    }
    flash[:notice] = '請求書を承認しました。'
    if params[:prev] &&  params[:prev] == 'signoff'
      redirect_to :action => 'sign_off_list'
    else
      redirect_to :action => 'show', :id => @invoice
    end
  end
  
  def do_sign_off
    if @invoice.state < Invoice::SIGNED_OFF
      il = InvoiceLog.new(:invoice => @invoice,
                          :kind => InvoiceLog::SIGN_OFF,
                          :remarks => "",
                          :done_by => current_user,
                          :done_at => Time.now)
      il.save!
      @invoice.state = Invoice::SIGNED_OFF
      @invoice.save!
    end
  end
  private :do_sign_off
  
  # 詳細画面
  def show
    #@invoice = Invoice.find(params[:id])
    @prev = params[:prev]
  end
  

  # 新しい請求書の画面
  def new
    @invoice = Invoice.new
    @partner = Partner.find params[:partner]
    @invoice.company_setting_id = params[:company]
    @company = @invoice.company_setting
    @detail_lines = []
    @ac_collection = AccountTitleTree.addableTree(0)
    @div_collection = Division.addable(@company)
  end
  

  # 明細行のレコードを作る
  # @return [明細行レコード, postされたrawデータ]
  def read_details new_exp, company
    count = params[:lines].to_i
    raise "internal error" if count == 0

    # 並べ替える (挿入行)
    details = []
    (0.. (count - 1)).each do |i|
      pa = params["detail#{i}"]
      if pa && pa[:amount].to_f != 0.0
        details << pa
      end
    end
    details.sort! do |x, y| x[:lineno].to_i <=> y[:lineno].to_i end

    result = []
    last = nil
    details.each_with_index do |pa, i|
      ac = pa[:account_title_id].to_i != 0 ?
                  AccountTitle.find(pa[:account_title_id]) : last.account_title
      div = Division.find pa[:division_id]
      vat = ac.get_real_vat pa[:vat_id], company.country
      detail = InvoiceDetail.new(
                :invoice => @invoice,
                :lineno => pa[:lineno],
                :account_title => ac,
                :division => div,
                :remarks => pa[:remarks],
                :vat_id => (vat ? vat.id : nil),
                :dim2_id => div.get_real_dim2(pa[:dim2_id]).id,
                :amount => nominal_amount(pa[:amount], new_exp) )
      last = detail
      result << detail
    end

    return [result, details]
  end
  private :read_details


  # モデルデータを更新 (create, update共通部分)
  def update_invoice_attrs invoice, params
    if !params[:bank_account].blank?
      batyp, baid = params[:bank_account].split("/")
      if batyp == "j"
        invoice.bank_account_id = baid
        invoice.en_bank_account_id = nil
      else
        invoice.bank_account_id = nil
        invoice.en_bank_account_id = baid
      end
    end
    if params[:no_bank]
      invoice.bank_account_id = nil 
      invoice.en_bank_account_id = nil
    end
    invoice.invoice_no = nil if invoice.invoice_no.to_s.strip == ''
  end
  private :update_invoice_attrs


  # 作成
  def create
    raise "current_user does not have any division." if !current_user.default_division

    @ac_collection = AccountTitleTree.addableTree(0)
    @div_collection = Division.addable(@company)
    @partner = Partner.find params[:id]
    @invoice = Invoice.new( invoice_params.permit(:company_setting_id, # newのみ
                                        :approval, :invoice_no, :buy_day,
                                        :due_day, :currency_id) )
    @invoice.attributes = {
      :partner => @partner,
      :division => current_user.default_division,
      :created_by => current_user,
      :state => 0,
      :payment_state => 0
    }
    update_invoice_attrs @invoice, params

    # 明細行の配列
    detail_records, @detail_lines = read_details @invoice.currency.exponent,
                                                 @invoice.company_setting
    begin
      Invoice.transaction do
        detail_records.each do |r|
          if r.division.company != @invoice.company_setting
            @invoice.errors.add_to_base "#{r.lineno}行目: カンパニーが請求書のと違います"
          end
        end
        if !@invoice.errors.empty?
          detail_records.each {|r| @invoice.invoice_details << r}
          raise ActiveRecord::RecordInvalid, @invoice 
        end

        # 保存
        @invoice.amount_total = 0
        detail_records.each do |r| @invoice.amount_total += r.amount end
        @invoice.save!
        detail_records.each do |r|
          r.invoice_id = @invoice.id
          r.save!
        end

        flash[:notice] = '請求書は正常に登録されました。支払依頼を出力してください。'
        if params[:prev] && params[:prev] == 'list'
          redirect_to :action => 'list'
        else
          redirect_to :action => 'show', :id => @invoice
        end
      end # of transaction
    rescue ActiveRecord::RecordInvalid
      render :action => 'new'
    end 
  end


  # 編集画面
  def edit
    #@invoice = Invoice.find params[:id]
    @company = @invoice.company_setting
    @partner = @invoice.partner
    @ac_collection = AccountTitleTree.addableTree(0)
    @div_collection = Division.addable(@company)
    if !@invoice.bank_account && !@invoice.en_bank_account
      @no_bank = true
    end
    @detail_lines = InvoiceDetail.find :all,
                            :conditions => ["invoice_id = ?", @invoice.id],
                            :order => "lineno, id"
  end
  

  # 自分が最後に更新したログ
  def last_modify_log
    il = InvoiceLog.find_by_sql([<<EOF, @invoice.id, @invoice.id]).first
SELECT * FROM invoice_logs WHERE invoice_id = ? AND done_at =
    (SELECT max(done_at) FROM invoice_logs WHERE invoice_id = ?)
EOF
    if (il && il.done_at) &&
       il.kind == InvoiceLog::MODIFY && il.done_by.id == current_user.id
      return il
    end
    
    return nil
  end
  private :last_modify_log


  # 既存のデータの更新
  def update
    #@invoice = Invoice.find params[:id]
    @company = @invoice.company_setting
    @partner = @invoice.partner
    @ac_collection = AccountTitleTree.addableTree(0)
    @div_collection = Division.addable(@company)

    begin
      Invoice.transaction do
        @invoice.attributes = invoice_params.permit(
                                        :approval, :invoice_no, :buy_day,
                                        :due_day, :currency_id)
        update_invoice_attrs @invoice, params
        
        # 詳細行は作り直す
        @invoice.invoice_details.clear 
        detail_records, @detail_lines = 
              read_details @invoice.currency.exponent, @invoice.company_setting

        detail_records.each do |r|
          if r.division.company != @invoice.company_setting
            @invoice.errors.add_to_base "#{r.lineno}行目: カンパニーが請求書のと違います"
          end
        end
        raise ActiveRecord::RecordInvalid, @invoice if !@invoice.errors.empty?

        detail_records.each do |r| 
          @invoice.invoice_details << r 
          r.save!
        end
        @invoice.update_amount_total
        @invoice.save!

        # 更新日などを記録
        il = last_modify_log() ||
             InvoiceLog.new(:invoice => @invoice,
                            :kind => InvoiceLog::MODIFY,
                            :done_by => current_user)
        il.attributes = {
          :remarks => (il.remarks || "") + params[:message],
          :done_at => Time.now
        }
        il.save!

        # 同時に承認
        if !params[:update_approval].blank?
          do_sign_off
        end
      end # transaction
    rescue ActiveRecord::StaleObjectError
      # 読み直す
      @invoice = Invoice.find params[:id]
      @invoice.errors.add_to_base 'ほかで同時に更新されました。'
      render :action => 'edit'
      return
    rescue ActiveRecord::RecordInvalid
      render :action => 'edit'
      return
    end

    flash[:notice] = '請求書は正常に更新されました。'
    if params[:prev] && params[:prev] == 'signoff'
      redirect_to :action => 'sign_off_list'
    else
      redirect_to :action => 'show', :id => @invoice
    end
  end

  
  # 削除
  def destroy
    #inv = Invoice.find params[:id]
    if @invoice.state >= 20
      cancel_invoice
      return
    end

    partner = @invoice.partner
    Invoice.transaction {
      InvoiceLog.delete_all ['invoice_id = ?', @invoice.id]
      InvoiceDetail.delete_all ['invoice_id = ?', @invoice.id]

      MatReceiving.where("invoice_id = ?", @invoice.id).each do |mr|
        MatReceivingCharge.delete_all ["mat_receiving_id = ?", mr.id]
      end
      MatReceiving.delete_all ["invoice_id = ?", @invoice.id]
      @invoice.destroy
    }
    flash[:notice] = '請求書を削除しました。'
    if params[:prev] && params[:prev] == 'list'
      redirect_to :action => 'list'
    elsif params[:prev] && params[:prev] == 'signoff'
      redirect_to :action => 'sign_off_list'
    else
      redirect_to :controller => 'partners', :action => 'show', :id => partner
    end
  end

  
  # キャンセル. 会計システム連動済みのときはこっち
  def cancel_invoice
    inv = Invoice.find params[:id]
    Invoice.transaction {
      inv.payment_state = 20
      inv.invoice_no = nil
      inv.save!
      il = InvoiceLog.new(:invoice => inv,
                          :kind => InvoiceLog::CANCEL,
                          :remarks => params[:message] || "",
                          :done_by => current_user,
                          :done_at => Time.now)
      il.save!
    }
    flash[:notice] = '請求書をキャンセルしました。'
    redirect_to :controller => 'partners', :action => 'show', :id => inv.partner
  end


  def next_month_day date
    # 月末日を特別扱い
    if date.day >= 28
      t = (date >> 2)
      return Date.new(t.year, t.mon, 1) - 1
    else
      return (date >> 1)
    end
  end
  private :next_month_day
  

  # コピーして新しい請求書を起票
  def copy_new
    #@invoice = Invoice.find params[:id]
    
    @invoice.attributes = {
      :buy_day => next_month_day(@invoice.buy_day),
      :due_day => next_month_day(@invoice.due_day),
      :invoice_no => nil,
      :approval => nil
    }
    @partner = @invoice.partner
    @company = @invoice.company_setting
    @ac_collection = AccountTitleTree.addableTree(0)
    @div_collection = Division.addable(@company)

    @detail_lines = InvoiceDetail.find :all,
                            :conditions => ["invoice_id = ?", @invoice.id],
                            :order => "lineno, id"
    # for compat.
    @detail_lines.each do |detail|
      if detail[:vat_code]
        v_ = Vat.find :first, 
                   :conditions => ["tax_side = ? AND tax_group = ?", 
                                   detail.account_title.tax_side, 
                                   detail.vat_code]
        detail.vat_id = v_.id if v_
      end
    end

    @no_bank = !@invoice.bank_account

    render :action => 'new'
  end


  # 支払依頼を印刷するリスト
  def payment_list
    if !params[:only_mm_in].blank?
      # 材料仕入 (全部)
      @invoices = Invoice.where("(origin_no LIKE ?) AND payment_state = 0", 
                                "MM_IN%")
    else
      # 自分が起票したもののみ
      @invoices = Invoice.find :all,
                    :conditions => ['created_by = ? AND payment_state = 0',
                                    current_user.id]
    end
  end

  
  # プレビュー画面の表示 & 確定
  def print_out
    @invoices = Invoice.find params[:pp], :order => 'partner_id, due_day'
    render :action => 'print_out'
    Invoice.transaction {
      @invoices.each {|inv|
        inv.payment_state = 10  # 印刷済み
        inv.save!
      }
    }
  end
  
  # 支払先を変更する (表示)
  def change_partner
    @invoice = Invoice.find params[:id]
  end
  
  # 支払先を変更する2
  def change_partner2
    @invoice = Invoice.find params[:id]

    @search = PartnerSearchBox.new(params[:search])
    @partners = @search.find_by_text
  end
  
  # 変更（実行）
  def change_partner_do
    @invoice = Invoice.find params[:id]
    @partner = Partner.find params[:partner]
    Invoice.transaction {
      @invoice.partner = @partner
      @invoice.bank_account = nil
      il = InvoiceLog.new(:invoice => @invoice,
                          :kind => InvoiceLog::MODIFY,
                          :remarks => '取引先を変更。',
                          :done_by => current_user,
                          :done_at => Time.now)
      il.save!
      if @invoice.save
        flash[:notice] = '請求書は正常に更新されました。'
        redirect_to :action => 'show', :id => @invoice
      else
        raise
      end
    }
  end
  
  # ログのremarksを更新する
  def up_remarks
    il = InvoiceLog.find params[:id]
    @invoice = il.invoice
    il.remarks = params[:last_log]
    if il.save
      flash[:notice] = 'remarksを更新しました。'
      redirect_to :action => 'show', :id => @invoice
    else
      raise
    end
  end
  
  # 関連する輸入報告
  def related_import
    @invoice = Invoice.find params[:id]
  end

  
  def related_import2
    @invoice = Invoice.find params[:id]
    @text = params[:text].to_s.strip
    if @text == ""
      @search = ValidatingBase.new
      @search.errors.add :base, "検索テキストがありません。"
      render "related_import"
      return
    end

    text = "%" + @text.gsub(/[ \t]/, '').upcase + "%"
    @declarations = ImportDeclaration.find :all,
          :joins => "LEFT JOIN imports_invoices ON imports_invoices.import_declaration_id = import_declarations.id",
          :conditions => ['company_id = ? AND (declaration_number LIKE ? OR bl_number LIKE ? OR exporter_name LIKE ? OR UPPER(description) LIKE ?) AND imports_invoices.invoice_id IS NULL', @invoice.company_setting.id, text, text, text, text]
  end
  

  # 実行
  def related_import3
    @invoice = Invoice.find params[:id]
    @decl = ImportDeclaration.find params[:decl]
    ii = ImportsInvoice.new :invoice => @invoice,
                            :import_declaration => @decl,
                            :created_by => current_user
    ii.save!
    @invoice.update_amount_total
    @invoice.save!

    flash[:notice] = '輸入報告を紐付けました。'
    redirect_to :action => 'show', :id => @invoice
  end


  # 関連づけを外す
  def remove_import
    @invoice = Invoice.find params[:id]
    @import_declaration = ImportDeclaration.find params[:import]
    ImportsInvoice.delete_all ["invoice_id = ? AND import_declaration_id = ?",
                               @invoice.id, @import_declaration.id]
    @invoice.update_amount_total
    @invoice.save!

    flash[:notice] = "関連づけを外しました。"
    redirect_to :action => "show", :id => @invoice
  end


  # 明細行の次元2を選択するダイアログを表示
  # SalesInvoicesController と共用
  def sel_dim2
    @company = Company.find params[:company]
    @ac = (params[:ac].to_i != 0) ? AccountTitle.find(params[:ac]) : nil
    @division = (!params[:div].blank?) ? Division.find(params[:div]) : nil

    cond = ["active IS TRUE AND country = ?", @company.country]
    if @ac
      cond[0] += " AND tax_side = ?"
      cond << @ac.tax_side
    end

    @sel = OpenStruct.new
    @sel.tax = params[:tax]
    @sel.dim2 = params[:dim2]
    @sel.line = params[:line]

    if @sel.tax.to_i < 0 && @ac
      # 科目に従う
      v_ = Vat.find :first, 
                   :conditions => ["tax_side = ? AND tax_group = ?", 
                                   @ac.tax_side, @ac.default_tax_type]
      @sel.tax = v_ ? v_.id : nil
    end
    if @sel.dim2.to_i == 0 && @division
      @sel.dim2 = @division.default_dim2.id
    end

    @taxes = [["不課税(対象外)", 0]] + 
             (Vat.find(:all, :conditions => cond).map do |r| [r.name, r.id] end)

    @dim2 = Dimension.find :all, 
                        :conditions => ["active IS TRUE AND axis = 2"],
                        :order => "dim_code"

    render :layout => false
  end


  def update_dim2
    @line = params[:line]
    @tax = params[:tax].to_i
    @dim2 = params[:dim2].to_i != 0 ? Dimension.find(params[:dim2]) : nil

    render :layout => false
  end


  # 明細行 (仕訳) を追加
  def insert_new_detail
    @invoice = Invoice.new # エラー対策
    @company = Company.find params[:company]

    @ac_collection = AccountTitleTree.addableTree(0)
    @div_collection = Division.addable(@company)
    @detail_obj = InvoiceDetail.new :division => current_user.default_division

    @ref_row = params[:ref]
    @newid = params[:newid].to_i
  end


  ###########################################
  private
  def set_invoice
    @invoice = Invoice.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def invoice_params
    params.fetch(:invoice, {})
  end

end # class InvoicesController
