# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


class ActionGroupsController < ApplicationController
  # GET /action_groups
  # GET /action_groups.xml
  def index
    @action_groups = ActionGroup.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @action_groups }
    end
  end

  # GET /action_groups/1
  # GET /action_groups/1.xml
  def show
    @action_group = ActionGroup.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @action_group }
    end
  end

  # GET /action_groups/new
  # GET /action_groups/new.xml
  def new
    @action_group = ActionGroup.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @action_group }
    end
  end

  # GET /action_groups/1/edit
  def edit
    @action_group = ActionGroup.find(params[:id])
  end

  # POST /action_groups
  # POST /action_groups.xml
  def create
    @action_group = ActionGroup.new(params[:action_group])

    respond_to do |format|
      if @action_group.save
        flash[:notice] = 'ActionGroup was successfully created.'
        format.html { redirect_to(@action_group) }
        format.xml  { render :xml => @action_group, :status => :created, :location => @action_group }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @action_group.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /action_groups/1
  # PUT /action_groups/1.xml
  def update
    @action_group = ActionGroup.find(params[:id])

    respond_to do |format|
      if @action_group.update_attributes(params[:action_group])
        flash[:notice] = 'ActionGroup was successfully updated.'
        format.html { redirect_to(@action_group) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @action_group.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /action_groups/1
  # DELETE /action_groups/1.xml
  def destroy
    @action_group = ActionGroup.find(params[:id])
    @action_group.destroy

    respond_to do |format|
      format.html { redirect_to(action_groups_url) }
      format.xml  { head :ok }
    end
  end
end
