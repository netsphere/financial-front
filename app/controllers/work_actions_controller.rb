# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 計画 (予算)
class WorkActionsController < ApplicationController
  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def index
    list
    render :action => 'list'
  end

  def list
    @work_actions = WorkAction.paginate :page => params[:page], :per_page => 20
  end

  # 横に長く表示
  def show
    @work_action = WorkAction.find params[:id]
    @amounts = GeneralLedger.find :all,
                         :conditions => ['work_action_id = ?', @work_action.id],
                         :order => 'date'
  end

  def new
    @work_action = WorkAction.new
  end

  def create
    @work_action = WorkAction.new(params[:work_action])
    if @work_action.save
      flash[:notice] = 'WorkAction was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @work_action = WorkAction.find(params[:id])
  end

  def update
    @work_action = WorkAction.find(params[:id])
    if @work_action.update_attributes(params[:work_action])
      flash[:notice] = 'WorkAction was successfully updated.'
      redirect_to :action => 'show', :id => @work_action
    else
      render :action => 'edit'
    end
  end

  def destroy
    WorkAction.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
end
