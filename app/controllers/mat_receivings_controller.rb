# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 材料仕入
class MatReceivingsController < ApplicationController
  before_filter :login_required
  before_filter :role_keiri_required, :only => [:update]

  
  # GET /mat_receivings
  # GET /mat_receivings.xml
  def index
    @mat_receivings = MatReceiving.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @mat_receivings }
    end
  end

  # GET /mat_receivings/1
  # GET /mat_receivings/1.xml
  def show
    @mat_receiving = MatReceiving.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @mat_receiving }
    end
  end

  # GET /mat_receivings/new
  # GET /mat_receivings/new.xml
  def new
    @mat_receiving = MatReceiving.new
  end


  # 特定のフィールドだけ, 編集可能
  # GET /mat_receivings/1/edit
  def edit
    @invoice = Invoice.find params[:invoice]
    @mat_receiving = MatReceiving.find(params[:id])
  end


  # POST /mat_receivings
  # POST /mat_receivings.xml
  def create
    @mat_receiving = MatReceiving.new(params[:mat_receiving])

    respond_to do |format|
      if @mat_receiving.save
        format.html { redirect_to(@mat_receiving, :notice => 'Mat receiving was successfully created.') }
        format.xml  { render :xml => @mat_receiving, :status => :created, :location => @mat_receiving }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @mat_receiving.errors, :status => :unprocessable_entity }
      end
    end
  end


  # PUT /mat_receivings/1
  # PUT /mat_receivings/1.xml
  def update
    @mat_receiving = MatReceiving.find(params[:id])
    @invoice = @mat_receiving.invoice

    # 転送前の分を一度に更新する
    begin
      MatReceiving.transaction do
        @mat_receiving.fixed_asset_flag = params[:mat_receiving][:fixed_asset_flag]
        @mat_receiving.save!
        
        # デフォルト値も更新
        part = @mat_receiving.part
        part.mat_type = params[:part][:mat_type]
        part.mat_class = @mat_receiving.part_mat_class
        part.fixed_asset_flag = @mat_receiving.fixed_asset_flag
        part.save!
      
        mats = MatReceiving.joins("LEFT JOIN invoices ON mat_receivings.invoice_id = invoices.id") \
                .where("(invoices.state < 20 AND invoices.payment_state < 20) AND mat_receivings.part_no = ?", part.part_no) \
                .select("mat_receivings.*")
        mats.each do |mat|
          mat.fixed_asset_flag = part.fixed_asset_flag
          mat.save!
        end
      end
    rescue ActiveRecord::RecordInvalid => e
      @error = e
      render :action => "edit"
      return
    end

    redirect_to( {:controller => "invoices", :action => "show", 
                    :id => @mat_receiving.invoice},
                   :notice => 'Mat receiving was successfully updated.' )
  end


  # DELETE /mat_receivings/1
  # DELETE /mat_receivings/1.xml
  def destroy
    @mat_receiving = MatReceiving.find(params[:id])
    @mat_receiving.destroy

    respond_to do |format|
      format.html { redirect_to(mat_receivings_url) }
      format.xml  { head :ok }
    end
  end
end
