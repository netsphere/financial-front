# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 役職マスタ
class WorkerPositionsController < ApplicationController
  before_filter :login_required

  before_filter :set_worker_position, :only => [:show, :edit, :update, :destroy]
  
  # GET /worker_positions
  # GET /worker_positions.xml
  def index
    @worker_positions = WorkerPosition.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @worker_positions }
    end
  end

  # GET /worker_positions/1
  # GET /worker_positions/1.xml
  def show
    #@worker_position = WorkerPosition.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @worker_position }
    end
  end

  # GET /worker_positions/new
  # GET /worker_positions/new.xml
  def new
    @worker_position = WorkerPosition.new
    @worker_position.active = true
  end

  
  # GET /worker_positions/1/edit
  def edit
    #@worker_position = WorkerPosition.find(params[:id])
  end

  # POST /worker_positions
  # POST /worker_positions.xml
  def create
    @worker_position = WorkerPosition.new(
                               worker_position_params.permit(:active, :name))

    if @worker_position.save
      flash[:notice] = 'WorkerPosition was successfully created.'
      redirect_to :action => "show", :id => @worker_position
    else
      render :action => "new"
    end
  end

  # PUT /worker_positions/1
  # PUT /worker_positions/1.xml
  def update
    #@worker_position = WorkerPosition.find(params[:id])

    if @worker_position.update_attributes(
                              worker_position_params.permit(:active, :name) )
      flash[:notice] = 'WorkerPosition was successfully updated.'
      redirect_to :action => "show", :id => @worker_position
    else
      render :action => "edit"
    end
  end

  
  # DELETE /worker_positions/1
  # DELETE /worker_positions/1.xml
  def destroy
    #@worker_position = WorkerPosition.find(params[:id])
    @worker_position.destroy

    redirect_to( :action => "index" )
  end

  
  ###########################################
  private
  def set_worker_position
    @worker_position = WorkerPosition.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def worker_position_params
    params.fetch(:worker_position, {})
  end

end # class WorkerPositionsController
