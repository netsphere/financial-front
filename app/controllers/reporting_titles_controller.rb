# -*- coding:utf-8 -*-

# ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


class ReportingTitlesController < ApplicationController
  # GET /reporting_titles
  # GET /reporting_titles.xml
  def index
    @reporting_titles = ReportingTitle.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @reporting_titles }
    end
  end

  # GET /reporting_titles/1
  # GET /reporting_titles/1.xml
  def show
    @reporting_title = ReportingTitle.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @reporting_title }
    end
  end

  # GET /reporting_titles/new
  # GET /reporting_titles/new.xml
  def new
    @reporting_title = ReportingTitle.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @reporting_title }
    end
  end

  # GET /reporting_titles/1/edit
  def edit
    @reporting_title = ReportingTitle.find(params[:id])
  end

  # POST /reporting_titles
  # POST /reporting_titles.xml
  def create
    @reporting_title = ReportingTitle.new(params[:reporting_title])

    respond_to do |format|
      if @reporting_title.save
        flash[:notice] = 'ReportingTitle was successfully created.'
        format.html { redirect_to(@reporting_title) }
        format.xml  { render :xml => @reporting_title, :status => :created, :location => @reporting_title }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @reporting_title.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /reporting_titles/1
  # PUT /reporting_titles/1.xml
  def update
    @reporting_title = ReportingTitle.find(params[:id])

    respond_to do |format|
      if @reporting_title.update_attributes(params[:reporting_title])
        flash[:notice] = 'ReportingTitle was successfully updated.'
        format.html { redirect_to(@reporting_title) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @reporting_title.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /reporting_titles/1
  # DELETE /reporting_titles/1.xml
  def destroy
    @reporting_title = ReportingTitle.find(params[:id])
    @reporting_title.destroy

    respond_to do |format|
      format.html { redirect_to(reporting_titles_url) }
      format.xml  { head :ok }
    end
  end
end
