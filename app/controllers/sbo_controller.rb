# -*- coding: utf-8 -*-

require "server_location_box"

#require "sbo/invoice"
#require "sbo/sales_invoice"
#require "sbo/receiving_slip"
#require "sbo/payment_slip"
require "sbo/xml_je_generator"
require "net/http"


class ServerConnectionInfo
  attr_accessor :database_server
  attr_accessor :company_db
  attr_accessor :database_type
  attr_accessor :database_username
  attr_accessor :database_password
  attr_accessor :company_username
  attr_accessor :company_password
  attr_accessor :language
  attr_accessor :license_server
end


class SboController < ApplicationController
  before_filter :login_required
  before_filter :role_keiri_required


  # (購買)請求書、販売請求書, 入金、出金
  # 実際の出力(XML出力)
  def export_apar_xml
    @company = Company.find params[:company]
    fixed_day = @company.fixed_gl_day

    @entries = []
    @deferred = []; @deferred_si = []; @deferred_ps = []; @deferred_rs = []
    
    @server_loc = ServerLocationBox.new params[:server_loc]
    if !@server_loc.valid?
      render "keiri/export"
      return
    end

    db = KeiriFront::Application.config.sbo_servers[
                                              @server_loc.company_db_idx.to_i]
    @di_session_id = di_login db[:server], db[:company_db], 
                              @server_loc.username, @server_loc.password
    if !@di_session_id
      @server_loc.errors[:base] = "ログインに失敗"
      render "keiri/export"
      return
    end

    generator = XmlJeGenerator.new @company
    @xml_entries = ""

    je_idx = 1

    # (購買)請求書
    inv_ids = session[:entries]
    inv_ids.each {|inv_id|
      inv = Invoice.find inv_id
      entry_day = [inv.buy_day, fixed_day + 1].max
      @xml_entries += generator.make_jebo je_idx, 
                               inv.make_entry2(generator, @company, entry_day)
      je_idx += 1
    }
    
    # 販売請求書
    si_ids = session[:si_entries]
    si_ids.each {|inv_id|
      inv = SalesInvoice.find inv_id
      entry_day = [inv.sell_day, fixed_day + 1].max
      @xml_entries += generator.make_jebo je_idx,
                              inv.make_entry2(generator, @company, entry_day)
      je_idx += 1
    }

    # 入金/出金伝票
    payment_ids = session[:payment_entries]
    payment_ids.each do |p_id|
      r = PaymentSlip.find p_id
      entry_day = r.date
      @xml_entries += generator.make_jebo je_idx, 
                                 r.make_entry2(generator, @company, entry_day)
      je_idx += 1
    end

    @dry_run = false 
    xml = render_to_string(:partial => "add_object")
    res_body = di_invoke db[:server], xml

    resdoc = REXML::Document.new res_body
    node = REXML::XPath.first(resdoc, "//Entry[@result='Success']")
    if node
      retkeys = REXML::XPath.match(resdoc, "//Entry[@result='Success']/RetKey")
      session[:retids] = (retkeys.collect do |node| node.text.to_i end)

       # レスポンスのindexとxmlのindexの数が合わない場合はエラー出力
      data_count = inv_ids.count + si_ids.count + payment_ids.count

      if session[:retids].count == data_count
        redirect_to :controller => "keiri", :action => "succeed_export",
                    :company => @company
        return
      end
    end

    # See RFC 6266: Use of the Content-Disposition Header Field in the
    #               Hypertext Transfer Protocol (HTTP)
    headers["Content-Type"] = "application/octet-stream; charset=UTF-8"
    headers["Content-Disposition"] = "attachment; filename=keirisys-export.txt"
    render :text => xml + "\n====================================\n" + 
                    res_body.html_safe, 
           :layout => false  # DEBUG
  end
  

  def di_login di_server, company_db, username, password
    require "rexml/document"

    raise TypeError, "di_server is #{di_server.class}" if !di_server.is_a?(DiServer)

    @db = ServerConnectionInfo.new
    @db.database_server = di_server.server
    @db.company_db = company_db
    @db.database_type = "dst_MSSQL2016"
    @db.database_username = di_server.db_username
    @db.database_password = di_server.db_password
    @db.company_username = username
    @db.company_password = password
    @db.language = "In_Japanese_Jp"
    @db.license_server = di_server.license_server

    uri = URI.parse(di_server.end_point + "login")
    req = Net::HTTP::Post.new di_server.end_point + "login"
    req.body = render_to_string(:partial => "login")

    response = Net::HTTP.start(uri.host, uri.port) do |http|
      http.request req
    end

    doc = REXML::Document.new response.body
    node = REXML::XPath.first(doc, "//SessionID")
    if !node
      return nil
    else
      return node.text
    end
  end


  # @return [String] HTTPレスポンスbody
  def di_invoke di_server, xml
    req = Net::HTTP::Post.new di_server.end_point + "service"
    req.body = xml

    uri = URI.parse(di_server.end_point + "service")
    response = Net::HTTP.start(uri.host, uri.port) do |http|
      http.read_timeout = 10 * 60 # 単位:秒
      http.request req
    end

    return response.body
  end


  SCRIPT_DIR =  "#{Rails.root}/script/sbo/BusinessPartners"

  # 取引先・社員マスタをテキストファイルとして出力する
  def export_bp_master
    action = "migrate_partners"

    `#{SboController::SCRIPT_DIR}/run_script.rb #{action}`
    redirect_to :action => "export_bp_master_log"
  end


  def export_bp_master_log
    action = "migrate_partners"

    begin
      @out = File.read SboController::SCRIPT_DIR + "/" + action + "_out.log"
    rescue Errno::ENOENT
      @out = "File Not Found: import_gl_out.log"
    end
    begin
      @err = File.read SboController::SCRIPT_DIR + "/" + action + "_err.log"
    rescue Errno::ENOENT
      @err = "File Not Found: import_gl_err.log"
    end
  end

end # of class SboController
