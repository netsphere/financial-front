# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 外貨表示の銀行口座.
# JP もありえる
class EnBankAccountsController < ApplicationController
  before_filter :login_required
  before_filter :set_en_bank_account, :only => [:show, :edit, :update, :destroy]

=begin
  # 取引先の外貨口座
  def index
    @partner = Partner.find params[:partner]
    @en_bank_accounts = EnBankAccount.find :all, 
                    :conditions => ["partner_id = ?", @partner.id]
  end
=end


  # 取引先の外貨口座
  def show
    #@en_bank_account = EnBankAccount.find params[:id]
    @partner = @en_bank_account.partner
  end


  # 自社の外貨口座
  def new
    @en_bank_account = EnBankAccount.new
    @our_bank = OurBank.find params[:our_bank]
=begin
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @en_bank_account }
    end
=end
  end

  def new_for_partner
    @partner = Partner.find params[:partner]
    @en_bank_account = EnBankAccount.new :partner => @partner
  end


  # 自社/取引先口座の編集
  def edit
    #@en_bank_account = EnBankAccount.find(params[:id])
    @our_bank = OurBank.find params[:our_bank] if !params[:our_bank].blank?
  end


  # 自社の外貨口座の作成
  def create
    @en_bank_account = EnBankAccount.new(
                en_bank_account_params.permit(:swift_code, :bank_name,
                                    :bank_addr1,
                                    :bank_addr2, :bank_country, :account_name,
                                    :account_addr1, :account_addr2,
                                    :iban_code, :account_no) )
    @our_bank = OurBank.find params[:our_bank]
    
    @en_bank_account.iban_code = nil if @en_bank_account.iban_code.to_s.strip == ''
    @en_bank_account.account_no = nil if @en_bank_account.account_no.to_s.strip == ''

    begin
      EnBankAccount.transaction do
        @en_bank_account.save!
        @our_bank.en_bank_account_id = @en_bank_account.id
        @our_bank.save!
        flash[:notice] = 'EnBankAccount was successfully created.'
        redirect_to :controller => 'our_banks', :action => 'show', :id => @our_bank
      end # transaction
    rescue ActiveRecord::RecordInvalid
      render :action => 'new'
    end
  end

  
  def create_for_partner
    @partner = Partner.find params[:partner]
    @en_bank_account = EnBankAccount.new(
                en_bank_account_params.permit(:swift_code, :bank_name,
                                    :bank_addr1,
                                    :bank_addr2, :bank_country, :account_name,
                                    :account_addr1, :account_addr2,
                                    :iban_code, :account_no) )
    @en_bank_account.partner = @partner
    @en_bank_account.iban_code = nil if @en_bank_account.iban_code.to_s.strip == ''
    @en_bank_account.account_no = nil if @en_bank_account.account_no.to_s.strip == ''

    begin
      EnBankAccount.transaction do
        @en_bank_account.save!
        flash[:notice] = "銀行口座を作成しました。"
        redirect_to :action => "show", :id => @en_bank_account, 
                    :partner => @partner
      end
    rescue ActiveRecord::RecordInvalid
      render :action => "new_for_partner"
    end
  end

  # PUT /en_bank_accounts/1
  # PUT /en_bank_accounts/1.xml
  def update
    #@en_bank_account = EnBankAccount.find(params[:id])
    @our_bank = OurBank.find params[:our_bank] if !params[:our_bank].blank?
    
    @en_bank_account.attributes = 
                en_bank_account_params.permit(:swift_code, :bank_name,
                                    :bank_addr1,
                                    :bank_addr2, :bank_country, :account_name,
                                    :account_addr1, :account_addr2,
                                    :iban_code, :account_no) 
    @en_bank_account.iban_code = nil if @en_bank_account.iban_code.blank?
    @en_bank_account.account_no = nil if @en_bank_account.account_no.blank?

    if @en_bank_account.save
      flash[:notice] = 'EnBankAccount was successfully updated.'
      if @our_bank
        redirect_to :controller => 'our_banks', :action => 'show', 
                    :id => @our_bank
      else
        redirect_to :action => "show", :id => @en_bank_account
      end
    else
      render :action => "edit"
    end
  end


  def destroy
    #@en_bank_account = EnBankAccount.find(params[:id])
    @partner = @en_bank_account.partner

    @en_bank_account.destroy
    if @partner 
      redirect_to :controller => "partners", :action => "show", :id => @partner
    else
      redirect_to :controller => "our_banks", :action => "index"
    end
  end

  
  ###########################################
  private
  def set_en_bank_account
    @en_bank_account = EnBankAccount.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def en_bank_account_params
    params.fetch(:en_bank_account, {})
  end

end # class EnBankAccountsController
