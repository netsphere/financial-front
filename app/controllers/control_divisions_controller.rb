# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# コストセンタ   TODO: クラス名の変更
#
# コストセンタは、カンパニーをまたぐ
#   => 異なるカンパニーを一つのコストセンタにすることがある
class ControlDivisionsController < ApplicationController
  #before_filter :login_required

  before_action :set_control_division,
                :only => [:show, :edit, :update, :destroy]
  
  
  # 一覧表示
  def index
    if params[:root]
      @root_controller = ControlDivision.find params[:root]
    else
      @root_controller = ControlDivision.roots.first 
    end
    if !@root_controller
      redirect_to :action => 'new_root'
      return
    end

    @pagy, @control_divisions = pagy(ControlDivision.where('parent_id = ?', @root_controller.id))
    # コストセンタに結びついていない部門
    @dangling_divisions = Division.find_by_sql [<<EOF, @root_controller.id]
SELECT divisions.*
    FROM divisions LEFT JOIN (
        SELECT control_divisions.id,
               controllers_divisions.division_id
            FROM control_divisions JOIN controllers_divisions
                ON control_divisions.id = controllers_divisions.control_division_id
            WHERE parent_id = ?) s
        ON divisions.id = s.division_id
    WHERE s.id IS NULL
EOF
  end

  
  # 一つのコストセンタ
  def show
    #@control_division = ControlDivision.find(params[:id])
  end

  def new
    @control_division = ControlDivision.new
    @root = ControlDivision.find params[:root]
  end
  
  # 作成する
  def create
    @root = ControlDivision.find params[:root]
    @control_division = ControlDivision.new(
                                    control_division_params.permit(:name) )

    begin
      ControlDivision.transaction {
        @control_division.save!
        @control_division.move_to_child_of @root

        flash[:notice] = 'ControlDivision was successfully created.'
        redirect_to :action => 'index', :root => @root
      }
    rescue
      render :action => 'new'
    end
  end


  # あるコストセンタの編集
  def edit
    #@control_division = ControlDivision.find params[:id]
    @root = @control_division.root
    @back = params[:back] || "show"
  end

  
  # 更新。ルートの付け替えはできない。
  def update
    #@control_division = ControlDivision.find params[:id]
    @control_division.attributes = control_division_params.permit(:name)
    if @control_division.save
      flash[:notice] = 'ControlDivision was successfully updated.'
      redirect_to :action => 'show', :id => @control_division
    else
      render :action => 'edit'
    end
  end

  
  # あるコストセンタの削除
  def destroy
    root = ControlDivision.find params[:root]
    #cc = ControlDivision.find params[:id]
    @control_division.destroy
    flash[:notice] = "コストセンタ #{@control_division.name} を削除しました。"
    redirect_to :action => 'index', :root => root
  end
  
  # 新しい集計木を作る（画面）
  def new_root
    @control_division = ControlDivision.new
  end

  # 部門集計木の生成
  def create_root
    @control_division = ControlDivision.new(
                                control_division_params.permit(:name) )
    # @root[:lft] = 1
    # @root[:rgt] = 2
    if @control_division.save
      flash[:notice] = '新しい部門集計木を作成しました。'
      redirect_to :action => 'index', :root => @root
    else
      render :action => 'new_root'
    end
  end

  # 集計木 (ルート) の編集画面
  def edit_root
    @root = ControlDivision.find params[:id]
  end

  def update_root
    @root = ControlDivision.find params[:id]
    @root.attributes = control_division_params.permit(:name)
    @root.save!
    redirect_to :action => "index", :root => @root
  end


  def destroy_root
    @root = ControlDivision.find params[:id]
    @root.destroy
    flash[:notice] = "部門集計木 #{@root.name} を削除しました。"
    redirect_to :action => "index"
  end

  
  ###########################################
  private
  def set_control_division
    @control_division = ControlDivision.find params[:id]
  end

  def control_division_params
    params.fetch(:control_division, {})
  end
  
end # class ControlDivisionsController
