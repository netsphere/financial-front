# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require "ostruct"
require 'invoice_search_box'
require 'will_paginate/array'


# 販売請求書 (売掛金管理)
class SalesInvoicesController < ApplicationController
  before_filter :login_required
  before_filter :role_keiri_required, :only => [:sign_off_list, :sign_off]
  before_filter :role_apar_required

  before_filter :set_invoice, :only => [:show, :edit, :update, :destroy,
                                        :copy_new, :cancel_invoice,
                                        :edit_journals, :update_journals]

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update, :sign_off ],
         :redirect_to => { :action => :index }


  # 汎用の一覧ページ
  def index
    @sales_invoices = SalesInvoice.paginate :page => params[:page],
                                            :per_page => 25
  end
  

  # （自部門の）請求書の一覧
  def list
    #初期表示…ログインユーザの部門IDから所属するコストセンタIDを取得
    div = current_user.default_division
    raise "current_user has not any division." if !div
    @cdiv = div.default_controller
    raise if !@cdiv
    #勘定科目、会計部門コンボボックスに表示する値を取得
    @cost_centers = current_user.viewable_cost_centers
    @ac_collection = AccountTitleTree.addableTree(1)

    if !params[:search]  #条件検索の入力値が含まれていない…トップ画面からの遷移・コストセンタ検索時
      #paramsにコストセンタ値を含む場合、その検索結果と置き換える
      @cdiv = ControlDivision.find params[:cost_center] if params[:cost_center] 
      @search = flash[:search]
      @sales_invoices = SalesInvoice.paginate :page => params[:page], :per_page => 25,
                 :conditions => ["division_id IN (SELECT division_id
                                  FROM controllers_divisions
                                  WHERE control_division_id = ?)", @cdiv.id],
                 :order => "sell_day DESC, id"
    else  #検索フォームの入力値が含まれている場合…条件検索実行時
      session[:search] = params[:search]
      session[:search].store("path", Rails.application.routes.recognize_path(request.referer))
      @selected = session[:search]["account_titles"].to_i
      @search = InvoiceSearchBox.new session[:search]
      if !@search.valid?
        flash[:search] = @search
        redirect_to :action => 'list'
        return
      end
      @sales_invoices = SalesInvoice.find_by_sql(@search.find_by_text)
      @sales_invoices = @sales_invoices.paginate(:page => params[:page], :per_page => SalesInvoice::PAGINATE_LIST)
   end
  end

  # 取引先別カンパニー別
  def listp
    @ac_collection = AccountTitleTree.addableTree(1)
    @partner = Partner.find params[:partner]
    @company = Company.find params[:company]

    if !params[:search]  #検索フォームの入力値が含まれていない…取引先画面からの遷移時
      @search = flash[:search]
      @sales_invoices = SalesInvoice.paginate :page => params[:page], :per_page => SalesInvoice::PAGINATE_LIST,
                     :conditions => ["partner_id = ? AND company_setting_id = ?",
                                     @partner.id, @company.id],
                 :order => "sell_day DESC, id"
    else  #検索フォームの入力値が含まれている…検索実行時
      session[:search] = params[:search]
      session[:search].store("partner", @partner.id.to_s)
      session[:search].store("company", @company.id.to_s)
      session[:search].store("path", Rails.application.routes.recognize_path(request.referer))
      @selected = session[:search]["account_titles"].to_i
      @search = InvoiceSearchBox.new session[:search]
      if !@search.valid?
        flash[:search] = @search
        redirect_to :action => 'listp', params: {"partner" => params[:partner], "company" => params[:company]}
        return
      end
      @sales_invoices = SalesInvoice.find_by_sql(@search.find_by_text)
      @sales_invoices = @sales_invoices.paginate(:page => params[:page], :per_page => SalesInvoice::PAGINATE_LIST)
    end
  end

  # XSL-FOをPDFに変換する
  def render_pdf tmpl_name
    require 'tempfile'

    raise "internal error. FOP_HOME missing" if !ENV["FOP_HOME"]

    inf = Tempfile.new('body', "#{Rails.root}/tmp")
    inf.write render_to_string(:action => tmpl_name, :layout => false)
    inf.close

    (outf = Tempfile.new('out', "#{Rails.root}/tmp")).close
    (errf = Tempfile.new('err', "#{Rails.root}/tmp")).close

    if @company.logo_image
      File.open("#{Rails.root}/tmp/logo.png", "wb") do |fp|
        fp.write @company.logo_image
      end
    else
      begin File.unlink "#{Rails.root}/tmp/logo.png"; rescue; end
    end

    # headers['Content-Type'] = 'application/pdf'
    r = system "#{ENV['FOP_HOME']}/fop -c #{Rails.root}/config/fop-conf.xml #{inf.path} -pdf #{outf.path} 2>#{errf.path}"

    if !r
      @xml_fname = inf.path
      @err_fname = errf.path
      raise "internal error"
    else
      send_file outf.path, :type => "application/pdf", :disposition => "inline"
    end

    inf.unlink
    #outf.unlink
    errf.unlink
    begin File.unlink "#{Rails.root}/tmp/logo.png"; rescue; end
  end
  private :render_pdf
  

  # 印刷時に、請求書番号を採番する
  def update_si_number
      to_save = false
      if !@invoice.si_number
        @invoice.si_number = (@company.si_counter += 1)
        @company.save!
        to_save = true
      end
      if !@invoice.issued_by
        @invoice.issued_by = current_user
        to_save = true
      end

      @invoice.save! if to_save
  end
  private :update_si_number


  # 作った請求書を出力
  def print_invoice
    @invoice = SalesInvoice.find params[:id]
    lang = params[:lang]
    raise SecutiryError if lang !~ /^[a-z][a-z]$/

    @company = @invoice.company_setting
    @contact = OurContact.find :first,
                :conditions => ["lang = ? AND company_setting_id = ?",
                                lang, @company.id] #"
    raise "contact not found" if !@contact

    SalesInvoice.transaction do
      update_si_number
      render_pdf "_print_invoice_" + lang

      # ログを記録しておく (表示するかは別)
      il = SiLog.new :sales_invoice => @invoice,
                     :kind => SiLog::PRINT_INVOICE,
                     :done_by => current_user,
                     :remarks => "",
                     :done_at => Time.now
      il.save!
    end
#    rescue
#      render :action => "render_pdf_error"
#    end
  end
 

  def show
    #@invoice = SalesInvoice.find(params[:id])
  end

  
  # 新規作成画面
  def new
    if !current_user.default_division
      flash[:alert] = "current_user does not have any division." 
      redirect_to :controller => "users", :action => "show", :id => current_user
      return
    end

    @partner = Partner.find params[:partner]
    @company = Company.find params[:company]

    @invoice = SalesInvoice.new(
      :company_setting => @company,
      :contact_name1 => @partner.name
    )
    @detail_lines = []
    @journal_lines = []
    @ac_collection = AccountTitleTree.addableTree(1)
    @div_collection = Division.addable(@company)
  end


  # 明細行のレコードを作る
  def create_details invoice, new_exp
    lines = params[:detail_lines].to_i
    raise "internal error: missing params[:detail_lines]" if lines == 0

    result = []
    # raise params.inspect # DEBUG
    (0 .. lines - 1).each do |i|
      pa = params["detail#{i}"].permit(:line_no, :advance, :description, :unit_price,
                                       :qty, :unit, :amount, :vat_code)
      #pa[:vat_code] = pa[:vat_code].to_i != 0 ? 20 : 0

      detail = SiDetail.new pa
      detail.attributes = {
        :sales_invoice => invoice,
        :unit_price => (pa[:unit_price].blank? ? nil :
                                  nominal_amount(pa[:unit_price], new_exp) )
      }
      if detail.unit_price.to_i != 0 || detail.qty.to_i != 0
        detail.amount = detail.unit_price * detail.qty
      else
        detail.amount = nominal_amount(pa[:amount], new_exp)
      end
      result << detail
    end

    # 末尾の空白行を取り除く
   no_advance_result = result.find_all{|x|!x.advance}.sort_by{|x|x.line_no}
    while no_advance_result_last = no_advance_result.last
      if no_advance_result_last.amount != 0 || no_advance_result_last.description.strip != ''
        break
      else
        no_advance_result.pop
      end
    end

   advance_result = result.find_all{|x|x.advance}.sort_by{|x|x.line_no}
    while advance_result_last = advance_result.last
      if advance_result_last.amount != 0 || advance_result_last.description.strip != ''
        break
      else
        advance_result.pop
      end
    end

    result = no_advance_result + advance_result
    return result
  end
  private :create_details


  # フォームの仕訳行から SiJournal の組を作る
  def journals_from_formdata invoice
    lines = params[:journal_lines].to_i
    raise "internal error: missing params[:journal_lines]" if lines == 0

    journals = []
    sum_amount = 0
    (0 .. lines - 1).each {|i|
      if (ja = params["journal#{i}"])[:amount].to_f != 0.0
        ac = AccountTitle.find( if ja[:account_title_id].to_i != 0
                                  ja[:account_title_id]
                                else
                                  journals[-1].account_title_id
                                end )
        div = Division.find ja[:division_id]
        vat = ac.get_real_vat ja[:vat_id], invoice.company_setting.country

        journal = SiJournal.new(
            :sales_invoice => invoice,
            :amount => nominal_amount(ja[:amount], invoice.currency.exponent),
            :lineno => ja[:lineno],
            :account_title => ac,
            :division => div,
            :vat_id => (vat ? vat.id : nil),
            :dim2_id => div.get_real_dim2(ja[:dim2_id]).id,
            :remarks => ja[:remarks] )

        journals << journal
        sum_amount += journal.amount
      end
    }
    
=begin
    if journals.empty?
      # 1行は作る
      journal = SiJournal.new params["journal0"]
      journal.attributes = {
        :sales_invoice => invoice,
        :amount => 0
      }
      journals << journal
    end
=end

    return [journals, sum_amount]
  end
  private :journals_from_formdata


  # フォームの明細行から, SiDetail の組を作る
  # create() / update() から呼び出される
  def update_invoice_from_formdata invoice
    invoice.si_details.clear
    details = create_details invoice, invoice.currency.exponent
    details.each {|r|
      invoice.si_details << r
    }
    
    invoice.our_banks.clear
    banks = params[:ba].blank? ? [] : OurBank.find(params[:ba])
    banks.each {|b|
      invoice.our_banks << b
    }

    return details
  end
  private :update_invoice_from_formdata


  # 作成
  def create
    raise "current_user does not have any division." if !current_user.default_division
    @partner = Partner.find params[:partner]
    @invoice = SalesInvoice.new(
                invoice_params.permit(:company_setting_id, # newのみ
                                      :zip, :addr1, :addr2,
                                      :contact_name1, :contact_name2,
                                      :contact_name3,
                                      :sell_day, :currency_id, :remarks,
                                      :due_day, :disable_due_day, :delivery,
                                      :keiri_memo, :including_tax) )
    @invoice.attributes = {
        :partner => @partner,
        :division => current_user.default_division,
        :created_by => current_user,
        :state => 0,
    }
    @detail_lines = update_invoice_from_formdata( @invoice )
    @company = @invoice.company_setting
    @journal_lines, sum_amount = journals_from_formdata( @invoice )
    @ac_collection = AccountTitleTree.addableTree(1)
    @div_collection = Division.addable(@company)

    begin
      SalesInvoice.transaction do
        # 請求書パートに金額を合わせる
        d_amount = @detail_lines.inject(0) {|acc, x| acc + x.amount}
=begin
        if (d_amount + @invoice.ex_vat_amount - sum_amount).abs >= 0.01
          @invoice.errors.add_to_base "明細行の合計と仕訳行の合計金額が異なります."
          raise ActiveRecord::RecordInvalid, @invoice
        end
=end
        @journal_lines.each {|j| @invoice.si_journals << j }

        @invoice.save!
      end # of transaction
    rescue ActiveRecord::RecordInvalid
      render :action => 'new'
      return
    end

    flash[:notice] = 'SalesInvoice was successfully created.'
    redirect_to :action => 'show', :id => @invoice
  end


  # 編集画面
  def edit
    #@invoice = SalesInvoice.find(params[:id])
    @company = @invoice.company_setting
    @partner = @invoice.partner

    if !@invoice.editable_by(current_user)
      flash[:alert] = "この請求書は編集できません。"
      redirect_to :action => 'show', :id => @invoice
    end

    @detail_lines = SiDetail.find :all, 
                         :conditions => ["sales_invoice_id = ?", @invoice.id],
                         :order => "line_no, id"
  end

  
  # 自分が最後に更新したログ
  def last_modify_log
    il = SiLog.find :first,
           :conditions => ['sales_invoice_id = ? AND done_at = (select max(done_at) FROM si_logs WHERE sales_invoice_id = ?)', @invoice.id, @invoice.id]
    if il && il.kind == SiLog::MODIFY && il.done_by.id == current_user.id
      return il
    end

    return nil
  end
  private :last_modify_log


  # 更新する
  def update
    #@invoice = SalesInvoice.find params[:id]
    @partner = @invoice.partner
    @company = @invoice.company_setting

    if !@invoice.editable_by(current_user)
      flash[:alert] = "この請求書は編集できません。"
      redirect_to :action => 'show', :id => @invoice
    end

    @invoice.attributes = invoice_params.permit(:zip, :addr1, :addr2,
                                      :contact_name1, :contact_name2,
                                      :contact_name3,
                                      :sell_day, :currency_id, :remarks,
                                      :due_day, :disable_due_day, :delivery,
                                      :keiri_memo, :including_tax)

    begin
      SalesInvoice.transaction {
        @detail_lines = update_invoice_from_formdata @invoice
        
        # 印刷ずみのときのみ担当者変更
        @invoice.issued_by = current_user if @invoice.issued_by
        @invoice.save!
        
        # 更新日などを記録
        il = last_modify_log() || 
             SiLog.new(:sales_invoice => @invoice,
                       :kind => SiLog::MODIFY,
                       :done_by => current_user)
        il.attributes = {
          :remarks => (il.remarks || ""),
          :done_at => Time.now
        }
        il.save!
      }
    rescue ActiveRecord::RecordInvalid => e
      render :action => 'edit'
      return
    end

    flash[:notice] = '請求書は正常に更新されました。'
    redirect_to :action => 'show', :id => @invoice
  end


  # 請求書発行依頼の表示
  def print_request
    @invoice = SalesInvoice.find params[:id]
  end

  
  # 削除
  def destroy
    #inv = SalesInvoice.find params[:id]
    partner = @invoice.partner
    SalesInvoice.transaction {
      cond = ['sales_invoice_id = ?', @invoice.id]
      SiDetail.delete_all cond
      SiBank.delete_all cond
      SiJournal.delete_all cond
      SiLog.delete_all cond
      @invoice.destroy
    }
    flash[:notice] = '請求書を削除しました。'
    redirect_to :controller => 'partners', :action => 'show', :id => partner,
                :company => @invoice.company_setting
  end

  
  # 承認を要するリスト（表示）
  def sign_off_list
    @invoices = SalesInvoice.find :all,
                    :conditions => ['state < ?', SalesInvoice::SIGNED_OFF]
  end


  # 経理承認
  def sign_off
    @invoice = SalesInvoice.find params[:id]
    SalesInvoice.transaction {
      if @invoice.state < SalesInvoice::SIGNED_OFF
        il = SiLog.new(:sales_invoice => @invoice,
                       :kind => SiLog::SIGN_OFF,
                       :remarks => "",
                       :done_by => current_user,
                       :done_at => Time.now)
        il.save!
        @invoice.state = SalesInvoice::SIGNED_OFF
        @invoice.save!
      end
    }
    flash[:notice] = '請求書を承認しました。'
    redirect_to :action => 'show', :id => @invoice
  end

  
  # コピーして新しい請求書を起票
  def copy_new
    #@invoice = SalesInvoice.find params[:id]
    @company = @invoice.company_setting
    @partner = @invoice.partner
    @ac_collection = AccountTitleTree.addableTree(1)
    @div_collection = Division.addable(@company)
    @invoice.attributes = {
      :sell_day => ((@invoice.sell_day + 1) >> 1) - 1,
      :due_day => ((@invoice.due_day + 1) >> 1) - 1,
      :si_number => nil,
    }
    @detail_lines = SiDetail.find :all, 
                         :conditions => ["sales_invoice_id = ?", @invoice.id],
                         :order => "line_no, id"

    @journal_lines = SiJournal.find :all,
                       :conditions => ["sales_invoice_id = ?", @invoice.id],
                       :order => "lineno, id"
    # for compat.
    @journal_lines.each do |journal|
      if journal[:vat_code]
        v_ = Vat.find :first, 
                   :conditions => ["tax_side = ? AND tax_group = ?", 
                                   journal.account_title.tax_side, 
                                   journal.vat_code]
        journal.vat_id = v_.id if v_
      end
    end
    
    render :action => 'new'
  end

  
  # 請求書をキャンセル
  def cancel_invoice
    #inv = SalesInvoice.find params[:id]
    SalesInvoice.transaction {
      @invoice.state = 30
      @invoice.save!
      il = SiLog.new :sales_invoice => @invoice,
                     :kind => InvoiceLog::CANCEL,
                     :remarks => params[:message] || "",
                     :done_by => current_user,
                     :done_at => Time.now
      il.save!
    }
    flash[:notice] = '請求書をキャンセルしました。'
    redirect_to :controller => 'partners', :action => 'show',
                :id => @invoice.partner, :company => @invoice.company_setting
  end

  # 宛て先を変更 (表示)
  def change_partner
    @invoice = SalesInvoice.find params[:id]
  end

  # 請求先を変更する2
  def change_partner2
    @invoice = SalesInvoice.find params[:id]

    @search = PartnerSearchBox.new(params[:search])
    @partners = @search.find_by_text
  end
  
  def change_partner_do
    @invoice = SalesInvoice.find params[:id]
    @partner = Partner.find params[:partner]
    SalesInvoice.transaction {
      @invoice.partner = @partner
      # @invoice.bank_account = nil
      il = SiLog.new(:sales_invoice => @invoice,
                     :kind => InvoiceLog::MODIFY,
                     :remarks => '取引先を変更。',
                     :done_by => current_user,
                     :done_at => Time.now)
      il.save!
      if @invoice.save
        flash[:notice] = '請求書は正常に更新されました。'
        redirect_to :action => 'show', :id => @invoice
      else
        raise
      end
    }
  end


  # 仕訳の編集（画面）
  def edit_journals
    #@invoice = SalesInvoice.find params[:id]
    @company = @invoice.company_setting

    @ac_collection = AccountTitleTree.addableTree(1)
    @div_collection = Division.addable(@company)
    @journal_lines = SiJournal.find :all,
                       :conditions => ["sales_invoice_id = ?", @invoice.id],
                       :order => "lineno, id"
  end


  # 仕訳の更新
  def update_journals
    #@invoice = SalesInvoice.find params[:id]
    @company = @invoice.company_setting
    @journal_lines, sum_amount = journals_from_formdata( @invoice )
    @ac_collection = AccountTitleTree.addableTree(1)
    @div_collection = Division.addable(@company)

    # 請求書パートの金額と一致しているか
    d_amount = SiDetail.where('sales_invoice_id = ?', @invoice.id) \
                       .select('sum(amount) as sub_total').order("").first

    detail_amount = @invoice.including_tax ? d_amount.sub_total.to_i : d_amount.sub_total.to_i + @invoice.ex_vat_amount

    if !d_amount || (sum_amount != detail_amount)
      @invoice.errors.add :base, '合計金額が請求金額と一致していません。'
      render :action => 'edit_journals'
      return
    end

    SalesInvoice.transaction {
      @invoice.si_journals.clear
      @journal_lines.each {|j| @invoice.si_journals << j }

      @invoice.save!

      il = last_modify_log() || 
           SiLog.new(:sales_invoice => @invoice,
                     :kind => SiLog::MODIFY,
                     :done_by => current_user)
      il.attributes = {
        :remarks => (il.remarks || "仕訳を更新。"),
        :done_at => Time.now
      }
      il.save!
    }

    flash[:notice] = '請求書は正常に更新されました。'
    redirect_to :action => 'show', :id => @invoice
  end


  # 明細行を追加
  def insert_new_detail
    @newid = params[:newid].to_i
    @ref_row = params[:ref]

    @invoice = SalesInvoice.new # エラー対策
    @detail_obj = SiDetail.new 
  end


  # 仕訳行を追加
  def insert_new_journal
    @ref_row = params[:ref]
    @newid = params[:newid].to_i
    @company = Company.find params[:company]

    @ac_collection = AccountTitleTree.addableTree(1)
    @div_collection = Division.addable(@company)
    @detail_obj = SiJournal.new :division => current_user.default_division
    @invoice = SalesInvoice.new # エラー対策
  end


=begin
  def sel_dim2
    @company = Company.find params[:company]
    @ac = (params[:ac].to_i != 0) ? AccountTitle.find(params[:ac]) : nil
    @division = (!params[:div].blank?) ? Division.find(params[:div]) : nil

    cond = ["active IS TRUE AND country = ?", @company.country]
    if @ac
      cond[0] += " AND tax_side = ?"
      cond << @ac.tax_side
    end

    @sel = OpenStruct.new
    @sel.tax = params[:tax]
    @sel.dim2 = params[:dim2]
    @sel.line = params[:line]

    if @sel.tax.to_i == 0 && @ac
      @sel.tax = Vat.find :first, 
                   :conditions => ["tax_side = ? AND tax_group = ?", 
                                   @ac.tax_side, @ac.default_tax_type]
    end
    if @sel.dim2.to_i == 0 && @division
      @sel.dim2 = @division.default_dim2.id
    end

    @taxes = Vat.find :all, :conditions => cond
    @dim2 = Dimension.find :all, 
                        :conditions => ["active IS TRUE AND axis = 2"],
                        :order => "dim_code"

    render :layout => false
  end
=end

  def update_dim2
    @line = params[:line]
    @tax = params[:tax].to_i
    @dim2 = params[:dim2].to_i != 0 ? Dimension.find(params[:dim2]) : nil

    render :layout => false
  end

  def up_subtotal_vat
    @tbl = params[:tbl].to_i
    @t = params[:t].to_i
    @s = params[:s].to_i
    @vat_opts = SiDetail::VAT_OPTS
    @vat_opts_disp = SiDetail::VAT_OPTS_DISP
  end

  
  ###########################################
  private
  def set_invoice
    @invoice = SalesInvoice.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def invoice_params
    params.fetch(:invoice, {})
  end

end # SalesInvoicesController
