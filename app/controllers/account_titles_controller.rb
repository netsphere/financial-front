# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 勘定科目
# 補助コード込みで entity を作成する。
class AccountTitlesController < ApplicationController
  
  class AcSearchBox < ValidatingBase
    attr_accessor :name

    validates :name, presence: true
    before_validation :norm

  private
    def norm
      self.name = name.to_s.unicode_normalize(:nfkc).strip
    end
  end

  
  before_action :role_keiri_required, 
                :only => [:new, :create, :edit, :update, :destroy]

  before_action :set_account_title, :only => [:show, :edit, :update, :destroy]
  
  
  def index
  end
  

  def search
    @search = AcSearchBox.new params[:search]
    if !@search.valid?
      render 'index', status: :unprocessable_entity
      return
    end

    text = '%' + @search.name.upcase + '%'
    @account_titles = AccountTitle \
                .where('upper(main_name) LIKE ? OR upper(name) LIKE ? OR kana_name LIKE ?', text, text, text) \
                .order('account_code, suppl_code')
  end


  # 一覧表示
  def list
    @pagy, @account_titles = pagy(AccountTitle.order('account_code', 'suppl_code'))
  end

  
  # 購買伝票で使用可能か選ぶ
  def select_addable
    @account_titles = AccountTitle.where('aggregate_flag is FALSE AND end_day is NULL AND kind = 0') \
                                  .order('account_code, suppl_code')
  end


  # 更新する
  def update_addable
    #list = params[:ss]
    AccountTitle.transaction do
      params[:account_title].each do |rid, attrs|
        r = AccountTitle.find rid
        r.attributes = attrs.permit(:payable_id, :receivable_id) 
        r.save!
      end
    end # transaction
    redirect_to( {:action => 'list'}, :notice => "更新しました。")
  end

  
  def show
    #@account_title = AccountTitle.find(params[:id])
  end

  def new
    @account_title = AccountTitle.new
    @account_title.kind = 0
  end

  
  # 作成
  def create
    @account_title = AccountTitle.new(
                        account_title_params.permit(:account_code,
                                                    :suppl_code,
                                                    :aggregate_flag, # newのみ
                                                    :main_name, :name,
                                                    :kana_name,
                                                    :description,
                                                    :end_day,
                                                    :sign,
                                                    :kind, :enable_invoice,
                                                    :payable_id,
                                                    :enable_si_invoice,
                                                    :receivable_id,
                                                    :tax_side,
                                                    :default_tax_type,
                                                    :req_partner) )
    if @account_title.kind == 1 || @account_title.kind == 2
      @account_title.enable_invoice = false
      @account_title.enable_si_invoice = false
    end
    @account_title.suppl_code = 0 if !@account_title.suppl_code
    @account_title.end_day = nil if !params[:disable]
    
    @account_title.create_user_id = current_user.id
    @account_title.update_user_id = current_user.id

    if @account_title.save
      flash[:notice] = 'AccountTitle was successfully created.'
      redirect_to :action => 'show', :id => @account_title
    else
      render 'new', status: :unprocessable_entity
    end
  end


  # 編集
  def edit
    #@account_title = AccountTitle.find(params[:id])
  end
  
  # 更新
  def update
    #@account_title = AccountTitle.find params[:id]
    @account_title.attributes =
                        account_title_params.permit(:account_code,
                                                    :suppl_code,
                                                    :main_name, :name,
                                                    :kana_name,
                                                    :description,
                                                    :end_day,
                                                    :sign,
                                                    :kind, :enable_invoice,
                                                    :payable_id,
                                                    :enable_si_invoice,
                                                    :receivable_id,
                                                    :tax_side,
                                                    :default_tax_type,
                                                    :req_partner) 
    if @account_title.kind == 1 || @account_title.kind == 2
      @account_title.enable_invoice = false
      @account_title.enable_si_invoice = false
    end
    @account_title.end_day = nil if !params[:disable]
    @account_title.update_user_id = current_user.id
    if @account_title.save
      flash[:notice] = 'AccountTitle was successfully updated.'
      redirect_to :action => 'show', :id => @account_title
    else
      render 'edit', status: :unprocessable_entity
    end
  end

  def destroy
    @account_title.destroy
    flash[:notice] = "削除しました"
    redirect_to :action => 'list'
  end


  ###########################################
  private
  def set_account_title
    @account_title = AccountTitle.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def account_title_params
    params.fetch(:account_title, {})
  end

end # class AccountTitlesController

