# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 債権管理
class ReceivingsController < ApplicationController
  #before_filter :login_required
  before_action :role_keiri_required


  # 未入金一覧
  def index
    @companies = Company.find :all, :order => 'id'
    
    today = Date.today
    @invoices = {}
    for company in @companies
      # due-dateを過ぎている請求
      @invoices[company.id] = SalesInvoice.find :all,
                     :conditions => [<<EOF, today, company.id],
(state = 10 OR state = 20) AND (due_day < ?) AND company_setting_id = ? AND
amount_total <> (SELECT coalesce(sum(receivings.amount), 0) FROM receivings
                     LEFT JOIN payment_slips 
                          ON receivings.cash_slip_id = payment_slips.id 
                     WHERE (payment_slips.state = 10 
                                           OR payment_slips.state = 20) AND 
                           sales_invoices.id = receivings.sales_invoice_id)
EOF
                     :order => 'partner_id, sell_day'
    end
  end
  

  # 未入金一覧 (表示)
  def remain
    today = Date.today
    
    @company = Company.find params[:company]
    @currency = Currency.find params[:currency]

    @due_day = params['due'] ? date_from_select(params['due'], 'day') : today + 5

    @invoices = SalesInvoice.find :all,
                  :conditions => [<<EOF, @due_day, @company.id, @currency.id],
(state = 10 OR state = 20) AND (due_day <= ?) AND company_setting_id = ? AND
    currency_id = ? AND
amount_total <> (SELECT coalesce(sum(amount + charge), 0) FROM receivings
    WHERE sales_invoices.id = receivings.sales_invoice_id)
EOF
                     :order => 'partner_id, sell_day'
  end


  # 入金リポート
  def day_report
    @company = Company.find params[:company]
    @day = params[:day] ? Date.parse(params[:day]) : date_from_select(params['report'], 'day')

    @receivings = Receiving.find :all,
             :select => 'receivings.*',
             :joins => 'JOIN our_banks ON our_banks.id = receivings.our_bank_id JOIN sales_invoices ON receivings.sales_invoice_id = sales_invoices.id',
             :conditions => ['our_banks.company_setting_id = ? AND date = ?',
                             @company.id, @day],
             :order => 'our_banks.id, sales_invoices.partner_id'
  end

  # GET /receivings/1
  # GET /receivings/1.xml
  def show
    @receiving = Receiving.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @receiving }
    end
  end

  # GET /receivings/1/edit
  def edit
    @receiving = Receiving.find(params[:id])
  end
  
  # フォームデータで配列を使う
  # http://www.pen-chan.jp/~tdiary/pen-chan/20070618.html
  # http://webos-goodies.jp/archives/51387214.html

  # POST /receivings
  # POST /receivings.xml
  def create
    @day = date_from_select(params['received'], 'day')
    @our_bank = OurBank.find params['received']['account']

    @received = []
    params[:inv].each {|key, r|
      if !r[:amount].blank? || !r[:charge].blank?
        amount = nominal_amount(r[:amount], @our_bank.currency.exponent)
        @received << Receiving.new(
                           :date => @day,
                           :amount => amount,
                           :charge => (r[:charge].blank? ? 0 : r[:charge]),
                           :our_bank_id => @our_bank.id,
                           :sales_invoice_id => key,
                           :state => 0,
                           :created_by => current_user
                         )
      end
    }
    
    Receiving.transaction {
      @received.each {|r|
        r.save!
      }
    }
    flash[:notice] = 'Receiving was successfully created.'
    redirect_to :action => 'day_report', :company => @our_bank.company_setting, :day => @day
  end

  # PUT /receivings/1
  # PUT /receivings/1.xml
  def update
    @receiving = Receiving.find(params[:id])

    respond_to do |format|
      if @receiving.update_attributes(params[:receiving])
        flash[:notice] = 'Receiving was successfully updated.'
        format.html { redirect_to(@receiving) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @receiving.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /receivings/1
  # DELETE /receivings/1.xml
  def destroy
    @receiving = Receiving.find(params[:id])
    company = @receiving.our_bank.company_setting
    day = @receiving.date
    @receiving.destroy
    
    flash[:notice] = '入金データを削除しました。'
    redirect_to :action => 'day_report', :company => company, :day => day
  end
end
