# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 経費精算リポート
class PettyExpensesController < ApplicationController
  #before_filter :login_required

  # 強制的にデータを作成、編集する機能
  before_action :role_keiri_required,
                :only => [:new, :create, :edit, :update, :destroy,
                          :edit_expense_entries, :update_expense_entries]

  before_action :set_expense_report,
                :only => [:show, :edit, :update, :destroy,
                          :edit_expense_entries, :update_expense_entries]

  
  def index
    # 経費精算リポートのうち, 未収入金の残高があるもの
    @expenses = PettyExpense.find :all, 
                  :conditions => [<<EOF]
receivable_amount_total <> 
    ( SELECT coalesce(SUM(receivings.amount), 0) 
          FROM receivings 
          LEFT JOIN payment_slips 
                       ON receivings.cash_slip_id = payment_slips.id
          WHERE (payment_slips.state = 10 OR payment_slips.state = 20) AND 
                receivings.petty_expense_id = petty_expenses.id )
EOF

    # 仮払金の残高があるもの
    @cash_advances = PettyCashAdvance.find :all,
                      :conditions => [<<EOF]
journal_amount <> 
    ( SELECT coalesce(SUM(petty_offsets.amount), 0)
          FROM petty_offsets
          WHERE petty_offsets.cash_advance_id = petty_cash_advances.id )
EOF

    # 最近の支払い
    @petty_pays = PettyPay.find :all, 
           :select => "demand_batch_id, MAX(demand_batch_close_date) AS demand_batch_close_date, MAX(created_at), SUM(amount) as amount",
           :group => "demand_batch_id",
           :order => "MAX(created_at) DESC",
           :limit => 5
  end


  # 経費精算リポートの一覧
  def list
    @expense_reports = PettyExpense.paginate( 
                                    :page => params[:page], :per_page => 25)
  end


  # 普通は、データインポートによる。
  # 何らかの理由で、強制的にデータを作りたいときの機能
  def new
    @expense_report = PettyExpense.new
    @petty_groups = PettyGroup.all # TODO: アクセス可能なカンパニーに絞る
  end
  

  def create
    @expense_report = PettyExpense.new(
                expense_report_params.permit(:petty_group_id, :report_name) )

    curr = @expense_report.petty_group.company.functional_currency
    @expense_report.user_id = current_user.id
    @expense_report.report_id = rand(100)
    @expense_report.reimb_currency_id = curr.id
    @expense_report.country_lang_name = if curr.code == "JPY"
                                          "JAPAN"
                                        else
                                          raise "internal error"
                                        end
    @expense_report.submit_date = Date.today
    @expense_report.image_required = false
    @expense_report.has_vat_entry = false
    @expense_report.has_ta_entry = false
    @expense_report.total_approved_amount = 0
    @expense_report.emp_amount_total = 0
    @expense_report.cc_amount_total = 0
    @expense_report.receivable_amount_total = 0
    
    if @expense_report.save
      redirect_to( {:action => "show", :id => @expense_report},
                   :notice => "経費精算リポートを作成しました。" )
    else
      @petty_groups = PettyGroup.all # TODO: アクセス可能なカンパニーに絞る
      render :action => "new"
    end
  end

  
  def show
    #@report = PettyExpense.find params[:id]
  end


  def edit
    @petty_groups = PettyGroup.all # TODO: アクセス可能なカンパニーに絞る
  end


  def update
    if @expense_report.update_attributes(
               expense_report_params.permit(:petty_group_id, :report_name) )
      redirect_to( {:action => "show", :id => @expense_report},
                   :notice => "経費精算リポートを更新しました." )
    else
      @petty_groups = PettyGroup.all # TODO: アクセス可能なカンパニーに絞る
      render :action => "edit"
    end
  end


  def destroy
    @expense_report.destroy
    redirect_to :action => "list"
  end


  def edit_expense_entries
    @entries = PettyExpenseEntry. \
                        where('petty_expense_id = ?', @expense_report.id). \
                        order('report_entry_key')
  end


  def update_expense_entries
    raise "internal error" if !@expense_report
    
    line_count = params[:lines].to_i

    begin
      pa = nil
      entry = nil
      PettyExpense.transaction do
        for i in 0..(line_count - 1) do
          pa = params["entry#{i}"]
          entry = PettyExpenseEntry.find_by_report_entry_key(pa[:report_entry_key])
          if !entry
            entry = PettyExpenseEntry.new(
                        :report_entry_key => rand(2 ** 31),
                        :petty_expense_id => @expense_report.id,
                        :spend_currency_code => "JPY", # TODO: fixme
                        :transaction_type => "REG",
                        :exchange_rate => 1.0,
                        :exchange_rate_direction => ' ',
                        :is_personal => false,
                        :vendor_name => '',  # TODO: ???
                        :receipt_received => false,
                        :receipt_type => ' ',
                        :employee_attendee_count => 0,
                        :spouse_attendee_count => 0,
                        :business_attendee_count => 0,
                        :payment_code => 'CASH', # TODO: fixme
                        :reimb_type => 1 )
            next if pa[:spend_amount].blank? || pa[:spend_amount].to_f == 0.0
          end
          entry.update_attributes!(
                        pa.permit(:expense_type_name, :transaction_date,
                                  :spend_amount, :approved_amount,
                                  :vendor_description, :foreign_or_domestic,
                                  :description) )
        end
      end # transaction
    rescue => e
      raise "#{pa.inspect} #{entry.inspect} #{e}"
    end
    redirect_to :action => "show", :id => @expense_report
  end

  
  ###########################################
  private
  def set_expense_report
    @expense_report = PettyExpense.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def expense_report_params
    params.fetch(:expense_report, {})
  end

end # class PettyExpensesController
