# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 勘定科目の集計マスタ
class AccountTitleTreesController < ApplicationController

  # GET /account_title_trees
  # GET /account_title_trees.xml
  def index
    @root_nodes = AccountTitle.find :all, :select => 'account_titles.*',
                   :joins => 'LEFT JOIN account_title_trees ON account_title_trees.child_id = account_titles.id',
                   :conditions => 'account_title_trees.child_id IS NULL',
                   :order => 'account_code, suppl_code'

    # 循環しないように
    @rest = AccountTitle.find(:all,
                  :conditions => 'aggregate_flag IS TRUE').collect {|r| r.id }

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @account_title_trees }
    end
  end

  # GET /account_title_trees/1
  # GET /account_title_trees/1.xml
  def show
    @account_title_tree = AccountTitleTree.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @account_title_tree }
    end
  end

  # GET /account_title_trees/new
  # GET /account_title_trees/new.xml
  def new
    @account_title_tree = AccountTitleTree.new
    raise # TODO: やり直す

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @account_title_tree }
    end
  end
  
  # GET /account_title_trees/1/edit
  def edit
    @parent = AccountTitle.find params[:parent_id]
    # 使用不可も対象
    @child_cand = AccountTitle.find :all,
               :conditions => ['id NOT IN (?)', @parent.self_and_ancestors],
               :order => 'account_code, suppl_code'
  end

  # POST /account_title_trees
  # POST /account_title_trees.xml
  def create
    v = params[:account_title_tree]
    AccountTitleTree.transaction {
      v[:child_id].each {|c|
        @account_title_tree = AccountTitleTree.new :parent_id => v[:parent_id],
                                                   :child_id => c
        @account_title_tree.save!
      }
    }
    respond_to do |format|
      flash[:notice] = 'AccountTitleTree was successfully created.'
      format.html { redirect_to(@account_title_tree) }
      format.xml  { render :xml => @account_title_tree, :status => :created, :location => @account_title_tree }
=begin      
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @account_title_tree.errors, :status => :unprocessable_entity }
      end
=end
    end
  end

  # PUT /account_title_trees/1
  # PUT /account_title_trees/1.xml
  def update
    @parent = AccountTitle.find params[:parent_id]
    v = params[:account_title_tree].nil? ? {child_id: [""]} : params[:account_title_tree]
    AccountTitleTree.transaction {
      forbidden = @parent.self_and_ancestors

      # いったんすべて削除
      AccountTitleTree.delete_all ['parent_id = ?', @parent.id]

      v[:child_id].each {|cid|
        raise if forbidden.include?(cid)
        if cid && cid.to_i != 0
          pair = AccountTitleTree.new :parent_id => @parent.id, :child_id => cid
          pair.save!
        end
      }
    }

    respond_to do |format|
      flash[:notice] = 'AccountTitleTree was successfully updated.'
      format.html { redirect_to :action => 'index' }
      format.xml  { head :ok }
=begin
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @account_title_tree.errors, :status => :unprocessable_entity }
      end
=end
    end
  end

  # DELETE /account_title_trees/1
  # DELETE /account_title_trees/1.xml
  def destroy
    @account_title_tree = AccountTitleTree.find(params[:id])
    @account_title_tree.destroy

    respond_to do |format|
      format.html { redirect_to(account_title_trees_url) }
      format.xml  { head :ok }
    end
  end
end
