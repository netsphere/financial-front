# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011-2013 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 消費税・VAT マスタ
class VatsController < ApplicationController
  #before_action :login_required
  before_action :role_keiri_required

  before_action :set_vat, :only => [:show, :edit, :update, :destroy]

  # GET /vats
  # GET /vats.xml
  def index
    @vats = Vat.all
  end


  # GET /vats/1
  # GET /vats/1.xml
  def show
    #@vat = Vat.find params[:id]
  end


  # GET /vats/new
  # GET /vats/new.xml
  def new
    @vat = Vat.new
    @vat.active = true
  end


  # GET /vats/1/edit
  def edit
    #@vat = Vat.find(params[:id])
  end


  # POST /vats
  # POST /vats.xml
  def create
    @vat = Vat.new vat_params.permit(:active, :tax_side, :tax_code, :name,
                                     :country, # newのみ
                                     :tax_rate, # newのみ
                                     :tax_group, :effective_day,
                                     :account_title_id, :sbo_code)
    #@vat.country = params[:vat][:country]
    #@vat.tax_rate = params[:vat][:tax_rate]

    @vat.create_user_id = current_user.id

    if @vat.save
      redirect_to( {:action => "show", :id => @vat}, 
                   :notice => 'Vat was successfully created.' )
    else
      render "new", status: :unprocessable_entity
    end
  end

  # PUT /vats/1
  # PUT /vats/1.xml
  def update
    #@vat = Vat.find(params[:id])

    if @vat.update(vat_params.permit(:active, :tax_side, :tax_code, :name,
                                     :tax_group, :effective_day,
                                     :account_title_id) )
      redirect_to( {:action => "show", :id => @vat}, 
                   :notice => 'Vat was successfully updated.' )
    else
      render "edit", status: :unprocessable_entity
    end
  end

  # DELETE /vats/1
  # DELETE /vats/1.xml
  def destroy
    #@vat = Vat.find(params[:id])
    @vat.destroy

    redirect_to( {:action => "index"}, 
                 :notice => 'Vat was successfully deleted.' )
  end


  ###########################################
private
  def set_vat
    @vat = Vat.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def vat_params
    params.fetch(:vat, {})
  end

end # class VatsController
