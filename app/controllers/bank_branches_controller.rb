# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 銀行支店
class BankBranchesController < ApplicationController
  before_filter :login_required

  before_filter :set_bank_branch, :only => [:show, :edit, :update, :destroy]

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :index }


=begin
  def index
    @bank_branches = BankBranch.paginate :page => params[:page], :per_page => 20
  end
=end


  # 支店の詳細
  def show
    #@branch = BankBranch.find params[:id]
    @bank = @bank_branch.bank
  end

  def new
    @bank = Bank.find params[:bank]
    @bank_branch = BankBranch.new( {:bank_id => @bank.id} )
  end


  def create
    @bank = Bank.find params[:bank]
    @bank_branch = BankBranch.new(
                 bank_branch_params.permit(:branch_code, :name, :kana_name) )
    @bank_branch.bank_id = @bank.id
    if @bank_branch.save
      flash[:notice] = 'BankBranch was successfully created.'
      redirect_to :action => 'show', :id => @bank_branch
    else
      render :action => 'new'
    end
  end

  def edit
    #@bank_branch = BankBranch.find(params[:id])
  end

  def update
    #@bank_branch = BankBranch.find(params[:id])
    if @bank_branch.update_attributes(
                 bank_branch_params.permit(:branch_code, :name, :kana_name) )
      flash[:notice] = 'BankBranch was successfully updated.'
      redirect_to :action => 'show', :id => @bank_branch
    else
      render :action => 'edit'
    end
  end

  def destroy
    @bank_branch.destroy
    redirect_to :controller => "banks", :action => "show",
                :id => @bank_branch.bank
  end

  
  ###########################################
  private
  def set_bank_branch
    @bank_branch = BankBranch.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def bank_branch_params
    params.fetch(:bank_branch, {})
  end

end # class BankBranchesController
