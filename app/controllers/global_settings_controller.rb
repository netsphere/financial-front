# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 大域設定
class GlobalSettingsController < ApplicationController
  #before_filter :login_required
  before_action :role_keiri_required

  before_action :set_global_setting,
                :only => [:show, :edit, :update, :show_specials, :select_specials,
                       :update_specials]


  # GET /global_settings
  # GET /global_settings.xml
  def show
    #@global_setting = GlobalSetting.find :first
    raise "internal error" if !@global_setting
  end

=begin
  # GET /global_settings/1
  # GET /global_settings/1.xml
  def show
    @global_setting = GlobalSetting.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @global_setting }
    end
  end
=end

  # GET /global_settings/1/edit
  def edit
    #@global_setting = GlobalSetting.first
  end


  # PUT /global_settings/1
  # PUT /global_settings/1.xml
  def update
    if @global_setting.update!(
                global_setting_params.permit(:partner_counter,
                                             :default_division_tree_root_id) )
      redirect_to( {:action => "show"},
                  :notice => 'Global setting was successfully updated.' )
    else
      render action: "edit", status: :unprocessable_entity
    end
  end


  #############################
  # 特殊な勘定科目

  def show_specials
    #@global_setting = GlobalSetting.first
    raise "internal error: missing the global setting." if !@global_setting
  end

  # 編集画面
  def select_specials
    #@global_setting = GlobalSetting.first
  end


  def update_specials
    #@global_setting = GlobalSetting.first
    if @global_setting.update_attributes(
                global_setting_params.permit(:bank_charge_account_title_id,
                                             :currency_gain_account_title_id,
                                             :vat_payment_account_title_id) )
      redirect_to( {:action => "show_specials"},
                  :notice => "特殊な勘定科目を更新しました。" )
    else
      render :action => "select_specials"
    end
  end


  private
  def set_global_setting
    @global_setting = GlobalSetting.first
  end

  def global_setting_params
    params.fetch(:global_setting, {})
  end
  
end # class GlobalSettingsController
