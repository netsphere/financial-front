# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


class HumanResourcesController < ApplicationController
  #before_filter :login_required
  before_action :role_hrd_required

  
  def index
  end
  
  def role_list
  end

  # アクティブでない部門に所属する（アクティブな）社員一覧
  def dangling_workers
    @danglings = User.find :all,
         :select => "users.*",
         :joins => "LEFT JOIN users_divisions ON users.id = users_divisions.user_id LEFT JOIN divisions ON users_divisions.division_id = divisions.id",
         :conditions => "users.active <> 0 AND users_divisions.default_pair <> 0 AND divisions.end_day IS NOT NULL"
  end
end
