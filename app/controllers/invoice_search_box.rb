# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

if !$__INVOICE_SEARCH__
$__INVOICE_SEARCH__ = 1

# 請求書の検索ボックス
class InvoiceSearchBox < ValidatingBase
  attr_accessor :account_titles
  attr_accessor :je_number
  attr_accessor :si_number
  attr_accessor :remarks
  attr_accessor :partner
  attr_accessor :company
  attr_accessor :path
  
  validate :must_fill
  
  # for validate
  def must_fill
    if account_titles.eql?("0") && je_number.blank? && si_number.blank? && remarks.blank?
      errors.add :base, "検索条件のいずれかを入力または選択してください。"
    end
  end

  def find_by_text
    case path[:controller]
      when "invoices"
        return Invoice.with_invoice_detail.
          select_invoice_list.
            partner_id(partner).
            company_setting_id(company).
            account_title_id(account_titles).
            je_number(je_number).
            invoice_no(si_number).
            remarks('%' + remarks.to_s + '%').
          group_list_element.
          by_buy_day.by_invoice_id.to_sql.gsub(/\"/,'') #paginate動作のためto_sqlでSQL文を生成
      when "sales_invoices"
        return SalesInvoice.with_si_detail.with_si_journal.
          select_sales_invoice_list.
            partner_id(partner).
            company_setting_id(company).
            account_title_id(account_titles).
            je_number(je_number).
            si_number(si_number).
            remarks('%' + remarks.to_s + '%').
          group_sinv_list_element.
          by_sell_day.by_sales_invoice_id.to_sql.gsub(/\"/,'') #paginate動作のためto_sqlでSQL文を生成
    end
  end
end

end  # !$__PARTNER_SEARCH__
