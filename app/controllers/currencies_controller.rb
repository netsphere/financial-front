# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require "ostruct"

# 通貨
class CurrenciesController < ApplicationController
  before_action :set_currency, :only => [:show, :edit, :update, :destroy]

  
  # 通貨マスタ (一覧表示)
  def index
    @currencies = Currency.all
  end

  
  def show
    #@currency = Currency.find params[:id]
  end
  
  # 新しい通貨画面
  def new
    @currency = Currency.new
  end
  
  # 新規登録
  def create
    @currency = Currency.new(currency_params.permit(:name, :code, :exponent))
    if @currency.save
      flash[:notice] = 'Currency was successfully created.'
      redirect_to :action => 'index'
    else
      render 'new', status: :unprocessable_entity
    end
  end
  
  # 編集画面
  def edit
    #@currency = Currency.find(params[:id])
  end

  def update
    #@currency = Currency.find(params[:id])
    if @currency.update(currency_params.permit(:name, :code))
      flash[:notice] = 'Currency was successfully updated.'
      redirect_to({:action => 'show', :id => @currency})
    else
      render 'edit', status: :unprocessable_entity
    end
  end

  def destroy
    @currency.destroy
    flash[:notice] = "通貨 #{@currency.name} を削除しました。"
    redirect_to :action => 'index'
  end

  
  # 為替レートの一覧・登録画面
  def rate
    @currencies = Currency.find :all
    @dates = CurrencyRate.paginate :page => params[:page], :per_page => 20,
                                   :select => 'distinct date',
                                   :order => 'date DESC'
    @limit = OpenStruct.new(:day => GlobalSetting.find(:first).currency_limit_day)
  end

  
  # 為替レート編集画面
  def edit_rate
    @day = Date.parse(params[:day])
    rates = {}
    Currency.find(:all).each {|c|
      cr = CurrencyRate.find :first,
                             :conditions => ['currency_id = ? AND date = ?',
                                             c.id, @day]
      rates[c.code] = cr.rate if cr && cr.rate
    }
    @rate = OpenStruct.new rates
  end
  
  # 為替レートの更新
  def update_rate
    @day = Date.parse(params[:day])
    raise if !@day
    Currency.transaction {
      Currency.find(:all).each {|c|
        cr = CurrencyRate.find(:first,
                   :conditions => ['currency_id = ? AND date = ?', c.id, @day]) ||
             CurrencyRate.new(:currency_id => c.id, :date => @day)
        cr.rate = params[:rate][c.code]
        cr.save!
      }
    }
    flash[:notice] = '為替レートを更新しました。'
    redirect_to :action => 'rate'    
  end


  # 為替レートの追加
  def create_rate
    day = date_from_select(params['new'], 'day')
    begin
      CurrencyRate.transaction do
        Currency.find(:all).each {|c|
          rate = params["v.#{c.id}"]
          if rate
            r = CurrencyRate.new(:date => day, :currency => c, :rate => rate)
            r.save!
          end
        }
      end # transaction
    rescue ActiveRecord::RecordInvalid => e
      @error = e
      rate
      render :rate
      return
    end
    flash[:notice] = '為替レートを更新しました。'
    redirect_to :action => 'rate'
  end

  
  def update_limit
    day = date_from_select(params['limit'], 'day')
    global = GlobalSetting.find(:first)
    global.currency_limit_day = day
    if global.save
      flash[:notice] = '有効期限を更新しました。'
      redirect_to :action => 'rate'
    else
      render :action => 'rate'
    end
  end
  
  # 為替レート行を削除
  def destroy_rate
    day = Date.parse params[:day]
    raise if !day
    CurrencyRate.delete_all ['date = ?', day]
    flash[:notice] = '為替レートを削除しました。'
    redirect_to :action => 'rate'
  end


#################################################33
private
  
  def set_currency
    @currency = Currency.find params[:id]
  end

  def currency_params
    params.fetch(:currency, {})
  end
  
end # class CurrenciesController
