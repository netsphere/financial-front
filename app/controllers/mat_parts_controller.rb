# -*- coding:utf-8 -*- 

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 資材マスタ
class MatPartsController < ApplicationController
  before_action :set_mat_part, :only => [:show, :edit, :update, :destroy]
  
  # GET /mat_parts
  # GET /mat_parts.xml
  def index
    @mat_parts = MatPart.paginate :page => params[:page], :per_page => 25,
                                  :order => "part_no"
  end

  
  # GET /mat_parts/1
  # GET /mat_parts/1.xml
  def show
    #@mat_part = MatPart.find(params[:id])
  end

  
  # GET /mat_parts/new
  # GET /mat_parts/new.xml
  def new
    @mat_part = MatPart.new
    @mat_part.fixed_asset_flag = false
  end

  
  # GET /mat_parts/1/edit
  def edit
    #@mat_part = MatPart.find(params[:id])
  end

  # POST /mat_parts
  # POST /mat_parts.xml
  def create
    @mat_part = MatPart.new(mat_part_params.permit(:part_no, :description,
                                  :mat_type, :mat_class, :fixed_asset_flag))

    begin
      @mat_part.save!
    rescue ActiveRecord::RecordInvalid
      render :action => "new"
      return
    end
    
    redirect_to({:action => "show", :id => @mat_part},
                  :notice => 'Mat part was successfully created.')
  end

  
  # PUT /mat_parts/1
  # PUT /mat_parts/1.xml
  def update
    #@mat_part = MatPart.find(params[:id])

    begin
      @mat_part.update_attributes!(
                  mat_part_params.permit(:part_no, :description, :mat_type,
                                         :mat_class, :fixed_asset_flag))
    rescue ActiveRecord::RecordInvalid
      render :action => "edit"
      return
    end
    
    redirect_to({:action => "show", :id => @mat_part},
                :notice => 'Mat part was successfully updated.')
  end

  
  # DELETE /mat_parts/1
  # DELETE /mat_parts/1.xml
  def destroy
    #@mat_part = MatPart.find(params[:id])
    @mat_part.destroy

    redirect_to( {:action => "index"},
                 :notice => "Part '#{@mat_part.part_no}' を削除しました。" )
  end


  ###########################################
  private
  def set_mat_part
    @mat_part = MatPart.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def mat_part_params
    params.fetch(:mat_part, {})
  end

end
