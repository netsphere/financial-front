# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 次元2
class DimensionsController < ApplicationController
  before_filter :login_required
  before_filter :role_keiri_required, 
                :only => [:new, :create, :edit, :update, :destroy]

  before_filter :set_dimension, :only => [:show, :edit, :update, :destroy]

  
  # GET /dimensions
  # GET /dimensions.xml
  def index
    @dimensions = Dimension.paginate :page => params[:page], :per_page => 20,
                               :conditions => ["axis = ?", 2],
                               :order => "dim_code"
  end

  # GET /dimensions/1
  # GET /dimensions/1.xml
  def show
    #@dimension = Dimension.find(params[:id])
  end


  # GET /dimensions/new
  # GET /dimensions/new.xml
  def new
    @dimension = Dimension.new
    @dimension.active = true
  end


  # GET /dimensions/1/edit
  def edit
    #@dimension = Dimension.find(params[:id])
  end


  # POST /dimensions
  # POST /dimensions.xml
  def create
    @dimension = Dimension.new(
                         dimension_params.permit(:active, :dim_code, :name))
    @dimension.create_user_id = current_user.id
    @dimension.axis = 2  # 決め打ち

    if @dimension.save
      redirect_to( {:action => "show", :id => @dimension}, 
                   :notice => 'Dimension was successfully created.' )
    else
      render :action => "new"
    end
  end


  # PUT /dimensions/1
  # PUT /dimensions/1.xml
  def update
    #@dimension = Dimension.find(params[:id])

    if @dimension.update_attributes(
                        dimension_params.permit(:active, :dim_code, :name))
      redirect_to( {:action => "show", :id => @dimension}, 
                   :notice => 'Dimension was successfully updated.' )
    else
      render :action => "edit"
    end
  end


  # DELETE /dimensions/1
  # DELETE /dimensions/1.xml
  def destroy
    #@dimension = Dimension.find(params[:id])
    begin
      @dimension.destroy
      redirect_to( {:action => "index"}, :notice => "次元を削除しました" )
    rescue ActiveRecord::InvalidForeignKey => err
      @error = "使用されているマスタは削除できません: " + err.message
      render :action => "show"
    end
  end


  ###########################################
  private
  def set_dimension
    @dimension = Dimension.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def dimension_params
    params.fetch(:dimension, {})
  end

end # class DimensionsController
