# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require 'time'
require 'date'
require 'digest/sha1'
require 'tempfile'

require 'cube'
require 'map_table'
#require 'validating_base'


# 総勘定元帳
class GeneralLedgerController < ApplicationController
  # 期間入力フォーム
  class ViewTerm < ValidatingBase
    def initialize(attributes = nil)
      @kubun = []
      today = Date.today
      @start_year, @start_mon = [today.year.to_s, today.mon.to_s]
      @end_year, @end_mon = [today.year.to_s, today.mon.to_s]

      super(attributes)

      if Date.valid_date?(@start_year.to_i, @start_mon.to_i, 1)
        @sd = Date.new(@start_year.to_i, @start_mon.to_i, 1)
      end
      if Date.valid_date?(@end_year.to_i, @end_mon.to_i, 1)
        @ed = (Date.new(@end_year.to_i, @end_mon.to_i, 1) >> 1) - 1
      end
    end
    attr_accessor :start_year, :start_mon, :end_year, :end_mon
    attr_accessor :ctr, :kubun
    attr_reader :sd, :ed, :ctr
  
    def validate
      errors.add_to_base('開始月と終了月の関係が正しくありません。') if @sd > @ed
      errors.add 'kubun', "は一つ以上選択してください。" if @kubun.empty?
    end
  end # class ViewTerm

  
  # 部門別　予算・実績対比表（期間選択画面）
  def index
    @cost_centers = current_user.viewable_cost_centers
    @term = ViewTerm.new if !@term
  end
  
  
  # 予算実績キューブから表示用のHTML表を作る。
  # @return MapTableインスタンス
  def create_amount_table(cube, cdiv, division)
    # 軸を作る
    
    # 元帳区分
    axis1 = Kubun.find cube.axis(1)
    if axis1.size >= 2
      # とりあえず1列目と2列目の比較
      axis1_0 = axis1[0].id
      axis1_1 = axis1[1].id
      axis1[2, 0] = lambda {|cube, key0, dmy_, key2, key3|
        r = cube[key0, axis1_0, key2, key3] - cube[key0, axis1_1, key2, key3]
      }
    end

    # 科目候補
    # 超ハードコーディングしてみる TODO:fixme
    ac_list = [
    [131, 141, 203, 205, 207, 211, 213, 224, 226, 229, 241],
    [781, 782, 783, 786],
    [701, 702],
    [711, 713, 714, 715],
    [721, 723, 724], # 722, 
    [731, 733, 735, 736, 737],
    [741, 742, 743, 744],
    [751, 752, 753, 754, 755, 757],
    [761, 756],
    [810, 804, 805, 806], # 801, 802, 803, 809,
    [811, 812, 813],
    [821, 822, 823, 824, 826], # 831, 832, 833, 835, 836, 837, 838, 839, 863,
    [818, 841, 842, 843, 862, 844, 845, 846, 861, 847, 848, 849, 850, 851, 852, 853, 854, 855, 856, 859, 860, 
     871, 872, 873, 874]
    ]

    # 科目軸 .. 科目候補にあって、実績が存在するものだけ
    axis3_exists = cube.axis(3)
    axis3 = []
    total_ac = []
    ac_list.each {|sub|
      sub_ac = []
      sub.each {|code|
        if axis3_exists.include?(code)
          axis3 << code
          sub_ac << code
          total_ac << code
        end
      }
      if sub_ac.size > 0
        axis3 << lambda {|cube, key0, key1, key2, dmy_|
          r = 0
          sub_ac.each {|code|
            r += cube[key0, key1, key2, code]
          }
          return r
        }
      end
    }
    axis3 << lambda {|cube, key0, key1, key2, dmy_|
      r = 0
      total_ac.each {|code|
        r += cube[key0, key1, key2, code]
      }
      return r
    }

    
    # HTML表にまとめる
    table = MapTable.new
    
    # 科目名を表示
    axis3.each_with_index {|accode, i|
      if accode.is_a?(Integer)
        ac = AccountTitle.find_by_account_code(accode)
        table.map 0, 2 + i, "#{ac.account_code}#{ac.main_name}",
                   :th => true, :align=>"left", :nowrap => 'nowrap'
      else
        table.map 0, 2 + i, "計", :th => true
      end
    }
    cube.axis(0).each_with_index {|key0, i0| # 年月
      x = 1 + i0 * axis1.size
      table.map x, 0, key0, :colspan => axis1.size
      axis1.each_with_index {|key1, i1|   # 元帳区分
        if key1.is_a?(Kubun)
          table.map(x + i1, 1, key1.long_name)
        else
          table.map(x + i1, 1, "差異")
        end
        axis3.each_with_index {|key3, i3|  # 勘定科目
          amount = cube[key0,
                        (key1.is_a?(Proc) ? key1 : key1.id),
                        (division ? division.id : nil),
                        key3]
          if key1.is_a?(Kubun) && key3.is_a?(Integer)
            if amount != 0
              table.map x + i1, 2 + i3, "<a href=\"/general_ledger/ledger?ym=#{key0}&amp;code=#{key3}&amp;cc=#{cdiv.id}&amp;kubun=#{key1.id}\">#{format_amount(amount, 0)}</a>", :align => "right"
            else
              table.map x + i1, 2 + i3, '-', :align => "right"
            end
          else
            table.map x + i1, 2 + i3, format_amount(amount, 0),
                       :style => "background-color:#eeeeff;", :align => "right"
          end
        }
      }
    }

    return table
  end
  private :create_amount_table
  
  
  # 部門別　予算・実績対比表の出力
  def comparison
    @term = ViewTerm.new params[:term]
    @term.validate
    if !@term.errors.empty?
      index()
      render :action => 'index'
      return
    end

    @cdiv = ControlDivision.find params[:cost_center]
    raise if !@cdiv
    
    gl = GeneralLedger.find :all,
              :select => 'date, kubun_id, division_id, account_title_id, sum(amount) as amount',
              :conditions => [<<EOF, @term.sd, @term.ed, @cdiv.id],
(kubun_id IN (#{@term.kubun.join(',')})) AND
(date BETWEEN ? AND ?) AND
division_id IN (SELECT division_id FROM controllers_divisions
                                   WHERE control_division_id = ?)
EOF
              :group => 'kubun_id, division_id, account_title_id, date'
    
    # キューブを作る
    # (年月, 元帳区分, 会計部門, 勘定科目) => 金額
    cube = Cube.new 4
    gl.each {|r|
      cube.add [year_mon(r.date), r.kubun_id, r.division_id, r.account_title.account_code], r.amount
    }
    
    # viewstate としてキューブを保存
    t = Tempfile.new('cube', RAILS_ROOT + '/tmp')
    path = t.path
    t.close! # いったんファイルを消す
    @vs_key = Digest::SHA1.hexdigest 'cube' + Time.now.to_f.to_s
    session[@vs_key] = {
      :cdiv => @cdiv,
      :cube => path, # キューブが大きくなることがある。ファイルに保存する。
      :term => @term
    }
    File.open(path, 'w') {|f| 
      s = Marshal.dump(cube)
      # raise s.to_s # DEBUG
      f.write s 
    }
    @table = create_amount_table(cube, @cdiv, nil)
  end

  
  # 各部門用: 予算実績対比を特定の会計部門でフィルタ。
  # TODO: もそっとグルーピングとかできんもんか？
  def comparison_filter
    if !params[:division].blank?
      @filter_division = Division.find params[:division]
    end
    @vs_key = params[:vs]
    @cdiv = session[@vs_key][:cdiv]
    @term = session[@vs_key][:term]
    cube = Marshal.load(File.read(session[@vs_key][:cube]))

    @table = create_amount_table(cube, @cdiv, @filter_division)
    render :action => 'comparison'
  end
  
  
  def year_mon d
    return sprintf("%04d%02d", d.year, d.mon)
  end
  private :year_mon

  # 経理専用: 部門別予算・実績対比表 (全部門)
  def view
    @term = ViewTerm.new
  end
  
  # 経理専用: 
  def view2
    @term = ViewTerm.new params[:term]
    @term.validate
    if !@term.errors.empty?
      render :action => 'view'
      return
    end

    @controllers = ControlDivision.find :all, :conditions => ['parent_id = ?', @term.ctr]
    
    @vs = Digest::SHA1.hexdigest Time.now.to_f.to_s
    session[@vs] = {
      :term => @term,
      :kubuns => @term.kubun,
      :root => @term.ctr }
  end
  
  # 元帳 (画面表示)
  def ledger
    /(20[0-9][0-9])([01][0-9])/ =~ params[:ym]; @year = $1.to_i; @mon = $2.to_i
    st = Date.new(@year, @mon, 1); ed = (st >> 1) - 1
    @accode = params[:code].to_i
    @kubun = Kubun.find params[:kubun]
    @cost_center = ControlDivision.find params[:cc]
    @gl = GeneralLedger.find :all,
                :select => 'general_ledger.*',
                :joins => 'JOIN account_titles ON general_ledger.account_title_id = account_titles.id',
                :conditions => [<<EOF, @kubun.id, st, ed, @accode, @cost_center.id],
kubun_id = ? AND
(date BETWEEN ? AND ?) AND
account_titles.account_code = ? AND
division_id IN (
    SELECT division_id FROM controllers_divisions WHERE control_division_id = ?)
EOF
                :order => "date, slip_number"
  end
  
  # 仕訳 (画面表示)
  def journal
    @slip_number = params[:seq]
    year = params[:y].to_i; mon = params[:m].to_i
    st = Date.new(year, mon, 1); ed = (st >> 1) - 1
    @entries = GeneralLedger.find :all,
                     :conditions => ['slip_number = ? AND date BETWEEN ? AND ?',
                                     @slip_number, st, ed]
  end
  
  # 経理専用: 全社　元帳のダウンロード（期間選択画面）
  def select_ledger
  end


  SCRIPT_DIR = File.dirname(__FILE__) + "/../../script"

  # ファイルが大きくなるので, 別プロセスで実行
  def export_all_ledgers
    @term = ViewTerm.new(params[:term])
    if !@term.sd || !@term.ed || @term.sd > @term.ed
      @term.errors.add_to_base "開始時期, 終了時期のいずれかがおかしい"
      render :action => "select_ledger"
      return
    end

    actual_kubun = Kubun.find_by_name('actual')
    raise if !actual_kubun

    `#{GeneralLedgerController::SCRIPT_DIR}/run_script.rb export_all_ledgers #{actual_kubun.id} #{@term.sd} #{@term.ed}`
    redirect_to :action => "export_all_ledgers_log"
  end


  def export_all_ledgers_log
    action = "export_all_ledgers"

    begin
      @out = File.read GeneralLedgerController::SCRIPT_DIR + "/" + action + "_out.log"
    rescue Errno::ENOENT
      @out = "File Not Found: import_gl_out.log"
    end
    begin
      @err = File.read GeneralLedgerController::SCRIPT_DIR + "/" + action + "_err.log"
    rescue Errno::ENOENT
      @err = "File Not Found: import_gl_err.log"
    end
  end


  # 経理専用: 元帳のダウンロード (実行)
  # ファイルが非常に大きくなることがある => 一時ファイルに少しずつ出力する。
  def item_ledger
    term = ViewTerm.new(params[:term])
    raise if !term.sd || !term.ed || term.sd > term.ed

    actual_kubun = Kubun.find_by_name('actual')
    raise if !actual_kubun
    
    if params[:item] && params[:item][:id]
      # 科目別
      item = AccountTitle.find params[:item][:id]
      cond = ['kubun_id = ? and (date between ? and ?) and account_title_id = ?',
              actual_kubun.id, term.sd, term.ed, item.id]
    else
      # 全部
      cond = ['kubun_id = ? AND (date BETWEEN ? AND ?)',
              actual_kubun.id, term.sd, term.ed]
    end
    
    tmpf = Tempfile.new('gl', RAILS_ROOT + '/tmp')
    tmpf.write(render_to_string(:partial => 'ledger_begin'))
    i = 0
    while true
      # (2008.5) カーソルと FETCH だと、なぜか、上手くいかなかった。
      @gl = GeneralLedger.find(:all, :conditions => cond,
                                     :order => "date, id",
                                     :offset => i * 20000,
                                     :limit => 20000)
      break if @gl.empty?
      tmpf.write(render_to_string(:partial => 'ledger_all'))
      i += 1
    end
    tmpf.close

    send_file tmpf.path, :type => 'application/vnd.ms-excel;charset=UTF-8',
                         :disposition => 'attachment',
                         :filename => '元帳.xls'
    # tmpf.unlink
  end


  # 経理専用: 予算・実績対比表のダウンロード (P/Lのみ)
  def download
    vs_key = params[:vs]
    term = session[vs_key][:term]
    root = ControlDivision.find session[vs_key][:root]
    kubuns = session[vs_key][:kubuns]
    control_division = ControlDivision.find params[:division]
    
    cond = [<<EOF, term.sd, term.ed, control_division.id]
(kubun_id IN (#{kubuns.join(',')})) AND
(date BETWEEN ? AND ?) AND
division_id IN (SELECT division_id FROM controllers_divisions
                                   WHERE control_division_id = ?)
EOF
    gl = GeneralLedger.find :all,
                            :select => "general_ledger.*",
                            :conditions => cond
    
    dl_output(root, control_division, term, gl) {|r|
      if r.account_title.account_code < 601 || r.account_title.account_code > 999 # TODO:fixme
        true
      else
        false
      end
    }
  end

  
  # 経理専用: 全社計 (部門別) ダウンロード実行
  def division_summary
    require "digest/md5"

    vs_key = params[:vs]
    term = session[vs_key][:term]
    root = ControlDivision.find session[vs_key][:root]
    kubuns = session[vs_key][:kubuns]

    pa = {
      :root => root,
      :term => term,
      :kubuns => kubuns,
      :fname => Digest::MD5.hexdigest(Time.now.to_s)
    }
    session[vs_key][:fname] = pa[:fname]

    pa_str = (Marshal.dump(pa).unpack("C*").map do |i| sprintf("%02x", i) end).join
    `#{GeneralLedgerController::SCRIPT_DIR}/run_script.rb division_summary #{pa_str}`
    redirect_to :action => "division_summary_log", :vs => vs_key
  end


  def division_summary_log
    vs_key = params[:vs]
    @fname = session[vs_key][:fname]
    action = "division_summary"

    begin
      @out = File.read GeneralLedgerController::SCRIPT_DIR + "/" + action + "_out.log"
    rescue Errno::ENOENT
      @out = "File Not Found: import_gl_out.log"
    end
    begin
      @err = File.read GeneralLedgerController::SCRIPT_DIR + "/" + action + "_err.log"
    rescue Errno::ENOENT
      @err = "File Not Found: import_gl_err.log"
    end
  end

  
  private
  def dl_output root, control_division, term, gl
    data = <<EOF
<html xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:x="urn:schemas-microsoft-com:office:excel"
      xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja"<
<head>
  <meta http-equiv='Content-Type' content='text/html;charset=utf-8'>
  <meta name='ProgId' content='Excel.Sheet'>
  <xml>
    <o:DocumentProperties>
      <o:Created>#{Time.now.iso8601}</o:Created>
      <o:Company>●</o:Company>
    </o:DocumentProperties>
  </xml>
  <style>
    @page {
        margin:.59in .39in .39in .39in;
        mso-header-margin:.51in;
        mso-footer-margin:.31in;
        mso-page-orientation:landscape;}
    td {
      font-size:9.0pt; }
  </style>
</head>
<body>
    <h1>#{control_division ? control_division.name : nil} #{term.sd}-#{term.ed} 実績</h1>
<table border="0" cellpadding="0" cellspacing="0" width="100%"
    style='border-collapse:collapse;table-layout:fixed;'>
<tr>
  <th>伝票日付</th>
  <th>年月</th>
  <th>区分</th>
  <th>伝票NO</th>
  <th>作成日</th>
  <th>作成者コード</th>
  <th>作成者名</th>
  <th>科目コード</th>
  <th>補助コード</th>
  <th>科目名</th>
  <th>科目名2</th>
  <th>部門コード</th>
  <th>部門名</th>
  <th>金額</th>
  <th>摘要</th>
</tr>
EOF

    gl.each {|r|
      next if block_given? && yield(r) 

      data << <<EOF
<tr>
  <td>#{r.date}</td>
  <td>#{sprintf("%04d%02d", r.date.year, r.date.mon)}</td>
  <td>#{Kubun.find(r.kubun_id).long_name}</td>
  <td>#{r.has_attribute?('slip_number') ? r.slip_number : ''}</td>
  <td>#{r.has_attribute?('created_at') && r.created_at ? r.created_at.strftime("%Y/%m/%d") : ''}</td>
  <td>#{r.has_attribute?('created_by') && r.created_by ? r.created_by.worker_code : nil}</td>
  <td>#{r.has_attribute?('created_by') && r.created_by ? r.created_by.name : nil}</td>
  <td>#{r.account_title.account_code}</td>
  <td>#{r.account_title.suppl_code}</td>
  <td>#{r.account_title.name}</td>
  <td>#{r.account_title.account_code}#{r.account_title.main_name}
  <td>#{control_division ? r.division.division_code : nil}</td>
  <td>#{control_division ? r.division.name :
            (r.division ? r.division.controller(root).name : '')}</td>
  <td>#{r.amount * r.account_title.sign}</td>
  <td>#{r.has_attribute?('remarks') ? r.remarks : ''}</td>
</tr>
EOF
    }
    data << "</table></body></html>"
    
    send_data data, :type => "application/vnd.ms-excel; charset=UTF-8", :disposition => %Q(attachment; filename="#{
    sprintf("%04d%02d", term.ed.year, term.ed.mon)}#{control_division ? control_division.name : 'all'}.xls")
  end
end

