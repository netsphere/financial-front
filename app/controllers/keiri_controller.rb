# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#require "server_location_box"


# 経理だけが使う関係
class KeiriController < ApplicationController
  #before_filter :login_required
  before_action :role_keiri_required
  

  def show
  end


  # 仕訳確認画面
  # TODO: 経理承認したときに、出力待ちテーブルにpushしたほうがいいかなぁ
  def export
    @company = (Company.find_by_id params[:company]) || Company.first
    functional_currency = @company.functional_currency
    raise "functional currency undefined." if !functional_currency
    fixed_day = @company.fixed_gl_day

    @entries = []
    @deferred = []; @deferred_si = []; @deferred_ps = []; @deferred_rs = []

    entry_ids = []; si_entry_ids = []
    global = GlobalSetting.find(:first)

    @server_loc = ServerLocationBox.new

    # 購買請求書
    invoices = Invoice.find :all,
                    :conditions => ['state = 10 AND company_setting_id = ?',
                                    @company.id],
                    :order => 'buy_day'
    invoices.each {|inv|
      entry_day = [inv.buy_day, fixed_day + 1].max
      if inv.currency_id != functional_currency.id &&
                  entry_day > global.currency_limit_day
        @deferred << inv.id
      else
        @entries += inv.make_entries(@company, entry_day)
        entry_ids << inv.id
      end
    }

    # 販売請求書
    invoices = SalesInvoice.find :all,
                     :conditions => ['state = 10 AND company_setting_id = ?',
                                     @company.id],
                     :order => 'sell_day'
    invoices.each {|inv|
      entry_day = [inv.sell_day, fixed_day + 1].max
      if inv.currency_id != functional_currency.id &&
                          entry_day > global.currency_limit_day
        @deferred_si << inv.id
      else
        @entries += inv.make_entries(@company, entry_day)
        si_entry_ids << inv.id
      end
    }

=begin    
    # 入金伝票
    receiving_entry_ids = []
    receivings = ReceivingSlip.find :all,
                    :joins => 'JOIN our_banks ON receiving_slips.our_bank_id = our_banks.id',
                    :conditions => ['state = 10 AND our_banks.company_setting_id = ?',
                                    @company.id],
                    :order => "date"
    receivings.each {|r|
      next if r.receivings.length == 0
      if r.our_bank.currency_id != functional_currency.id &&
                                          r.date > global.currency_limit_day
        @deferred_rs << r.id
      else
        @entries += r.make_entries(@company, r.date) # 伝票日付は入金日
        receiving_entry_ids << r.id
      end
    }
=end

    # 入金/出金伝票
    payment_ids = []
    payment_slips = PaymentSlip.find :all,
                 :conditions => ["state = 10 AND company_id = ?", @company.id],
                 :order => "date"
    payment_slips.each do |payment_slip|
      if payment_slip.payments.length == 0 && 
                                  payment_slip.petty_pays.length == 0 &&
                                  payment_slip.receivings.length == 0
        next 
      end

      if payment_slip.date > global.currency_limit_day
        @deferred_ps << payment_slip.id
      else
        # 含まれる請求書の日付が為替期限より先でも出力不可
        defer = false
        payment_slip.payments.each do |inv_pay|
          if inv_pay.invoice.buy_day > global.currency_limit_day
            defer = true
            break
          end
        end
        if defer
          @deferred_ps << payment_slip.id
        else
          @entries += payment_slip.make_entries(@company, payment_slip.date)
          payment_ids << payment_slip.id
        end
      end
    end

    # 経理承認を並行しておこなっても大丈夫なように, この時点の一覧を保存.
    # [制限事項] エクスポートを並行しておこなうと, ダブってしまう. TODO: ??
    @size = entry_ids.size + si_entry_ids.size + #receiving_entry_ids.size + 
            payment_ids.size
    session[:entries] = entry_ids
    session[:si_entries] = si_entry_ids
    #session[:receiving_entries] = receiving_entry_ids
    session[:payment_entries] = payment_ids
  end


  # セッション情報から, 請求書などをエクスポート済みにする.
  # セッションのクリアは, 呼び出し側でおこなうこと
  def close_documents company
    raise TypeError if !company.is_a?(Company)

    fixed_day = company.fixed_gl_day

    inv_ids = session[:entries] || []
    si_ids = session[:si_entries] || []
    payment_ids = session[:payment_entries] || []

    retids = session[:retids] || []
    cnt = 0 #retidsカウンター

    export_at = Time.now

    # 連動済みとしてマークする。
    Invoice.transaction {
      inv_ids.each.with_index do |inv_id, i|
        inv = Invoice.find inv_id
        inv.state = 20
        inv.journal_export_at = export_at
        inv.journal_day = [inv.buy_day, fixed_day + 1].max
        inv.je_number = retids[i]
        cnt += 1
        inv.save!
      end

      si_ids.each.with_index(cnt) do |inv_id, i|
        inv = SalesInvoice.find inv_id
        inv.state = 20
        inv.journal_export_at = export_at
        inv.journal_day = [inv.sell_day, fixed_day + 1].max
        inv.je_number = retids[i]
        inv.save!
      end

      payment_ids.each do |p_id|
        r = PaymentSlip.find p_id
        r.state = 20
        r.journal_export_at = export_at
        r.save!
      end
    } # of transaction
  end
  private :close_documents


  # [完了]ボタン (実行)
  def finish_export
    company = Company.find params[:company]
    close_documents company

    session[:entries] = nil
    session[:si_entries] = nil
    session[:payment_entries] = nil

    flash[:notice] = '仕訳出力済みとマークしました。'
    redirect_to :action => "export", :company => company
  end


  # XML連携でエクスポートに成功した場合の, リダイレクト先
  def succeed_export
    @company = Company.find params[:company]
    fixed_day = @company.fixed_gl_day

    inv_ids = session[:entries] || []
    si_ids = session[:si_entries] || []
    payment_ids = session[:payment_entries] || []

    @entries = []
    @size = inv_ids.size + si_ids.size + payment_ids.size
    @retids = session[:retids] || []

    inv_ids.each {|inv_id|
      inv = Invoice.find inv_id
      entry_day = [inv.buy_day, fixed_day + 1].max

      @entries += inv.make_entries(@company, entry_day)
    }
      
    si_ids.each {|inv_id|
      inv = SalesInvoice.find inv_id
      entry_day = [inv.sell_day, fixed_day + 1].max

      @entries += inv.make_entries(@company, entry_day)
    }

    payment_ids.each do |p_id|
      r = PaymentSlip.find p_id
      @entries += r.make_entries(@company, r.date)
    end

    # 支払依頼などをclose.
    close_documents @company
  end


  # 過去のエクスポートの一覧の表示
  def export_old_gl
    @export_ats = Invoice.find_by_sql <<EOF
(SELECT distinct journal_export_at FROM invoices)
UNION
(SELECT distinct journal_export_at FROM sales_invoices)
-- UNION
-- (SELECT distinct journal_export_at FROM receiving_slips)
UNION
(SELECT distinct journal_export_at FROM payment_slips)
ORDER BY journal_export_at DESC
LIMIT 30
EOF
  end


  # 再出力
  def export_old_gl2
    at = Marshal.load(params[:at])

    # TODO: receiving_slips のときは, company が直接取れない
    inv = nil
    [Invoice, SalesInvoice, PaymentSlip].each do |klass|
      inv = klass.find :first, :conditions => ["journal_export_at = ?", at]
      break if inv
    end
    company = inv.company_setting

    @entries = []

    # 購買請求書
    Invoice.find(:all, 
                 :conditions => ["journal_export_at = ?", at]).each do |inv|
      @entries += inv.make_entries( company, inv.journal_day )
    end
    
    # 販売請求書
    SalesInvoice.find(:all,
                   :conditions => ["journal_export_at = ?", at]).each do |inv|
      @entries += inv.make_entries( company, inv.journal_day )
    end
=begin
    # 入金伝票
    ReceivingSlip.find(:all, 
                   :conditions => ["journal_export_at = ?", at]).each do |r|
      @entries += r.make_entries( company, r.date )
    end
=end
    # 出金伝票
    PaymentSlip.find(:all,
                   :conditions => ["journal_export_at = ?", at]).each do |r|
      @entries += r.make_entries( company, r.date )
    end

    # See RFC 6266: Use of the Content-Disposition Header Field in the
    #               Hypertext Transfer Protocol (HTTP)
    headers["Content-Type"] = "application/vnd.ms-excel; charset=UTF-8"
    headers["Content-Disposition"] = "attachment; filename=keirisys-export.csv"
    render 'taisho/export_apar', :layout => false
  end


  # 総勘定元帳 (仕訳) のインポート画面
  def import_gl
    require 'csv'

    @fname = '/shiwake.csv'
    if FileTest.exist?(IMPORT_DATA_DIR + @fname)
      data = sjis2utf File.read(IMPORT_DATA_DIR + @fname)
      @head = []
      
      idx = 0
      CSV::Reader.parse(data) {|line|
        idx += 1; next if idx == 1
        break if idx == 10
        @head << line
      }
    end
  end

  
  SCRIPT_DIR = File.dirname(__FILE__) + '/../../script'
  def do_import_gl
    company = Company.find params[:import][:company_id]

    `#{KeiriController::SCRIPT_DIR}/run_import_gl.rb #{company.id}`
    redirect_to :action => 'import_gl_log', :company => company
  end

  
  # ログを表示
  def import_gl_log
    begin
      @out = File.read KeiriController::SCRIPT_DIR + '/import_gl_out.log'
    rescue Errno::ENOENT
      @out = "File Not Found: import_gl_out.log"
    end
    begin
      @err = File.read KeiriController::SCRIPT_DIR + '/import_gl_err.log'
    rescue Errno::ENOENT
      @err = "File Not Found: import_gl_err.log"
    end
  end

  
  # 取引先（支払先）のインポート画面
  def import_partners
    `#{KeiriController::SCRIPT_DIR}/run_import_partners.rb`
    redirect_to :action => 'import_partners_log'
  end
  

  # 勘定科目のインポート
  def import_account_titles
    `#{KeiriController::SCRIPT_DIR}/run_import_account_titles.rb`
    redirect_to :action => 'import_account_titles_log'
  end

  
  # 社員マスタアップロード画面
  def import_workers
  end

  # ログ表示
  def import_partners_log
    @out = File.read KeiriController::SCRIPT_DIR + '/import_partners_out.log'
    @err = File.read KeiriController::SCRIPT_DIR + '/import_partners_err.log'
  end
  
  # 勘定科目のインポートのログ表示
  def import_account_titles_log
    @out = File.read KeiriController::SCRIPT_DIR + '/import_account_titles_out.log'
    @err = File.read KeiriController::SCRIPT_DIR + '/import_account_titles_err.log'
  end

end
