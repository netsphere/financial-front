# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require 'validating_base'

class BankAccountSearchBox < ValidatingBase
  attr_accessor :text
end


# 自社銀行口座
class OurBanksController < ApplicationController
  before_filter :login_required

  before_filter :role_keiri_required,
                :only => [:new, :create, :edit, :update, :destroy]
  
  before_filter :set_our_bank, :only => [:show, :edit, :update, :destroy]

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :bind_account ],
         :redirect_to => { :action => :index }

  
  def index
    @company = params[:company].blank? ? Company.first :
                                         Company.find(params[:company])
    @our_banks = OurBank.paginate :joins => "LEFT JOIN account_titles ON our_banks.account_title_id = account_titles.id",
                       :conditions => ['our_banks.company_setting_id = ?', @company.id],
                       :order => "account_titles.account_code, account_titles.suppl_code, our_banks.id",
                       :page => params[:page], :per_page => 20
  end

  def show
    #@our_bank = OurBank.find(params[:id])
  end

  
  # 新しい自社銀行口座/現金 (編集画面)
  def new
    @our_bank = OurBank.new
    @company = Company.find params[:company]
    @our_bank.company_setting_id = @company.id
  end

  
  def create
    @company = Company.find our_bank_params[:company_setting_id]
    @our_bank = OurBank.new(
                        our_bank_params.permit(:company_setting_id, # newのみ
                                               :name,
                                               :account_title_id,
                                               :currency_id,
                                               :applicant_code,
                                               :remarks) )
    @our_bank.applicant_code = nil if @our_bank.applicant_code.to_i == 0
    @our_bank.create_user_id = current_user.id
    
    if @our_bank.save
      flash[:notice] = '新しい自社資金口座を作成しました。'
      redirect_to :action => 'show', :id => @our_bank
    else
      render 'new'
    end
  end


  # 銀行口座マスタと紐付ける (選択)
  def select_account
    @our_bank = OurBank.find params[:id]
    # @company = params[:company]
  end


  # 銀行口座マスタと紐づける (検索結果)
  def new2
    @search = BankAccountSearchBox.new params[:search]
    @our_bank = OurBank.find params[:id]

    # @company = params[:company]
    if @search.text.to_s.strip == ''
      @search.errors.add :base, '検索テキストを入力してください。'
      render :action => 'select_account'
      return
    end
    t = '%' + Bank.zen2han(@search.text.strip) + '%' # TODO:fixme
    if @search.text.to_i != 0
      @bank_accounts = BankAccount.find :all, :conditions => ['ac_number = ?', @search.text], :limit => 31
    else
      @bank_accounts = BankAccount.find :all, :conditions => ['kana_name like ?', t], :limit => 31
    end
  end


  # 銀行口座マスタと紐づける (作成)
  def bind_account
    @our_bank = OurBank.find params[:id]
    @our_bank.bank_account_id = params[:bank_account]

    begin
      OurBank.transaction do
        @our_bank.save!
        flash[:notice] = 'OurBank was successfully created.'
        redirect_to :action => 'show', :id => @our_bank
      end # transaction
    rescue ActiveRecord::RecordNotUnique => e
      @error = e
      render :action => 'select_account'
    end
  end
  

  # 編集画面
  def edit
    #@our_bank = OurBank.find params[:id]
    @bank_account = @our_bank.bank_account || @our_bank.en_bank_account
    @company = @our_bank.company_setting
  end


  def update
    raise if !(request.post? || request.put?)

    @company = @our_bank.company_setting
    @our_bank.attributes = our_bank_params.permit(:name,
                                               :account_title_id,
                                               :currency_id,
                                               :applicant_code,
                                               :remarks)
    @our_bank.applicant_code = nil if @our_bank.applicant_code.to_i == 0

    if @our_bank.save
      flash[:notice] = 'OurBank was successfully updated.'
      redirect_to :action => 'show', :id => @our_bank, 
                                      :company => @our_bank.company_setting
    else
      render :action => 'edit'
    end
  end

  def destroy
    @our_bank.destroy
    redirect_to :action => 'index'
  end


  ###########################################
  private
  def set_our_bank
    @our_bank = OurBank.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def our_bank_params
    params.fetch(:our_bank, {})
  end

end # class OurBanksController
