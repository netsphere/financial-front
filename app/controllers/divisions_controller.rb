# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 会計部門
# 組織コードを含む前提
class DivisionsController < ApplicationController
  class DivSearchBox < ValidatingBase
    attr_accessor :name

    validates :name, presence: true
    before_validation :norm

  private
    def norm
      self.name = name.to_s.unicode_normalize(:nfkc).strip
    end
  end


  before_action :role_keiri_required, 
                :only => [:new, :create, :edit, :update, :destroy]

  before_action :set_division, :only => [:show, :edit, :update, :destroy]


  # 一覧表示
  def index
    @pagy, @divisions = pagy(Division.all.order('division_code'))
    @search = DivSearchBox.new 
  end


  def search
    @search = DivSearchBox.new params[:divisions_controller_div_search_box]
    if !@search.valid?
      @pagy, @divisions = pagy(Division.all.order('division_code'))
      render 'index', status: :unprocessable_entity
      return
    end

    text = "%" + @search.name.upcase + "%"
    @divisions = Division.find :all,
                      :conditions => ["upper(name) LIKE ? OR kana_name LIKE ?",
                                      text, text],
                      :order => "division_code"
  end


  def show
    #@division = Division.find(params[:id])
  end


  def new
    err = ControlDivision.roots.empty?
    ControlDivision.roots.each do |root|
      if root.children.empty?
        err = true
        break
      end
    end

    if err
      flash[:alert] = "コストセンタがありません。先にコストセンタを設定してください。"
      redirect_to :controller => "control_divisions", :action => "index"
      return
    end

    @division = Division.new
  end
  

  # 作成
  def create
    @division = Division.new(
                        division_params.permit(:division_code, :name,
                                               :kana_name, :company_id,
                                               :default_dim2_id, :end_day,
                                               :purchase_active) )
    @division.end_day = nil if !params[:disable]
    @division.create_user_id = current_user.id

    begin
      Division.transaction {
        @division.save!
        params[:ctr].each {|cid|
          cdiv = ControlDivision.find cid
          ControllersDivision.create :control_division => cdiv, :division => @division
        }
        flash[:notice] = 'Division was successfully created.'
        redirect_to :action => 'show', :id => @division
      }
    rescue ActiveRecord::RecordInvalid
      render :action => 'new'
    end
  end

  
  # 編集画面
  def edit
    #@division = Division.find(params[:id])
    @page = params[:page]
  end

  def update
    #@division = Division.find params[:id]
    @division.attributes = division_params.permit( :division_code, :name,
                                        :kana_name, :company_id,
                                        :default_dim2_id, :end_day,
                                        :purchase_active )
    @division.end_day = nil if !params[:disable]
    begin
      Division.transaction {
        @division.save!
        ControllersDivision.delete_all ['division_id = ?', @division.id]
        params[:ctr].each {|cid|
          cdiv = ControlDivision.find cid
          ControllersDivision.create :control_division => cdiv, :division => @division
        }
        flash[:notice] = 'Division was successfully updated.'
        redirect_to :action => 'show', :id => @division
      }
    rescue
      render :action => 'edit'
    end
  end

  
  # 削除する
  def destroy
    raise "division not found." if !@division
    
    # コストセンタから切り離す
    ControllersDivision.delete_all ['division_id = ?', @division.id]
    @division.destroy
    
    redirect_to :action => 'index'
  end

  
  # 購買伝票で使用可にするかの一覧
  def select_addable
    @divisions = Division.find(:all,
                       :conditions => 'end_day is NULL',
                       :order => 'division_code')
  end
  
  # 更新する
  def update_addable
    list = params[:ss] || []
    Division.transaction {
      Division.find(:all).each {|r|
        orig_active = r.purchase_active
        r.purchase_active = list.index(r.id.to_s) ? 1 : 0   # activeでチェックを入れる
        r.save! if orig_active != r.purchase_active
      }
    }
    redirect_to :action => 'index'
  end


  ###########################################
  private
  def set_division
    @division = Division.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def division_params
    params.fetch(:division, {})
  end

end # class DivisionsController
