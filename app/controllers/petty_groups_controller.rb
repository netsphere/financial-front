# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 小口グループ
# カンパニーと同じか, カンパニーより１段階小さい
class PettyGroupsController < ApplicationController
  before_filter :login_required
  before_filter :role_keiri_required,
                :only => [:new, :create, :edit, :update, :destroy]

  before_filter :set_petty_group, :only => [:show, :edit, :update, :destroy]

  
  # GET /petty_groups
  # GET /petty_groups.xml
  def index
    @petty_groups = PettyGroup.all
  end

  # GET /petty_groups/1
  # GET /petty_groups/1.xml
  def show
    #@petty_group = PettyGroup.find(params[:id])
  end


  # GET /petty_groups/new
  # GET /petty_groups/new.xml
  def new
    @petty_group = PettyGroup.new
  end


  # POST /petty_groups
  # POST /petty_groups.xml
  def create
    @petty_group = PettyGroup.new(
                        petty_group_params.permit(:company_id, :group_code,
                                                  :name, :description))
    @petty_group.create_user_id = current_user.id
    @petty_group.description = "" if !@petty_group.description

    if @petty_group.save
      redirect_to( {:action => "show", :id => @petty_group}, 
                   :notice => 'Petty group was successfully created.')
    else
      render :action => "new"
    end
  end


  def edit
    #@petty_group = PettyGroup.find(params[:id])
  end


  # PUT /petty_groups/1
  # PUT /petty_groups/1.xml
  def update
    #@petty_group = PettyGroup.find(params[:id])

    if @petty_group.update_attributes(
                        petty_group_params.permit(:company_id, :group_code,
                                                  :name, :description) )
      redirect_to( {:action => "show", :id => @petty_group}, 
                   :notice => 'Petty group was successfully updated.')
    else
      render :action => "edit"
    end
  end


  # DELETE /petty_groups/1
  # DELETE /petty_groups/1.xml
  def destroy
    #@petty_group = PettyGroup.find(params[:id])
    @petty_group.destroy
    redirect_to( {:action => "index"}, :notice => "削除しました。" )
  end

  
  ###########################################
  private
  def set_petty_group
    @petty_group = PettyGroup.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def petty_group_params
    params.fetch(:petty_group, {})
  end

end # class PettyGroupsController
