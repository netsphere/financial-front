# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#require 'upload_module'
#require 'partner_search_box'
#require 'will_paginate/array'

# 取引先
class PartnersController < ApplicationController
  include UploadModule

  before_action :role_apar_required

  before_action :set_partner, :only => [:show, :edit, :update, :destroy]
  

  # 検索フォームの表示
  def index
  end

  
  def search
    @search = PartnerSearchBox.new params.require(:search)
    if !@search.valid?
      render 'index', status: :unprocessable_entity
      return
    end
    @pagy, @partners = pagy(@search.find_by_text)
    @result_count = @pagy.count
    #@partners = @partners.paginate(:page => params[:page], :per_page => Partner::PAGINATE_LIST) 
  end


  def show
    @invoices = {}
    @sales_invoices = {}

    @company = params[:company].blank? ? current_user.default_company :
                                         Company.find(params[:company])  
    if !@company
      @invoices = []
      @sales_invoices = []
    else
      # 請求書リスト
      @invoices = Invoice.paginate :page => params[:page],
                    :per_page => 15,
                    :conditions => ['partner_id = ? AND company_setting_id = ?', 
                                    @partner.id, @company.id],
                    :order => 'buy_day DESC, id'
      @sales_invoices = SalesInvoice.paginate :page => params[:sipage],
                    :per_page => 15,
                    :conditions => ['partner_id = ? AND company_setting_id = ?', 
                                    @partner.id, @company.id],
                    :order => 'sell_day DESC, si_number'
    end
  end


  # 新しい取引先
  def new
    @partner = Partner.new
  end
  

  # 新しく作成
  def create
    @partner = Partner.new partner_params.permit(:name, :kana_name, :hq_zip,
                                :hq_addr, :industry, :remarks, :zaimu_id,
                                :tdb_code)
    #@partner.zaimu_id = get_next_zaimu_id() if @partner.zaimu_id.to_i == 0
    @partner.created_by = current_user
    @partner.updated_by = current_user
    if @partner.save
      flash[:notice] = '取引先は、正常に作成されました。'
      redirect_to({action:'show', id:@partner.id} )
    else
      render 'new', status: :unprocessable_entity
    end
  end
  
  # 編集画面の表示
  def edit
    #@partner = Partner.find(params[:id])
  end
  
  # 更新
  def update
    #@partner = Partner.find(params[:id])
    @partner.updated_by = current_user
    if @partner.update_attributes(
                        partner_params.permit(:name, :kana_name, :hq_zip,
                                              :hq_addr, :industry, :remarks,
                                              :zaimu_id,
                                              :tdb_code))
      flash[:notice] = 'Partner was successfully updated.'
      redirect_to :action => 'show', :id => @partner
    else
      render :action => 'edit'
    end
  end

  
  # 削除
  def destroy
    #@partner = Partner.find params[:id]
    @invoices = {}
    @sales_invoices = {}

    s =  "取引先 #{@partner.name} は、正常に削除されました。"
    begin
      @partner.destroy
    rescue ActiveRecord::InvalidForeignKey => e
      @error = e.to_s
      render :action => "show", :id => @partner
      return
    end
    flash[:notice] = s
    redirect_to :action => 'index'
  end


private ###########################################
  
  def set_partner
    @partner = Partner.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def partner_params
    params.fetch(:partner, {})
  end

end # class PartnersController
