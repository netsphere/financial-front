# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# ユーザ（社員）
class UsersController < ApplicationController
  class SearchBox < ValidatingBase
    attr_accessor :text, :code
  end


  before_action :role_hrd_required,
                :only => [:new_qualification, :create_qualification,
                          :edit_qualification, :update_qualification,
                          :destroy_qualification]
  
  before_action :set_worker, :only => [:show, :edit, :update, :destroy,
                        :edit_division_history, :update_division_history,
                        :update_default_division ]

  
  # 検索画面
  def index
  end
  
  # 検索結果
  def search
    @search = SearchBox.new(params[:search])
    if @search.text.to_s.strip == '' && @search.code.to_s.strip == ''
      @search.errors.add :base, '検索テキストを指定してください。'
      render :action => 'index'
      return
    end

    c = []; v = []
    if @search.text.to_s.strip != ''
      text = '%' + normalize_kc(@search.text.strip).upcase + '%' # PostgreSQL only. TODO:fixme
      c << '(upper(name) LIKE ? OR kana_name LIKE ? OR upper(email) LIKE ?)'
      v << text << text << text
    end
    if @search.code.to_s.strip != ''
      c << 'worker_code = ?'
      v << @search.code
    end
    @users = User.find :all,
                       :conditions => [c.join(' AND '), *v],
                       :limit => 40
  end
  
  # 一覧
  def list
    @workers = User.paginate :page => params[:page], :per_page => 25,
                             :order => 'worker_code'
  end

  def show
    #@worker = User.find params[:id]
    @exist_gl = GeneralLedger.find :first,
                                   :conditions => ['created_by = ?', @worker.id]
  end


  def new
    @worker = User.new
    @worker.active = 1
  end


  def create
    @worker = User.new(worker_params.permit(:worker_code, :name, :kana_name,
                        :email, :active, :keiri_flag,
                        :marketing_flag, :apar_flag, :hrd_flag,
                        :login, :password, :password_confirmation) )
    if @worker.save
      flash[:notice] = 'Worker was successfully created.'
      redirect_to :action => 'show', :id => @worker
    else
      render :action => 'new'
    end
  end

  def edit
    #@worker = User.find params[:id]
  end

  
  def update
    if !current_user.has_role?(:hrd)
      redirect_to '/'
      return
    end

    @worker.assign_attributes(
           worker_params.permit(:worker_code, :name, :kana_name,
                                :email, :active, :keiri_flag,
                                :marketing_flag, :apar_flag, :hrd_flag,
                                :password, :password_confirmation) )
    @worker.login = worker_params[:login] if !@worker.login

    begin
      @worker.save!
      flash[:notice] = 'Worker was successfully updated.'
      redirect_to :action => 'show', :id => @worker
    rescue ActiveRecord::RecordInvalid
      render :action => 'edit'
    end
  end

  
  # 社員の削除
  def destroy
    #u = User.find params[:id]
    User.transaction {
      UsersDivision.delete_all ['user_id = ?', @worker.id]
      @worker.destroy
    }
    flash[:notice] = "社員 #{@worker.worker_code} #{@worker.name} を削除しました。"
    redirect_to :action => 'index'
  end
  
  # 取り扱える会計部門の編集画面
  def edit_divisions
    @worker = User.find params[:id]
    @divisions = @worker.divisions.find :all, 
                      :conditions => ['users_divisions.effective_day IS NULL']
  end

  
  # 取り扱える会計部門を更新
  def update_divisions
    require 'set'
    @worker = User.find params[:id]
    new_divs = params[:div].collect {|v| v.to_i}.delete_if {|v| v == 0}
    UsersDivision.transaction {
      # 一度全部消して、新たに作成する
      UsersDivision.delete_all ["user_id = ? AND effective_day IS NULL", @worker.id]
      new_divs.each_with_index {|d, i|
        r = UsersDivision.new :user => @worker,
                              :division_id => d,
                              :effective_day => nil,
                              :default_pair => 0
        r.save!
      }
    } # of transaction
    flash[:notice] = "Worker was successfully updated."
    redirect_to :action => 'show', :id => @worker
  end


  # 社内での経歴の編集 (画面)
  def edit_division_history
    #@worker = User.find params[:id]

    @user_divisions = UsersDivision \
                        .where('user_id = ? AND effective_day IS NOT NULL', 
                               @worker.id) \
                        .order('effective_day')
  end

  
  # 異動情報を更新
  def update_division_history
    #@worker = User.find params[:id]
    
    lines = params[:lines].to_i
    raise "internal error" if lines == 0

    @user_divisions = []
    default_pair = nil
    for i in 0..(lines - 1) do
      user_division_params =
                params["division#{i}"].permit(:effective_day, :endenable,
                                              :end_day, :division_id,
                                              :worker_position_id)
      endenable = user_division_params[:endenable]
      user_division_params.delete(:endenable)

      ud = UsersDivision.new user_division_params
      if ud.division_id
        ud.user_id = @worker.id
        ud.end_day = nil if !endenable
        if !ud.end_day && !default_pair
          ud.default_pair = 1
          default_pair = ud
        else
          ud.default_pair = 0
        end
        @user_divisions << ud
      end
    end

    begin
      UsersDivision.transaction {
        UsersDivision.delete_all ['user_id = ? AND effective_day IS NOT NULL',
                                  @worker.id]
        @user_divisions.each {|ud|
          ud.save!
        }
      }
      flash[:notice] = "異動情報を更新しました。"
      redirect_to :action => 'show', :id => @worker
    rescue ActiveRecord::RecordNotUnique => e
      @error = e
      render :action => "edit_division_history"
    end
  end


  # 主たる所属部門
  def update_default_division
    if worker_params.blank?
      @error = "いずれかの行を選択してください。"
      render :action => "show", :id => @worker
      return
    end
    
    pair = UsersDivision.find worker_params[:default_division]
    if pair.end_day
      @error = "終了日がある行は、主たる部門にできません。"
      render :action => "show", :id => @worker
      return
    end
    
    UsersDivision.transaction do
      UsersDivision.update_all ["default_pair = 0 WHERE user_id = ? AND id <> ?", @worker.id, pair.id]
      pair.default_pair = 1
      pair.save!
    end

    flash[:notice] = "主たる所属部門を更新しました。"
    redirect_to :action => "show", :id => @worker
  end
  

  # 新しい資格
  def new_qualification
    @worker = User.find params[:id]
    @uq = UsersQualification.new
  end


  # 資格の作成
  def create_qualification
    @worker = User.find params[:id]
    @uq = UsersQualification.new params[:uq].permit(:qualification_id,
                                                    :effective_day, :expire_day)
    @uq.user_id = @worker.id
    @uq.expire_day = nil if !params[:onend]
    
    if @uq.save
      flash[:notice] = "資格情報を登録しました。"
      redirect_to :action => 'show', :id => @worker
    else
      render :action => 'new'
    end
  end

  
  # 資格の編集 (画面)
  def edit_qualification
    @worker = User.find params[:id]
    @uq = UsersQualification.find params[:uq]
  end

  
  # 資格の更新
  def update_qualification
    @worker = User.find params[:id]
    @uq = UsersQualification.find params[:uq][:id]
    @uq.attributes = params[:uq].permit(:qualification_id,
                                        :effective_day, :expire_day)
    @uq.expire_day = nil if !params[:onend]
    
    if @uq.save
      flash[:notice] = "資格情報を更新しました。"
      redirect_to :action => 'show', :id => @worker
    else
      render :action => 'edit'
    end
  end

  
  def destroy_qualification
    @worker = User.find params[:id]
    @uq = UsersQualification.find params[:uq]

    @uq.destroy
    flash[:notice] = "資格情報を削除しました。"
    redirect_to :action => 'show', :id => @worker
  end


  ###########################################
  private
  def set_worker
    @worker = User.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def worker_params
    params.fetch(:worker, {})
  end

end # class UsersController
