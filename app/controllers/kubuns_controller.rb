# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 元帳の予算・実績区分
class KubunsController < ApplicationController
  before_filter :login_required
  before_filter :role_keiri_required

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :index }

  def index
=begin
    if !Kubun.find(:first)
      redirect_to :action => "new" 
      return
    end
=end
    @kubuns = Kubun.paginate :page => params[:page], :per_page => 20
  end


  def show
    @kubun = Kubun.find(params[:id])
  end


  def new
    @kubun = Kubun.new
  end

  def create
    @kubun = Kubun.new(params[:kubun])
    if @kubun.save
      flash[:notice] = 'Kubun was successfully created.'
      redirect_to :action => 'index'
    else
      render :action => 'new'
    end
  end

  def edit
    @kubun = Kubun.find(params[:id])
  end


  def update
    @kubun = Kubun.find(params[:id])
    if @kubun.update_attributes(params[:kubun])
      flash[:notice] = 'Kubun was successfully updated.'
      redirect_to :action => 'show', :id => @kubun
    else
      render :action => 'edit'
    end
  end


  def destroy
    Kubun.find(params[:id]).destroy
    redirect_to( {:action => 'index'}, :notice => "元帳区分を削除しました。" )
  end
end
