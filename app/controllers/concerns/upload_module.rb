# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 添付ファイルをアップロードする。
module UploadModule
  # エントリを削除する
  def destroy_attach
    prev = params[:prev]
    AttachFile.transaction {
      file = AttachFile.find(params[:id])
      AttachFileRef.delete_all ['attach_file_id = ?', file.id]
      file.destroy
    }
    flash[:notice] = "添付ファイルを削除しました。"
    redirect_to :action => 'show', :id => prev
  end
end
