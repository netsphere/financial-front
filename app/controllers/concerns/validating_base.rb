# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# ActiveRecord::Baseでないクラスのインスタンスで、Validationsの機能を使う。
# http://wiki.rubyonrails.org/rails/pages/HowToUseValidationsWithoutExtendingActiveRecord
class ValidatingBase
  # Rails3-
  include ActiveModel::Validations
  # こっちも追加で必要。Ref 公式ドキュメント
  include ActiveModel::Validations::Callbacks  # before_validation()

  # extend ActiveModel::Translation でもよいが。
  class << self
    def human_attribute_name attribute, options = {}
      attribute.to_s
    end
  end # class << self


  def [](key)
    instance_variable_get(key)
  end
  
  def initialize attributes = {}
    if attributes
      attributes.each do |key, value|
        send "#{key}=", value
      end
    end
  end
end
