# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 取引先の検索ボックス
class PartnerSearchBox < ValidatingBase
  #include Pagy::Backend コントローラ以外は不可 undefined local variable or method `params' for an instance of PartnerSearchBox
  
  attr_accessor :code

  attr_accessor :text

  validate :must_fill


  # for `validate()`
  def must_fill
    if code.blank? && text.blank?
      errors.add :base, "取引先コードか検索テキストの, いずれかを入力してください。"
    end
  end


  # return [Array of Partner]
  def find_by_text
    if @text.to_s.strip != ''
      t = @text.unicode_normalize(:nfkc).strip.upcase 
      case
      when (t[0].eql?('|') && !t[t.length-1].eql?('|')) #前方検索
        t.slice!(0) ; t += '%'
      when (t[t.length-1].eql?('|') && !t[0].eql?('|')) #後方検索
        t = '%' + t.chop!
      else #部分検索
        t = '%' + t.strip + '%'
      end
      return Partner.where('upper(name) LIKE ? OR kana_name LIKE ? OR upper(remarks) LIKE ?', t, t, t)
                    .order("zaimu_id")
                    .limit(Partner::PAGINATE_MAX)
    end

    if @code.to_s.strip != ""
      # vendor code だけ文字列. うーむ...
      sql = Partner.joins("LEFT JOIN bp_addresses ON partners.id = bp_addresses.partner_id")
               .joins("LEFT JOIN bank_accounts ON partners.id = bank_accounts.partner_id")
               .select("DISTINCT partners.*")
               .order("partners.zaimu_id")
              .limit(Partner::PAGINATE_MAX)
      t = @code.to_s.strip
      if /[^0-9]/ =~ t
        sql = sql.where("upper(bp_addresses.vendor_code) = ?", t.upcase)
      else
        sql = sql.where("upper(bp_addresses.vendor_code) = ? OR partners.zaimu_id = ? OR bank_accounts.zaimu_ac = ?", @code, @code, @code)
      end

      return sql
    end

    return nil
  end
end

