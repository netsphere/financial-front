# -*- coding: utf-8 -*-

# 会計システムサーバを選ぶボックス
class ServerLocationBox < ValidatingBase
  attr_accessor :company_db_idx
  attr_accessor :username
  attr_accessor :password

  validates_presence_of :company_db_idx
  validates_presence_of :username
  validates_presence_of :password
end


