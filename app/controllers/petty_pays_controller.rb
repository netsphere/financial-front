# -*- coding: utf-8 -*-


# 小口の支払い
class PettyPaysController < ApplicationController

  def index
    @petty_pays = PettyPay.find :all, 
           :select => "demand_batch_id, MAX(demand_batch_close_date) AS demand_batch_close_date, MAX(created_at), SUM(amount) as amount",
           :group => "demand_batch_id",
           :order => "MAX(created_at)"
  end


  # batch_id の中身
  def list
    batch_id = params[:batch_id]

    @petty_pays = PettyPay.find :all, 
                             :conditions => ["demand_batch_id = ?", batch_id]
  end
end
