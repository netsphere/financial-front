# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


class BpAddressesController < ApplicationController
  before_filter :login_required

  before_filter :set_bp_address, :only => [:show, :edit, :update, :destroy]

  # すべての一覧
  def index
    @bp_addresses = BpAddress.paginate(:page => params[:page], :per_page => 25).order("id")
  end


  # 取引先のアドレス一覧
  def list
    @partner = Partner.find params[:partner]
    @bp_addresses = BpAddress.where("partner_id = ?", @partner.id )
  end


  def new
    @partner = Partner.find params[:partner]
    @bp_address = BpAddress.new :partner_id => @partner
  end


  def create
    @partner = Partner.find params[:partner]
    @bp_address = BpAddress.new bp_address_params.permit(:vendor_code, :name)
    @bp_address.partner_id = @partner.id
    @bp_address.created_by = current_user.id
    begin
      BpAddress.transaction do
        @bp_address.save!
      end
    rescue ActiveRecord::RecordInvalid
      render :action => "new"
      return
    end

    redirect_to :action => "index", :partner => @partner
  end


  def edit
    @partner = @bp_address.partner
  end


  def update
    @partner = @bp_address.partner
    if @bp_address.update_attributes(
                      bp_address_params.permit(:vendor_code, :name) )
      redirect_to :action => "show", :id => @bp_address
    else
      render :action => "edit"
    end
  end


  def destroy
    @bp_address.destroy
    redirect_to :action => "list", :partner => @bp_address.partner
  end
  
  
  ###########################################
  private
  def set_bp_address
    @bp_address = BpAddress.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def bp_address_params
    params.fetch(:bp_address, {})
  end

end # class BpAddressesController
