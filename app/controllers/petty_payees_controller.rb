# -*- coding:utf-8 -*-

# 小口精算のコーポレートカード・マスタ
class PettyPayeesController < ApplicationController
  before_filter :set_petty_payee, :only => [:show, :edit, :update, :destroy]

  # GET /petty_payees
  # GET /petty_payees.json
  def index
    @petty_payees = PettyPayee.all
  end

  # GET /petty_payees/1
  # GET /petty_payees/1.json
  def show
  end

  # GET /petty_payees/new
  def new
    @petty_payee = PettyPayee.new
  end

  # GET /petty_payees/1/edit
  def edit
  end

  # POST /petty_payees
  # POST /petty_payees.json
  def create
    @petty_payee = PettyPayee.new(petty_payee_params)

    if @petty_payee.save
        redirect_to @petty_payee, :notice => 'Petty payee was successfully created.' 
    else
        render :new 
    end
  end

  # PATCH/PUT /petty_payees/1
  # PATCH/PUT /petty_payees/1.json
  def update
    if @petty_payee.update(petty_payee_params)
        redirect_to @petty_payee, :notice => 'Petty payee was successfully updated.' 
    else
        render :edit 
    end
  end

  # DELETE /petty_payees/1
  # DELETE /petty_payees/1.json
  def destroy
    @petty_payee.destroy
    redirect_to petty_payees_url, :notice => 'Petty payee was successfully destroyed.' 
  end


  private
  # Use callbacks to share common setup or constraints between actions.
  def set_petty_payee
    @petty_payee = PettyPayee.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def petty_payee_params
    params.fetch(:petty_payee, {})
  end
end
