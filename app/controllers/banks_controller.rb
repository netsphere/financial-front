# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 日本国内の銀行マスタ
# 銀行と支店の組
class BanksController < ApplicationController
  class SearchBox < ValidatingBase
    attr_accessor :bank_name, :branch_name
  
    def validate
      if bank_name.to_s.strip == '' && branch_name.to_s.strip == ''
        errors.add :base, '銀行名または支店名を入力してください。'
      end
      bank_name = bank_name.to_s.strip
      branch_name = branch_name.to_s.strip
    end
  end


  before_action :set_bank, :only => [:show, :edit, :update, :destroy]

  
  # 検索画面
  def index
  end
  
  def search
    @search = SearchBox.new params[:search]
    @search.validate
    if !@search.errors.empty?
      render :action => 'index'
      return 
    end
    
    text_b = '%' + Bank.zen2han(@search.bank_name) + '%'
    b_zen = '%' + normalize_kc(@search.bank_name) + '%'
    text_br = '%' + Bank.zen2han(@search.branch_name) + '%'
    br_zen = '%' + normalize_kc(@search.branch_name) + '%'

    if @search.branch_name == ''
      # 銀行だけ
      @s = Bank.find :all,
              :conditions => ['name LIKE ? OR kana_name LIKE ?', b_zen, text_b],
              :limit => 31
    else
      if @search.bank_name == ''
        # 支店だけ
        @s = BankBranch.find :all,
                  :conditions => ['name LIKE ? OR kana_name LIKE ?', 
                                  br_zen, text_br],
                  :limit => 31
      else
        # 両方
        @s = BankBranch.find :all,
             :select => 'bank_branches.*', 
             :joins => 'JOIN banks ON bank_branches.bank_id = banks.id',
             :conditions => ['(bank_branches.name LIKE ? OR bank_branches.kana_name LIKE ?) AND (banks.name LIKE ? OR banks.kana_name LIKE ?)', 
                             br_zen, text_br, b_zen, text_b],
             :limit => 31
      end
    end
  end
  
  # 銀行の一覧
  def list
    @banks = Bank.paginate :order => "id", :page => params[:page],
                           :per_page => 30
  end
  

  # 銀行の詳細
  def show
    #@bank = Bank.find params[:id]

    @branches = BankBranch.paginate :conditions => ["bank_id = ?", @bank.id],
                                    :order => "branch_code", 
                                    :page => params[:page], :per_page => 30
  end

  def new
    @bank = Bank.new
  end


  def create
    @bank = Bank.new(bank_params.permit(:name, :kana_name) )
    @bank.id = params[:bank_code]
    begin
      Bank.transaction do 
        @bank.save!
        flash[:notice] = 'Bank was successfully created.'
        redirect_to :action => 'list'
      end
    rescue ActiveRecord::RecordInvalid
      render :action => 'new'
    rescue ActiveRecord::RecordNotUnique
      @bank.errors.add :id, "すでに同じ番号の金融機関があります。"
      render :action => "new"
    end
  end

  def edit
    #@bank = Bank.find(params[:id])
  end

  def update
    #@bank = Bank.find(params[:id])
    if @bank.update_attributes(bank_params.permit(:name, :kana_name) )
      flash[:notice] = 'Bank was successfully updated.'
      redirect_to :action => 'show', :id => @bank
    else
      render :action => 'edit'
    end
  end


  def destroy
    @bank.destroy
    redirect_to :action => 'list'
  end

  
  ###########################################
  private
  def set_bank
    @bank = Bank.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def bank_params
    params.fetch(:bank, {})
  end

end # class BanksController
