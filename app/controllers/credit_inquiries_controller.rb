# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 信用調査
# 継続記録し、推移を把握できるようにする。
class CreditInquiriesController < ApplicationController
  before_filter :login_required

  before_filter :set_credit_inquiry, :only => [:show, :edit, :update, :destroy]

  
  # GET /credit_inquiries
  # GET /credit_inquiries.xml
  def index
    @partner = Partner.find(params[:partner])
    @credit_inquiries = CreditInquiry.find :all, :conditions => ['partner_id = ?', @partner.id]
  end

  # GET /credit_inquiries/1
  # GET /credit_inquiries/1.xml
  def show
    #@credit_inquiry = CreditInquiry.find(params[:id])
    @partner = @credit_inquiry.partner

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @credit_inquiry }
    end
  end

  # GET /credit_inquiries/new
  # GET /credit_inquiries/new.xml
  def new
    @credit_inquiry = CreditInquiry.new
    @partner = Partner.find(params[:partner])
  end

  # GET /credit_inquiries/1/edit
  def edit
    #@credit_inquiry = CreditInquiry.find(params[:id])
    @partner = Partner.find(params[:partner])
  end

  # POST /credit_inquiries
  # POST /credit_inquiries.xml
  def create
    @credit_inquiry = CreditInquiry.new(
                credit_inquiry_params.permit(:report_date, :score, :remarks))
    @partner = Partner.find(params[:partner])

    @credit_inquiry.attributes = {
      :partner_id => @partner.id,
      :create_user_id => current_user.id
    }
    @credit_inquiry.score = nil if @credit_inquiry.score.to_i == 0

    if @credit_inquiry.save
      flash[:notice] = 'CreditInquiry was successfully created.'
      redirect_to :action => 'show', :id => @credit_inquiry, :partner => @partner
    else
      render :action => "new" 
    end
  end

  # PUT /credit_inquiries/1
  # PUT /credit_inquiries/1.xml
  def update
    #@credit_inquiry = CreditInquiry.find(params[:id])
    @partner = @credit_inquiry.partner

    if @credit_inquiry.update_attributes(
                 credit_inquiry_params.permit(:report_date, :score, :remarks))
      flash[:notice] = 'CreditInquiry was successfully updated.'
      redirect_to :action => "show", :id => @credit_inquiry
    else
      render :action => "edit"
    end
  end

  # DELETE /credit_inquiries/1
  # DELETE /credit_inquiries/1.xml
  def destroy
    #@credit_inquiry = CreditInquiry.find(params[:id])
    @credit_inquiry.destroy
    @partner = Partner.find(params[:partner])

    flash[:notice] = '信用調査実績を削除しました。'
    redirect_to :action => 'index', :partner => @partner
  end


  ###########################################
  private
  def set_credit_inquiry
    @credit_inquiry = CreditInquiry.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def credit_inquiry_params
    params.fetch(:credit_inquiry, {})
  end

end # class CreditInquiriesController
