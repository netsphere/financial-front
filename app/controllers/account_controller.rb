# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# ユーザ認証
class AccountController < ApplicationController
  skip_before_action :require_login, only:['sign_in']
  
  # If you want "remember me" functionality, add this before_filter to Application Controller
  #before_action :login_from_cookie
  #protect_from_forgery :except => [:authenticate, :get, :count]

=begin
  # say something nice, you goof!  something sweet.
  def index
    redirect_to(:action => 'signup') unless logged_in? || User.count > 0
  end
=end


  def sign_in
    return if !request.post?

    if login(params[:login], params[:password])
      flash[:notice] = "ログインに成功しました。"
      redirect_back_or_to({controller:'welcome', action:'index'} )
    else
      @error = "ログインIDまたはパスワードが違います。"
      # HTTP 422 Validation error. この status を付けないとエラー表示されない.
      render 'sign_in', status: :unprocessable_entity
    end
  end


  # 最初のユーザだけを登録できる。
  def signup
    if User.find(:first)
      flash[:notice] = 'すでにユーザが登録されています。'
      redirect_to :controller => 'users', :action => 'index'
      return
    end
      
    @user = User.new
    @user.attributes = {
      :active => 1,
      :keiri_flag => true,    # 最初のユーザは経理ロール
      :marketing_flag => false,
      :apar_flag => true,
      :hrd_flag => true,
    }
    return unless request.post?  # 入力フォーム表示

    @user.attributes = params[:user].permit(:login, :password,
                                      :password_confirmation, :worker_code,
                                      :name, :kana_name, :email)
#    User.transaction {
      #role = Role.find_by_keiri(1)
      #raise if !role
=begin
      div = Division.new :name => '経理 (変更してください)', 
                         :kana_name => 'ケイリ',
                         :division_code => 'xxx',
                         # :role_id => role.id,
                         :purchase_active => 1
      div.save!
=end
    @user.save!
=begin
      UsersDivision.create! :user_id => @user.id,
                            :division_id => div.id,
                            :default_pair => 1,
                            :effective_day => Date.today
    }
=end
    set_current_user @user
    redirect_back_or_default(:controller => 'users', :action => 'index')
    flash[:notice] = "Thanks for signing up!"
  rescue ActiveRecord::RecordInvalid
    render :action => 'signup'
  end

  
  def sign_out
    logout()

    flash[:notice] = "You have been logged out."
    redirect_back_or_to(:controller => '/welcome', :action => 'index')
  end

  
  # パスワードの編集
  def edit_password
  end

  
  def update_password
    @worker = current_user
    begin
      User.transaction do
        @worker.update!(params[:worker].permit(:password, :password_confirmation))
      end
    rescue ActiveRecord::RecordInvalid
      render 'edit_password', status: :unprocessable_entity
      return
    end
    
    flash[:notice] = 'パスワードを変更しました。'
    redirect_to( {:controller => 'users', :action => 'show', :id => @worker} )
  end


  # @user を XML形式で返す
  # もし @user が nil の場合は status code 500 を返す.
  def respond_user
    headers['Content-Type'] = 'application/xml'
    request.format = "xml" # 強制
    respond_to do |format|
      format.xml {
        render :action => 'get', :status => (@user ? 200 : 500),  :layout => false
      }
    end
  end
  private :respond_user

  
  # 別システムから認証機能を利用する
  # TODO: もっとまともなのにすること。
  def authenticate
    if request.post? 
      @user = User.authenticate(params[:login], params[:password])
    end
    respond_user
  end

  
  def get
    @user = User.find_by_id params[:id]
    respond_user
  end


  def get_by_login
    @user = User.find_by_login params[:id]
    respond_user
  end
  

  def count
    @count = User.count
    headers['Content-Type'] = 'application/xml'
    request.format = "xml"
    respond_to do |format|
      format.xml { render :status => 200, :layout => false }
    end
  end


  #############################################################################
  # omniauth

  # 外部サービスでの認証が成功すると、ここにコールバックされる
  def create
    auth_hash = env["omniauth.auth"]
    
    case params[:provider]
    when "concur_oauth2"
      # この段階では、ユーザが識別できない。もう一度APIを叩く
      token = auth_hash[:credentials][:token]

      raise "DEBUG: " + auth_hash.inspect
      
=begin
ログインIDはメールアドレス.
curl -X GET --header "Accept: application/json" 
     --header "Authorization: OAuth トークン" 
     "https://www.concursolutions.com/api/v3.0/common/users?user=ログインID"

ここで、実際に試せる.
https://developer.concur.com/api-explorer/v3-0/Users.html
=end
      
    else
      raise "unknown provider: " + auth_hash.inspect
    end
  end
end

