# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 輸入申告 / 輸入許可通知書
class ImportDeclarationsController < ApplicationController
  before_filter :login_required

  before_filter :set_import_declaration,
                :only => [:show, :edit, :update, :destroy]

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post,
         :only => [ :create, :destroy, :update, :remove_ref ],
         :redirect_to => { :action => :index }

  # GET /import_declarations
  # GET /import_declarations.xml
  def index
    @import_declarations = ImportDeclaration.paginate :page => params[:page],
                                  :per_page => 25,
                                  :order => 'permit_date DESC, id'
  end

  # GET /import_declarations/1
  # GET /import_declarations/1.xml
  def show
    #@import_declaration = ImportDeclaration.find(params[:id])
  end
  

  # (購買)請求書から[新しい輸入報告...] の場合は、invoice パラメータがある。
  def new
    @import_declaration = ImportDeclaration.new
    if params[:invoice]
      @invoice = Invoice.find(params[:invoice]) 
      @import_declaration.company_id = @invoice.company_setting_id
    end
  end


  # POST /import_declarations
  # POST /import_declarations.xml
  def create
    pa = import_declaration_params.permit(:permit_date, :declaration_number,
                                          :company_id,  # newのみ
                                          :exporter_name,
                                          :bl_number, :description,
                                          :invoice_incoterm_id,
                                          :invoice_price, :invoice_currency_id,
                                          :customs_tariff, :consumption_tax,
                                          :vat_local)
    @import_declaration = ImportDeclaration.new pa
    @import_declaration.attributes = {
      :declaration_number => pa[:declaration_number].gsub(/[ \t]/, ''),
      :bl_number => pa[:bl_number].gsub(/[ \t]/, ''),
      :invoice_price => nominal_amount(pa[:invoice_price], 
                               @import_declaration.invoice_currency.exponent),
      :created_by => current_user,
    }

    if params[:invoice]
      @invoice = Invoice.find(params[:invoice])
      @import_declaration.company_id = @invoice.company_setting.id
    end
    
    begin
      @import_declaration.transaction {
        @import_declaration.save!

        if @invoice
          ii = ImportsInvoice.new :import_declaration => @import_declaration,
                                  :invoice => @invoice,
                                  :created_by => current_user
          ii.save!
          @invoice.update_amount_total
          @invoice.save!
        end
      }
      flash[:notice] = 'ImportDeclaration was successfully created.'
      redirect_to :action => 'show', :id => @import_declaration
    rescue ActiveRecord::RecordInvalid
      render :action => "new" 
    end
  end


  # GET /import_declarations/1/edit
  def edit
    #@import_declaration = ImportDeclaration.find(params[:id])
  end


  # PUT /import_declarations/1
  # PUT /import_declarations/1.xml
  def update
    #@import_declaration = ImportDeclaration.find params[:id]
    
    pa = import_declaration_params.permit(:permit_date, :declaration_number,
                                          :exporter_name,
                                          :bl_number, :description,
                                          :invoice_incoterm_id,
                                          :invoice_price, :invoice_currency_id,
                                          :customs_tariff, :consumption_tax,
                                          :vat_local)
    @import_declaration.attributes = pa
    @import_declaration.attributes = {
      :declaration_number => pa[:declaration_number].gsub(/[ \t]/, ''),
      :bl_number => pa[:bl_number].gsub(/[ \t]/, ''),
      :invoice_price => nominal_amount(pa[:invoice_price], 
                                @import_declaration.invoice_currency.exponent)
    }

    begin
      @import_declaration.save!

      ii = ImportsInvoice.find_by_import_declaration_id @import_declaration.id
      if ii && ii.invoice
        ii.invoice.update_amount_total
        ii.invoice.save!
      end
    rescue ActiveRecord::RecordInvalid
      render :action => "edit"
      return
    end

    flash[:notice] = 'ImportDeclaration was successfully updated.'
    redirect_to :action => 'show', :id => @import_declaration
  end


  # DELETE /import_declarations/1
  # DELETE /import_declarations/1.xml
  def destroy
    #@import_declaration = ImportDeclaration.find params[:id]
    @import_declaration.destroy
    
    flash[:notice] = '輸入報告を削除しました。'
    redirect_to :action => 'index' 
  end


  def remove_ref
    ii = ImportsInvoice.find params[:ii]
    ii.destroy
    ii.invoice.update_amount_total
    ii.invoice.save!

    flash[:notice] = '関連付けを外しました。'
    redirect_to :action => 'show', :id => ii.import_declaration
  end
  
  # 請求書と関連付け (画面)
  def ref_invoice
    @import_declaration = ImportDeclaration.find params[:id]
  end
  
  def ref_invoice2
    @import_declaration = ImportDeclaration.find params[:id]
  end

  
  ###########################################
  private
  def set_import_declaration
    @import_declaration = ImportDeclaration.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def import_declaration_params
    params.fetch(:import_declaration, {})
  end

end # class ImportDeclarationsController
