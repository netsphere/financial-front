# -*- coding:utf-8 -*-


# 財務大将へのエクスポート
class TaishoController < ApplicationController 
  before_filter :login_required
  before_filter :role_keiri_required


  # (購買)請求書、販売請求書, 入金、出金
  # 実際の出力(CSV出力)
  def export_apar
    company = Company.find params[:company]
    fixed_day = company.fixed_gl_day
    
    @entries = []
    
    # (購買)請求書
    inv_ids = session[:entries]
    inv_ids.each {|inv_id|
      inv = Invoice.find inv_id
      entry_day = [inv.buy_day, fixed_day + 1].max
      @entries += inv.make_entries(company, entry_day)
    }
    
    # 販売請求書
    si_ids = session[:si_entries]
    si_ids.each {|inv_id|
      inv = SalesInvoice.find inv_id
      entry_day = [inv.sell_day, fixed_day + 1].max
      @entries += inv.make_entries(company, entry_day)
    }

=begin    
    # 入金伝票
    receiving_ids = session[:receiving_entries]
    receiving_ids.each {|r_id|
      r = ReceivingSlip.find r_id
      entry_day = r.date
      @entries += r.make_entries(company, entry_day)
    }
=end

    # 出金伝票
    payment_ids = session[:payment_entries]
    payment_ids.each do |p_id|
      r = PaymentSlip.find p_id
      entry_day = r.date
      @entries += r.make_entries(company, entry_day)
    end

    # See RFC 6266: Use of the Content-Disposition Header Field in the
    #               Hypertext Transfer Protocol (HTTP)
    headers["Content-Type"] = "application/vnd.ms-excel; charset=UTF-8"
    headers["Content-Disposition"] = "attachment; filename=keirisys-export.csv"
    render :layout => false
  end


  # 小口の仕訳出力
  def export_petty
    @petty_group = PettyGroup.find_by_id(params[:petty_group]) || 
                   PettyGroup.find(:first)
    company = @petty_group.company

    # payは 出金伝票としてエクスポートする

    entry_day = nil  # 小口では引数の日付は使わない

    @entries = []
    report_ids = []
    ca_ids = []

    # 小口経費精算リポート
    reports = PettyExpense.find :all, 
                      :conditions => ["journal_export_at IS NULL AND petty_group_id = ?", @petty_group.id]
    reports.each do |report|
      @entries += report.make_entries(company, entry_day)
      report_ids << report.id
    end

    # 仮払い
    cas = PettyCashAdvance.find :all,
                      :conditions => ["journal_export_at IS NULL AND petty_group_id = ?", @petty_group.id]
    cas.each do |ca|
      @entries += ca.make_entries(company, entry_day)
      ca_ids << ca.id
    end

    @size = report_ids.length + ca_ids.length
    session[:report_ids] = report_ids
    session[:ca_ids] = ca_ids
  end


  # 実際の出力
  def export_petty2
    @petty_group = PettyGroup.find params[:petty_group]
    company = @petty_group.company

    @entries = []

    # 小口経費精算リポート
    report_ids = session[:report_ids]
    report_ids.each do |report_id|
      report = PettyExpense.find report_id
      @entries += report.make_entries( company, nil )
    end

    # 仮払い
    ca_ids = session[:ca_ids]
    ca_ids.each do |ca_id|
      ca = PettyCashAdvance.find ca_id
      @entries += ca.make_entries( company, nil )
    end

    # See RFC 6266: Use of the Content-Disposition Header Field in the
    #               Hypertext Transfer Protocol (HTTP)
    headers["Content-Type"] = "application/vnd.ms-excel; charset=UTF-8"
    headers["Content-Disposition"] = "attachment; filename=petty-export.csv"
    render :layout => false
  end


  def finish_export_petty
    @petty_group = PettyGroup.find params[:petty_group]

    report_ids = session[:report_ids]
    ca_ids = session[:ca_ids]

    exported_at = Time.now

    PettyExpense.transaction do 
      report_ids.each do |report_id|
        report = PettyExpense.find report_id
        report.journal_export_at = exported_at
        report.save!
      end

      ca_ids.each do |ca_id|
        ca = PettyCashAdvance.find ca_id
        ca.journal_export_at = exported_at
        ca.save!
      end
    end # of transaction

    flash[:notice] = '仕訳出力済みとマークしました。'
    redirect_to :action => "export_petty", :petty_group => @petty_group
  end

end # of class TaishoController
