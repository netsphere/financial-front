xml.instruct! :xml, :version => "1.0", :encoding => 'UTF-8'
if @user
xml.user {
  xml.id @user.id
  xml.name @user.name
  xml.worker_code @user.worker_code
  xml.keiri_flag @user.keiri_flag
  xml.marketing_flag @user.marketing_flag
}
end
