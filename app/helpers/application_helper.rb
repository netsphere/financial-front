# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# viewのヘルパ
module ApplicationHelper
  # Pagination
  include Pagy::Frontend

  
  def edit_amount(n, exp)
    if n
      if exp != 0
        (Float(n) / (10.0 ** exp)).to_s
      else
        n.to_s
      end
    else
      ""
    end
  end
  
  
  # 時刻を整形
  # ISO 8601だと 2008-01-13T23:51+09 だが見栄えがしない
  # TODO: ユーザプロファイルによるタイムゾーン
  def format_time t
    t ? t.strftime("%Y-%m-%d %H:%M") : nil
  end
  
  
  # 年が同じなら短く表示
  def format_date_short d, today
    if d
      d.year == today.year ? "#{d.mon}/#{d.day}" : d.to_s
    else
      nil
    end
  end
  
  
  def date_select2 obj, method
    r = date_select obj, method
    b = <<EOF
<button type="button" onClick="openCalendar(this.form.#{obj}_#{method}_1i, this.form.#{obj}_#{method}_2i, this.form.#{obj}_#{method}_3i, event, &#39;yy/mm/dd&#39;);" style="padding:0">
  <span class="glyphicon glyphicon-calendar"></span>
</button>
EOF
    r << b.html_safe
    r
  end
  
  
  def select_cost_center_tag name, cost_centers, selected
    s = "<select name=\"#{name}\">\n"
    grp = nil
    cost_centers.each do |c|
      if c.parent != grp
        s << "</optgroup>\n" if grp
        s << "<optgroup label=\"#{h(c.parent.name)}\">\n"
        grp = c.parent
      end
      if c.id == selected
        s << "<option selected=\"selected\" value=\"#{c.id}\">#{h(c.name)}"
      else
        s << "<option value=\"#{c.id}\">#{h(c.name)}"
      end
    end
    s << "</select>"
    return s.html_safe
  end
  
  
  def select_account_title_tag name, account_title_tree, selected, ac_changed, f_id
    s = "<select name=\"#{f_id}\[#{name}\]\" id=\"#{f_id}_#{name}\" onchange=\"#{ac_changed}\" class=\"chosen-select\" style=\"width:250px;\">\n"
    s << (f_id == 'detail0' || f_id == 'journal0' ? "" : "<option value=\"0\">        〃\n")
    grp = nil
    account_title_tree.each do |c|
      if c.root_account_code != grp
        s << "</optgroup>\n" if grp
        account_code = c.root_account_code == 999999 ? '' : c.root_account_code
        s << "<optgroup label=\"#{h(account_code)} #{h(c.root_name)}\">\n"
        grp = c.root_account_code
      end
      if c.child_id == selected
        s << "<option selected=\"selected\" value=\"#{h(c.child_id)}\">#{h(c.account_code)} #{h(c.name)}\n"
      else
        s << "<option value=\"#{h(c.child_id)}\">#{h(c.account_code)} #{h(c.name)}\n"
      end
    end
    s << "</select>"
    return s.html_safe
  end
  
  
  def select_division_tag name, cost_centers, selected, div_changed, f_id
    s = "<select name=\"#{f_id}\[#{name}\]\" id=\"#{f_id}_#{name}\" onchange=\"#{div_changed}\" class=\"chosen-select\">\n"
    grp = nil
    cost_centers.each do |c|
      if c.control_divisions.first.id!= grp
        s << "</optgroup>\n" if grp
        s << "<optgroup label=\"#{h(c.control_divisions.first.id)} #{h(c.control_divisions.first.name)}\">\n"
        grp = c.control_divisions.first.id
      end
      if c.id == selected
        s << "<option selected=\"selected\" value=\"#{h(c.id)}\">#{h(c.division_code)} #{h(c.name)}\n"
      else
        s << "<option value=\"#{h(c.id)}\">#{h(c.division_code)} #{h(c.name)}\n"
      end
    end
    s << "</select>"
    return s.html_safe
  end


end # module ApplicationHelper


