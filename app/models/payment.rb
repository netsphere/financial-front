# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 出金伝票の債務側の１行
class Payment < ActiveRecord::Base
  belongs_to :invoice
  belongs_to :payment_slip
  belongs_to :account_title
  belongs_to :currency


  class << self
    # 債務の消し込みと, 為替差損益を埋める
    # @return [新しい仕訳エントリ, 債務の為替レート]
    def make_partial_entry2 generator, company, entry_day, payables, 
                            current_rate
      raise TypeError if !company.is_a?(Company)
      raise TypeError if !entry_day.is_a?(Date)
      raise TypeError if !payables.is_a?(Array)

      inv_currency = payables.first.currency

      je_list = []
      sys_amt_ttl = 0
      fc_amt_ttl = 0
      payables.each do |payable|
        raise TypeError if !payable.is_a?(Payment)

        # 請求書全部の金額. 
        dmy, gl_rate = payable.invoice.amount_total_local_and_rate

        je = JournalLine.new
        je.slip_date = entry_day

        # 借方
        je.debtor.account_title = payable.account_title
        je.debtor.division_code = nil
        je.debtor.dim2_code = nil
        je.debtor.partner_code = (payable.invoice.bank_account_id ? 
                    payable.invoice.bank_account.zaimu_ac : 
                    payable.invoice.partner.zaimu_id)
        je.debtor.vat_code = nil

        # 貸方: 相手勘定 => 為替差損益で使う
        je.creditor.contra_account = generator.convert_partner(
                                          payable.account_title.account_code, 
                                          je.debtor.partner_code)

        # 金額など
        # 一部支払いがある. 請求書の金額ではないことに注意
        je.sys_amount = (BigDecimal.new(payable.amount.to_s) * gl_rate /
               (10 ** payable.currency.exponent) + BigDecimal.new("0.5")).floor
        sys_amt_ttl += je.sys_amount
        if inv_currency.id != company.functional_currency.id
          je.fc_currency_code = payable.currency.code
          je.fc_amount = BigDecimal.new(payable.amount.to_s) / 
                                             (10 ** payable.currency.exponent)
          fc_amt_ttl += je.fc_amount
        end

        # 摘要
        je.remarks = [payable.invoice.partner.name, 
                      payable.invoice.first_detail_remarks,
                      "支払"].join("/")

        je_list << je
      end # of payables.each

      # 為替差損益
      if inv_currency.id != company.functional_currency.id
        diff = sys_amt_ttl - 
               (BigDecimal.new(fc_amt_ttl.to_s) * current_rate + 
                                                BigDecimal.new("0.5")).floor
        if diff <= -1 || diff >= +1
          je = JournalLine.new

          je.creditor.account_title = AccountTitle.currency_gain
          je.creditor.division_code = company.bank_division.division_code
          je.creditor.dim2_code = company.bank_division.default_dim2.dim_code
          je.creditor.vat_code = 40

          # 相手勘定
          je.creditor.contra_account = je_list.first.creditor.contra_account

          je.sys_amount = diff
          je.remarks = "出金 為替差"

          je_list << je
          sys_amt_ttl -= diff
        end
      end

      return [je_list, sys_amt_ttl]
    end
  end # class << self


  # 債務側の金額を埋める
  # @return [新しい仕訳エントリ, 為替レート]
  def make_partial_entry company, entry_day
    gl_amt, gl_rate = invoice.amount_total_local_and_rate

    # 一部支払いがある。請求書の金額ではない
    e = {
      :dr_account_title => account_title,
      :dr_division => nil,
      :dr_partner_code => (invoice.bank_account_id ? 
                    invoice.bank_account.zaimu_ac : invoice.partner.zaimu_id),
      :amount => (BigDecimal.new(self.amount.to_s) / 
                                              10 ** self.currency.exponent *
                 gl_rate + BigDecimal.new("0.5")).floor,
      :vat => nil,
      :remarks => [invoice.partner.name, invoice.first_detail_remarks,
                   "支払"].join("/")
    }

    [e, gl_rate]
  end
end
