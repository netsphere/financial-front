# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 添付ファイル
class AttachFile < ActiveRecord::Base
  has_many :partners, :through => :attach_file_refs
  has_many :invoices, :through => :attach_file_refs
  
  belongs_to :created_by, :foreign_key => 'create_user_id',
             :class_name => 'User'
  belongs_to :attach_kind
  belongs_to :division

  # エクスプローラで作れる最長. (OSはもっと長く作れるが, 実用ではない)
  validates_length_of :original_filename, :maximum => 260

  validates_length_of :content_type, :maximum => 100
end


# TODO: バイナリデータをいちいち取得しない
# http://www.simplegimmick.com/2007/09/acts-as-blob/
