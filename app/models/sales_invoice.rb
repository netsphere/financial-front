# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 販売請求書
class SalesInvoice < Document
  include CommonScope

  has_many :si_details, :dependent => :destroy
  has_many :si_journals, :dependent => :destroy
  has_many :si_banks, :dependent => :destroy

  has_many :our_banks, :through => :si_banks
  
  belongs_to :company_setting, :class_name => "Company"

  belongs_to :partner
  belongs_to :division   # 所管部門
  belongs_to :currency
  belongs_to :created_by, :foreign_key => 'create_user_id', :class_name => 'User'
  belongs_to :issued_by, :foreign_key => 'issued_by', :class_name => 'User'
  belongs_to :receivable_account, :foreign_key => 'receivable_account_id',
                                  :class_name => 'AccountTitle'
  #販売請求書検索一覧表示用
  scope :with_si_detail, -> { joins(%{LEFT JOIN si_details on sales_invoices.id = si_details.sales_invoice_id}) }
  scope :with_si_journal, -> { joins(%{LEFT JOIN si_journals on sales_invoices.id = si_journals.sales_invoice_id}) }
  scope :select_sales_invoice_list, -> { select("sales_invoices.id, partner_id, sell_day, amount_total, currency_id, state, si_number, due_day, including_tax ")}
  scope :si_number, ->(si_number) { where("COALESCE(cast(si_number as text),'') = ?", si_number) if si_number.present? }
  scope :remarks, ->(remarks) { where("COALESCE(si_journals.remarks,'') LIKE ?", remarks) if remarks.present? }
  scope :group_sinv_list_element, -> { group("sales_invoices.id, partner_id, sell_day, amount_total, si_number, currency_id")}
  scope :by_sell_day, -> { order(sell_day: :desc)}
  scope :by_sales_invoice_id, -> { order("sales_invoices.id")}
  
  before_save :update_dependency

  DELIVERY_TYPE_NAMES = [nil, '担当者持参', '経理から郵送']
  TAX_TYPE_NAMES = [nil, '税抜', '税込']
  SIGNED_OFF = 10
  
  #  paginate定数
  PAGINATE_MAX = 100
  PAGINATE_LIST = 25

  validates_presence_of :delivery
  validates_presence_of :partner
  validates_presence_of :division
  validate :check_fields
  validates :keiri_memo, length: { maximum: 200 , too_long: "%{count}文字以内で入力してください。" }

  # for validate
  def check_fields
    errors.add(:base, '明細が1行もありません。') if si_details.empty?
    errors.add(:base, '仕訳 (計上) が1行もありません。') if si_journals.empty?
    errors.add(:base, '半角英数以外は登録できません。') if je_number.to_s.match(/^\w+$|^$/).nil?
  end

  # 小計額
  def sub_total

    sub_total = 0

      si_details.each_with_index {|line, i| 
        sub_total += line.amount
       }

    return sub_total
  end

  # 消費税
  # 明細行の合計 x 税率 ... 個別行ごとの税額の合計ではない
  # 戻り値は (10 ** currency.exponent) した額
  def ex_vat_amount

    vat = 0; 
    ex_vat_amount_each.each do |i|
          vat +=  i[1]
    end

    if vat >= 0
      return vat.floor
    else
      return vat.ceil
    end
  end

  # 各税率の消費税額
  # 明細行の合計 x 税率 ... 個別行ごとの税額の合計ではない
  def ex_vat_amount_each

    vat = {}

    if including_tax 
      SiDetail::VAT_OPTS_DISP.each do |i|
        vat["vat_total#{i[1][0]}"] = 0
        vat_temp = 0        
        si_details.each do |r|
          if r.vat_code == i[0]
            vat_temp += r.amount
          end
        end
        vat["vat_total#{i[1][0]}"] +=  calc_vat(vat_temp,i[1][1])
      end   
    else
      subtotal_each = ex_subtotal_each
      SiDetail::VAT_OPTS_DISP.each do |i|
        vat_temp = subtotal_each["ex_subtotal#{i[1][0]}"] * i[1][1]
          if vat_temp >= 0
            vat["vat_total#{i[1][0]}"] = vat_temp.floor
          else
            vat["vat_total#{i[1][0]}"] = vat_temp.ceil
          end
      end
    end
    return vat
  end

  # 税率ごとの小計額(明細の税率ごと合計)
  def ex_subtotal_each

    subtotal_each = {}

    SiDetail::VAT_OPTS_DISP.each do |i|
      subtotal_each["ex_subtotal#{i[1][0]}"] = 0
      si_details.each do |r|
        if r.vat_code == i[0]
          subtotal_each["ex_subtotal#{i[1][0]}"] +=  r.amount
        end
      end
    end
    return subtotal_each
  end

  # 税込みの消費税額計算
  def calc_vat amount, tax_rate

    amount_temp = (BigDecimal.new(amount.to_s) / (10 ** currency.exponent))
    vat_temp = amount_temp / (1 + tax_rate )
    if vat_temp >= 0
      vat_temp = vat_temp.ceil
    else
      vat_temp = vat_temp.floor
    end
    vat_temp = (BigDecimal.new(vat_temp.to_s) * (10 ** currency.exponent))

    vat = (vat_temp * tax_rate)
    if vat >= 0
      return vat.floor
    else
      return vat.ceil
    end
  end

  # 債権科目と請求額合計を更新
  def update_dependency
    self.receivable_account = get_receivable_of_heavy_account(si_journals)
    raise "AR setting is missing!" if !self.receivable_account
    self.amount_total = si_journals.inject(0) {|result, item| result += item.amount}
  end
  private :update_dependency
  

  # 一番金額の大きな科目 (主たる科目) に対応する債権科目
  def get_receivable_of_heavy_account(journals)
    if !journals || journals.size < 1
      return nil # valid でエラー
    end

    a = {}
    journals.each {|r|
      a[r.account_title_id] = (a[r.account_title_id] || 0) + r.amount
    }
    return AccountTitle.find(a.to_a.sort_by {|x| x[1]}.last[0]).receivable
  end
  private :get_receivable_of_heavy_account


  # 未入金の額（残額）
  # 入金伝票が未承認のときは、未入金金額は減らさない
  # @param [Hash] options  オプション
  # @option options [PaymentSlip] :except_receiving  入金額から除外するもの
  def remain_amount options = {}
    if options[:except_receiving]
      raise TypeError if !options[:except_receiving].is_a?(PaymentSlip)

      cond = [ <<EOF, self.id, options[:except_receiving].id ]
(payment_slips.state = 10 OR payment_slips.state = 20) AND 
sales_invoice_id = ? AND payment_slips.id <> ?
EOF
    else
      cond = [ <<EOF, self.id ]
(payment_slips.state = 10 OR payment_slips.state = 20) AND 
sales_invoice_id = ?
EOF
    end

    # 入金済の金額
    r = Receiving.where(*cond) \
          .joins("JOIN payment_slips ON receivings.cash_slip_id = payment_slips.id") \
          .select('sum(receivings.amount) AS received') \
          .order("") \
          .first

    if r && r.received
      return amount_total - r.received.to_i # - r.charge.to_i
    else
      return amount_total
    end
  end
  

  # 仕訳ver2
  def make_entry2 generator, company, entry_day
    raise TypeError if !company.is_a?(Company)
    raise ArgumentError if company.id != self.company_setting.id
    raise TypeError if !entry_day.is_a?(Date)

    je_lines = []

    si_journals.each do |line|
      gl_amt, rate = get_gl_amount(company, entry_day, line.amount, currency)
      vat_code = if line.vat_code
                   line.vat_code # 旧コード. for compat.
                 else
                   x_ = line.account_title.get_real_vat( line.vat_id, 
                                                      company.country )
                   x_ ? x_.tax_code : "40" # for compat.
                 end

      je = JournalLine.new
      je.slip_date = entry_day
      je.entry_no = 40000 + id   # TODO:またもや決め打ち。fixme

      # 借方
      je.debtor.account_title = receivable_account
      je.debtor.division_code = nil
      je.debtor.partner_code = partner.zaimu_id

      # 貸方
      je.creditor.account_title = line.account_title
      je.creditor.division_code = line.division.division_code
      je.creditor.dim2_code = line.dim2.dim_code
      je.creditor.partner_code = (line.account_title.req_partner ? 
                                                      partner.zaimu_id : nil)
      je.creditor.vat_code = vat_code

      # 相手勘定
      je.debtor.contra_account = (je.creditor.partner_code ?
                       generator.convert_partner(je.creditor.account_title.account_code, je.creditor.partner_code) : 
                       je.creditor.account_title)
      je.creditor.contra_account = generator.convert_partner(
                                       je.debtor.account_title.account_code, 
                                       je.debtor.partner_code)

      # 金額など
      if currency.id != company.functional_currency.id
        je.sys_amount = gl_amt  # 円も固定する
        je.fc_currency_code = currency.code
        je.fc_amount = (BigDecimal.new(line.amount.to_s) / 
                                          (10 ** currency.exponent)).round(2)
      else
        je.sys_amount = gl_amt
        je.fc_currency_code = nil
        je.fc_amount = 0.0
      end

      je.due_date = due_day
      je.entry_worker_code = created_by.worker_code

      # 摘要
      je.remarks = line.remarks
      if currency.code != company.functional_currency.code
        # raise "#{currency.code.inspect}, #{rate.inspect}" # DEBUG
        je.remarks << "/" <<
                       format_amount(line.amount, currency.exponent) <<
                       currency.code << "@" << rate.to_s
      end

      je_lines << je
    end

    return je_lines
  end


  # 販売請求書 -> 仕訳
  # @param entry_day 仕訳日付. 締め後入力だと、販売日ではない
  def make_entries company, entry_day
    raise TypeError if !company.is_a?(Company)
    raise ArgumentError if company.id != self.company_setting.id
    raise TypeError if !entry_day.is_a?(Date)

    result = []
    si_journals.each {|line|
      amount, rate = get_gl_amount(company, entry_day, line.amount, currency)
      vat_code = if line.vat_code
                   line.vat_code # for compat.
                 else
                   x_ = line.account_title.get_real_vat( line.vat_id, 
                                                      company.country )
                   x_ ? x_.tax_code : "40" # for compat.
                 end

      e = {
        :entry_day => entry_day,
        :entry_no => 40000 + id,  # TODO:またもや決め打ち。fixme

        :dr_account_title => receivable_account,
        :dr_division => nil,
        :dr_dim2 => nil,
        :dr_partner_code => partner.zaimu_id,

        :cr_account_title => line.account_title,
        :cr_division => line.account_title.req_partner ? nil : line.division,
        :cr_dim2 => line.dim2,
        :cr_partner_code => line.account_title.req_partner ? partner.zaimu_id : nil,

        :amount => amount,
        :vat => vat_code,
        :due_day => due_day,
        :created_by => created_by
      }
      e[:remarks] = [line.division.name, partner.name, line.remarks].join('/')
      if currency.code != company.functional_currency.code
        # raise "#{currency.code.inspect}, #{rate.inspect}" # DEBUG
        e[:remarks] << " " <<
                       format_amount(line.amount, currency.exponent) <<
                       currency.code << "@" << rate.to_s
      end
      result << e
    }
    return result
  end
  

  # 仕訳にした機能通貨建ての金額, レート
  # @return [金額, 為替レート]
  def yen_amount_total
    entry_day = journal_day || [sell_day, company_setting.fixed_gl_day + 1].max
    return get_gl_amount(company_setting, entry_day, amount_total, currency)
  end


  # current_user が編集してもよいか
  # TODO: 会計システム連動後も編集できるようにしているが、考えどころ。
  def editable_by user
    state < Invoice::SIGNED_OFF || (user.has_role?(:keiri) && state < 30)
  end


  # 最初の明細行の内容
  # @return [String] 内容
  def first_detail_remarks
    si_journals.first.remarks
  end

end
