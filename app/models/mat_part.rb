# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 資材(Part)マスタ
class MatPart < ActiveRecord::Base
  has_many :mat_receivings

  validate :check_fa_flag
  validates_presence_of :part_no
  validates_uniqueness_of :part_no

  before_save :normalize_string
  
  MAT_TYPES = {"P" => "PART", "RM" => "RAW MATERIAL", "T" => "TOOL"}

  MAT_CLASSES = {"R" => "ROTABLE", "C" => "CONSUMABLE", "E" => "EXPENDABLE"}
  
  # for validate
  def check_fa_flag
    if mat_class != "R" && fixed_asset_flag
      errors.add(:fixed_asset_flag, "must non-FA when mat_class = 'C/E'")
    end
  end
  private :check_fa_flag


  # for before_save
  def normalize_string
    self.part_no = normalize_kc(part_no).upcase
    self.description = normalize_kc(description).upcase
  end
  private :normalize_string
end # class MatPart
