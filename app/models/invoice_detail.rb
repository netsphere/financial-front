# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 請求書の明細行
class InvoiceDetail < ActiveRecord::Base
  belongs_to :invoice
  belongs_to :account_title
  belongs_to :division
  belongs_to :dim2, :class_name => "Dimension"
  belongs_to :vat

  # for compat.
  VAT_NAMES = {0 => "(科目に従う)", 10 => "税込5%", 30 => "非課税", 40 => "―"}
  
  before_save :normalize_string

  validates_presence_of :account_title_id
  validates_presence_of :division_id
  validates_presence_of :dim2_id
  validate :check_fields
  
  # for before_save
  def normalize_string
    self.remarks = normalize_kc remarks
  end

  
  # for validate
  def check_fields
    if !dim2 || dim2.axis != 2
      errors.add "dim2_id", "軸が2ではありません。"
    end
    # errors.add "vat_code", "は0以外でなければなりません。" if vat_code == 0
  end


  class << self
    def vat_ary
      a = VAT_NAMES.to_a.sort {|x, y| x[0] <=> y[0]}
      a.collect {|k, v| [v, k]}
    end
  end # class << self
end
