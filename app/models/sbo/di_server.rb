# -*- coding:utf-8 -*- 

class DiServer
  attr_reader :server
  attr_reader :db_username
  attr_reader :db_password

  # ポート番号必要
  attr_reader :license_server
  attr_reader :end_point

  #attr_accessor :companies

  def initialize s, u, p, l, e #, companies = []
    @server = s
    @db_username = u
    @db_password = p
    @license_server = l
    @end_point = e

    #self.companies = companies
  end
end
