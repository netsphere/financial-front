# -*- coding: utf-8 -*-

require "erubis/helper"


class Sbo::XmlJeGenerator

  def initialize company
    raise TypeError if !company.is_a?(Company)

    @company = company
  end


  # @param i 100倍した金額
  def bigint2str i
    if i >= 0
      return sprintf("%d.%02d", i / 100, i % 100)
    else
      return sprintf("%d.%02d", i / 100, (-i) % 100)
    end
  end


  def date2str d
    if !d
      return nil
    else
      raise TypeError, "d is #{d}:#{d.class}" if !d.is_a?(Date)
   
      return sprintf("%04d%02d%02d", d.year, d.mon, d.day)
    end
  end


  def account_code ac, curr_code
    raise TypeError if !ac.is_a?(AccountTitle)
    raise TypeError if curr_code && !curr_code.is_a?(String)

    if [115, 116, 168, 176, 178, 241, 
                                  305, 306, 321, 325].include?(ac.account_code)
      return sprintf("%03d0_%s", ac.account_code, 
                     curr_code ? curr_code : @company.functional_currency.code)
    else
      sprintf("%03d0_%03d", ac.account_code, ac.suppl_code)
    end
  end


  # 取引先コードを変換する
  # @return [String] SBO取引先コード
  def convert_partner ac_code, partner_code
    if ac_code.is_a?(String)
      raise ArgumentError, "ac_code: #{ac_code}" if ac_code.length != 8
      ac_code = ac_code[0..2].to_i
    elsif !ac_code.is_a?(Integer)
      raise TypeError, "ac_code is #{ac_code}:#{ac_code.class}"
    end
    raise TypeError if partner_code && !partner_code.is_a?(Integer)

    if ac_code == 115 || ac_code == 116 || ac_code == 117 || 
        ac_code == 168 || ac_code == 176 ||
        ac_code == 333 
      # 販売請求書
      if !partner_code
        return "000000C"
      else
        return sprintf("%06dC", partner_code)
      end
    elsif ac_code == 305 || ac_code == 306 || ac_code == 321 ||
        ac_code == 325 || ac_code == 413 ||
        ac_code == 161 || ac_code == 162 ||
        ac_code == 178 || ac_code == 241 
      # 出金伝票
      # (購買)請求書: 前渡金
      if !partner_code
        return "000000V"
      else         
        return sprintf("%06dV", partner_code)
      end
    end

    return nil
  end


  # 社員コードを変換
  def convert_worker ac_code, worker_code
    if ac_code.is_a?(String)
      raise ArgumentError, "ac_code: #{ac_code}" if ac_code.length != 8
      ac_code = ac_code[0..2].to_i
    elsif !ac_code.is_a?(Integer)
      raise TypeError, "ac_code is #{ac_code}:#{ac_code.class}"
    end
    if worker_code
      raise TypeError if !worker_code.is_a?(String)
      raise ArgumentError if worker_code.length != 6
    end

    if ac_code == 170
      return sprintf("E%sC", worker_code)
    elsif ac_code == 323
      return sprintf("E%sV", worker_code)
    end

    return nil
  end


  # 2桁コードから長いコードに変換
  # @param vat_code 税コード. 今のところ10, 20, ... for compat.
  def convert_vat ac, vat_code, vat_rate = 0
    raise TypeError if !ac.is_a?(AccountTitle)
    if vat_code
      raise TypeError if !vat_code.is_a?(String)
      raise ArgumentError if vat_code.length != 2 && vat_code.length != 4
    end

    return nil, vat_rate if !vat_code
    
    ac_code = ac.account_code
    vat_code = vat_code.to_i

    # 資金, 債権・債務は付けない
    if ac_code == 101 || ac_code == 103 || ac_code == 107 ||
        ac_code == 115 || ac_code == 116 || 
        ac_code == 168 || ac_code == 176 ||
        ac_code == 178 || ac_code == 241 ||
        ac_code == 305 || ac_code == 306 || 
        ac_code == 321 || ac_code == 325 ||
        ac_code == 326 || ac_code == 327 || ac_code == 328 
      return nil, vat_rate
    end

    # 輸入消費税, 立替金, 前渡金, 前払費用, 前受金, 預り金
    if ac_code == 173 || ac_code == 175 || 
        ac_code == 161 || ac_code == 162 ||
        ac_code == 332 || ac_code == 333 || 
        ac_code == 335 || ac_code == 336 ||
        ac_code == 334
      return nil, vat_rate
    end
   
    return nil, vat_rate if vat_code == 40   # 不課税

    case ac.tax_side
    when 0
      return nil, vat_rate   # 不課税で固定

    when 1   # 課税売上
      begin
        result = Vat.where(tax_code: vat_code, tax_side: ac.tax_side).select('sbo_code, tax_rate').first
        return result.sbo_code, result.tax_rate.to_i
      rescue
        raise "internal error: unknown vat_code(sales side): "
      end
    when 2   # 課税仕入
      begin
        result = Vat.where(tax_code: vat_code, tax_side: ac.tax_side).select('sbo_code, tax_rate').first
        return result.sbo_code, result.tax_rate.to_i
      rescue
        raise "internal error: unknown vat_code(purchase side): #{vat_code}"
      end
    else
      raise "internal error: unknown tax_side"
    end
  end


  # row要素を作る
  def make_JournalEntries_Lines_row lineno, line, myside, tag #, other
    raise TypeError if !line.is_a?(JournalLine)
    raise TypeError if !myside.is_a?(DebtorCreditor)
    raise TypeError if !tag.is_a?(String)
    raise ArgumentError if tag != "Debit" && tag != "Credit"
    #raise TypeError if !other.is_a?(DebtorCreditor)

    # 金額部分 ################################################################
    post_amt = ""

    # 正規化(x100)したVAT金額 ...0に向かって丸める
    vat_amount_sys = 0
    vat_amount_fc = 0
    vat_rate = 0

    vat_code, vat_rate = convert_vat(myside.account_title, myside.vat_code)

    if vat_rate != 0
      # 金額はマイナスのこともあることに注意
      # 0に向かって丸める
      vat_amount_sys = (line.sys_amount * vat_rate / (100 + vat_rate)).to_i * 100
      vat_amount_fc = (line.fc_amount * 100 * vat_rate / (100 + vat_rate)).to_i
    end

    if vat_amount_sys != 0 || vat_amount_fc != 0.0
      # VatAmount要素は, AutoVAT="Yes"のときのみ書ける.
      # https://scn.sap.com/thread/817055

      base_amt_sys = line.sys_amount * 100 - vat_amount_sys
      base_amt_fc = line.fc_amount * 100 - vat_amount_fc

      if line.fc_currency_code  #fc_amount != 0.0
        # 外貨建てかつ消費税課税取引の場合, 
        # 本体価格 (外貨, 機能通貨) と消費税額 (機能通貨) に分けて投入する.
        # AutoVAT="Yes"でも通る.
        # 現行の制限事項:
        #     - XML経由では GrossValFc フィールドに書き込めない
        #     - 外貨建ての VAT のフィールドがない.
        # See ticket #298: 外貨費用が0.00になってしまう
        post_amt = "  <FC#{tag}>#{bigint2str(base_amt_fc)}</FC#{tag}> <!-- #{line.fc_amount} -->\n" +
                   "  <FCCurrency>#{line.fc_currency_code}</FCCurrency>\n" +
                   "  <#{tag}>#{bigint2str(base_amt_sys)}</#{tag}>\n" +
                   "  <VatAmount>#{bigint2str(vat_amount_sys)}</VatAmount>\n"
                   # "  <GrossValFc>#{line.fc_amount}</GrossValFc>\n" # +

        if myside.account_title.tax_side == 1 && vat_rate != 0 # 課税売上
          # workaround:
          # 課税仕入側は入るのに, 課税売上側は, 次のエラーになる.
          #   <ErrCode>-5002</ErrCode>
          #   <ErrMsg>You cannot edit the VAT Amount field  [JDT1.VatAmount]
          # 振替伝票を手打ちで作った場合の金額と違いがなく、原因不明。
          # => 外貨金額は0.00でやむなしとする。
          post_amt << "  <GrossValue>#{line.sys_amount}</GrossValue>\n"
        end
      else
        post_amt = "  <#{tag}>#{bigint2str(base_amt_sys)}</#{tag}>\n" +
                   "  <GrossValue>#{line.sys_amount}</GrossValue>\n" 
      end
    else
      # VAT金額なし
      post_amt = "  <#{tag}>#{line.sys_amount}</#{tag}>\n"
      if line.fc_currency_code   #fc_amount != 0.0
        post_amt += "  <FC#{tag}>#{line.fc_amount}</FC#{tag}>\n"
        post_amt += "  <FCCurrency>#{line.fc_currency_code}</FCCurrency>\n"
      end
    end

    if vat_code
      post_amt += "  <TaxGroup>#{vat_code}</TaxGroup>\n"
    else
      post_amt += "  <TaxGroup nil=\"true\"></TaxGroup>\n"
    end

    # 勘定科目 ################################################################
    # 統制勘定の有無で, <ShortName> の書き方が変わる
    post_ac = ""

    ac_code = account_code(myside.account_title, line.fc_currency_code)
    if !myside.partner_code && !myside.worker_code
      # 統制勘定ではない
      post_ac = "  <AccountCode>#{ac_code}</AccountCode>\n" +
                "  <ShortName>#{ac_code}</ShortName>\n"

      if myside.division_code
        raise ArgumentError if !myside.dim2_code
        post_ac += <<EOF
  <ProjectCode>#{myside.division_code + myside.dim2_code}</ProjectCode>
  <CostingCode>#{myside.division_code + myside.dim2_code}</CostingCode>
  <CostingCode2>#{myside.division_code}</CostingCode2>
  <CostingCode3>#{myside.dim2_code}</CostingCode3>
EOF
      else
        # 必須: ダミーで埋める
        fallback = @company.company_setting.fallback_division
        post_ac += <<EOF
  <ProjectCode>#{sprintf("%05d", fallback.division_code) + fallback.default_dim2.dim_code}</ProjectCode>
  <CostingCode>#{sprintf("%05d", fallback.division_code) + fallback.default_dim2.dim_code}</CostingCode>
  <CostingCode2>#{sprintf("%05d", fallback.division_code)}</CostingCode2>
  <CostingCode3>#{fallback.default_dim2.dim_code}</CostingCode3>
EOF
      end
    else
      # 統制勘定
      if myside.partner_code
        post_ac = <<EOF
  <AccountCode>#{ac_code}</AccountCode>
  <ShortName>#{convert_partner(ac_code, myside.partner_code)}</ShortName>
EOF
      elsif myside.worker_code
        post_ac = <<EOF
  <AccountCode>#{ac_code}</AccountCode>
  <ShortName>#{convert_worker(ac_code, myside.worker_code)}</ShortName>
EOF
      else
        raise "internal error"
      end

      # 支払保留
      if myside.payment_block
        post_ac += "  <PaymentBlock>tYES</PaymentBlock>\n"
      end
    end

    # 相手勘定 ###############################################################
    # 単に相手側の ShortName を入れればよい
    if myside.contra_account.is_a?(AccountTitle)
      post_ac += "  <ContraAccount>#{account_code(myside.contra_account,
                                     line.fc_currency_code)}</ContraAccount>\n"
    elsif myside.contra_account.is_a?(String)
      if myside.contra_account.length < 7 || myside.contra_account.length > 8
        raise ArgumentError, "contra: #{myside.contra_account}"
      end
      post_ac += "  <ContraAccount>#{myside.contra_account}</ContraAccount>\n"
    else
      #raise ArgumentError, "contra missing"
    end
=begin
    if !other.partner_code
      if other.account_code != "3999_000"
        post_ac += "  <ContraAccount>#{other.account_code}</ContraAccount>\n"
      end
    else
      post_ac += "  <ContraAccount>#{other.partner_code}</ContraAccount>\n"
    end
=end

    post_data = "    <row>\n" + post_amt + post_ac + <<EOF
      <Line_ID>#{lineno}</Line_ID>
      <LineMemo>#{Erubis::XmlHelper.escape_xml(line.remarks.split(//u)[0..49].join())}</LineMemo>
    </row>
EOF

    return post_data
  end


  # @param je_lines [Array<JournalLine>] 伝票1枚分の仕訳リスト
  def make_jebo je_idx, je_lines
    raise TypeError if !je_lines.is_a?(Array)
    return "" if je_lines.empty?

    auto_vat = true

    # ReferenceDate 転記日付
    # TaxDate 伝票日付
    post_data = <<EOF
<BO> <!-- #{je_idx} -->
  <AdmInfo>
    <Version>2</Version>
    <Object>30</Object>
  </AdmInfo>
  <JournalEntries>
    <row>
      <ReferenceDate>#{date2str(je_lines.first.slip_date)}</ReferenceDate>
      <TaxDate>#{date2str(je_lines.first.slip_date)}</TaxDate>
      <DueDate>#{date2str(je_lines.first.due_date)}</DueDate>
      <AutoVAT>#{auto_vat ? "tYES" : "tNO"}</AutoVAT>
      <Reference3>#{je_lines.first.entry_worker_code}</Reference3>
    </row>
  </JournalEntries>
  <JournalEntries_Lines>
EOF
    lineno = 0
    dr_amt = 0
    cr_amt = 0
    je_lines.each do |line|
      next if line.sys_amount == 0 && line.fc_amount == 0.0
      
      if line.debtor.account_title #.account_code != "3999_000"
        post_data += make_JournalEntries_Lines_row lineno, line, 
                                         line.debtor, "Debit"
        lineno += 1
        dr_amt += line.sys_amount
      end
      if line.creditor.account_title #.account_code != "3999_000"
        post_data += make_JournalEntries_Lines_row lineno, line,
                                         line.creditor, "Credit"
        lineno += 1
        cr_amt += line.sys_amount
      end
    end
    
    return "" if lineno == 0   # 0 transactionは不可

    if dr_amt != cr_amt
      raise "貸借不一致: dr_amt = #{dr_amt}, cr_amt = #{cr_amt}"

      # DEBUG
      #post_data += "貸借不一致: dr_amt = #{dr_amt}, cr_amt = #{cr_amt}"
    end

    post_data += <<EOF
  </JournalEntries_Lines>
  <PrimaryFormItems>
  </PrimaryFormItems>
  <WithholdingTaxData>
  </WithholdingTaxData>
</BO>
EOF

    return post_data
  end
end
