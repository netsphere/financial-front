
class SiLog < ActiveRecord::Base
  belongs_to :sales_invoice
  belongs_to :done_by, :class_name => 'User', :foreign_key => 'done_by'
  before_save :normalize_string
  
  MODIFY = 1
  SIGN_OFF = 2
  PRINT_INVOICE = 4
  KIND_NAMES = [nil, "modified", "signed-off", "cancelled", "printed invoice"]

  def normalize_string
    self.remarks = normalize_kc remarks
  end
end
