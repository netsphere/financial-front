# -*- coding:utf-8 -*-

# 小口グループ
class PettyGroup < ActiveRecord::Base
  belongs_to :company

  validates_presence_of :group_code
  validates_uniqueness_of :group_code

  validates_presence_of :name
  #validates_presence_of :description

  before_save :normalize_string

  # for before_save
  def normalize_string
    self.group_code = normalize_kc(group_code)
    self.name = normalize_kc(name)
  end
  private :normalize_string
end
