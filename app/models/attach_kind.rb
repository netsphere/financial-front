# -*- coding:utf-8 -*-

# ファイル種別
class AttachKind < ActiveRecord::Base
  validates_presence_of :name
end
