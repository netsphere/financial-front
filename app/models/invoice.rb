# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require "ostruct" # OpenStruct
#require "sbo/xml_je_generator"


# (購買) 請求書
class Invoice < Document
  include CommonScope

  belongs_to :company_setting, :class_name => "Company"
  belongs_to :partner
  belongs_to :division
  belongs_to :bank_account
  belongs_to :en_bank_account
  belongs_to :currency
  belongs_to :created_by, :foreign_key => 'created_by', :class_name => 'User'

  has_many :invoice_details, :dependent => :destroy
  has_many :attach_file_refs
  has_many :attach_files, :through => :attach_file_refs
  has_many :imports_invoices
  has_many :import_declarations, :through => :imports_invoices
  has_many :mat_receivings

  #購買請求書検索一覧表示用scope
  scope :with_invoice_detail, -> { joins(%{LEFT JOIN invoice_details on invoices.id = invoice_details.invoice_id}) }
  scope :select_invoice_list, -> { select("invoices.id, partner_id, buy_day, amount_total, currency_id, state, invoice_no, due_day ")}
  scope :invoice_no, ->(invoice_no) { where("COALESCE(invoice_no,'') = ?", invoice_no) if invoice_no.present? }
  scope :remarks, ->(remarks) { where("COALESCE(remarks,'') LIKE ?", remarks) if remarks.present? }
  scope :group_list_element, -> { group("invoices.id, partner_id, buy_day, amount_total, invoice_no, currency_id")}
  scope :by_buy_day, -> { order(buy_day: :desc)}
  scope :by_invoice_id, -> { order("invoices.id")}

  SIGNED_OFF = 10
  
  #  paginate定数
  PAGINATE_MAX = 100
  PAGINATE_LIST = 25

  before_save :normalize_string

  validate :check_fields
  validate :check_fields_on_create, :on => :create
  validate :check_fields_on_update, :on => :update

  
  # for before_save
  def normalize_string
    self.invoice_no = normalize_kc invoice_no
  end


  # 請求金額の合計を更新。save は呼び出し側ですること。
  # 正規化された金額, 輸入消費税含む。
  def update_amount_total
    a = InvoiceDetail.where('invoice_id = ?', id) \
                     .select('sum(amount) AS sum_amount').order("").first
    
    self.amount_total = a.sum_amount.to_i + amount_of_import_vat +
                        amount_of_mat_receivings

    return self
  end


  # 関連づけられている輸入消費税（地方税含む。）の合計金額
  # @return 正規化した金額
  def amount_of_import_vat
    v = ImportDeclaration.find :first,
             :select => 'sum(consumption_tax) AS sum_vat, sum(vat_local) AS sum_local_vat',
             :joins => 'LEFT JOIN imports_invoices ON import_declarations.id = imports_invoices.import_declaration_id',
             :conditions => ['imports_invoices.invoice_id = ?', id],
             :order => ""
    
    return v ? v.sum_vat.to_i + v.sum_local_vat.to_i : 0
  end


  # 誤差が蓄積されるので, 正規化は最後におこなう
  # @return 正規化した金額
  def amount_of_mat_receivings
    # 1件ずつ見る
    ttl = 0.0
    v = MatReceiving.where("invoice_id = ?", self.id).each do |mat|
      ttl += mat.total_amount + mat.tax_amount

      # charges
      c = MatReceivingCharge.select("SUM(total_amount) AS sum_amount, SUM(tax_amount) AS sum_tax") \
                            .where("mat_receiving_id = ?", mat.id).order("")
      if !c.empty?
        ttl += c.first.sum_amount.to_f + c.first.sum_tax.to_f
      end
    end

    return nominal_amount(ttl, currency.exponent)
  end


  # 作成, 更新共通
  # for validate
  def check_fields
    # errors.add_to_base('明細が1行もありません。') if invoice_details.empty? && import_declarations.empty?
    
    if due_day.is_a?(Date) && buy_day.is_a?(Date) && (due_day < buy_day)
      errors.add('buy_day', '請求締め日は支払日より前にしてください')
    end
    errors.add('je_number', '半角英数字以外は登録できません。') if je_number.to_s.match(/^\w+$|^$/).nil?

  end


  # for validate
  def check_fields_on_create
    if invoice_no
      a = Invoice.find :first,
               :conditions => ['partner_id = ? AND invoice_no = ?',
                               partner_id, invoice_no]
      errors.add('invoice_no', '請求書番号が重複しています。') if a
    end
  end

  
  # for validate
  def check_fields_on_update
    if invoice_no
      a = Invoice.find :first,
               :conditions => ['partner_id = ? AND invoice_no = ? AND id <> ?',
                               partner_id, invoice_no, id]
      errors.add('invoice_no', '請求書番号が重複しています。') if a
    end
  end


  # 購買請求書 -> 仕訳
  # 仕訳確認画面から呼び出される.
  # @return [Array of Hash] 仕訳行の配列
  def make_entries company, entry_day
    raise TypeError if !company.is_a?(Company)
    raise ArgumentError if company.id != self.company_setting.id
    raise TypeError if !entry_day.is_a?(Date)

    result = []
    invoice_details.each {|detail|
      amount, rate = get_gl_amount company, entry_day, detail.amount, currency
      vat_code = if detail.vat_code
                   detail.vat_code # for compat.
                 else
                   x_ = detail.account_title.get_real_vat( detail.vat_id, 
                                                      company.country )
                   x_ ? x_.tax_code : "40" # for compat.
                 end

      line = {
        :entry_day => entry_day,
        :entry_no => 10000 + id,  # TODO:決め打ち。fixme
        
        # 借方
        :dr_account_title => detail.account_title,
        :dr_division => detail.division,
        :dr_dim2 => detail.dim2,
        :dr_partner_code => (detail.account_title.req_partner ? 
                                                     partner.zaimu_id : nil),

        # 貸方
        :cr_account_title => detail.account_title.payable,
        :cr_division => nil,
        :cr_dim2 => nil,
        :cr_partner_code => (bank_account_id ? 
                                   bank_account.zaimu_ac : partner.zaimu_id),

        :amount => amount,
        :vat => vat_code,
        :due_day => (bank_account ? due_day : nil),
        :created_by => created_by,
      }
      # 摘要
      if currency.id == company.functional_currency.id
        line[:remarks] = [ # detail.division.name, partner.name,
                            detail.remarks].join('/')
      else
        line[:remarks] = [ # detail.division.name, partner.name,
          "#{detail.remarks} #{format_amount(detail.amount, currency.exponent)}#{currency.code}@#{rate}"].join('/')
      end
      line[:remarks] << "/稟議#" + approval if !approval.blank?

      result << line
    }

    # 材料仕入 #############
    if !mat_receivings.empty?
      generator = Sbo::XmlJeGenerator.new company  # TODO: fixme!!!!

      jes = MatReceiving.make_entry2 self, generator, entry_day
      jes.each do |je|
        line = {
          :entry_day => entry_day,
          :entry_no => 10000 + id,  # TODO:決め打ち。fixme
          :dr_account_title => je.debtor.account_title,
          :cr_account_title => je.creditor.account_title,
          :cr_partner_code => je.creditor.partner_code,
          :amount => je.sys_amount,
          :vat => je.debtor.vat_code,
          :due_day => je.due_date,
          :remarks => je.remarks
        }
        result << line
      end
    end

    # 輸入消費税 ###########
    vat_payment_ac = AccountTitle.vat_payment
    import_declarations.each {|imp|
      line = {
        :entry_day => entry_day,
        :entry_no => 10000 + id,  # TODO:決め打ち。fixme

        # 借方
        :dr_account_title => vat_payment_ac,
        :dr_division => nil,
        :dr_partner_code => nil,

        # 貸方
        :cr_account_title => AccountTitle.find(vat_payment_ac.payable_id),
        :cr_division => nil,
        :cr_partner_code => (bank_account ? bank_account.zaimu_ac : partner.zaimu_id),

        :amount => imp.consumption_tax + imp.vat_local,
        :vat => 40,
        :due_day => (bank_account ? due_day : nil),
        :created_by => created_by,
        :remarks => "輸入消費税 ID#" + [imp.format_declaration_number, imp.permit_date, imp.exporter_name].join('/')
      }
      result << line
    }
    return result
  end


  # 仕訳ver2
  # @return [Array of JournalLine]
  def make_entry2 generator, company, entry_day
    raise TypeError if !company.is_a?(Company)
    raise ArgumentError if company.id != self.company_setting_id
    raise TypeError if !entry_day.is_a?(Date)

    je_lines = []

    # サービス行
    self.invoice_details.each do |detail|
      gl_amount, rate = get_gl_amount company, entry_day, detail.amount, 
                                      currency

      vat_code = if detail.vat_code
                   detail.vat_code # 旧コード. for compat.
                 else
                   x_ = detail.account_title.get_real_vat( detail.vat_id, 
                                                      company.country )
                   x_ ? x_.tax_code : "40" # for compat.
                 end

      je = JournalLine.new
      je.slip_date = entry_day
      je.entry_no = 10000 + self.id

      # 借方
      je.debtor.account_title  = detail.account_title
      je.debtor.division_code = detail.division.division_code
      je.debtor.dim2_code = detail.dim2 ? detail.dim2.dim_code : 
                                          detail.division.default_dim2.dim_code
      je.debtor.partner_code = (detail.account_title.req_partner ?
                                                      partner.zaimu_id : nil)
      je.debtor.vat_code = vat_code

      # 貸方
      je.creditor.account_title = detail.account_title.payable
      je.creditor.division_code = nil
      je.creditor.partner_code = 
                  (bank_account_id ? bank_account.zaimu_ac : partner.zaimu_id)
      je.creditor.payment_block = (self.bank_account_id ? false : true)
      je.creditor.vat_code = nil

      # 相手勘定
      # 前渡金/未払金 という仕訳の場合, 貸方の相手勘定は"前渡金"ではなく取引先しか入らない
      if !detail.account_title.payable
        raise "payable missing: #{detail.account_title.account_code}/#{detail.account_title.suppl_code}"
      end
      je.debtor.contra_account = generator.convert_partner(
                                   detail.account_title.payable.account_code,
                                   je.creditor.partner_code)
      je.creditor.contra_account = (je.debtor.partner_code ? 
                         generator.convert_partner(je.debtor.account_title.account_code, je.debtor.partner_code) : 
                         je.debtor.account_title)

      # 金額など
      je.sys_amount = gl_amount # 外貨建てのときも, 円を設定
      if currency.id != company.functional_currency.id
        je.fc_currency_code = currency.code
        je.fc_amount = (BigDecimal.new(detail.amount.to_s) / 
                                           (10 ** currency.exponent)).round(2)
      else
        je.fc_currency_code = nil
        je.fc_amount = 0.0
      end

      je.due_date = self.due_day
      je.entry_worker_code = created_by ? created_by.worker_code : nil

      # 摘要
      if currency.id == company.functional_currency.id
        je.remarks = detail.remarks
      else
        je.remarks = [ 
          detail.remarks,
          "#{format_amount(detail.amount, currency.exponent)}#{currency.code}@#{rate}"].join('/')
      end
      je.remarks << "/稟議#" + approval if !approval.blank?

      je_lines << je
    end # of invoice_details.each

    # 材料仕入
    if !mat_receivings.empty?
      je_lines += MatReceiving.make_entry2 self, generator, entry_day
    end

    # 輸入消費税
    if !import_declarations.empty?
      je_lines += ImportDeclaration.make_entry2 self, generator, entry_day
    end

    return je_lines
  end


  # 未払残額 (合計, 輸入消費税含む。)
  # 出金伝票が未承認のときは, 未払い残額は減らさない
  # @param [Hash] options   オプション
  # @option options [PaymentSlip] :except_payment  出金額から除外する支払い
  def remain_amount options = {}
    if options[:except_payment]
      raise TypeError if !options[:except_payment].is_a?(PaymentSlip)

      cond = [ <<EOF, self.id, options[:except_payment].id ]
(payment_slips.state = 10 OR payment_slips.state = 20) AND invoice_id = ? AND 
payment_slips.id <> ?
EOF
    else
      cond = ["(payment_slips.state = 10 OR payment_slips.state = 20) AND invoice_id = ?", self.id]
    end

    # 支払済の金額
    r = Payment.where(*cond) \
            .joins("JOIN payment_slips ON payments.payment_slip_id = payment_slips.id") \
            .select("sum(payments.amount) AS amount_payed") \
            .order("") \
            .first

    return r ? amount_total - r.amount_payed.to_i : amount_total
  end


  def add_remain detail_ary, payable_ac, add_amount
    if !payable_ac.is_a?(AccountTitle)
      raise TypeError, "payable_ac = #{payable_ac.class}" 
    end

    f = false
    detail_ary.each do |dtl|
      raise RuntimeError, "internal error" if !dtl.payable_id

      if dtl.payable_id == payable_ac.id
        dtl.sum_amount = dtl.sum_amount.to_i + add_amount
        f = true
        break
      end
    end
    
    if !f
      detail_ary << (OpenStruct.new :sum_amount => add_amount, 
                                     :payable_id => payable_ac.id)
    end
  end
  private :add_remain


  # 勘定科目ごとの未払残額
  # @return [Hash] {勘定科目id => 金額}
  def remain_ac_amounts
    inv_amounts = InvoiceDetail.find :all,
            :select => 'sum(amount) AS sum_amount, drac.payable_id',
            :joins => "LEFT JOIN account_titles AS drac ON invoice_details.account_title_id = drac.id",
            :conditions => ['invoice_id = ?', id],
            :group => "drac.payable_id"

    # 輸入消費税
    vat_amount = amount_of_import_vat.to_i
    if vat_amount != 0
      add_remain inv_amounts, AccountTitle.vat_payment.payable, vat_amount
    end

    # 材料仕入
    mat_amount = amount_of_mat_receivings
    if mat_amount != 0
      add_remain( inv_amounts, 
              AccountTitle.where("account_code = ?", 305).first, # TODO: FIX!
              mat_amount )
    end

    result = {}
    inv_amounts.each do |dtl|
      payed = Payment.where("(payment_slips.state = 10 OR payment_slips.state = 20) AND invoice_id = ? AND account_title_id = ?", id, dtl.payable_id) \
                .joins("LEFT JOIN payment_slips ON payments.payment_slip_id = payment_slips.id") \
                .select("sum(payments.amount) AS amount_payed") \
                .order("") \
                .first

      if payed && payed.amount_payed
        result[dtl.payable_id] = dtl.sum_amount.to_i - payed.amount_payed.to_i
      else
        result[dtl.payable_id] = dtl.sum_amount.to_i
      end
    end

    return result
  end


  # 最初の明細行の内容
  # @return [String] 内容
  def first_detail_remarks
    d = invoice_details.first
    if d
      return d.remarks
    else
      d = import_declarations.first
      if d
        return "#{d.exporter_name} ID##{d.declaration_number}"
      else
        return "(明細なし)"
      end
    end
  end


  # 仕訳で利用する機能通貨建ての金額、そのレート
  # payment_slip で利用される
  def amount_total_local_and_rate
    entry_day = journal_day || [buy_day, company_setting.fixed_gl_day + 1].max
    return get_gl_amount(company_setting, entry_day, amount_total, currency)
  end

end # class Invoice
