# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 出金伝票と統合
if SchemaMigration::VERSION < "20140501025048"


# 入金伝票
class ReceivingSlip < Document

  belongs_to :our_bank
  has_many :receivings

  SIGNED_OFF = 10


  # 仕訳ver2
  def make_entry2 generator, company, entry_day
    raise TypeError if !company.is_a?(Company)
    raise ArgumentError if company.id != self.our_bank.company_setting_id
    raise TypeError if !entry_day.is_a?(Date)

    return [] # TODO: impl.
  end
  

  # 仕訳を生成
  # @return [Array] 仕訳の配列
  #
  # 請求書の通貨と入金口座の通貨が同じ
  #   (1) 通貨=JPY || 債権の発生月と回収月が同じ => 債権が何行でもこの形.
  #     預金 / 売掛金 100
  #   (2) 次月以降に回収
  #     000  / 売掛金    100
  #     預金 / 000       110
  #     000  / 為替差損益  10
  # 入金口座が円で請求書がそれ以外 (入金日レートは為替レートマスタのと違うことに注意)
  #     000  / 売掛金    100
  #     000  / 売掛金    200   
  #     預金 / 000       310
  #     000  / 為替差損益  10
  def make_entries company, entry_day
    raise TypeError if !company.is_a?(Company)
    raise ArgumentError if company.id != self.our_bank.company_setting_id
    raise TypeError if !entry_day.is_a?(Date)

    return [] if receivings.size == 0 

    result = []
    #inv_currency = receivings.first.sales_invoice.currency
    inv_amount_total = ( receivings.inject(0) do |sum, r| sum + r.amount end )

    cash_amount_total, cash_rate = 
                          get_gl_amount( company, entry_day, self.amount, 
                                         our_bank.currency )
    # 機能通貨での金額
    #journaled_cash = BigDecimal.new("0.0")

    receivings.each do |rcv|
      if rcv.amount != 0
        # 本体価格
        e, rcv_rate = rcv.make_partial_entry company, entry_day

        if rcv.currency == our_bank.currency && rcv_rate == cash_rate
          # 1行の伝票
          e.merge!( {
            :entry_day => entry_day, 
            :entry_no => 50000 + id,
      
            :dr_account_title => our_bank.account_title,
            :dr_division => nil, 
            :dr_partner_code => nil,

            :due_day => nil,
            :created_by => User.find(created_by),
          } )
          if rcv.currency.code != company.functional_currency.code
            e[:remarks] << " " <<
                   format_amount(rcv.amount, rcv.currency.exponent) <<
                   rcv.currency.code << "@" << rcv_rate.to_s 
          end
          result << e

          #journaled_cash += e[:amount]
        else
          # 通貨が違う, or 債権発生月が違う
          remarks = e[:remarks].dup
          e.merge!( {
            :entry_day => entry_day, 
            :entry_no => 50000 + id,
      
            :dr_account_title => nil,
            :dr_division => nil, 
            :dr_partner_code => nil,

            :due_day => nil,
            :created_by => User.find(created_by),
          } )
          if rcv.currency.code != company.functional_currency.code
            e[:remarks] << " " <<
                   format_amount(rcv.amount, rcv.currency.exponent) <<
                   rcv.currency.code << "@" << rcv_rate.to_s 

            remarks << " " << 
                   format_amount(rcv.amount, rcv.currency.exponent) <<
                   rcv.currency.code
          end
          result << e

          acash = ( BigDecimal.new(cash_amount_total.to_s) * 
                    BigDecimal.new(rcv.amount.to_s) / inv_amount_total + 
                    BigDecimal.new("0.5") ).floor
          e = {
            :entry_day => entry_day, :entry_no => 50000 + id,
      
            :dr_account_title => our_bank.account_title,
            :dr_division => nil, :dr_partner_code => nil,
      
            :cr_account_title => nil,
            :cr_division => nil, :cr_partner_code => nil,
      
            :amount => acash,
            :vat => nil, :due_day => nil,
            :created_by => User.find(created_by),

            :remarks => remarks.dup
          }
          if our_bank.currency.code != company.functional_currency.code
            e[:remarks] << "; 入金額" <<
                 format_amount(self.amount, our_bank.currency.exponent) <<
                 our_bank.currency.code 
          end
          result << e

          #journaled_cash += acash

          # 為替差損益
          e = {
            :entry_day => entry_day, :entry_no => 50000 + id,

            :dr_account_title => nil,
            :dr_division => nil, :dr_partner_code => nil,

            :cr_account_title => AccountTitle.currency_gain,
            :cr_division => company.bank_division,
            :cr_partner_code => nil,

            :amount => acash - ((rcv.amount.to_f / 10 ** rcv.currency.exponent) * rcv_rate + 0.5).floor,
            :vat => 40, :due_day => nil,
            :created_by => User.find(created_by),
        
            :remarks => remarks + " 為替差"
          }
          result << e
        end
      end

      # 手数料当方負担
      if rcv.charge != 0
        e = {
          :entry_day => entry_day,
          :entry_no => 50000 + id,

          :dr_account_title => AccountTitle.bank_charge,
          :dr_division => company.bank_division,
          :dr_partner_code => nil,

          :cr_account_title => our_bank.account_title,
          :cr_division => nil,
          :cr_partner_code => nil,

          :amount => rcv.charge,
          :vat => 10,
          :due_day => nil,
          :created_by => User.find(created_by)
        }
        e[:remarks] = [company.bank_division.name, inv.partner.name, "当方負担手数料"].join('/')
        result << e
      end
    end # loop of 債権
    
    return result
  end

end




end # obsolete
