# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 銀行の支店
class BankBranch < ActiveRecord::Base
  belongs_to :bank

  validates_presence_of :branch_code
  
  validates_presence_of :name
  validates_presence_of :kana_name
  validate :normalize_and_check_fields


  # for validate
  def normalize_and_check_fields
    if kana_name
      self.kana_name = Bank.zen2han( normalize_kc(kana_name).upcase )
      if /[^ A-Z0-9ｦｱ-ﾝﾞﾟ\\.,｢｣\(\)\/-]/ =~ kana_name
        errors.add :kana_name, "使用不可の文字が含まれます"
      end
    end
  end

  
  def self.find_by_code(bank_code, branch_code)
    raise TypeError if !bank_code.is_a?(Integer)
    raise TypeError if !branch_code.is_a?(Integer)
    find(:first, :conditions => ['bank_id = ? AND branch_code = ?', bank_code, branch_code])
  end
end
