# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 勘定科目
class AccountTitle < ActiveRecord::Base
  belongs_to :receivable, foreign_key: 'receivable_id',
                          class_name: 'AccountTitle',
                          optional: true
  belongs_to :payable, foreign_key: 'payable_id',
                       class_name: 'AccountTitle',
                       optional: true
  belongs_to :created_by, :foreign_key => 'create_user_id',
                          :class_name => 'User'
  belongs_to :updated_by, :foreign_key => 'update_user_id',
                          :class_name => 'User'

  # 集計先
  has_many :account_title_trees, :foreign_key => 'parent_id'
  has_many :children, :through => :account_title_trees

  # for compat.
  #VAT_NAMES = {10 => '税込5%', 20 => '外税5%', 30 => '非課税', 40 => '―'}

  KIND_NAMES = {
    1 => "売掛金・未収入金 [販売請求書]",
    2 => "買掛金・未払金 [購買請求書]",
    3 => "仮払消費税 [輸入報告]", 
    0 => "その他"}

  validates_presence_of :account_code
  validates_presence_of :suppl_code
  validates_uniqueness_of :suppl_code, :scope => :account_code
  validates_presence_of :main_name
  validates_presence_of :name
  validates_presence_of :kana_name

  validate :check_fields

  #`before_save()` では遅い
  before_validation :normalize_string


  class << self
    # 売掛金・未収入金科目の一覧
    def receivable_candidates
      AccountTitle.where('(end_day IS NULL) AND kind = 1')
                  .order('account_code, suppl_code' )
    end

    # 買掛金・未払金科目の一覧
    def payable_candidates
      AccountTitle.where('(end_day IS NULL) AND kind = 2')
                  .order('account_code, suppl_code')
    end
  
    # 送金手数料
    def bank_charge
      GlobalSetting.first.bank_charge_account_title
    end
  
    def currency_gain
      GlobalSetting.first.currency_gain_account_title
    end
  

    # 仮払消費税
    # @return [AccountTitle] 仮払消費税(JP)の勘定科目
    def vat_payment
      GlobalSetting.first.vat_payment_account_title
    end


    def vat_ary(ary = nil)
      a = VAT_NAMES.to_a.sort {|x, y| x[0] <=> y[0]}
      r = []
      a.each {|k, v|
        r << [v, k] if !ary || ary.include?(k)
      }
      return r
    end

  end # class << self


  def name2
    return "#{account_code} #{name}"
  end
  
  # 自分自身と祖先は禁止
  def self_and_ancestors
    ret = last = [self.id]
    while !last.empty?
      last = AccountTitleTree.find(:all,
                                   :conditions => ['child_id IN (?)', last]
             ).collect {|r| r.parent_id}
      ret += last
    end
    return ret
  end

  # 直接の子供と子孫が被ったら警告
  # 直接の子供ではない子孫を返す
  def descendant
    ret = []
    last = AccountTitleTree.find(:all,
               :conditions => ['parent_id = ?', self.id]).collect {|r| r.child_id}
    while !last.empty?
      last = AccountTitleTree.find(:all,
               :conditions => ['parent_id IN (?)', last]).collect {|r| r.child_id}
      ret += last
    end
    return ret
  end


  # @param vat_id_or_zero  -1       ... 科目に従う
  #                        0 or nil ... nil
  #                        >= 1     ... vat id
  # @param country 2文字コード ex) "JP"
  def get_real_vat vat_id_or_zero, country
    raise TypeError if !country.is_a?(String) || country.length != 2

    if vat_id_or_zero.to_i >= 1
      Vat.find vat_id_or_zero
    elsif vat_id_or_zero.to_i == 0
      nil
    else
      Vat.find :first, 
             :conditions => ["tax_side = ? AND tax_group = ? AND country = ?",
                             self.tax_side, self.default_tax_type, country]
    end
  end


private

  # For `validate()`
  def check_fields
    if sign != 1 && sign != -1
      errors.add 'sign', '内部エラー: 符号は+1または-1に限る'
    end
  end

  # For `before_validation()`
  def normalize_string
    self.main_name   = main_name.unicode_normalize(:nfkc)
    self.name        = name.unicode_normalize(:nfkc)
    self.kana_name   = kana_name.unicode_normalize(:nfkc).gsub(/[ 　\t\r\n・]/, '')
    self.description = description.unicode_normalize(:nfc)

    # 消費税不可
    self.default_tax_type = 0 if tax_side == 0
  end
 
end
