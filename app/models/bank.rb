# -*- coding: utf-8 -*-

# 銀行
class Bank < ActiveRecord::Base
  validates_presence_of :id  # 金融機関コード
  validates_presence_of :name
  validates_presence_of :kana_name

  has_many :bank_branches

  validate :normalize_and_check_fields
  
  ZEN = %w(ア イ ウ エ オ ァ ィ ゥ ェ ォ
     カ キ ク ケ コ ガ ギ グ ゲ ゴ ヵ ヶ
     サ シ ス セ ソ ザ ジ ズ ゼ ゾ
     タ チ ツ テ ト ダ ヂ ヅ デ ド ッ
     ナ ニ ヌ ネ ノ
     ハ ヒ フ ヘ ホ バ ビ ブ ベ ボ パ ピ プ ペ ポ
     マ ミ ム メ モ
     ヤ ユ ヨ ャ ュ ョ
     ラ リ ル レ ロ
     ワ ヲ ン ヮ ヰ ヱ
     ヴ ・ ー)
  HAN = %w(ｱ ｲ ｳ ｴ ｵ ｱ  ｲ  ｳ  ｴ  ｵ
     ｶ ｷ ｸ ｹ ｺ ｶﾞ ｷﾞ ｸﾞ ｹﾞ ｺﾞ ｶ ｹ
     ｻ ｼ ｽ ｾ ｿ ｻﾞ ｼﾞ ｽﾞ ｾﾞ ｿﾞ
     ﾀ ﾁ ﾂ ﾃ ﾄ ﾀﾞ ﾁﾞ ﾂﾞ ﾃﾞ ﾄﾞ ﾂ
     ﾅ ﾆ ﾇ ﾈ ﾉ
     ﾊ ﾋ ﾌ ﾍ ﾎ ﾊﾞ ﾋﾞ ﾌﾞ ﾍﾞ ﾎﾞ ﾊﾟ ﾋﾟ ﾌﾟ ﾍﾟ ﾎﾟ
     ﾏ ﾐ ﾑ ﾒ ﾓ
     ﾔ ﾕ ﾖ ﾔ ﾕ ﾖ
     ﾗ ﾘ ﾙ ﾚ ﾛ
     ﾜ ｦ ﾝ ﾜ ｲ ｴ
     ｳﾞ . -)

  # 使用可能文字
  # ｦ U+FF66
  CHRS = /[ A-Z0-9ｦｱ-ﾝﾞﾟ\\.,｢｣\(\)\/-]/
  

  # for validate
  def normalize_and_check_fields
    if kana_name
      self.kana_name = Bank.zen2han( normalize_kc(kana_name).upcase )
      if /[^ A-Z0-9ｦｱ-ﾝﾞﾟ\\.,｢｣\(\)\/-]/ =~ kana_name
        errors.add :kana_name, "使用不可の文字が含まれます"
      end
    end
  end


  class << self
    # 振込データなどを作れるようにする。
    def zen2han s
      return nil if !s
      r = ''
      s.split(//u).each {|c|
        if (i = ZEN.index(c)) != nil
          r << HAN[i]
        else
          r << c
        end
      }
      r
    end
  end # class << self

end

