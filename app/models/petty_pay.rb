# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 小口精算の支払い
# 出金伝票を使うため, このクラスは Document の派生ではない。
class PettyPay < ActiveRecord::Base
  belongs_to :user
  belongs_to :petty_group
  belongs_to :expense, :class_name => "PettyExpense"
  belongs_to :cash_advance, :class_name => "PettyCashAdvance"
  belongs_to :cash_slip, :class_name => "PaymentSlip"
  belongs_to :currency


  # @return [仕訳エントリ, 為替レート]
  def make_partial_entry company, entry_day
    if expense
      gl_amt, gl_rate = expense.gl_emp_amount_and_rate
    elsif cash_advance
      gl_amt, gl_rate = cash_advance.gl_amount_and_rate
    else
      raise "internal error"
    end

    e = {
      :dr_account_title => AccountTitle.find_by_account_code(323), # TODO: fix
      :dr_division => nil,
      :dr_partner_code => nil, 
      :dr_worker_code => user.worker_code,
      :amount => gl_amt,
      :vat => nil,
      :remarks => ["小口支払", user.worker_code + " " + user.name].join("/")
    }

    [e, gl_rate]
  end


  class << self
    # 小口精算の支払い伝票: 借方部分
    # @return [(array of JournalLine, Int)]  (伝票の行の配列, gl金額)
    def make_partial_entry2 generator, company, entry_day, payables
      raise TypeError if !company.is_a?(Company)
      raise TypeError if !entry_day.is_a?(Date)
      raise TypeError, "payables = #{payables}" if !payables.respond_to?(:each)

      je_list = []
      sys_amt_ttl = 0

      ac = AccountTitle.find_by_account_code(323) # TODO: fixme!!!

      payables.each do |payable|
        raise TypeError if !payable.is_a?(PettyPay)
        next if payable.amount == 0

        je = JournalLine.new
        je.slip_date = entry_day

        # 借方
        je.debtor.account_title = ac
        je.debtor.worker_code = payable.user.worker_code

        # 金額など
        je.sys_amount = payable.amount
        sys_amt_ttl += je.sys_amount

        je.entry_worker_code = payable.user.worker_code
        je.remarks = ["小口精算",
                      sprintf("%s %s", payable.user.worker_code, 
                              payable.user.name),
                      "出金"].join("/")

        je_list << je
      end

      return [je_list, sys_amt_ttl]
    end
  end # class << self
end
