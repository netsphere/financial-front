# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 出金伝票の現金・預金側の１行
class PaymentBank < ActiveRecord::Base
  belongs_to :our_bank
  belongs_to :payment_slip

  validate :check_fields

  # for validate
  def check_fields
    errors.add :inv_amount, "金額がマイナス" if inv_amount < 0.0
    if payment_slip.slip_type == 1
      errors.add :cash_amount, "金額がマイナス" if cash_amount < 0.0
    elsif payment_slip.slip_type == 2
      errors.add :cash_amount, "金額がマイナス" if cash_amount > 0.0      
    else
      raise "internal error."
    end
  end


  # 入金・出金の行
  # @return [JournalLine]  仕訳の行
  def make_partial_entry2 generator, company, entry_day, apar, inv_currency,
                          inv_current_rate
    raise TypeError if !inv_currency.is_a?(Currency)

    result = []
    sys_amt_ttl = 0
    fc_amt_ttl = 0

    je = JournalLine.new
    je.slip_date = entry_day
    #je.entry_no = 51000 + self.id

    # 債務のマイナス金額(借方残)が残って、そこから入金、という組み合わせがある
    contra = if apar.is_a?(Payment)
               generator.convert_partner(
                        apar.account_title.account_code, 
                        apar.invoice.bank_account_id ? 
                                apar.invoice.bank_account.zaimu_ac : 
                                apar.invoice.partner.zaimu_id)
             elsif apar.is_a?(Receiving)
               if apar.sales_invoice_id
                 generator.convert_partner(
                          apar.sales_invoice.receivable_account.account_code,
                          apar.sales_invoice.partner.zaimu_id)
               elsif apar.petty_expense_id
                 sprintf("E%06dC", apar.petty_expense.user.worker_code.to_i)
               else
                 raise "internal error"
               end
             elsif apar.is_a?(PettyPay)
               # TODO: fixme!
               generator.convert_worker(323, apar.user.worker_code)
             else
               raise "internal error"
             end

    if self.cash_amount >= 0
      # 入金
      je.debtor.account_title = self.our_bank.account_title
      je.debtor.contra_account = contra

      je.remarks = "入金"
    else
      # 出金
      je.debtor.account_title = nil

      je.creditor.account_title = self.our_bank.account_title
      je.creditor.contra_account = contra

      je.remarks = "支払い"
    end

    # 金額
    if self.our_bank.currency_id != company.functional_currency.id
      gl_cash, cash_rate = get_gl_amount( company, entry_day, 
                 self.cash_amount >= 0 ? self.cash_amount : -self.cash_amount, 
                 self.our_bank.currency )
      je.sys_amount = gl_cash

      je.fc_currency_code = self.our_bank.currency.code
      if self.cash_amount >= 0
        je.fc_amount = BigDecimal.new(self.cash_amount.to_s) / 
                              (10 ** self.our_bank.currency.exponent)
      else
        je.fc_amount = -BigDecimal.new(self.cash_amount.to_s) / 
                              (10 ** self.our_bank.currency.exponent)
      end
    else
      je.sys_amount = self.cash_amount >= 0 ? self.cash_amount : 
                                              -self.cash_amount
    end
    if self.cash_amount >= 0
      sys_amt_ttl += je.sys_amount
      fc_amt_ttl += je.fc_amount
    else
      sys_amt_ttl -= je.sys_amount
      fc_amt_ttl -= je.fc_amount
    end

    #je.entry_worker_code = created_by.worker_code
    result << je

    if self.our_bank.currency_id != inv_currency.id
      # 外貨建て債権・債務 vs. 機能通貨の口座

      # 複数通貨の口座があるので, 都度レートを取る
      #dmy, cash_rate = get_gl_amount( company, entry_day, 1000, our_bank

      diff = (BigDecimal.new(self.inv_amount.to_s) * inv_current_rate /
                (10 ** inv_currency.exponent) + BigDecimal.new("0.5")).floor - 
             (if self.cash_amount >= 0
                sys_amt_ttl
              else
                -sys_amt_ttl
              end)
      if diff <= -1 || diff >= +1
        je = JournalLine.new
        je.slip_date = entry_day
        #je.entry_no = 51000 + self.id
          
        je.creditor.account_title = AccountTitle.currency_gain
        je.creditor.division_code = company.bank_division.division_code
        je.creditor.dim2_code = company.bank_division.default_dim2.dim_code
        je.creditor.vat_code = 40

        # 相手勘定
        je.creditor.contra_account = self.our_bank.account_title

        if self.cash_amount >= 0
          je.sys_amount = -diff
        else
          je.sys_amount = diff
        end
        sys_amt_ttl -= je.sys_amount

        je.remarks = "銀行レート 為替差"
        result << je
      end
    end
    
    return [result, sys_amt_ttl]
  end

end
