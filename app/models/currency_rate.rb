
class CurrencyRate < ActiveRecord::Base
  belongs_to :currency

  validates_presence_of :rate
end
