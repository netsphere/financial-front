# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 銀行口座 (日本の銀行以外または日本語以外)
class EnBankAccount < ActiveRecord::Base

  belongs_to :partner

  validates_presence_of :swift_code
  validates_length_of :bank_name, :within => 1..40
  validates_length_of :bank_addr1, :within => 1..50
  validates_length_of :bank_addr2, :maximum => 80

  validates_length_of :bank_country, :is => 2

  validates_length_of :account_name, :within => 1..40
  validates_length_of :account_addr1, :within => 1..80
  validates_length_of :account_addr2, :maximum => 80

  validate :check_fields

  before_save :normalize_string

  # for before_save
  def normalize_string
    a = [:swift_code, :bank_name, :bank_addr1, :bank_addr2, :bank_country,
         :account_name, :account_addr1, :account_addr2, :account_no, :iban_code]
    a.each do |name|
      if self[name]
        self[name] = normalize_kc(self[name]).upcase
      end
    end
  end


  # for validate
  def check_fields
    if iban_code.blank? && account_no.blank?
      errors.add :base, "IBANか口座番号のどちらかには入力してください。"
    elsif iban_code && account_no
      errors.add :base, 'IBANと口座番号はどちらか片方だけ入力してください。'
    end
  end

  
  def format_long
    return "#{bank_name} / #{iban_code || account_no} / #{account_name}"
  end
end
