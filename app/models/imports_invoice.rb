# 多対多で紐付け
# 通関業者と仕入先、など。
class ImportsInvoice < ActiveRecord::Base
  belongs_to :invoice
  belongs_to :import_declaration
  belongs_to :created_by, :foreign_key => 'create_user_id', :class_name => 'User'
end
