module CommonScope
  extend ActiveSupport::Concern
 
  included do
    #請求書検索共通scope
    scope :partner_id, ->(partner_id) { where("cast(partner_id as text) = ?", partner_id) if partner_id.present? }
    scope :company_setting_id, ->(company_setting_id) { where("cast(company_setting_id as text) = ?", company_setting_id) if company_setting_id.present? }
    scope :account_title_id, ->(account_titles) { where("cast(account_title_id as text) = ?", account_titles) if account_titles != "0" }
    scope :je_number, ->(je_number) { where("COALESCE(cast(je_number as text),'') = ?", je_number) if je_number.present? }
  end
end
