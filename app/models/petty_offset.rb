
class PettyOffset < ActiveRecord::Base
  belongs_to :expense, :class_name => "PettyExpense"
  belongs_to :cash_advance, :class_name => "PettyCashAdvance"
	
  belongs_to :currency
end
