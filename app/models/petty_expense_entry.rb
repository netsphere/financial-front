# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 経費精算リポート - 明細行
class PettyExpenseEntry < ActiveRecord::Base
  belongs_to :expense_report, :class_name => 'PettyExpense',
             :foreign_key => 'petty_expense_id'
  has_many :allocations, :class_name => "PettyExpenseAllocation"

  validates_uniqueness_of :report_entry_key

  validates_presence_of :expense_type_name
  validates_presence_of :transaction_date
  
  validates_presence_of :spend_currency_code

  # 業務目的 ※20190425 コンカーから空データが連携される可能性があるため、チェックをはずす
  #validates_presence_of :description

  # 取引先名
  # 日当などで、取引先名がない場合がある。
  #validates_presence_of :vendor_description
  
end
