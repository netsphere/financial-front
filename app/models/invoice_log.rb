class InvoiceLog < ActiveRecord::Base
  belongs_to :invoice
  belongs_to :done_by, :class_name => 'User', :foreign_key => 'done_by'
  
  MODIFY = 1
  SIGN_OFF = 2
  CANCEL = 3

  KIND_NAMES = [nil, "modified", "signed-off", "cancelled"]
  
  before_save :normalize_string
  
  def normalize_string
    self.remarks = normalize_kc remarks
  end
end
