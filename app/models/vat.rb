# -*- coding:utf-8 -*-

# 消費税・税金マスタ
class Vat < ApplicationRecord
  belongs_to :account_title

  # "JP"
  validates_length_of :country, :is => 2

  # 課税売上 (1) or 課税仕入 (2)
  validates :tax_side, presence: true

  # 例: "JPS10"
  validates :tax_code, presence: true, uniqueness: { scope: :tax_side }

  validates :name, presence: true, uniqueness:true
  
  validates_presence_of :tax_rate

  validate :check_fields

  # 税サイド
  TAX_SIDES = {
    #0 => "不課税(消費税不可)",   勘定科目側だけ発生
    1 => "売上",
    2 => "仕入"
  }

  # 税グループ
  TAX_TYPES = {
#     0 => "不課税(対象外)",    勘定科目側だけ発生
    10 => "課税(現行税率)",
    13 => "課税(現行税率以外)",
    30 => "非課税",
    60 => "売上/輸出免税"
  }

  # for validate
  def check_fields
    errors.add("tax_side", "課税売上または仕入を選んでください.") if tax_side != 1 && tax_side != 2
  end
end
