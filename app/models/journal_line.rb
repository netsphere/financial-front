# -*- coding:utf-8 -*-

# 借方 / 貸方部分
class DebtorCreditor 
  attr_reader :account_title
  def account_title= a
    if a
      raise TypeError, "must be a AccountTitle, but #{a.class}" if !a.is_a?(AccountTitle)
    end
    @account_title = a
  end

  attr_reader :division_code
  def division_code= d
    if d
      raise TypeError if !d.is_a?(String)
      raise ArgumentError if d.length != 5
    end
    @division_code = d
  end

  attr_accessor :dim2_code

  # 取引先コード(数字)
  attr_reader :partner_code
  def partner_code= code
    if code
      raise TypeError, "code is '#{code}':#{code.class}" if !code.is_a?(Integer)
    end
    @partner_code = code
  end

  attr_reader :worker_code
  def worker_code= code
    if code
      raise TypeError if !code.is_a?(String)
      raise ArgumentError if code.length != 6
    end
    @worker_code = code
  end


  # 税コード 10, 30, 40, ...
  attr_reader :vat_code
  def vat_code= v
    if v
      v = v.to_s if v.is_a?(Integer)
      raise TypeError, "v is #{v}:#{v.class}" if !v.is_a?(String)
      raise ArgumentError if v.length !=2 && v.length != 4
    end
    @vat_code = v
  end


  # 相手勘定
  # 諸口があるので, JournalLine生成のときにあらかじめ作る必要あり
  attr_reader :contra_account
  def contra_account= ca
    if ca
      raise TypeError, "ca is #{ca}:#{ca.class}" if !(ca.is_a?(AccountTitle) || ca.is_a?(String))
    end
    @contra_account = ca
  end


  # 支払保留
  # こっちで出金伝票を作るものは, yesにする
  attr_reader :payment_block
  def payment_block= pb
    if pb
      raise TypeError if pb != true && pb != false
    end
    @payment_block = pb
  end
end


# 仕訳の1行
class JournalLine
  # 転記日付
  attr_accessor :slip_date

  attr_accessor :entry_no

  attr_reader :debtor
  attr_reader :creditor

  # 機能通貨での金額. 消費税込み
  attr_accessor :sys_amount

  # 外貨建ての金額
  attr_accessor :fc_amount
  attr_accessor :fc_currency_code

  attr_accessor :due_date

  attr_accessor :remarks

  attr_reader :entry_worker_code
  def entry_worker_code= w
    if w
      raise TypeError, "w is #{w}:#{w.class}" if !w.is_a?(String)
    end
    @entry_worker_code = w
  end


  def initialize 
    @sys_amount = 0.0
    @fc_amount = 0.0
    @debtor = DebtorCreditor.new
    @creditor = DebtorCreditor.new
  end
end
