# -*- coding:utf-8 -*-

# BPアドレス
class BpAddress < ActiveRecord::Base
  belongs_to :partner

  validates_presence_of :vendor_code
  validates_uniqueness_of :vendor_code
  validates_presence_of :name

  before_save :normalize_string
  

  # for before_save
  def normalize_string
    self.vendor_code = normalize_kc(vendor_code).upcase
    self.name = normalize_kc(name)
  end
end
