# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'digest/sha1'


# ユーザ (社員)
class User < ActiveRecord::Base
  authenticates_with_sorcery!
  
  has_many :users_divisions
  has_many :divisions, :through => :users_divisions
  
  # Virtual attribute for the unencrypted password
  attr_accessor :password
  attr_accessor :password_confirmation

  validates_presence_of :worker_code
  validates_uniqueness_of :worker_code, :message => "が重複しています。"
  validates_presence_of :name
  validates_presence_of :kana_name

  validates_presence_of :login
  validates_uniqueness_of :login

  #validates_presence_of :email  ナル可
  validates_uniqueness_of :email
  
  validate :check_fields
  validate :check_fields_on_create, :on => :create
  #validate :check_fields_on_update, :on => :update

  before_save :encrypt_password
  before_save :normalize_string

  
  # for before_save
  def normalize_string
    self.worker_code = worker_code.unicode_normalize(:nfkc).upcase
    self.name        = name.unicode_normalize(:nfkc)
    self.kana_name   = kana_name.unicode_normalize(:nfkc)

    self.login = nil if login.to_s.strip == ""
    self.email = nil if email.to_s.strip == ""
  end


  # create, update 共通のチェックルーチン
  # for validate
  def check_fields
    #errors.add "worker_code", "は0以外でなければなりません。" if worker_code == 0
    if !email.blank?
      errors.add 'email', "には半角英数のみ使えます: #{email}" if email =~ /[^a-zA-Z0-9_@.-]/
    end

    if !login.blank?
      errors.add 'email', 'がありません。' if !email || email.strip == ''
      errors.add 'login', 'には半角英数のみ使えます。' if login =~ /[^a-zA-Z0-9_.-]/
      if !password.blank?
        errors.add 'password', 'が短すぎます。' if password.length < 3
        errors.add 'password_confirmation', 'が一致していません。' if password != password_confirmation
      end
    end
  end
  

  # for validate
  def check_fields_on_create
    if !login.blank?
      errors.add 'password', 'がありません。' if password.blank?
    end
  end

 
  def has_role?(name)
    if name == :keiri
      return keiri_flag ? true : false
    elsif name == :marketing
      return marketing_flag ? true : false
    elsif name == :hrd
      return hrd_flag ? true : false
    elsif name == :apar
      return apar_flag ? true : false
    else
      raise ArgumentError
    end
  end


  def accessible_companies
#    if has_role?(:keiri)
#      return Company.find :all
#    end
    Company.where(<<EOF, id)
company_settings.id IN (SELECT company_id FROM divisions 
   JOIN users_divisions ON divisions.id = users_divisions.division_id
   WHERE users_divisions.user_id = ? AND users_divisions.end_day IS NULL)
EOF
  end


  # @return [Company or nil] 主たる所属部門のカンパニー
  def default_company
    div = default_division
    div ? div.company : nil
  end


  # 主な所属部門
  # @return [Division or nil] 部門.
  def default_division
    ud = UsersDivision.where('user_id = ? AND end_day IS NULL AND default_pair <> 0', id).first
    ud ? ud.division : nil
  end


  # ●● TODO: 経理承認と全社閲覧を分割すること
  def viewable_cost_centers
    if has_role?(:keiri)
      @cost_centers = ControlDivision.find :all, 
                                      :conditions => ["parent_id IS NOT NULL"]
      @cost_centers.sort! do |x, y| x.root.id <=> y.root.id end
    else
      # ユーザの管理対象部門を含むコストセンタのみ
      cost_centers = ControlDivision.find :all,
             :select => 'DISTINCT control_divisions.*',
             :joins => "JOIN controllers_divisions ON control_divisions.id = controllers_divisions.control_division_id",
             :conditions => ["controllers_divisions.division_id IN (SELECT division_id FROM users_divisions WHERE user_id = ? AND end_day IS NULL) AND parent_id IS NOT NULL", self[:id] ]
      # デフォルト集計木のみ表示
      default_root_id = GlobalSetting.first.default_division_tree_root_id
      @cost_centers = cost_centers.delete_if {|r|
        r.root.id != default_root_id
      }
    end
  end


  # Sorcery: `login()` から callback される
  def self.authenticate(login, password)
    user = where('login = ? AND active <> 0', login).take # need to get the salt
    # raise "login = '#{login}', #{u.inspect}" DEBUG
    if user && user.crypted_password == user.encrypt(password)
      yield user, nil
    else
      yield nil, "user not found or the password error"
    end
  end

  # Encrypts the password with the user salt
  def encrypt(password)
    raise if !salt
    Digest::SHA1.hexdigest("--#{salt}--#{password}--")
  end


private
  # for `before_save`
  def encrypt_password
    return if password.blank?
    self.salt = Digest::SHA1.hexdigest("--#{Time.now.to_s}--#{login}--") if new_record?
    self.crypted_password = encrypt(password)
  end

  def password_required?
    crypted_password.blank? || !password.blank?
  end
  
  # from acts_as_authenticated
  ###########################################################################
end
