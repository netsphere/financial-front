# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 入金/出金伝票
class PaymentSlip < Document
  belongs_to :company
  belongs_to :created_by, :foreign_key => "create_user_id", 
                          :class_name => "User"

  # 債権. 取引先と小口精算 (社員) を兼ねる.
  has_many :receivings, :foreign_key => "cash_slip_id"

  # 債務. 取引先と仮払い (社員) が別.
  has_many :payments
  has_many :petty_pays, :foreign_key => "cash_slip_id"

  # 入金・出金
  has_many :payment_banks

  SIGNED_OFF = 10


  # 通貨が同じ && レートも同じ
  def je_single_entry result, e, company, entry_day, 
                                             payable, inv_rate, payment_bank
    e.merge!( {
      :entry_day => entry_day, 
      :entry_no => 51000 + id,

      :cr_account_title => payment_bank.our_bank.account_title,
      :cr_division => nil, 
      :cr_partner_code => nil,

      :due_day => nil,
      :created_by => created_by,
    } )
    if payable.currency.code != company.functional_currency.code
      e[:remarks] << " " <<
              format_amount(payable.amount, payable.currency.exponent) <<
              payable.currency.code << "@" << inv_rate.to_s
    end
    result << e
  end
  private :je_single_entry


  # 出金口座が1つ
  # @param inv_amount_total 外貨での債務金額合計
  # @param cash_amount_total 機能通貨での出金額合計
  def je_single_bank result, e, company, entry_day, 
                          payable, inv_rate, payment_bank, 
                          cash_amount_total, inv_amount_total
    remarks = e[:remarks].dup
    e.merge!( {
      :entry_day => entry_day, 
      :entry_no => 51000 + id,
                        
      :cr_account_title => nil,
      :cr_division => nil, 
      :cr_partner_code => nil,

      :due_day => nil,
      :created_by => created_by,
    } )
    if payable.currency.code != company.functional_currency.code
      e[:remarks] << " " <<
              format_amount(payable.amount, payable.currency.exponent) <<
              payable.currency.code << "@" << inv_rate.to_s
    end
    result << e

    cash_amount_local = ( BigDecimal.new(cash_amount_total.to_s) *
                          BigDecimal.new(payable.amount.to_s) / inv_amount_total +
                          BigDecimal.new("0.5") ).floor(0)
    e = {
      :entry_day => entry_day, :entry_no => 51000 + id,
      :dr_account_title => nil,
      :dr_division => nil, :dr_partner_code => nil,

      :cr_account_title => payment_bank.our_bank.account_title,
      :cr_division => nil, :cr_partner_code => nil,
      :amount => cash_amount_local,
      :vat => nil, 
      :due_day => nil,
      :created_by => created_by,
      :remarks => remarks.dup
    }
    if payment_bank.our_bank.currency.code != company.functional_currency.code
      e[:remarks] << "; 出金額" <<
                    format_amount(payment_bank.cash_amount.to_f * 
                            (payable.amount.to_f / inv_amount_total.to_f), 
                                  payment_bank.our_bank.currency.exponent) <<
                    payment_bank.our_bank.currency.code
    end
    result << e
          
    # 為替差損益
    e = {
      :entry_day => entry_day, :entry_no => 51000 + id,
      :dr_account_title => nil,
      :dr_division => nil, :dr_partner_code => nil,
      :cr_account_title => AccountTitle.currency_gain,
      :cr_division => company.bank_division,
      :cr_partner_code => nil,
      :amount => result[-2][:amount] - cash_amount_local,
      :vat => 40, 
      :due_day => nil,
      :created_by => created_by,
      :remarks => remarks + " 為替差"
    }
    result << e
  end
  private :je_single_bank


  # 歯抜けのエントリを補充する
  # @param je_ary [Array of JournalLine] 更新すべき仕訳行のセット
  def update_fields generator, je_ary, entry_day
    if !payment_banks.empty?
      dr_contra = payment_banks.first.our_bank.account_title
      cr_contra = dr_contra
    else
      # 相殺伝票
      if !receivings.empty?
        dr_contra = generator.convert_partner(
                           "1150_JPY", # TODO: fixme
                           receivings.first.sales_invoice.partner.zaimu_id)
      end
      if !payments.empty?
        cr_contra = generator.convert_partner(
                           "3210_JPY", # TODO: fixme
                           (payments.first.invoice.bank_account_id ? 
                              payments.first.invoice.bank_account.zaimu_ac : 
                              payments.first.invoice.partner.zaimu_id) )
      end
    end

    je_ary.each do |je|
      je.slip_date = entry_day
      je.entry_no = 51000 + self.id

      if je.debtor.account_title && !je.debtor.contra_account 
        # 複数口座のこともあるが、とりあえず.  TODO: fixme
        je.debtor.contra_account = dr_contra
      end
      if je.creditor.account_title && !je.creditor.contra_account
        je.creditor.contra_account = cr_contra
      end

      je.entry_worker_code = created_by.worker_code
    end

    return je_ary
  end


  def inv_currency
    if payments.size > 0
      return payments.first.currency
    elsif petty_pays.size > 0
      return petty_pays.first.currency
    elsif receivings.size > 0
      return receivings.first.currency
    end

    return nil
  end


  # 仕訳ver2
  # 取引先ごとに区分すれば, あとはすべて諸口を通して計上すればok
  # @return [Array of JournalLine] 仕訳データ
  def make_entry2 generator, company, entry_day
    raise TypeError if !company.is_a?(Company)
    raise ArgumentError if company.id != self.company_id
    raise TypeError if !entry_day.is_a?(Date)

    # offset伝票 = 入金/出金がない、がありうる
    if payments.size == 0 && petty_pays.size == 0 && receivings.size == 0
      return []
    end

    # 債権・債務の通貨は, ただ1種類
    inv_curr = self.inv_currency
    raise "internal error." if !inv_curr

    dmy, inv_current_rate = get_gl_amount(company, entry_day, 1000, 
                                          inv_curr)
    #raise inv_current_rate.to_s # DEBUG

    result = []

    # 次のいずれか:
    #   1. 取引先が複数あって, 入金/出金口座が一つだけ
    #   2. 取引先が一つだけで, 出金口座が複数 (ex. 外貨 + 円預金)
    x_ = Partner.find_by_sql [<<EOF, self.id, self.id]
SELECT DISTINCT invoices.partner_id AS id
    FROM payments
    JOIN invoices ON payments.invoice_id = invoices.id
    WHERE payment_slip_id = ?
UNION
SELECT DISTINCT sales_invoices.partner_id AS id
    FROM receivings
    LEFT JOIN sales_invoices ON receivings.sales_invoice_id = sales_invoices.id
    WHERE cash_slip_id = ?
EOF
    partner_count = x_ ? x_.size : 0

    raise "internal error: m:n" if partner_count > 1 && payment_banks.size > 1

=begin
    petty_amt_ttl = (petty_pays.inject(0) do |sum, r| sum + r.amount end)
    inv_amount_total = (payments.inject(0) do |sum, r| sum + r.amount end) +
                       petty_amt_ttl
=end

    # 債務 ###############################################################
    # 取引先, 取引先口座ごとに, 為替差損益を挿入する
    payables = Payment.find :all,
                 :select => "payments.*, invoices.partner_id, invoices.bank_account_id",
                 :joins => "JOIN invoices ON payments.invoice_id = invoices.id",
                 :conditions => ["payment_slip_id = ?", self.id],
                 :order => "invoices.partner_id, invoices.bank_account_id"
    pid = nil
    bid = nil
    ary = []
    sys_amt_ttl = 0
    payables.each do |payable|
      if pid != payable.partner_id || bid != payable.bank_account_id
        if !ary.empty?
          tmp_je, tmp_gl_amt = Payment.make_partial_entry2(generator, company, 
                                                 entry_day, 
                                                 ary, inv_current_rate)
          result += update_fields( generator, tmp_je, entry_day )
          sys_amt_ttl += tmp_gl_amt
=begin
          if partner_count > 1 
            tmp_je, tmp_gl_amt = payment_banks.first.make_partial_entry2( 
                                   generator, company, entry_day, inv_curr)
            result += update_fields( tmp_je, entry_day )
            sys_amt_ttl += tmp_gl_amt
          end
=end
        end

        ary = []
        pid = payable.partner_id
        bid = payable.bank_account_id
      end
      ary << payable
    end
    if !ary.empty?
      tmp_je, tmp_gl_amt = Payment.make_partial_entry2(generator, company, 
                                            entry_day, 
                                            ary, inv_current_rate)
      result += update_fields( generator, tmp_je, entry_day )
      sys_amt_ttl += tmp_gl_amt
=begin
      if partner_count > 1 
        tmp_je, tmp_gl_amt = payment_banks.first.make_partial_entry2( 
                                   generator, company, entry_day, inv_curr)
        result += update_fields( tmp_je, entry_day )
        sys_amt_ttl += tmp_gl_amt
      end
=end
    end

    # 小口精算. 出金部分は最後に作れば ok
    tmp_je, tmp_gl_amt = PettyPay.make_partial_entry2( generator, company, 
                                                       entry_day, petty_pays )
    result += update_fields( generator, tmp_je, entry_day )
    sys_amt_ttl += tmp_gl_amt

    # 債権の消し込み. こちらも, 内部照合のため, 取引先ごとにまとめる
    # 社員未収入金も含む.
    receivables = Receiving.find :all,
                 :select => "receivings.*, sales_invoices.partner_id",
                 :joins => "LEFT JOIN sales_invoices ON receivings.sales_invoice_id = sales_invoices.id",
                 :conditions => ["cash_slip_id = ?", self.id],
                 :order => "sales_invoices.partner_id"
    pid = nil
    ary = []
    receivables.each do |receivable|
      if pid != receivable.partner_id
        if !ary.empty?
          tmp_je, tmp_gl_amt = Receiving.make_partial_entry2(generator, company,
                                                entry_day,
                                                ary, inv_current_rate)
          result += update_fields( generator, tmp_je, entry_day )
          sys_amt_ttl += tmp_gl_amt
=begin
          if partner_count > 1 
            tmp_je, tmp_gl_amt = payment_banks.first.make_partial_entry2( 
                                   generator, company, entry_day, inv_curr)
            result += update_fields( tmp_je, entry_day )
            sys_amt_ttl += tmp_gl_amt
          end
=end
        end

        ary = []
        pid = receivable.partner_id
      end
      ary << receivable
    end
    if !ary.empty?
      tmp_je, tmp_gl_amt = Receiving.make_partial_entry2(generator, company, 
                                        entry_day, ary, inv_current_rate)
      result += update_fields( generator, tmp_je, entry_day )
      sys_amt_ttl += tmp_gl_amt
=begin
      if partner_count > 1 
        tmp_je, tmp_gl_amt = payment_banks.first.make_partial_entry2( 
                                   generator, company, entry_day, inv_curr)
        result += update_fields( tmp_je, entry_day )
        sys_amt_ttl += tmp_gl_amt
      end
=end
    end

    # 入金/出金 ################################################################
    # 純粋な相殺のときは生成なし
    if payment_banks.size > 0
      apar = if !receivings.empty?
               receivings.first
             elsif !payments.empty?
               payments.first
             elsif !petty_pays.empty?
               petty_pays.first
             else
               raise "internal error"
             end
      payment_banks.each do |payment_bank|
        tmp_je, tmp_gl_amt = payment_bank.make_partial_entry2 generator, 
                                          company, 
                                          entry_day, 
                                          apar,
                                          inv_curr, inv_current_rate
        result += update_fields( generator, tmp_je, entry_day )
        sys_amt_ttl += tmp_gl_amt
      end
    end

    # 誤差を消す #############################################################
    if sys_amt_ttl != 0
      je = JournalLine.new
      je.slip_date = entry_day
      je.entry_no = 51000 + self.id

      je.debtor.account_title = AccountTitle.find :first, 
                     :conditions => ["account_code = 859 AND suppl_code = 0"]
      je.debtor.division_code = company.bank_division.division_code
      je.debtor.dim2_code = company.bank_division.default_dim2.dim_code
      je.debtor.contra_account = payment_banks.first.our_bank.account_title

      je.sys_amount = -sys_amt_ttl
      je.remarks = "伝票差額"
      je.entry_worker_code = created_by.worker_code

      result << je
    end

    return result
  end


  # 仕訳を生成
  # @return [Array] 仕訳の配列
  #
  # 請求書の通貨と出金口座の通貨が同じ
  #    (1) 通貨=JPY || 債務の発生月と支払月が同じ
  #
  def make_entries company, entry_day
    require "sbo/xml_je_generator"
    jes = make_entry2( (XmlJeGenerator.new company), company, entry_day )
    result = []
    jes.each do |je|
      r = {
        :entry_day => je.slip_date,
        :entry_no => je.entry_no,

        :dr_account_title => je.debtor.account_title,
        :dr_division => (je.debtor.division_code ? 
                Division.find_by_division_code(je.debtor.division_code) : nil),
        :dr_partner_code => je.debtor.partner_code,
        
        :cr_account_title => je.creditor.account_title,
        :cr_division => (je.creditor.division_code ?
            Division.find_by_division_code(je.creditor.division_code) : nil),
        :cr_partner_code => je.creditor.partner_code,

        :amount => je.sys_amount,
        :vat => nil,
        :due_day => nil,
        :created_by => je.entry_worker_code,
        :remarks => je.remarks,
      }
      result << r
    end
    return result

=begin
    raise TypeError if !company.is_a?(Company)
    raise ArgumentError if company.id != self.company_id
    raise TypeError if !entry_day.is_a?(Date)

    if payments.size == 0 && petty_pays.size == 0
      return [] 
    end

    # 債務の通貨は1種類
    if payments.size > 0
      inv_curr = payments.first.currency
    elsif petty_pays.size > 0
      inv_curr = petty_pays.first.currency
    else
      raise "internal error."
    end

    result = []

    # 特別に、出金通貨がJPYで請求通貨がJPY以外の時は, 一つの口座からの出金でであっても, 全体で
    # 諸口を通す => 1円ずれることがあるのの対策
    if payment_banks.size == 1 &&
       !(payment_banks.first.our_bank.currency == company.functional_currency &&
                                 inv_curr != company.functional_currency)
      # 出金口座が一つ => 場合によっては諸口は通さない
      payment_bank = payment_banks.first
      cash_amount_total, cash_rate_local = 
                        get_gl_amount(company, entry_day, 
                                      payment_bank.cash_amount, 
                                      payment_bank.our_bank.currency)
      inv_amount_total = (payments.inject(0) do |sum, r| sum + r.amount end) +
                         (petty_pays.inject(0) do |sum, r| sum + r.amount end)

      payments.each do |payable|
        next if payable.amount == 0
        
        e, inv_rate = payable.make_partial_entry company, entry_day
        if inv_curr == payment_bank.our_bank.currency &&
               inv_rate == cash_rate_local
          je_single_entry result, e, company, entry_day, 
                                            payable, inv_rate, payment_bank
        else
          je_single_bank result, e, company, entry_day, 
                                   payable, inv_rate, payment_bank,
                                   cash_amount_total, inv_amount_total
        end
      end

      # 小口の支払い
      petty_pays.each do |payable|
        next if payable.amount == 0

        e, inv_rate = payable.make_partial_entry company, entry_day
        if inv_curr == payment_bank.our_bank.currency &&
                inv_rate == cash_rate_local
          je_single_entry result, e, company, entry_day,
                                          payable, inv_rate, payment_bank
        else
          je_single_bank result, e, company, entry_day, 
                                   payable, inv_rate, payment_bank,
                                   cash_amount_total, inv_amount_total
        end
      end
    else
      # 口座が2つ以上 => 全体を諸口を通す
      payable_total = BigDecimal.new("0.00")

      payments.each do |payable|
        next if payable.amount == 0

        e, inv_rate = payable.make_partial_entry company, entry_day
        e.merge!( {
          :entry_day => entry_day, 
          :entry_no => 51000 + id,
                        
          :cr_account_title => nil,
          :cr_division => nil, 
          :cr_partner_code => nil,

          :due_day => nil,
          :created_by => created_by,
        } )
        if payable.currency.code != company.functional_currency.code
          e[:remarks] << " " <<
              format_amount(payable.amount, payable.currency.exponent) <<
              payable.currency.code << "@" << inv_rate.to_s
        end

        payable_total += e[:amount]
        result << e
      end

      petty_pays.each do |payable|
        next if payable.amount == 0

        e, inv_rate = payable.make_partial_entry company, entry_day
        e.merge!( {
          :entry_day => entry_day, 
          :entry_no => 51000 + id,
                        
          :cr_account_title => nil,
          :cr_division => nil, 
          :cr_partner_code => nil,

          :due_day => nil,
          :created_by => created_by,
        } )
        payable_total += e[:amount]
        result << e
      end

      # 出金 ##########################################################
      cash_total = 0
      payment_banks.each do |payment_bank|
        cash_amount_local, cash_rate_local = 
                         get_gl_amount(company, entry_day, 
                                       payment_bank.cash_amount, 
                                       payment_bank.our_bank.currency)
        e = {
          :entry_day => entry_day, 
          :entry_no => 51000 + id,
          :dr_account_title => nil,
          :dr_division => nil, :dr_partner_code => nil,

          :cr_account_title => payment_bank.our_bank.account_title,
          :cr_division => nil, 
          :cr_partner_code => nil,
          :amount => cash_amount_local,
          :vat => nil, :due_day => nil,
          :created_by => created_by,
          :remarks => "支払い"
        }
        if payment_bank.our_bank.currency.code != 
                                          company.functional_currency.code ||
                 payment_bank.our_bank.currency.code != inv_curr.code
          e[:remarks] << " " <<
                    format_amount(payment_bank.inv_amount, 
                                  inv_curr.exponent) <<
                    inv_curr.code << "@" << 
                    ((cash_amount_local.to_f / (payment_bank.inv_amount.to_f / 10 ** inv_curr.exponent) * 10000 + 0.5).floor.to_f / 10000).to_s
          if payment_bank.our_bank.currency.code != 
                                          company.functional_currency.code
            e[:remarks] << "; 出金額" << format_amount(payment_bank.cash_amount, payment_bank.our_bank.currency.exponent) << payment_bank.our_bank.currency.code
          end
        end
        result << e
        cash_total += cash_amount_local
      end

      # 為替差損益 (合計)
      if (payable_total - cash_total) != 0
        e = {
          :entry_day => entry_day, :entry_no => 51000 + id,
          :dr_account_title => nil,
          :dr_division => nil, :dr_partner_code => nil,
          :cr_account_title => AccountTitle.currency_gain,
          :cr_division => company.bank_division,
          :cr_partner_code => nil,
          :amount => payable_total - cash_total,
          :vat => 40, :due_day => nil,
          :created_by => created_by,
          :remarks => "為替差"
        }
        result << e
      end
    end

    return result
=end
  end


  # 支払う相手先の一覧
  def invoice_partners
    partners = Partner.find_by_sql [<<EOF, self.id]
SELECT DISTINCT partners.* 
    FROM payments
         LEFT JOIN invoices ON payments.invoice_id = invoices.id 
         LEFT JOIN partners ON invoices.partner_id = partners.id
    WHERE payment_slip_id = ?
EOF
    return partners
  end
end

