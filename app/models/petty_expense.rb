# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 経費精算リポート (ヘッダ部)
class PettyExpense < Document
  belongs_to :user
  belongs_to :petty_group
  belongs_to :reimb_currency, :class_name => "Currency"

  has_many :entries, :class_name => "PettyExpenseEntry"
  has_many :ca_offsets, :class_name => "PettyOffset", :foreign_key => "expense_id"

  has_many :pays, :class_name => "PettyPay", :foreign_key => "expense_id"

  validates_presence_of :report_name


  # 仕訳にした機能通貨建ての金額, レート
  # @return [金額, 為替レート]
  def gl_receivable_amount_total
    entry_day = processing_date
    return get_gl_amount(petty_group.company, entry_day, 
                         receivable_amount_total, reimb_currency)
  end


  # 仕訳にした機能通貨建ての金額, レート
  # @return [金額, 為替レート]
  def gl_emp_amount_and_rate
    entry_day = processing_date
    return get_gl_amount(petty_group.company, entry_day, 
                         emp_amount_total, reimb_currency)
  end


  # 支払先に応じて, 貸方を更新
  # @return [Hash] 仕訳の一部
  def gl_payee_cr entry, alloc
    if entry.payment_code == "CASH"
      return { :cr_account_title => Worker_payable,
               :cr_division => nil,
               :cr_partner_code => nil,
               :cr_worker_code => self.user.worker_code }
    elsif entry.payment_code == "CBCP"
      return { :cr_account_title => CcPayable,
               :cr_division => nil,
               :cr_partner_code => 926993,   # TODO: fixme!!
               :cr_worker_code => nil }
    end
    raise "internal error"
  end
  private :gl_payee_cr


  # 社員未払金
  Worker_payable = AccountTitle.find :first, 
                          :conditions => ["account_code = 323"] # TODO: hard-coded!
  # コーポレートカード
  CcPayable = AccountTitle.where("account_code = 325").first # TODO: fixme!!


  # 小口経費精算リポート -> 仕訳
  def make_entries company, entry_day
    raise TypeError if !company.is_a?(Company)

    # 社員未収入金
    worker_receivable = AccountTitle.find :first,
                          :conditions => ["account_code = 170"] # TODO: hard-coded!
    # 社員仮払金
    advance = AccountTitle.find :first,
                          :conditions => ["account_code = 172"] # TODO: hard-coded!

    result = []
    entries.each do |entry|
      #raise "internal error: id = #{self.id}; #{entry.id}" if entry.allocations.length < 1

      tmp_entry_id = 0
      entry.allocations.each do |alloc|
        line = {
          :entry_day => processing_date,
          :entry_no => 2000 + self.id,

          # 借方: 経費
          :dr_account_title => alloc.account_title,
          :dr_division => alloc.division,
          :dr_partner_code => nil,
          :dr_worker_code => nil,

          :amount => alloc.amount,
          :vat => alloc.vat_code,
          :due_day => nil,
          :created_by => self.user,
          :remarks => ["小口精算", 
                       self.user.worker_code + " " + self.user.name, 
                       self.report_id + " " + self.report_name, 
                       entry.vendor_description, 
                       entry.description].join("/")
        }
        # 貸方
        if emp_amount_total != 0 || cc_amount_total != 0
          line.merge!( gl_payee_cr(entry, alloc) )  # 社員未払金 or CC会社への未払金
        else
          # 諸口でつなぐ
          line.merge!( {
            :cr_account_title => nil,
            :cr_division => nil,
            :cr_partner_code => nil,
            :cr_worker_code => nil,
          } )
        end

         # 勘定科目が170 [社員未収入金] の場合
        if alloc.account_title.account_code == worker_receivable.account_code
           # 貸借不一致が発生するため、２行目のマイナス行はスキップ
          if result.last && tmp_entry_id == alloc.petty_expense_entry_id
            next if (alloc.amount + result.last[:amount] == 0) && (alloc.amount.abs == result.last[:amount].abs) && 
                    alloc.account_title.account_code == result.last[:dr_account_title].account_code
          end
           # 社員番号を出力
          line.merge!( { :dr_worker_code => self.user.worker_code } )
        end
        tmp_entry_id = alloc.petty_expense_entry_id

        result << line
      end # of entry.allocations.each
    end

    # 仮払いの使用 ～諸口でつなぐ
    ca_offsets.each do |offset|
      line = {
        :entry_day => processing_date,
        :entry_no => 2000 + self.id,
        
        :dr_account_title => nil,     # 借方:諸口

        :cr_account_title => advance, # 貸方:社員仮払金
        :cr_worker_code => offset.cash_advance.user.worker_code,

        :amount => offset.amount,
        :vat => nil,
        :created_by => self.user,
        :remarks => ["小口精算", 
                     self.user.worker_code + " " + self.user.name, 
                     self.report_id + " " + self.report_name,
                     "仮払い使用 " + offset.cash_advance.name].join("/")
      }
      result << line
    end

    if ca_offsets.length > 0 || (cc_amount_total == 0 && receivable_amount_total != 0)
      # 諸口でつないだ場合
      if emp_amount_total != 0
       ca_offset_total = ca_offsets.sum(:amount)
        # 社員未払金
        line = {
          :entry_day => processing_date,
          :entry_no => 2000 + self.id,
        
          :dr_account_title => nil,

          :cr_account_title => Worker_payable,
          :cr_worker_code => self.user.worker_code,

          :amount => -ca_offset_total, # 仮払いの使用分を社員未払金から減らす
          :vat => nil,
          :created_by => self.user,
          :remarks => ["小口精算", 
                       self.user.worker_code + " " + self.user.name, 
                       self.report_id + " " + self.report_name].join("/")
        }
        result << line
      end

      if receivable_amount_total != 0
        line = {
          :entry_day => processing_date,
          :entry_no => 2000 + self.id,
        
          :dr_account_title => worker_receivable,
          :dr_worker_code => self.user.worker_code,

          :cr_account_title => nil,
          :cr_worker_code => nil,

          :amount => receivable_amount_total,
          :vat => nil,
          :created_by => self.user,
          :remarks => ["小口精算", 
                       self.user.worker_code + " " + self.user.name, 
                       self.report_id + " " + self.report_name,
                       "要回収"].join("/")
        }
        result << line
      end
    end

    return result
  end


  # 社員未収入金の残高
  def remain_receivable_amount
    r = Receiving.where("(payment_slips.state = 10 OR payment_slips.state = 20) AND petty_expense_id = ?", self.id)
             .joins("LEFT JOIN payment_slips ON receivings.cash_slip_id = payment_slips.id")
             .select("SUM(receivings.amount) AS amount_received")
             .order("")
             .first

    return r ? receivable_amount_total - r.amount_received.to_i : 
               receivable_amount_total
  end
  
end
