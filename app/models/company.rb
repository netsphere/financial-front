# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# カンパニー
class Company < ActiveRecord::Base
  self.table_name = "company_settings"

  belongs_to :functional_currency,
             :foreign_key => 'currency_id', :class_name => 'Currency'

  has_one :company_setting

  validate :check_fields
  validates_presence_of :name
  validates_presence_of :name_en
  validates_presence_of :country
  validates_length_of :tax_reg_number, :maximum => 14

  before_save :normalize_string
  
  
  # @return 銀行手数料の計上部門
  def bank_division
    if company_setting
      Division.find company_setting.bank_division_id
    else
      nil
    end
  end

  
private
  
  # for validate
  def check_fields
    if fixed_day < fixed_gl_day
      errors.add('fixed_day', "確定済みの決算日と同じか新しい日にしてください。")
    end
  end


  # for `before_save`
  def normalize_string
    self.name_en = name_en.unicode_normalize(:nfkc).strip
    self.country = country.unicode_normalize(:nfkc).strip.upcase
  end

end # class Company
