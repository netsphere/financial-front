# -*- coding:utf-8 -*-

# 小口 - 仮払い
class PettyCashAdvance < Document
  belongs_to :user
  belongs_to :petty_group
  belongs_to :reimb_currency, :class_name => "Currency"

  has_many :ca_offsets, :class_name => "PettyOffset", 
                        :foreign_key => "cash_advance_id"
  has_many :pays, :class_name => "PettyPay", :foreign_key => "cash_advance_id"


  # 仕訳にした機能通貨建ての金額, レート
  # @return [金額, 為替レート]
  def gl_amount_and_rate
    entry_day = processing_date
    return get_gl_amount(petty_group.company, entry_day, 
                         journal_amount, reimb_currency)
  end


  # 仮払い申請 -> 仕訳
  def make_entries company, entry_day
    raise TypeError if !company.is_a?(Company)

    worker_ca = AccountTitle.find :first, 
                           :conditions => ["account_code = 172"]
    worker_payable = AccountTitle.find :first, 
                          :conditions => ["account_code = 323"] # hard-coded!

    result = []
    line = {
      :entry_day => processing_date,
      :entry_no => 2500 + self.id,
    
      # 借方
      :dr_account_title => worker_ca,
      :dr_division => nil,
      :dr_partner_code => nil,
      :dr_worker_code => self.user.worker_code,

      :cr_account_title => worker_payable,
      :cr_division => nil,
      :cr_partner_code => nil,
      :cr_worker_code => self.user.worker_code,

      :amount => journal_amount,
      :vat => nil,
      :due_day => nil,
      :created_by => self.user,
      :remarks => ["社員仮払", self.user.worker_code + " " + self.user.name,
                   self.name ].join("/")
    }
    result << line

    return result
  end

end
