
class Qualification < ActiveRecord::Base
  has_many :users_qualifications
  has_many :users, :through => :users_qualifications
  
  validates_presence_of :name
  validates_presence_of :kana_name

  before_save :normalize_string
  
  def normalize_string
    self.name = normalize_kc(name)
    self.kana_name = normalize_kc(kana_name)
  end
end
