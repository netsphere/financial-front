# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 自社銀行口座
class OurBank < ActiveRecord::Base
  belongs_to :currency

  # 日本の口座
  belongs_to :bank_account

  belongs_to :en_bank_account, :dependent => :destroy
  belongs_to :company_setting, :class_name => "Company"

  belongs_to :account_title
  has_many :sales_invoices, :through => :si_banks

  validates_presence_of :name 
  validates_length_of :applicant_code, :maximum => 10
  
=begin  
  def currency
    if en_bank_account
      ret = en_bank_account.currency
    else
      # 国内口座, 現金
      ret = Currency.find_by_code('JPY')
    end
    raise TypeError if !ret.is_a?(Currency) # DEBUG
    ret
  end
=end
  
  class << self
    # そのまま grouped_options_for_select に渡せる配列
    def grouped_list company
      raise TypeError if !company.is_a?(Company)

      our_banks = OurBank.find(:all,
              :select => "our_banks.*, account_titles.account_code AS account_code, account_titles.suppl_code AS suppl_code",
              :joins => "LEFT JOIN account_titles ON our_banks.account_title_id = account_titles.id",
              :conditions => ["account_titles.end_day IS NULL AND company_setting_id = ?", company.id ],
              :order => "account_titles.account_code, account_titles.suppl_code")

      result = []
      ac = nil
      ary = []
      our_banks.each do |our_bank|
        if ac != our_bank.account_code
          ac = our_bank.account_code
          ary = []
          result << ["#{ac} #{AccountTitle.find_by_account_code(ac).main_name}", ary]
        end
        ary << ["#{our_bank.account_code}/#{our_bank.suppl_code} #{our_bank.name} (#{our_bank.currency.code})", our_bank.id]
      end

      # raise result.inspect
      result
    end
  end # class << self


  def format_long
    if bank_account
      return bank_account.format_long
    elsif en_bank_account
      return en_bank_account.format_long + " (#{currency.name})"
    else
      return remarks
    end
  end
end
