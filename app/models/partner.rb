# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 取引先（仕入先）
class Partner < ActiveRecord::Base
  has_many :bank_accounts
  has_many :invoices
  has_many :sales_invoices
  has_many :attach_file_refs
  belongs_to :created_by, :foreign_key => 'created_by', :class_name => 'User'
  belongs_to :updated_by, :foreign_key => 'updated_by', :class_name => 'User'
  # has_many :attach_files, :through => :attach_file_refs
  
  validates_presence_of :name
  validates_presence_of :kana_name
  validates_uniqueness_of :zaimu_id
  validates_format_of :hq_zip, :with => /\A[0-9-]*\z/

  validates_uniqueness_of :tdb_code, :allow_nil => true

  # `before_save` より先に呼び出される.
  before_validation :normalize
  
  validate :must_be_unique_code

  # paginate定数
  PAGINATE_MAX = 100
  PAGINATE_LIST = 25


  # ファイルの中身を取得しない
  def attach_files
    AttachFile.from('attach_files AS a')
        .joins('JOIN attach_file_refs ON a.id = attach_file_refs.attach_file_id')
        .where('attach_file_refs.partner_id = ?', self['id'])
        .select('a.id, a.original_filename, a.content_type, a.attach_kind_id, a.remarks, a.division_id, a.create_user_id, a.created_at')
  end


  def all_bank_accounts activeonly = true
    ba = BankAccount.find :all,
          :conditions => ["partner_id = ?" + (activeonly ? " AND active" : ""),
                          self.id]
    ba += EnBankAccount.find( :all,
                       :conditions => ["partner_id = ?", self.id] )
    return ba
  end


private ####################################################################

  # `before_validation`
  def normalize
    self.zaimu_id = get_next_zaimu_id() if self.zaimu_id.to_i == 0
    
    self.name      = name.to_s.unicode_normalize(:nfkc).strip
    self.kana_name = kana_name.to_s.unicode_normalize(:nfkc).strip.gsub(/[ 　\t\r\n・]/, '')
    self.remarks   = remarks.to_s.unicode_normalize(:nfkc).strip
    self.hq_zip    = hq_zip.to_s.unicode_normalize(:nfkc).strip
    self.hq_addr   = hq_addr.to_s.unicode_normalize(:nfkc).strip 
  end
  
  # for `validate`
  def must_be_unique_code
    ba = BankAccount.where("zaimu_ac = ?", self.zaimu_id).take
    if ba && ba.partner_id != self.id
      errors.add :zaimu_id, "取引先「" + ba.partner.name + "」の銀行口座とコードが重複"
    end
  end

  # 10刻みで加算する
  def get_next_zaimu_id
    cs = GlobalSetting.first
    raise "internal error" if !cs
    
    while Partner.find_by_zaimu_id(cs.partner_counter)
      cs.partner_counter += 10 - (cs.partner_counter % 10)
    end
    r = cs.partner_counter
    cs.partner_counter += 10
    cs.save!
    return r
  end
  
end
