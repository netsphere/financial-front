# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 入金の1行
class Receiving < ActiveRecord::Base
  # 親
  belongs_to :cash_slip, :class_name => "PaymentSlip"

  # いずれか一方
  belongs_to :sales_invoice
  belongs_to :petty_expense

  #belongs_to :our_bank
  belongs_to :currency

  belongs_to :created_by, :class_name => 'User', :foreign_key => 'created_by'

  # credit noteがある。 => 正値とは限らない
  # validates_numericality_of :amount, :greater_than_or_equal_to => 0

  validates_numericality_of :charge, :greater_than_or_equal_to => 0

  
  # 貸方のみを埋める
  def partial_sales_invoice je
    dmy, gl_rate = self.sales_invoice.yen_amount_total

    # 貸方
    je.creditor.account_title = self.sales_invoice.receivable_account
    je.creditor.partner_code = self.sales_invoice.partner.zaimu_id
    je.creditor.vat_code = nil

    # 摘要
    je.remarks = [self.sales_invoice.partner.name,
                  self.sales_invoice.si_journals.first.remarks,
                  "入金"].join("/")

    return gl_rate
  end


  # 貸方のみを埋める
  def partial_petty_payback je
    je.creditor.account_title = 
                    AccountTitle.find_by_account_code(170) # TODO: fixme!!!
    worker = self.petty_expense.user
    je.creditor.worker_code = worker.worker_code

    # 摘要
    je.remarks = [sprintf("%06d: %s", worker.worker_code.to_i, worker.name),
                  self.petty_expense.report_name,
                  "入金"].join("/")

    return 1.00
  end


  class << self
    # 債権の消し込みと, 為替差損益を作る
    # 販売請求書 (取引先) の場合と経費精算書 (社員) の両方がある
    # @param receivables [Array of Receiving] 取引先ごとの、今回消し込む債権
    def make_partial_entry2 generator, company, entry_day, receivables,
                            current_rate
      raise TypeError if !company.is_a?(Company)
      raise TypeError if !entry_day.is_a?(Date)
      raise TypeError if !receivables.is_a?(Array)

      inv_currency = receivables.first.currency

      je_list = []
      sys_amt_ttl = 0
      fc_amt_ttl = 0
      receivables.each do |receivable|
        raise TypeError if !receivable.is_a?(Receiving)

        je = JournalLine.new
        je.slip_date = entry_day

        if receivable.sales_invoice_id
          gl_rate = receivable.partial_sales_invoice je
        elsif receivable.petty_expense_id
          gl_rate = receivable.partial_petty_payback je
        else
          raise "internal error"
        end

        # 借方: 相手勘定 => 為替差損益で使う
        je.debtor.contra_account = generator.convert_partner(
                                         je.creditor.account_title.account_code,
                                         je.creditor.partner_code)

        # 金額など
        je.sys_amount = (BigDecimal.new(receivable.amount.to_s) * gl_rate / 
            (10 ** receivable.currency.exponent) + BigDecimal.new("0.5")).floor
        sys_amt_ttl += je.sys_amount
        if inv_currency.id != company.functional_currency.id
          je.fc_currency_code = receivable.currency.code
          je.fc_amount = BigDecimal.new(receivable.amount.to_s) / 
                                        (10 ** receivable.currency.exponent)
          fc_amt_ttl += je.fc_amount
        end

        je_list << je
      end # of receivables.each

      # 為替差損益
      if inv_currency.id != company.functional_currency.id
        #if receivables.first.cash_slip.id == 5169
        #  raise "#{BigDecimal.new(fc_amt_ttl.to_s)}" # DEBUG
        #end
        diff = (BigDecimal.new(fc_amt_ttl.to_s) * current_rate +
                                 BigDecimal.new("0.5")).floor -
               sys_amt_ttl
        if diff <= -1 || diff >= +1
          je = JournalLine.new

          je.creditor.account_title = AccountTitle.currency_gain
          je.creditor.division_code = company.bank_division.division_code
          je.creditor.dim2_code = company.bank_division.default_dim2.dim_code
          je.creditor.vat_code = 40

          # 相手勘定
          je.creditor.contra_account = je_list.first.debtor.contra_account

          je.sys_amount = diff
          je.remarks = "入金 為替差"

          je_list << je
          sys_amt_ttl += diff
        end
      end

      return [je_list, -sys_amt_ttl]
    end

  end # class << self


  # 貸方だけ埋める
  def make_partial_entry company, entry_day
    raise TypeError if !company.is_a?(Company)
    raise TypeError if !entry_day.is_a?(Date)

    if sales_invoice
      # 取引先
      gl_amt, gl_rate = sales_invoice.yen_amount_total

      # 一部入金がある。請求書の金額ではない.
      e = {
        :cr_account_title => sales_invoice.receivable_account,
        :cr_division => nil,
        :cr_partner_code => sales_invoice.partner.zaimu_id,
        :amount => (BigDecimal.new(self.amount.to_s) / 
                                              10 ** self.currency.exponent *
                    gl_rate + BigDecimal.new("0.5")).floor,
        :vat => nil,
        :remarks => [sales_invoice.partner.name, 
                     sales_invoice.si_journals.first.remarks,
                     "入金"].join("/")
      }
    else
      # 社員
      raise "internal error" if !petty_expense

      gl_amt, gl_rate = petty_expense.gl_receivable_amount_total
      e = {
        :cr_account_title => AccountTitle.find_by_account_code(170), # TODO:
        :cr_division => nil,
        :cr_partner_code => nil,  # TODO: 社員コード
        :amount => gl_amt,
        :vat => nil,
        :remarks => ["#{petty_expense.user.name} (#{petty_expense.user.worker_code})",
                     "社員未収入金", "入金"].join("/")
      }
    end

    [e, gl_rate]
  end
end
