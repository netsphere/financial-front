# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 会計部門
class Division < ActiveRecord::Base
  has_many :controllers_divisions
  has_many :control_divisions, :through => :controllers_divisions
  # belongs_to :role
  belongs_to :company

  belongs_to :default_dim2, :class_name => "Dimension", 
             :foreign_key => "default_dim2_id"

  validates_presence_of   :company_id
  
  validates_presence_of   :division_code
  validates_uniqueness_of :division_code

  validates_presence_of   :name
  validates_uniqueness_of :name

  validates_presence_of   :kana_name

  before_save :normalize_string

  # for before_save
  def normalize_string
    self.division_code = division_code.upcase
    self.name = normalize_kc(name)
    self.kana_name = normalize_kc(kana_name)
  end
  
  class << self
     # 購買請求書・販売請求書共通
    def addable company = nil
      raise TypeError if company && !company.is_a?(Company)

      #  大域設定（木'bumon'）ID取得
      bumon_id = GlobalSetting.find(1).default_division_tree_root_id
      
      cond = "(end_day IS NULL) AND purchase_active <> 0 AND control_divisions.parent_id = ?"
      if company
        cond = [cond + " AND company_id = ?",bumon_id , company.id]
      end

      return Division.eager_load(:control_divisions).where(cond).order('control_divisions.id, divisions.division_code')

    end
  end # class << self

  def name2
    return "#{division_code} #{name}"
  end

  # 「所属」の視点でのコストセンタ
  def default_controller
    default_root_id = GlobalSetting.find(:first).default_division_tree_root_id
    control_divisions.each {|cost_center|
      return cost_center if default_root_id == cost_center.root().id
    }
    raise # DEBUG
  end
  
  # 
  def controller(root_controller)
    raise if !root_controller
    ControlDivision.find :first,
                 :select => 'control_divisions.*',
                 :joins => 'JOIN controllers_divisions ON
                     control_divisions.id = controllers_divisions.control_division_id',
                 :conditions => ['division_id = ? AND parent_id = ?',
                                 attributes['id'], root_controller.id]
  end


  def get_real_dim2 dim2_id_or_zero
    if dim2_id_or_zero.to_i != 0
      Dimension.find dim2_id_or_zero
    else
      self.default_dim2
    end
  end

end
