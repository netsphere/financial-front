# -*- coding:utf-8 -*-

# 販売請求書の明細行
class SiDetail < ActiveRecord::Base
  belongs_to :sales_invoice

  validate :check_fields

  VAT_OPTS = {
    0 => "なし",
    1010 => "10%",
    18 => "8%(標準)",  # for compat.
    1208 => "軽減8%",
  }

# コード=＞[0:判別用,1:税率,2:表示名称,2:表示名称(en)]
  VAT_OPTS_DISP = {
    0 => ["0",0,"-","-"],
    1010 => ["10",0.1,"10%","10%"],
    18 => ["8",0.08,"8%","8%"],  # for compat.
    20 => ["5",0.05,"5%","5%"],  # for compat.
    1208 => ["8_r",0.08,"軽8%","R8%"],
  }

  # for validate
  def check_fields
    if unit_price.to_i != 0 || qty.to_i != 0
      if (unit_price.to_i * qty.to_i - amount.to_i) != 0
        raise
      end
    end
  end
end
