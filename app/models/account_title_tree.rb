# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 勘定科目の集計木
# 帳票をまたいで共有される
class AccountTitleTree < ActiveRecord::Base
  belongs_to :parent, :foreign_key => 'parent_id', :class_name => 'AccountTitle'
  belongs_to :child, :foreign_key => 'child_id', :class_name => 'AccountTitle'

  class << self
    # 請求書で追加可能な科目一覧を得る(購買/販売共通)
    def addableTree kbn
    
str = <<-EOS
/* 2.WITH recursiveで再帰クエリを有効にし1と3で取得した結果を仮想表(smr)に格納する */
WITH recursive smr(parent_id, child_id, account_code, name, root_account_code, root_name, aggregate_flag, end_day, kind, enable_invoice, enable_si_invoice, suppl_code) AS (
/* 1.再帰クエリの出発点となるデータ（ルートに直接紐づくリーフ）を取得する */
SELECT att.parent_id, att.parent_id, atl.account_code, atl.name, atl.account_code, atl.name, atl.aggregate_flag, atl.end_day, atl.kind, atl.enable_invoice, atl.enable_si_invoice, atl.suppl_code
 FROM account_title_trees att
 JOIN account_titles AS atl ON att.parent_id = atl.id
 WHERE parent_id not in(select distinct(child_id) FROM account_title_trees)
 GROUP BY att.parent_id, atl.name, atl.account_code, atl.aggregate_flag, atl.end_day, atl.kind, atl.enable_invoice, atl.enable_si_invoice, atl.suppl_code 
/* 3.2のsmrより1データ取り出し以下SQL結果と結合。残ったデータをまた2に格納する。 */
/*   2と3が繰り返される事で最終的にリーフ階層が平滑化されルートでグルーピングされたデータがsmrに形成される。 */
/*   （一度下のSQLに投入されたsmrのデータが再度投入される事はない。） */
UNION ALL
SELECT att.parent_id, att.child_id, atl.account_code, atl.name, smr.root_account_code, smr.root_name, atl.aggregate_flag, atl.end_day, atl.kind, atl.enable_invoice, atl.enable_si_invoice, atl.suppl_code 
 FROM smr, account_title_trees att
 JOIN account_titles AS atl ON att.child_id = atl.id
 WHERE smr.child_id = att.parent_id
 AND atl.end_day IS NULL
 AND smr.aggregate_flag = true)/* 集計フラグを持たない親との結合は除外 */
/* 4.最後にsmrに格納されたデータを整形するための以下SQLが流れ、呼び出し元メソッドに渡される。 */
SELECT child_id, root_account_code, root_name, account_code, name
FROM
(
 /* 4-1.smrに格納されたデータを取得 */
 SELECT child_id, root_account_code, root_name,account_code, name, kind, enable_invoice, enable_si_invoice, suppl_code
 FROM smr
 WHERE aggregate_flag = false 
 AND child_id not in(SELECT distinct(s1.child_id) FROM smr s1, smr s2
 WHERE s1.child_id = s2.parent_id) /* 親もいれば孫もいる子は除外 */
 UNION
 /* 4-2.どのルートにも紐づかない勘定科目を取得しUNIONで4-1とマージする */
 SELECT id as child_id, 999999 as root_account_code, 'その他' as root_name, account_code, name, kind, enable_invoice, enable_si_invoice, suppl_code
 FROM account_titles
 WHERE id NOT IN
 (SELECT distinct(ID) FROM
  (SELECT parent_id ID
   FROM account_title_trees
   UNION
   SELECT child_id ID
   FROM account_title_trees
  ) ATID
 )
 AND end_day is null
 AND aggregate_flag = false
) SMR 
EOS

      str2 = 'WHERE kind = 0 AND enable_invoice = true ' if kbn == 0
      str2 = 'WHERE enable_si_invoice = true ' if kbn == 1 
      str2 += 'ORDER BY root_account_code,account_code,suppl_code'
      find_by_sql([str+str2])
    end
  end # class << self
end
