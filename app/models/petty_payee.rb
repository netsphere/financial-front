# -*- coding:utf-8 -*-

# コーポレートカード・マスタ
class PettyPayee < ActiveRecord::Base
  validates_presence_of :payment_type
  validates_uniqueness_of :payment_type
  
  validates_presence_of :name

  validate :check_fields

  belongs_to :ar_title, :class_name => "AccountTitle"
  belongs_to :ap_title, :class_name => "AccountTitle"
  
  # for validate
  def check_fields
    case payment_type
    when "Company"
      return
    when "Employee"
      errors.add(:ar_title_id, "必須") if ar_title_id.blank?
      errors.add(:ap_title_id, "必須") if ap_title_id.blank?
    else
      # コーポレートカード
      errors.add(:ar_title_id, "必須") if ar_title_id.blank?
      errors.add(:ap_title_id, "必須") if ap_title_id.blank?
      errors.add(:bp_code, "必須") if bp_code.to_i == 0
    end
  end
  private :check_fields
  
end
