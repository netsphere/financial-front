# -*- coding:utf-8 -*-

# 入金伝票のログ
class RsLog < ActiveRecord::Base
  belongs_to :receiving_slip
  belongs_to :payment_slip
  belongs_to :done_by, :class_name => 'User', :foreign_key => 'done_by'
  before_save :normalize_string
  
  MODIFY = 1
  SIGN_OFF = 2
  CANCEL = 3
  
  KIND_NAMES = [nil, "modified", "signed-off", "cancelled" ]

  def normalize_string
    self.remarks = normalize_kc remarks
  end
end
