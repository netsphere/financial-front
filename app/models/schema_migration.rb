# -*- coding: utf-8 -*-


class SchemaMigration < ActiveRecord::Base
  VERSION = (SchemaMigration.find :first, 
                                  :select => "version", 
                                  :order => "version DESC").version
end

