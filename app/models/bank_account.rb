# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 取引先銀行口座
class BankAccount < ActiveRecord::Base
  belongs_to :bank_branch
  belongs_to :partner

  belongs_to :created_by, :foreign_key => 'create_user_id', 
             :class_name => 'User'
  belongs_to :updated_by, :foreign_key => 'update_user_id', 
             :class_name => 'User'
  
  ACTYPE_NAMES = {1 => '普通', 2 => '当座', 9 => '貯蓄'}
  
  validates_presence_of :zaimu_ac
  validates_uniqueness_of :zaimu_ac
  
  validates_presence_of :kana_name
  validates_length_of :kana_name, :maximum => 30
  
  validates_presence_of :ac_number

  # before_saveより先に呼び出されてしまう
  #validates_format_of :kana_name, :without => /[^ A-Z0-9ｦｱ-ﾝﾞﾟ\\.,｢｣\(\)\/-]/
  validate :normalize_and_check_fields

  #before_save :normalize_string

  # 口座名義まで
  def format_long
    branch = bank_branch
    return "#{bank_name} (#{BankAccount::ACTYPE_NAMES[ac_type]}) #{ac_number} #{kana_name}"
  end
  

  # 銀行名+支店名
  def bank_name
    return "#{bank_branch.bank.name}/#{bank_branch.name}"
  end

  
  # for validate
  def normalize_and_check_fields
    if kana_name
      self.kana_name = Bank.zen2han( normalize_kc(kana_name).upcase )
      if /[^ A-Z0-9ｦｱ-ﾝﾞﾟ\\.,｢｣\(\)\/-]/ =~ kana_name
        errors.add :kana_name, "使用不可の文字が含まれます"
      end
    end

    other = Partner.find :first, :conditions => ["zaimu_id = ?", zaimu_ac]
    if other && other.id != self.partner_id
      errors.add :zaimu_ac, "ほかの取引先の取引先コードと被っています"
    end
  end
end
