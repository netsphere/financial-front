# -*- coding:utf-8 -*-

# 次元2
class Dimension < ActiveRecord::Base

  validates_presence_of :dim_code
  validates_uniqueness_of :dim_code

  validates_presence_of :name

  before_save :normalize_string

  # for before_save
  def normalize_string
    self.name = normalize_kc(name)
  end

end # class Dimension
