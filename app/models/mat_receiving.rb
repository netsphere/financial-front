# -*- coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 資材(Part)の仕入
class MatReceiving < ActiveRecord::Base
  belongs_to :invoice
  belongs_to :part, :foreign_key => "part_no", :primary_key => "part_no", 
                    :class_name => "MatPart"
  has_many :charges, :class_name => "MatReceivingCharge"

  validate :check_fa_flag

  TAX_CODE_MAP = {
    "JPVAT" => "10",
    "JP"    => "10",
    "JP8"   => "18",
    "ZR"    => "40",
    "JP10"  => "1010",
    "NA"    => nil,
  }

  # for validate
  def check_fa_flag
    if part_mat_class != "R" && fixed_asset_flag
      errors.add(:fixed_asset_flag, "must non-FA when mat_class = 'C'")
    end
  end


  def currency
    if !currency_code
      return nil
    else
      curr = Currency.find_by_code(currency_code)
      raise "unknown currency_code: #{currency_code}" if !curr
      return curr
    end
  end


  # chargeの合計金額
  # @return 正規化した金額
  def amount_of_charges
    v = MatReceivingCharge.select("SUM(total_amount) AS sum_amount, SUM(tax_amount) AS sum_tax") \
          .where("mat_receiving_id = ?", self.id).order("").first
    if v && v.sum_amount.to_f + v.sum_tax.to_f != 0.0
      return nominal_amount(v.sum_amount.to_f + v.sum_tax.to_f, 
                            currency.exponent)
    else
      return 0
    end
  end


  class << self
    # 支払依頼の明細行に表示するための一覧. 仕訳生成でも使う
    # @return 円でも端数があるので, 正規化しない。
    #         order_type -> mat.type -> fixed_asset_flag の Hash.
    #         P-order FAの場合は, MatReceivingの配列,
    #         そうでないときは, {tax_code => amount}
    def journals invoice
      raise TypeError if !invoice.is_a?(Invoice)
      
      result = {}

      MatReceiving.where("invoice_id = ?", invoice.id).each do |r|
        part = r.part
        result[r.order_type] ||= {}
        result[r.order_type][part.mat_type] ||= {}

        if r.order_type == "P" && r.fixed_asset_flag
          result[r.order_type][part.mat_type][true] ||= []
          result[r.order_type][part.mat_type][true] << r

          c = MatReceivingCharge.select("SUM(total_amount) AS sum_amount, SUM(tax_amount) AS sum_tax") \
                              .where("mat_receiving_id = ?", r.id).order("")
          if !c.empty?
            result[r.order_type][part.mat_type][true].last.total_amount += 
                               c.first.sum_amount.to_f + c.first.sum_tax.to_f
          end
        else
          # P order以外
          result[r.order_type][part.mat_type][r.fixed_asset_flag] ||= {}
          result[r.order_type][part.mat_type][r.fixed_asset_flag][r.tax_code] ||= 0.0

          result[r.order_type][part.mat_type][r.fixed_asset_flag][r.tax_code] +=
                                                 r.total_amount + r.tax_amount

          c = MatReceivingCharge.select("SUM(total_amount) AS sum_amount, SUM(tax_amount) AS sum_tax") \
                              .where("mat_receiving_id = ?", r.id).order("")
          if !c.empty?
            result[r.order_type][part.mat_type][r.fixed_asset_flag][r.tax_code] +=
                               c.first.sum_amount.to_f + c.first.sum_tax.to_f
          end
        end
      end

      return result
    end


    # TODO: FIXME!!!!!!!  ここから
    ACDEF = {
      "P" => { # P-order
        "P" => { # mat_type 
          true => AccountTitle.where("account_code = ? AND suppl_code = ?", 207, 3).first,
          false => AccountTitle.where("account_code = ? AND suppl_code = ?", 731, 0).first },
        "T" => {
          true => AccountTitle.where("account_code = ? AND suppl_code = ?", 251, 0).first,
          false => AccountTitle.where("account_code = ? AND suppl_code = ?", 737, 0).first },
        "RM" => AccountTitle.where("account_code = ? AND suppl_code = ?", 737, 0).first,
      },
      "L" => { # L-order
        "P" => AccountTitle.where("account_code = ? AND suppl_code = ?", 721, 3).first,
        "T" => AccountTitle.where("account_code = ? AND suppl_code = ?", 736, 0).first
      },
      "OTHER" => { # R-order などなど
        "P" => AccountTitle.where("account_code = ? AND suppl_code = ?", 733, 0).first,
        "T" => AccountTitle.where("account_code = ? AND suppl_code = ?", 826, 2).first,
      },
    }
    AC_AP = AccountTitle.where("account_code = ?", 305).first
    DIV_CODE = "71012"
    DIM2_CODE = "100"
    # TODO: FIXME!!!!!  ここまで


    def get_ac order_type, mat_type, fixed_asset_flag
      if order_type == "P" || order_type == "L"
        if ACDEF[order_type][mat_type].is_a?(Hash)
          return ACDEF[order_type][mat_type][fixed_asset_flag]
        end
        raise "internal error" if !ACDEF[order_type][mat_type].is_a?(AccountTitle)
        return ACDEF[order_type][mat_type]
      end

      return ACDEF["OTHER"][mat_type]
    end


    # 1行の JournalLine を生成
    # @param mat_or_amount P-order FAのときは MatReceiving, それ以外は fc_amount
    # @return [JournalLine] 仕訳の1行
    def make_entry2_line invoice, generator, entry_day, 
                         order_type, mat_type, fixed_asset_flag, tax_code, 
                         mat_or_amount
      je = JournalLine.new
      je.slip_date = entry_day
      je.entry_no = 10000 + invoice.id

      # 借方
      je.debtor.account_title = get_ac(order_type, mat_type, fixed_asset_flag)
      je.debtor.division_code = DIV_CODE
      je.debtor.dim2_code = DIM2_CODE
      je.debtor.partner_code = nil
      je.debtor.vat_code = if TAX_CODE_MAP.member?(tax_code)
                             TAX_CODE_MAP[tax_code]
                           else
                             raise "internal error: unknown tax #{tax_code}"
                           end

      # 貸方
      je.creditor.account_title = AC_AP  # 固定
      je.creditor.division_code = nil
      je.creditor.partner_code = (invoice.bank_account_id ? 
                                               invoice.bank_account.zaimu_ac : 
                                               invoice.partner.zaimu_id)
      je.creditor.payment_block = false # 自動引き落としは, ありえない
      je.creditor.vat_code = nil

      # 相手勘定
      je.debtor.contra_account = generator.convert_partner(
                                       je.creditor.account_title.account_code, 
                                       je.creditor.partner_code)
      je.creditor.contra_account = je.debtor.account_title

      # 金額など
      fc_amount = if order_type == "P" && fixed_asset_flag
                    mat_or_amount.total_amount + mat_or_amount.tax_amount
                  else
                    mat_or_amount
                  end
      gl_amount, rate = get_gl_amount( invoice.company_setting, 
                                   entry_day, 
                                   nominal_amount(fc_amount, 
                                                  invoice.currency.exponent),
                                   invoice.currency )

      je.sys_amount = gl_amount # 外貨建てのときも円を設定
      if invoice.currency.code != 
                            invoice.company_setting.functional_currency.code
        je.fc_currency_code = invoice.currency.code
        je.fc_amount = fc_amount  # ここはそのままの金額でOK
      else
        je.fc_currency_code = nil
        je.fc_amount = 0.0
      end

      je.due_date = invoice.due_day
            
      # 摘要  -- 50文字しか入らないので節約する
      if order_type == "P" && fixed_asset_flag
        part = mat_or_amount.part
        je.remarks = [invoice.origin_no, 
                      "Mat.type #{part.mat_type}",
                      "PN #{part.part_no} #{part.description[0..9]}",
                      "SN #{mat_or_amount.part_serial}"].join("/")
      else
        je.remarks = [invoice.origin_no, "#{order_type} order", 
                      "Mat.type #{mat_type}",
                          (fixed_asset_flag ? "FA" : "Non-FA")].join("/")
      end

      return je
    end

    
    # 支払依頼に紐づいた, 材料仕入セクションの仕訳
    def make_entry2 invoice, generator, entry_day
      raise TypeError if !invoice.is_a?(Invoice)

      je_lines = []

      journals(invoice).each do |order_type, xx|
        xx.each do |mat_type, x|
          x.each do |fixed_asset_flag, y|
            if order_type == "P" && fixed_asset_flag 
              # MatReceivingの配列
              y.each do |mat|
                if !mat.is_a?(MatReceiving)
                  raise TypeError, "mat must be MatReceiving, but #{mat.class}" 
                end

                je = make_entry2_line( invoice, generator, entry_day,
                                order_type, mat_type, fixed_asset_flag,
                                mat.tax_code, mat )
                je_lines << je
              end
            else
              # P-order FA以外
              y.each do |tax_code, fc_amount|
                raise TypeError, fc_amount.inspect if !fc_amount.is_a?(Numeric)

                je = make_entry2_line( invoice, generator, entry_day,
                                order_type, mat_type, fixed_asset_flag,
                                tax_code, fc_amount )
                je_lines << je
              end
            end
          end
        end
      end

      return je_lines
    end

  end # class << self
end
