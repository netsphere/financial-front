# -*- coding:utf-8 -*-

require "application_helper"


# 販売請求書にもとづく仕訳
class SiJournal < ActiveRecord::Base
  belongs_to :sales_invoice
  belongs_to :account_title
  belongs_to :division
  belongs_to :dim2, :class_name => "Dimension"
  belongs_to :vat

  before_save :normalize_string


  # for before_save
  def normalize_string
    self.remarks = normalize_kc remarks
  end
end
