# -*- coding: utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 輸入申告 / 輸入許可通知書
class ImportDeclaration < Document
  belongs_to :invoice_currency, :class_name => 'Currency'
  belongs_to :created_by, :foreign_key => 'create_user_id', 
                                                       :class_name => 'User'
  belongs_to :company

  has_many :imports_invoices
  has_many :invoices, :through => :imports_invoices

  validates_length_of :declaration_number, :within => 6..11
  validates_uniqueness_of :declaration_number

  validates_presence_of :exporter_name

  validates_length_of :bl_number, :within => 3..18

  before_save :normalize_string
  
  Incoterms = {   # TODO: マスタを作るか？
    # 1 => "EXW",
    # 2 => FCA
    # 3 => FAS
    4 => "FOB",
    5 => "CFR (C&F)",
    6 => "CIF",
    # 7 => CPT
    # 8 => CIP
    # 9 => DAF    2010廃止
    # 10 => DES   2010廃止
    # 11 => DEQ   2010廃止
    # 12 => DDU   2010廃止
    12 => "DAP (Delivered At Place)",  # 2010新設
    # 13 => DDP
  }


  def normalize_string
    %w(exporter_name description).each {|attr|
      self[attr] = normalize self[attr]
    }
    self["exporter_name"] = exporter_name.upcase

    # 関税・消費税はフリーのことがある
    %w(customs_tariff consumption_tax vat_local).each {|attr|
      self[attr] = 0 if !self[attr]
    }

    # AWBは"-"削除
    self["bl_number"] = self["bl_number"].gsub("-", "")
  end
  
  def format_declaration_number
    if declaration_number
      str = declaration_number.to_s
      sprintf("%s %s %s", str[0..2], str[3..6], str[7..-1])
    else
      nil
    end
  end

  def format_bl_number
    if bl_number
      str = bl_number.to_s
      sprintf("%s-%s", str[0..2], str[3..-1])
    else
      nil
    end
  end


  class << self
    # 支払依頼に紐づいた ID の仕訳
    def make_entry2 invoice, generator, entry_day
      raise TypeError if !invoice.is_a?(Invoice)

      vat_payment_ac = AccountTitle.vat_payment
      je_lines = []

      invoice.import_declarations.each do |imp|
        je = JournalLine.new
        je.slip_date = entry_day
        je.entry_no = 10000 + invoice.id

        # 借方
        je.debtor.account_title = vat_payment_ac
        je.debtor.division_code = nil
        je.debtor.partner_code = nil
        je.debtor.vat_code = nil

        # 貸方
        je.creditor.account_title = AccountTitle.find(vat_payment_ac.payable_id)
        je.creditor.division_code = nil
        je.creditor.partner_code = 
                  (invoice.bank_account_id ? invoice.bank_account.zaimu_ac : 
                                             invoice.partner.zaimu_id)
        je.creditor.payment_block = (invoice.bank_account_id ? false : true)
        je.creditor.vat_code = nil

        # 相手勘定
        je.debtor.contra_account = generator.convert_partner(
                                       je.creditor.account_title.account_code, 
                                       je.creditor.partner_code)
        je.creditor.contra_account = je.debtor.account_title

        # 金額など
        je.sys_amount = imp.consumption_tax + imp.vat_local

        je.due_date = (invoice.bank_account ? invoice.due_day : nil)
        je.entry_worker_code = invoice.created_by.worker_code

        # 摘要
        je.remarks = "輸入消費税 ID#" + [imp.format_declaration_number, 
                                 imp.permit_date, imp.exporter_name].join('/')

        je_lines << je
      end

      return je_lines
    end
  end # class << self

end


