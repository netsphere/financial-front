
class GlobalSetting < ActiveRecord::Base
  belongs_to :bank_charge_account_title, 
        :foreign_key => "bank_charge_account_title_id", 
        :class_name => "AccountTitle"

  belongs_to :currency_gain_account_title, 
        :foreign_key => "currency_gain_account_title_id", 
        :class_name => "AccountTitle"

  belongs_to :vat_payment_account_title, 
        :foreign_key => "vat_payment_account_title_id", 
        :class_name => "AccountTitle"

  validates_presence_of :partner_counter
end
