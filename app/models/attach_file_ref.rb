# -*- coding:utf-8 -*-


# 添付ファイルと Document
# TODO: 別テーブルに分ける意味がない. AttachFile に統合する.
class AttachFileRef < ActiveRecord::Base
  belongs_to :attach_file

  # いずれか一方に値を設定.
  belongs_to :partner
  belongs_to :invoice
  
  validate :check_fields


  # for validate()
  def check_fields
    if !partner_id && !invoice_id
      errors.add(:base, "internal error: 文書への紐付けがない")
    end
  end
  private :check_fields
  
end
