"use strict";

function my_round(v, n) 
{
  var p = Math.pow(10, n);
  return Math.round(v * p) / p;
}

/** 0の方向へ丸める */
function my_rounddown(v, n)
{
  var p = Math.pow(10, n);

  if (v >= 0 )
    return Math.floor(v * p) / p;
  else
    return Math.ceil(v * p) / p;
}


/**
 * 明細行のなかで, 金額列を更新
 *   @param idx  0のとき, (単価, 数量) => 金額
 *               1のとき, (単価, 数量) をクリア.
 */
function up_total(line, idx) {
  var tbl = $('#detail')[0];
  // var r = tbl.rows[line + 1];
  if (idx == 0) {
    $('#detail' + String(line) + '_amount')[0].value = 
               Number($('#detail' + String(line) + '_unit_price')[0].value) * 
               Number($('#detail' + String(line) + '_qty')[0].value);
    // var c = r.cells[4].firstChild;
    // c.value = $F(r.cells[1].firstChild) * $F(r.cells[2].firstChild);
  }
  else {
    $('#detail' + String(line) + '_unit_price')[0].value = null;
    $('#detail' + String(line) + '_qty')[0].value = null;
    $('#detail' + String(line) + '_unit')[0].value = null;
  }
  up_subtotal();
}


/**
 * 明細の, 小計行を更新
 */
function up_subtotal() {
  var tbl = $('#detail')[0];
  var s = 0.0;
  var t = 0.0;
 
  $.ajax({
    url: "/sales_invoices/up_subtotal_vat",
    data: {
            tbl: tbl.rows.length,
            s: s,
            t: t
          },
    success: function(data) {
             },
    error: function() {
             alert("up_subtotal_vat: 通信エラーが発生しました");
           }
  });


}


/**
 * 明細行を追加する
 * @param ref_row 物理的な明細1行目 = 'r0'
 * @param advance 立替金フラグ
 */
function insert_new_detail( ref_row,advance) 
{
  var tbl = $('#detail')[0];

  // 行数
  var rid = $('#detail_lines')[0].value;
  $.ajax({
    url: "/sales_invoices/insert_new_detail",
    data: {
            ref: ref_row,
            newid: rid,
            advance: advance
          },
    success: function(data) {
             },
    error: function() {
             alert("insert_new_detail: 通信エラーが発生しました");
           }
  });
}


////////////////////////////////////////////////////////////////////////////
// 仕訳フォーム

function ac_changed(i) {
  var tax = $('#journal' + String(i) + '_vat_id');
  tax[0].value = -1;
  $('#journal' + String(i) + '_tax_name').text('(科目に従う)');
}


function div_changed(i) {
  var tax = $('#journal' + String(i) + '_dim2_id');
  tax[0].value = 0; // Number(tax[0].value) + 1; // DEBUG
  $('#journal' + String(i) + '_dim2_name').text('(部門に従う)');
}


/**
 * 仕訳の合計行
 */
function update_journal_total()
{
  var tbl = $('#journal_tbl')[0];
  var s = 0.0;
  for (var i = 0; i < (tbl.rows.length - 1) / 2 - 1; i += 1) {
    // v = parseFloat($F(tbl.rows[i].cells[1].firstChild));
    var v = parseFloat( $('#journal' + i + '_amount').val() );
    if (!isNaN(v)) s += v;
  }
  $('#journal_total')[0].innerHTML = my_round(s, 4);
}


/**
 * 仕訳行を追加する
 * @param ref_row 物理的な明細1行目 = 'j0'
 */
function insert_new_journal( company_id, ref_row ) 
{
  var tbl = $('#journal_tbl')[0];

  // 行数
  var rid = $('#journal_lines')[0].value;

  $.ajax({
    url: "/sales_invoices/insert_new_journal",
    data: {
            company: String(company_id),
            ref: ref_row,
            newid: String(rid)
          },
    success: function(data) {
             },
    error: function() {
             alert("insert_new_journal: 通信エラーが発生しました");
           }
  });
}


/**
 * 税, 次元2を選択するダイアログを表示
 */
function open_sel_dim2(line, url) {
  var ln = "#journal" + String(line);

  $.ajax({
    url: url,
    data: { 
      line: line,
      ac: $(ln + "_account_title_id")[0].value,
      div: $(ln + "_division_id")[0].value,
      tax: $(ln + "_vat_id")[0].value,
      dim2: $(ln + "_dim2_id")[0].value
    },
    //type: "POST",
    //dataType: 'html',
    success: function(data) {
             },
    error: function() {
             alert("sel_dim2: 通信エラーが発生しました");
           }
  });
} 


/** 更新ボタン */
function update_dim2() 
{
  var line = $('#sel_line')[0].value;

  $.ajax({
      url: "/sales_invoices/update_dim2",
      data: {
        line: line,
        tax: $('#sel_tax')[0] ? $('#sel_tax')[0].value : null,
        dim2: $('#sel_dim2')[0].value
      },
      success: function(data) {
               },
      error: function() {
               alert("update_dim2: 通信エラーが発生しました");
             }
  });

}
