"use strict";

function my_round(v, n) 
{
  var x = Math.round(v * 10000);
  return x / 10000;
}


/**
 * 合計行を更新
 */
function up_total() 
{
  var tbl = $('#detail')[0];
  var s = 0.0;
  for (var i = 0; i < (tbl.rows.length - 1) / 2 - 1; i += 1) {
    // v = parseFloat($F(tbl.rows[i].cells[1].firstChild));
    var v = parseFloat( $('#detail' + i + '_amount').val() );
    if (!isNaN(v)) s += v;
  }
  $('#total')[0].innerHTML = my_round(s, 4);
}


/**
 * 税, 次元2を選択するダイアログを表示
 */
function open_sel_dim2(line, url) 
{
  var ln = "#detail" + String(line);

  $.ajax({
    url: url,
    data: { 
      line: line,
      ac: $(ln + "_account_title_id")[0].value,
      div: $(ln + "_division_id")[0].value,
      tax: $(ln + "_vat_id")[0].value,
      dim2: $(ln + "_dim2_id")[0].value
    },
    //type: "POST",
    //dataType: 'html',
    success: function(data) {
             },
    error: function() {
             alert("open_sel_dim2(): 通信エラーが発生しました");
           }
  });
} 


function ac_changed(i) {
  var tax = $('#detail' + String(i) + '_vat_id');
  tax[0].value = -1;
  $('#detail' + String(i) + '_tax_name').text('(科目に従う)');
}


function div_changed(i) {
  var tax = $('#detail' + String(i) + '_dim2_id');
  tax[0].value = 0; // Number(tax[0].value) + 1; // DEBUG
  $('#detail' + String(i) + '_dim2_name').text('(部門に従う)');
}


/**
 * 明細行を追加する
 * @param ref_row 物理的な明細1行目 = 'r0'
 */
function insert_new_detail( company_id, ref_row ) 
{
  var tbl = $('#detail')[0];

  // 行数
  var rid = $('#lines')[0].value;

  $.ajax({
    url: "/invoices/insert_new_detail?company=" + String(company_id) +
                            "&ref=" + ref_row + "&newid=" + String(rid),
    success: function(data) {
             },
    error: function() {
             alert("insert_new_detail: 通信エラーが発生しました");
           }
  });
}


/** モーダルダイアログ: 更新ボタン */
function update_dim2() 
{
  var line = $('#sel_line')[0].value;

  $.ajax({
      url: "/invoices/update_dim2",
      data: {
        line: line,
        tax: $('#sel_tax')[0] ? $('#sel_tax')[0].value : null,
        dim2: $('#sel_dim2')[0].value
      },
      success: function(data) {
               },
      error: function() {
               alert("update_dim2: 通信エラーが発生しました");
             }
  });
}
