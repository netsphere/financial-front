"use strict";


/** 検索ウィンドウを開く */
function open_sel_bank() 
{
  $.ajax({
    url: "/bank_accounts/open_sel_bank",
    data: {
      bank: 0,
      branch: 0
    },
    success: function(data) {
             },
    error: function() {
             alert("open_sel_bank(): 通信エラーが発生しました");
           }
  });
}


function list_banks()
{
  $.ajax({
    url: "/bank_accounts/list_banks",
    data: {
      bank: $('#bank_text').val(),
      branch: $('#branch_text').val()
    },
    success: function(data) {
             },
    error: function() {
             alert("open_sel_bank(): 通信エラーが発生しました");
           }
  });
}


function update_bank( branch_id )
{
  $.ajax({
      url: "/bank_accounts/update_bank",
      data: {
        branch: branch_id
      },
      success: function(data) {
               },
      error: function() {
               alert("update_bank(): 通信エラーが発生しました");
             }
  });
}
