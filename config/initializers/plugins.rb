
Dir.glob( "#{Rails.root}/vendor/plugins/*" ) do |fname|
  #puts fname
  if File.directory?(fname)
    $LOAD_PATH << fname + "/lib"
    require fname + "/init"
  end
end
