# -*- coding:utf-8 -*-

# Be sure to restart your server when you modify this file.

#Rails.application.config.session_store :cookie_store, 
#                                       key: '_KeiriFront_session'

require "rails_sup/rails_session_manager"
require "wafu2/session/file_storage"

Rails.application.configure do |app|
  app.config.session_store Wafu::Session::RailsSessionManager,
                  :database_manager => {
                            :class => Wafu::Session::FileStorage,
                            :dir => Rails.root.to_s + "/tmp/sessions",
                            :holdtime => (60 * 60) * 3
                          },
                  :key => '_session_id',     # CGI::Sessionと合わせる
                  :path => "/"
end
