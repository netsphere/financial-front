# -*- coding: utf-8 -*-

module Messages
  TOP_PAGE = <<EOF
<p>(2014.4.30)<br />
トップメニューに表示するテキスト。
EOF


  INVOICE_FOOTER = <<EOF
<p>支払依頼の下部に表示するテキスト
EOF

end # module Messages
