# -*- mode:ruby; coding:utf-8 -*-

# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


Rails.application.routes.draw do

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  # Render dynamic PWA files from app/views/pwa/*
  get "service-worker" => "rails/pwa#service_worker", as: :pwa_service_worker
  get "manifest" => "rails/pwa#manifest", as: :pwa_manifest


  # You can have the root of your site routed with "root"
  root 'welcome#index'


  ########################
  # グローバルマスタ

  # アカウント
  resource 'account', controller: "account" do
    match 'sign_in', via:[:get, :post]
    post 'sign_out'

    get 'edit_password'
    post 'update_password'
  end

  # 大域設定
  # コントローラは `GlobalSettingsController` (複数形)
  resource 'global_settings' do
    get 'show_specials'
  end

  # 経理メニュー
  resource 'keiri', controller: "keiri" do
    collection do
      get 'export'
      get 'export_old_gl'

      get 'import_account_titles'
      get 'import_account_titles_log'

      get 'import_gl'
      get 'import_gl_log'

      get 'import_partners'
      get 'import_partners_log'

      get 'import_banks'
    end
  end

  resources 'currencies' do
    collection do
      get 'rate'
    end
  end

  resources 'companies' do
    member do
      get 'gl_fix'
    end
  end

  # 銀行
  resources 'banks' do
    collection do
      get 'list'
      get 'search'
    end
  end
  
  resources 'bank_branches'


  ########################
  # dimensions
  
  # 会計部門
  resources 'divisions' do
    collection do
      get 'search'
      get :select_addable
    end
  end
  
  resources 'control_divisions' do
    collection do
      get 'new_root'
    end
    member do
      get 'edit_root'
    end
  end

  # 次元2
  resources 'dimensions'

  # 勘定科目
  resources 'account_titles' do
    collection do
      get 'list'
      get 'search'
      get 'select_addable'   # ● どんなUI?
    end
  end
  
  resources 'account_title_trees'
  
  # 3. 元帳区分
  resources 'kubuns'

  # 4. 消費税・VAT
  resources 'vats'

  
  ########################
  # 取引先
  
  resources 'partners' do
    collection do
      get 'search'
    end

    member do
      get 'bank_accounts', to: "bank_accounts#list"
      get 'bp_addresses', to: "bp_addresses#list"
    end
  end

  resources 'credit_inquiries'

  resources 'bank_accounts'

  resources 'en_bank_accounts'

  resources 'bp_addresses'

  
  ########################
  # 社員

  resources 'users'

  # 人事メニュー
  resource 'human_resources', controller: "human_resources"

  # 役職
  resources 'worker_positions'

  resources 'qualifications'

  
  ########################
  # 販売

  resources 'our_contacts'

  resources 'our_banks'

  # 販売請求書
  resources 'sales_invoices'

  # 債権管理
  resources 'receivings'

  
  ########################
  # 購買・経費

  resources 'invoices'

  resources 'import_declarations'

  resources 'mat_receivings'

  # 経費精算リポート
  resources 'petty_expenses'

  resources 'petty_pays'

  # 小口精算関係のマスタ
  resources 'petty_groups'

  resources 'petty_payees'

  resources 'petty_cash_advances'

  
  ########################
  # 資材

  resources 'mat_parts'
  

  ########################
  # 資金

  resources 'payment_slips'

  resources 'receiving_slips'

  
  #####################################
  # 予算実績管理

  # 総勘定元帳
  resource 'general_ledger', controller: "general_ledger" do
    collection do
      get 'select_ledger'
    end
  end

  resources 'reports'


  #####################################
  # 添付ファイル

  resources 'attach_kinds'

  resources 'attach_files'

  
  #####################################
  # データ連携

  resource 'sbo', controller: "sbo" do
    collection do
      get 'export_bp_master'
      get 'export_bp_master_log'
    end
  end

  resource 'taisho', controller: "taisho"

end
