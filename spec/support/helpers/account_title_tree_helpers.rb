module AccountTitleTreeHelpers

  def childCount(ac_col)
    rowCnt = 0
    ac1000 = ac2000 = ac9000 = acetc = 0
    ac_col.count.times do
      ac1000 += 1 if ac_col[rowCnt][chkColumn] == match1000
      ac2000 += 1 if ac_col[rowCnt][chkColumn] == match2000
      ac9000 += 1 if ac_col[rowCnt][chkColumn] == match9000
      acetc += 1 if ac_col[rowCnt][chkColumn] == matchetc
      rowCnt+=1
    end
    return {:ac1000 => ac1000, :ac2000 => ac2000, :ac9000 => ac9000, :acetc => acetc} 
  end

  def childNum(ac_col)
    rowCnt = 0
    ac1000 = ac2000 = ac9000 = []
    ac_col.count.times do
      ac1000 << ac_col[rowCnt][cidColumn] if ac_col[rowCnt][chkColumn] == match1000
      ac2000 << ac_col[rowCnt][cidColumn] if ac_col[rowCnt][chkColumn] == match2000
      ac9000 << ac_col[rowCnt][cidColumn] if ac_col[rowCnt][chkColumn] == match9000
      rowCnt+=1
    end
    return {:ac1000 => ac1000.sort.uniq, :ac2000 => ac2000.sort.uniq, :ac9000 => ac9000.sort.uniq} 
  end

  def getRoot(ac_col)
    rowCnt = 0 ; rootNum = []
    ac_col.count.times do
      rootNum << ac_col[rowCnt][chkColumn]
      rowCnt+=1
    end
    return rootNum.sort.uniq
  end

  def getChild
    rowCnt = 0 ; i = 0 ; childNum = [] ; invoice_child = [] ; si_invoice_child = []
    2.times do
      AccountTitleTree.addableTree(i).count.times do
        childNum << AccountTitleTree.addableTree(i)[rowCnt][chkColumn]
        rowCnt+=1
      end
      invoice_child = childNum.sort.uniq if i == 0 ; si_invoice_child = childNum.sort.uniq  if i == 1
      childNum = []
      rowCnt = 0
      i+=1
    end
    return {:invoice_child => invoice_child, :si_invoice_child => si_invoice_child}
  end

  def getAllChild
    ac1000 = ac2000 = ac9000 = 0
    ac1000 = cnt_sql1.where('account_code between 101 and 103').count
    ac2000 = cnt_sql1.where('account_code between 106 and 201 or account_code in(214)').count
    ac9000 = cnt_sql1.where('account_code between 202 and 213 or account_code in(104,105)').count
    return {:ac1000 => ac1000, :ac2000 => ac2000, :ac9000 => ac9000} 
  end

end
