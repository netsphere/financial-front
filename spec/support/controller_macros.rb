module ControllerMacros
  def login
    set_current_user User.authenticate("testuser", "transnet")
  end

  def login2
    FactoryBot.create(:user,
      id: '2',
      name: 'テストユーザ2',
      kana_name: 'テストユーザ二',
      created_at: '2019-02-04 15:51:36.057454',
      login: 'testuser2',
      email: 'testuser2@transnet.test',
      worker_code: '2001')
    set_current_user User.authenticate("testuser2", "transnet")
  end

end
