require 'rails_helper'

describe Division, type: :model  do
  fixtures :account_titles
  fixtures :account_title_trees
  fixtures :controllers_divisions
  fixtures :control_divisions
  fixtures :divisions
  fixtures :company_settings
  fixtures :company
  fixtures :global_settings

  let(:company) {Company.find(i)}
  let(:i){1}
  subject {Division.addable company}
      
  describe "addableメソッド（購買・販売請求書共通）" do
  
    context '会社' do

      context '会計部門データが存在する会社' do
        it "会計部門データ取得（3件）" do
          expect(subject.count).to eq 3
        end
      end
      
      context '会計部門データが存在しない会社' do
        let(:i){2}
        it "0件で返ってくる" do
          expect(subject.count).to eq 0
        end
      end
        
      context '存在しない会社' do
        let(:i){3}
        it "ActiveRecord::RecordNotFoundが帰ってくる" do
          expect{subject}.to raise_error(ActiveRecord::RecordNotFound)
        end
      end
        
      context '無効ステータスとなっている会社' do
        let(:i){2}
        it "0件で返ってくる" do
          Company.where("id=2").update_all(active: 'false')
          expect(subject.count).to eq 0
        end
      end
        
      context '不正な会社ID（数字以外）' do
        let(:i){"ABC"}
        it "ActiveRecord::RecordNotFoundが帰ってくる" do
          expect{subject}.to raise_error(ActiveRecord::RecordNotFound)
        end
      end

    end

    context '大域設定（木-bumon）' do
      context 'コストセンタ上に存在し大域設定にあるIDが指定されている' do
        it "会計部門データ取得（3件）" do
          expect(subject.count).to eq 3
        end
      end
      
      context 'コストセンタ上に存在し大域設定にある他のIDが指定されている' do
        it "会計部門データ取得（1件）" do
          GlobalSetting.where(id: 1).update_all(default_division_tree_root_id: '6')
          expect(subject.count).to eq 1
        end
      end
      
      context 'コストセンタ上に存在するが大域設定に無いIDが指定されている' do
        it "0件で返ってくる" do
          GlobalSetting.where(id: 1).update_all(default_division_tree_root_id: '3')
          expect(subject.count).to eq 0
        end
      end
        
    end

    context 'Divisionテーブル固有ステータス' do
      context 'end_dayが全データに入っている' do
        it "0件で返ってくる" do
          Division.update_all(end_day: '2018-12-31')
          expect(subject.count).to eq 0
        end
      end

      context '請求書追加可否コードが前レコードで非アクティブ' do
        it "0件で返ってくる" do
          Division.update_all(purchase_active: 0)
          expect(subject.count).to eq 0
        end
      end

    end

  end
end
