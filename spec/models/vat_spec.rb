require 'rails_helper'

describe Vat, type: :model  do
  describe 'validations', focus: true do

    describe 'sbo_code' do
      context 'presence' do
        it 'blank' do
          vat = FactoryBot.build(:vat, :purchase, sbo_code: '')
          vat.valid?
          expect(vat.errors.messages[:sbo_code]).to include("can't be blank")
        end
      end

      context 'length' do
        it 'too long' do
          vat = FactoryBot.build(:vat, :purchase, sbo_code: 'JP123456')
          expect(vat.valid?).not_to be true
        end

        it 'just 6' do
          vat = FactoryBot.build(:vat, :purchase, sbo_code: 'JP1234')
          expect(vat.valid?).to be true
        end

        it 'less than 6' do
          vat = FactoryBot.build(:vat, :purchase, sbo_code: 'JP123')
          expect(vat.valid?).to be true
        end
      end
    end
  end
end
