require 'rails_helper'

describe SalesInvoice, type: :model  do

  let(:sinv){FactoryBot.create(:sales_invoice)}
  subject {sinv}

  #scopeテスト用
  fixtures :controllers_divisions
  fixtures :sales_invoices
  fixtures :si_details
  fixtures :si_journals
  fixtures :users_divisions
  fixtures :divisions
  fixtures :dimensions
  fixtures :our_banks
  fixtures :partners
  fixtures :account_titles
  
  describe "scope" do
    let!(:sinv) { sales_invoices(:sinv01) }
    let!(:sinv_all) {SalesInvoice.count}
    
    describe "si_number" do
      subject { SalesInvoice.si_number(si_number) }
      let!(:sinv) { sales_invoices(:sinv03) }
      context "DBに存在する請求書番号が渡された場合" do
        let!(:si_number) {'1000000001'}
        it "検索結果に含まれること" do
          expect(subject).to include sinv
        end
        it "1件抽出されること" do
        
          expect(subject.count).to eq 1
        end
      end
      context "DBと一部が一致する内容が渡された場合" do
        let!(:si_number) {'0000000'}
        it "検索結果に含まれないこと" do
          expect(subject.where(si_number: si_number).count).to eq 0
        end
        it "0件になること" do
          expect(subject.count).to eq 0
        end
      end
      context "空値が渡された場合" do
        let!(:si_number) {''}
        it "検索結果に含まれないこと" do
          expect(subject.where(si_number: si_number).count).to eq 0
        end
        it "全件抽出されること" do
          expect(subject.count).to eq sinv_all
        end
      end
      context "nilが渡された場合" do
        let!(:si_number) {nil}
        it "検索結果に含まれないこと" do
          expect(subject.where(si_number: si_number).count).to eq 0
        end
        it "全件抽出されること" do
          expect(subject.count).to eq sinv_all
        end
      end
    end
    
    describe "remarks" do
      subject { SalesInvoice.with_si_journal.remarks('%' + remarks.to_s + '%') }
      let!(:sjrn_all) {SiJournal.count}
      context "DBと一致する内容が渡された場合" do
        let!(:remarks) {'テスト199'}
        it "検索結果に含まれること" do
          expect(subject.select("si_journals.remarks").map(&:remarks).grep(/#{remarks}/)).to include remarks
        end
        it "1件抽出されること" do
        
          expect(subject.count).to eq 1
        end
      end
      context "DBと一部が一致する内容が渡された場合" do
        let!(:remarks) {'テスト1'}
        it "検索結果に含まれること" do
          expect(subject.select("si_journals.remarks").map(&:remarks).grep(/#{remarks}/)).to include remarks
        end
        it "複数件抽出されること" do
          expect(subject.count).to be > 2
        end
      end
      context "存在しない内容が渡された場合" do
        let!(:remarks) {'テストテストテスト'}
        it "検索結果に含まれないこと" do
          expect(subject.count).to eq 0
        end
        it "0件になること" do
          expect(subject.count).to eq 0
        end
      end
      context "空値が渡された場合" do
        let!(:remarks) {''}
        it "検索結果に含まれる" do
          expect(subject.count).not_to eq 0
        end
        it "全件抽出されること" do
          expect(subject.count).to eq sjrn_all
        end
      end
      context "nilが渡された場合" do
        let!(:remarks) {nil}
        it "検索結果に含まれる" do
          expect(subject.count).not_to eq 0
        end
        it "全件抽出されること" do
          expect(subject.count).to eq sjrn_all
        end
      end
    end
  end

  describe "validation" do
    describe "JE番号" do
      context '入力規則' do
        it "JE番号があれば登録できる" do
          sinv.je_number = "312345"
          sinv.save
          is_expected.to be_valid
        end
    
        it "JE番号がなくても登録できる" do
          sinv.je_number = nil
          sinv.save
          is_expected.to be_valid
        end
    
      end
    
      context '文字数' do
        it "入力長が最大のときは登録できる" do
          sinv.je_number = "a" * 6
          expect {sinv.save}.not_to raise_error(ActiveRecord::StatementInvalid)
        end
        
        it "入力長が短い（4文字）でも登録できる" do
          sinv.je_number = "a" * 4
          expect {sinv.save}.not_to raise_error(ActiveRecord::StatementInvalid)
        end
        
        it "入力長が最大のときは登録できない" do
          sinv.je_number = "a" * 7
          expect {sinv.save}.to raise_error(ActiveRecord::StatementInvalid)
        end
      end
    
      context '文字種' do
        context '通常文字は全てエラー' do
          it "マルチバイト文字" do
            sinv.je_number = "３１２３４５"
            is_expected.to be_invalid
          end
          
           it "英字、カナ" do
            sinv.je_number = "ＴＥＳＴテス"
            is_expected.to be_invalid
          end
          
          it "カタカナ" do
            sinv.je_number = "テストアカウ"
            is_expected.to be_invalid
          end
          
          it "半角カタカナ" do
            sinv.je_number = "ﾃｽﾄｱｶｳ"
            is_expected.to be_invalid
          end
        end
      
        context "symbolも全てエラー" do
          it "-=^~¥|" do
            sinv.je_number = "-=^~¥|"
            is_expected.to be_invalid
          end
            
          it "_/?.>," do
            sinv.je_number = "_/?.>,"
            is_expected.to be_invalid
          end
            
          it "<!¥$%&" do
            sinv.je_number = "<!¥$%&"
            is_expected.to be_invalid
          end
            
          it "'()@`[" do
            sinv.je_number = "'()@`["
            is_expected.to be_invalid
          end
            
          it "{]}:*;" do
            sinv.je_number = "{]}:*;"
            is_expected.to be_invalid
          end
            
          it "+" do
            sinv.je_number = "+"
            is_expected.to be_invalid
          end
            
          it "XSSチェック" do
            sinv.je_number = %("#)
            is_expected.to be_invalid
          end
        end
      end
    end
  end
  
  describe "methods" do
    describe "最初の明細行の内容" do
      context "請求明細" do
        it "存在する場合、明細1行目内容を返す" do
          expect(sinv.first_detail_remarks).to eq(sinv.si_journals.first.remarks)
        end
        it "存在しない（空値）場合でもそのまま返す" do
          sinv = FactoryBot.build(:sales_invoice)
          sinv.si_journals.first.remarks = ''
          sinv.save
          expect(sinv.first_detail_remarks).to eq ''
        end
      end
    end
  end

  describe "経理メモ" do

    let(:sinv){FactoryBot.create(:sales_invoice)}
    subject {sinv}
    
    context '入力規則' do
      it "経理メモがあれば登録できる" do
        sinv.keiri_memo = "ああああ\r\nいいいい\r\nうううう"
        is_expected.to be_valid
      end

      it "経理メモがなくても登録できる" do
        sinv.keiri_memo = nil
        is_expected.to be_valid
      end
    end

    context '文字数' do
      it "入力長が最大のときは登録できる" do
        sinv.keiri_memo = "a" * 200
        is_expected.to be_valid
      end
      
      it "入力長が最大を超えた時は登録できない" do
        sinv.keiri_memo = "a" * 201
        is_expected.to be_invalid
      end
    end

    context '文字種' do
      context '通常文字' do
        it "マルチバイト文字のとき" do
          sinv.keiri_memo = "ＴＥＳＴテスト"
          is_expected.to be_valid
        end
        
        it "カタカナのとき" do
          sinv.keiri_memo = "テストアカウント"
          is_expected.to be_valid
        end
        
        it "半角カタカナのとき" do
          sinv.keiri_memo = "ﾃｽﾄｱｶｳﾝﾄ"
          is_expected.to be_valid
        end
      end
    
      context "symbol" do
        it "-=^~¥|_/?.>,<" do
          sinv.keiri_memo = "-=^~¥|_/?.>,<"
          is_expected.to be_valid
        end
          
        it "!¥$%&'()" do
          sinv.keiri_memo = "!¥$%&'()"
          is_expected.to be_valid
        end
          
        it "@`[{]}:*;+" do
          sinv.keiri_memo = "@`[{]}:*;+"
          is_expected.to be_valid
        end
          
        it "XSSチェック" do
          sinv.keiri_memo = %("#)
          is_expected.to be_valid
        end
      end
    end

  end
end
