require 'rails_helper'

#require File.expand_path("spec/support/helpers/account_title_tree_helpers.rb") <- in rails_helper.rb
describe AccountTitleTree, type: :model  do
  fixtures :account_titles
  fixtures :account_title_trees

  let(:ac_collection){AccountTitleTree.addableTree(i)}
  let(:cnt_sql1){AccountTitle.
                 where("end_day is null").
                 where("id in (?)", AccountTitleTree.
                 select("distinct(parent_id) from account_title_trees \
                  union all select distinct((child_id) )"))}
  let(:aggregate_on){cnt_sql1.where("aggregate_flag=true")}
  let(:aggregate_off){cnt_sql1.where("aggregate_flag=false")}
  let(:sql1000){inv_or_sinv.where('account_code between 101 and 103')}
  let(:sql2000){inv_or_sinv.where('account_code between 106 and 201 or account_code in(214)')}
  let(:sql9000){inv_or_sinv.where('account_code between 202 and 213 or account_code in(104,105)')}
  let(:match1000){1000}
  let(:match2000){2000}
  let(:match9000){9000}
  let(:matchetc){999999}
  let(:nonRootChild){[1,2]}
  let(:cidColumn){"child_id"}
  let(:execChildCount){childCount(ac_collection)}
 
  describe "addableTreeメソッド" do
    let(:chkColumn){"root_account_code"}

    # テストケース雛形（書式上、先に書く）

    shared_examples_for 'コントローラ検索結果の検証' do
      subject{execChildCount}
      context 'ルート(1000)' do
        it "Specの検索結果と等しいこと" do
          expect(subject[:ac1000]).to eq sql1000.count
        end
      end
      context 'ルート(2000)' do
        it "Specの検索結果と等しいこと" do
          expect(subject[:ac2000]).to eq sql2000.count
        end
      end
      context 'ルート(9000)' do
        it "Specの検索結果と等しいこと" do
          expect(subject[:ac9000]).to eq sql9000.count
        end
      end
    end
    
    shared_examples_for '表示される子の数' do
      subject{execChildCount}
      context 'ルート(1000)' do
        it{expect(subject[:ac1000]).to eq childs[:ac1000]}
      end
      context 'ルート(2000)' do
        it{expect(subject[:ac2000]).to eq childs[:ac2000]}
      end
      context 'ルート(9000)' do
        it{expect(subject[:ac9000]).to eq childs[:ac9000]}
      end
    end
    
    shared_examples_for '表示される親の数' do
      subject{getRoot(ac_collection)}
      it{expect(subject.length).to eq parents}
    end
    
    shared_examples_for '自身の請求書種別のルートのみ存在する' do
      subject{getRoot(ac_collection)}
      it{expect(subject).to eq parents_account_id}
    end
    
    shared_examples_for '異なる請求書種別のルートは存在しない' do
      subject{getRoot(ac_collection)}
      it{expect(subject).not_to eq not_include_parents_account_id}
    end
    
    shared_examples_for 'ルートに属さない子は存在しない(1000)' do
      subject{childNum(ac_collection)}
      it{expect((subject[:ac1000] & nonRootChild).size).to eq 0}
    end
    
    shared_examples_for 'ルートに属さない子は存在しない(2000)' do
      subject{childNum(ac_collection)}
      it{expect((subject[:ac2000] & nonRootChild).size).to eq 0}
    end
    
    shared_examples_for 'ルートに属さない子は存在しない(9000)' do
      subject{childNum(ac_collection)}
      it{expect((subject[:ac9000] & nonRootChild).size).to eq 0}
    end

    shared_examples_for 'ルートに属さない子が2つ存在する' do
      subject{childCount(ac_col)}
      it{expect(subject[:acetc]).to eq 2}
    end
    
    shared_examples_for '購買／販売請求書共通テストケース' do
      context 'コントローラ検索結果の検証' do
        it_behaves_like 'コントローラ検索結果の検証'
      end
      context '表示される子の数' do
        it_behaves_like '表示される子の数'
      end
      context '表示される親の数' do
        it_behaves_like '表示される親の数'
      end
      context '自身の請求書種別のルートのみ存在する' do
        it_behaves_like '自身の請求書種別のルートのみ存在する'
      end
      context '異なる請求書種別のルートは存在しない' do
        it_behaves_like '異なる請求書種別のルートは存在しない'
      end
      context 'ルートに属さない子は存在しない(1000)' do
        it_behaves_like 'ルートに属さない子は存在しない(1000)'
      end
      context 'ルートに属さない子は存在しない(2000)' do
        it_behaves_like 'ルートに属さない子は存在しない(2000)'
      end
      context 'ルートに属さない子は存在しない(9000)' do
        it_behaves_like 'ルートに属さない子は存在しない(9000)'
      end
      context 'ルートに属さない子が2つ存在する' do
        it_behaves_like 'ルートに属さない子が2つ存在する'
      end
    end        
    
    ## 以下からテストケース
    describe '購買請求書画面からのアクセス' do
      let(:i){0}
      let(:inv_or_sinv){aggregate_off.where("enable_invoice=true").where("kind = 0")}
      let(:childs){{:ac1000 => 1, :ac2000 => 0, :ac9000 => 9}}
      let(:parents){3}
      let(:parents_account_id){[1000,9000,999999]}
      let(:not_include_parents_account_id){[2000]}
      let(:ac_col){AccountTitleTree.addableTree(0)}
      context '購買／販売請求書共通テストケース' do
        it_behaves_like '購買／販売請求書共通テストケース'
      end
      context '個別テストケース' do
        context '正常系' do
          context '購買／販売請求書それぞれに自分の種別以外の子はその他以外いない' do
            let(:chkColumn){"child_id"}
            subject{getChild}
            it{expect(subject[:invoice_child] & subject[:si_invoice_child]).to include(23)}
          end
          context '表示されていない子の数' do
            subject{getAllChild}
            it{expect(subject[:ac1000] - execChildCount[:ac1000]).to eq 2}
            it{expect(subject[:ac9000] - execChildCount[:ac9000]).to eq 5}
          end
          context '親と孫のいる子は存在しない' do
            let(:cidColumn){"child_id"}
            subject{childNum(ac_collection)}
            it{expect((subject[:ac1000] & [101,102]).size).to eq 0}
            it{expect((subject[:ac9000] & [104,203,105,205,211]).size).to eq 0}
          end
          context 'ルート直下で孫のいない子' do
            let(:cidColumn){"child_id"}
            subject{childNum(ac_collection)}
            it 'ルート9000に1個存在する' do
              expect((subject[:ac9000] & [204]).size).to eq 1
            end
          end
        end
        
        context 'イレギュラー／異常系' do
          context 'ルート1000の親子全ての販売請求フラグを1にする' do
            before do
              AccountTitle.where("id in('1000','101','102','103')").update_all("enable_si_invoice=true")
            end
            subject{childCount(ac_col)}
            it '全て問題なく表示されること' do
              expect(subject[:ac1000]).to eq childs[:ac1000]
            end
          end
          context 'ルート1000の親子全ての種別を1にする' do
            before do
              AccountTitle.where("id in('1000','101','102','103')").update_all("kind=1")
            end
            context 'ルートの存在を確認' do
              subject{getRoot(ac_col)}
              it '表示されなくなること' do
                expect((subject & [1000]).size).to eq 0
              end
            end 
            context '子の存在を確認' do
              subject{childCount(ac_col)}
              it '表示されなくなること' do
                expect(subject[:ac1000]).to eq 0
              end
            end
          end
          context '1000の種別0に戻し、終了日に本日以前の日付を入れる' do
            before do
              AccountTitle.where("id in('1000','101','102','103')").update_all("end_day='2019-02-01'")
            end
            context 'ルートの存在を確認' do
              subject{getRoot(ac_col)}
              it '表示されなくなること' do
                expect((subject & [1000]).size).to eq 0
              end
            end 
            context '子の存在を確認' do
              subject{childCount(ac_col)}
              it '表示されなくなること' do
                expect(subject[:ac1000]).to eq 0
              end
            end
          end
          context '1000の終了日を消し子全ての購買請求フラグを0にする' do
            before do
              AccountTitle.where("id in('1000','101','102','103')").update_all("enable_invoice=false")
            end
            context 'ルートの存在を確認' do
              let(:chkColumn){"account_code"}
              subject{getRoot(ac_col)}
              it '表示されなくなること' do
                expect((subject & [1000]).size).to eq 0
              end
            end 
            context '子の存在を確認' do
              subject{childCount(ac_col)}
              it '表示されなくなること' do
                expect(subject[:ac1000]).to eq 0
              end
            end
          end
          context '1000の子に9000を登録する' do
            before do
              AccountTitleTree.create(parent_id: 1000, child_id: 9000)
            end
            context 'ルートの存在を確認' do
              subject{getRoot(ac_col)}
              it '1000のみ表示される' do
                expect((subject & [1000,9000]).size).to eq 1
              end
            end 
            context '子の存在を確認' do
              subject{childCount(ac_col)}
              it '1000と9000の子が全て表示される' do
                expect(subject[:ac1000]).to eq 10
              end
            end
          end
        end
      end
    end

    describe '販売請求書画面からのアクセス' do
      let(:i){1}
      let(:inv_or_sinv){aggregate_off.where("enable_si_invoice=true")}
      let(:childs){{:ac1000 => 0, :ac2000 => 3, :ac9000 => 0}}
      let(:parents){2}
      let(:parents_account_id){[2000,999999]}
      let(:not_include_parents_account_id){[1000,9000]}
      let(:ac_col){AccountTitleTree.addableTree(1)}
      
      context '購買／販売請求書共通テストケース' do
        it_behaves_like '購買／販売請求書共通テストケース'
      end
      context '個別テストケース' do
        context '正常系' do
          context '表示されていない子の数' do
            subject{getAllChild}
            it{expect(subject[:ac2000] - execChildCount[:ac2000]).to eq 2}
          end
          context '親と孫のいる子は存在しない' do
            let(:cidColumn){"child_id"}
            subject{childNum(ac_collection)}
            it{expect((subject[:ac2000] & [106,107]).size).to eq 0}
          end
          context 'ルート直下で孫のいない子が1個存在する' do
            subject{childNum(ac_collection)}
            it{expect((subject[:ac2000] & [108]).size).to eq 1}
          end
        end
        
        context 'イレギュラー／異常系' do
          context '親子全ての購買請求フラグを1にする' do
            before do
              AccountTitle.where("id in('2000','106','107','108','201','214')").update_all("enable_invoice=true")
            end
            subject{childCount(ac_col)}
            it '全て問題なく表示されること' do
              expect(subject[:ac2000]).to eq childs[:ac2000]
            end
          end
          context 'ルート2000の親子全ての種別を0にする' do
            before do
              AccountTitle.where("id in('2000','106','107','108','201','214')").update_all("kind=0")
            end
            context 'ルートの存在を確認' do
              subject{getRoot(ac_col)}
              it '問題なく表示されること' do
                expect((subject & [2000]).size).to eq 1
              end
            end 
            context '子の存在を確認' do
              subject{childCount(ac_col)}
              it '問題なく表示されること' do
                expect(subject[:ac2000]).to eq 3
              end
            end
          end
          context '2000の107終了日に本日以前の日付を入れる' do
            before do
              AccountTitle.where("id ='107'").update_all("end_day='2019-02-01'")
            end
            subject{childNum(ac_col)}
            it '214が表示されないこと' do
              expect((subject[:ac2000] & [214]).size).to eq 0
            end
          end
          context '2000の106集計フラグを0にする' do
            before do
              AccountTitle.where("id ='106'").update_all("aggregate_flag = false")
            end
            context '子の存在を確認' do
              subject{childNum(ac_col)}
              it '106は表示される' do
                expect((subject[:ac2000] & [106]).size).to eq 1
              end
              it '元の子である107,201,214は表示されないこと' do
                expect((subject[:ac2000] & [107,201,214]).size).to eq 0
              end
            end 
          end
          context '2000の108集計フラグを1にする' do
            before do
              AccountTitle.where("id ='108'").update_all("aggregate_flag = true")
            end
            context '子の存在を確認' do
              subject{childNum(ac_col)}
              it '108が表示されないこと' do
                expect((subject[:ac2000] & [108]).size).to eq 0
              end
            end 
          end
          context '2000の集計フラグを0にする' do
            before do
              AccountTitle.where("id = 2000").update_all("aggregate_flag = false")
            end
            context 'ルートの存在を確認' do
              subject{getRoot(ac_col)}
              it '2000は存在しないこと' do
                expect((subject & [2000]).size).to eq 0
              end
            end 
            context '子の存在を確認' do
              subject{childNum(ac_col)}
              it '2000配下の子も全て表示されないこと' do
                expect((subject[:ac2000] & [106,107,108,201,214]).size).to eq 0
              end
            end 
          end
          context '親子全ての販売請求フラグを0にする' do
            before do
              AccountTitle.where("id in (106,107,108,201,214,2000)").update_all("enable_si_invoice = false")
            end
            context 'ルートの存在を確認' do
              subject{getRoot(ac_col)}
              it '2000は存在しないこと' do
                expect((subject & [2000]).size).to eq 0
              end
            end 
            context '子の存在を確認' do
              subject{childNum(ac_col)}
              it '2000配下の子も全て表示されないこと' do
                expect((subject[:ac2000] & [106,107,108,201,214]).size).to eq 0
              end
            end 
          end
        end
      end
    end
    
  end
end
