require 'rails_helper'

describe Invoice, type: :model  do

  #scopeテスト用
  fixtures :invoices
  fixtures :invoice_details
  fixtures :partners
  fixtures :account_titles
  fixtures :users_divisions
  fixtures :divisions
  fixtures :dimensions
  fixtures :our_banks

  describe "scope" do
    let!(:inv) { invoices(:inv01) }
    let!(:inv_all) {Invoice.count}
    
    describe "partner_id" do
      subject { Invoice.partner_id(id) }
      context "存在する取引先の値が渡された場合" do
        let!(:inv) { invoices(:inv01) }
        let!(:id){'1'}
        it "検索結果に含まれること" do
          is_expected.to include inv
        end
      end
      context "存在しない取引先の値が渡された場合" do
        let!(:id) {'0'}
        it "検索結果に含まれないこと" do
          expect(subject.where(partner_id: id).count).to eq 0
        end
        it "0件になること" do
          expect(subject.count).to eq 0
        end
      end
      context "空値が渡された場合" do
        let!(:id) {''}
        it "検索結果に含まれないこと" do
          expect(subject.where(partner_id: id).count).to eq 0
        end
        it "全件抽出されること" do
          expect(subject.count).to eq inv_all
        end
      end
      context "nilが渡された場合" do
        let!(:id) { nil }
        it "検索結果に含まれないこと" do
          expect(subject.where(partner_id: id).count).to eq 0
        end
        it "全件抽出されること" do
          expect(subject.count).to eq inv_all
        end
      end
    end

    describe "company_setting_id" do
      subject { Invoice.company_setting_id(id) }
      context "存在する会社IDが渡された場合" do
        let!(:inv) { invoices(:inv01) }
        let!(:id){'1'}
        it "検索結果に含まれること" do
          is_expected.to include inv
        end
      end
      context "存在しない取引先の値が渡された場合" do
        let!(:id) {'0'}
        it "検索結果に含まれないこと" do
          expect(subject.where(company_setting_id: id).count).to eq 0
        end
        it "0件になること" do
          expect(subject.count).to eq 0
        end
      end
      context "空値が渡された場合" do
        let!(:id) {''}
        it "検索結果に含まれないこと" do
          expect(subject.where(company_setting_id: id).count).to eq 0
        end
        it "全件抽出されること" do
          expect(subject.count).to eq inv_all
        end
      end
      context "nilが渡された場合" do
        let!(:id) { nil }
        it "検索結果に含まれないこと" do
          expect(subject.where(company_setting_id: id).count).to eq 0
        end
        it "全件抽出されること" do
          expect(subject.count).to eq inv_all
        end
      end
    end

    describe "account_title_id" do
      let!(:idtl_all) {InvoiceDetail.count}
      subject { Invoice.with_invoice_detail.account_title_id(id) }
      context "存在する勘定科目IDが渡された場合" do
        let!(:id){'100'}
        it "検索結果に含まれること" do
          is_expected.to include inv
        end
      end
      context "存在しない取引先の値が渡された場合" do
        let!(:id) {'1000'}
        it "検索結果に含まれないこと" do
          expect(subject.where("cast(invoice_details.account_title_id as text) = ?", id).count).to eq 0
        end
        it "0件になること" do
          expect(subject.count).to eq 0
        end
      end
      context "空値が渡された場合" do
        let!(:id) {''}
        it "検索結果に含まれないこと" do
          expect(subject.where("cast(invoice_details.account_title_id as text) = ?", id).count).to eq 0
        end
        it "0件になること" do
          expect(subject.count).to eq 0
        end
      end
      context "nilが渡された場合" do
        let!(:id) { nil }
        it "検索結果に含まれないこと" do
          expect(subject.where("cast(invoice_details.account_title_id as text) = ?", id).count).to eq 0
        end
        it "0件になること" do
          expect(subject.count).to eq 0
        end
      end
    end

    describe "je_number" do
      subject { Invoice.je_number(je_number) }
      let!(:inv) { invoices(:inv01) }
      context "DBに存在するJE番号が渡された場合" do
        let!(:je_number) {'999901'}
        it "検索結果に含まれること" do
          expect(subject).to include inv
        end
        it "1件抽出されること" do
        
          expect(subject.count).to eq 1
        end
      end
      context "DBと一部が一致する内容が渡された場合" do
        let!(:je_number) {'9999'}
        it "検索結果に含まれないこと" do
          expect(subject.where(je_number: je_number).count).to eq 0
        end
        it "0件になること" do
          expect(subject.count).to eq 0
        end
      end
      context "空値が渡された場合" do
        let!(:je_number) {''}
        it "検索結果に含まれないこと" do
          expect(subject.where(je_number: je_number).count).to eq 0
        end
        it "全件抽出されること" do
          expect(subject.count).to eq inv_all
        end
      end
      context "nilが渡された場合" do
        let!(:je_number) {nil}
        it "検索結果に含まれないこと" do
          expect(subject.where(je_number: je_number).count).to eq 0
        end
        it "全件抽出されること" do
          expect(subject.count).to eq inv_all
        end
      end
    
    end
  end
  
end
