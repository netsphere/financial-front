require 'rails_helper'

describe Invoice, type: :model  do

  #scopeテスト用
  fixtures :controllers_divisions
  fixtures :invoices
  fixtures :invoice_details
  fixtures :users_divisions
  fixtures :divisions
  fixtures :dimensions
  fixtures :our_banks
  fixtures :partners
  fixtures :account_titles

  describe "scope" do
    let!(:inv) { invoices(:inv01) }
    let!(:inv_all) {Invoice.count}
    
    describe "invoice_no" do
      subject { Invoice.invoice_no(invoice_no) }
      let!(:inv) { invoices(:inv03) }
      context "DBに存在する請求書番号が渡された場合" do
        let!(:invoice_no) {'1000000001'}
        it "検索結果に含まれること" do
          expect(subject).to include inv
        end
        it "1件抽出されること" do
        
          expect(subject.count).to eq 1
        end
      end
      context "DBと一部が一致する内容が渡された場合" do
        let!(:invoice_no) {'0000000'}
        it "検索結果に含まれないこと" do
          expect(subject.where(invoice_no: invoice_no).count).to eq 0
        end
        it "0件になること" do
          expect(subject.count).to eq 0
        end
      end
      context "空値が渡された場合" do
        let!(:invoice_no) {''}
        it "検索結果に含まれないこと" do
          expect(subject.where(invoice_no: invoice_no).count).to eq 0
        end
        it "全件抽出されること" do
          expect(subject.count).to eq inv_all
        end
      end
      context "nilが渡された場合" do
        let!(:invoice_no) {nil}
        it "検索結果に含まれないこと" do
          expect(subject.where(invoice_no: invoice_no).count).to eq 0
        end
        it "全件抽出されること" do
          expect(subject.count).to eq inv_all
        end
      end
    end
  
    describe "remarks" do
      subject { Invoice.with_invoice_detail.remarks('%' + remarks.to_s + '%') }
      let!(:idtl_all) {InvoiceDetail.count}
      context "DBと一致する内容が渡された場合" do
        let!(:remarks) {'テスト199'}
        it "検索結果に含まれること" do
          expect(subject).to include inv
        end
        it "1件抽出されること" do
        
          expect(subject.count).to eq 1
        end
      end
      context "DBと一部が一致する内容が渡された場合" do
        let!(:remarks) {'テスト1'}
        it "検索結果に含まれること" do
          expect(subject).to include inv
        end
        it "複数件抽出されること" do
          expect(subject.count).to be > 2
        end
      end
      context "存在しない内容が渡された場合" do
        let!(:remarks) {'テストテストテスト'}
        it "検索結果に含まれないこと" do
          expect(subject.count).to eq 0
        end
        it "0件になること" do
          expect(subject.count).to eq 0
        end
      end
      context "空値が渡された場合" do
        let!(:remarks) {''}
        it "検索結果に含まれる" do
          expect(subject.count).not_to eq 0
        end
        it "全件抽出されること" do
          expect(subject.count).to eq idtl_all
        end
      end
      context "nilが渡された場合" do
        let!(:remarks) {nil}
        it "検索結果に含まれる" do
          expect(subject.count).not_to eq 0
        end
        it "全件抽出されること" do
          expect(subject.count).to eq idtl_all
        end
      end
    end
  end
  
  describe "validate" do
    describe "JE番号" do
    
      subject {inv}
      let(:inv){FactoryBot.create(:invoice)}
    
      context '入力規則' do
        it "JE番号があれば登録できる" do
          inv.je_number = "912345"
          inv.save
          is_expected.to be_valid
        end
    
        it "JE番号がなくても登録できる" do
          inv.je_number = nil
          inv.save
          is_expected.to be_valid
        end
    
      end
    
      context '文字数' do
        it "入力長が最大のときは登録できる" do
          inv.je_number = "a" * 6
          expect {inv.save}.not_to raise_error(ActiveRecord::StatementInvalid)
        end
        
        it "入力長が短い（4文字）でも登録できる" do
          inv.je_number = "a" * 4
          expect {inv.save}.not_to raise_error(ActiveRecord::StatementInvalid)
        end
        
        it "入力長が最大のときは登録できない" do
          inv.je_number = "a" * 7
          expect {inv.save}.to raise_error(ActiveRecord::StatementInvalid)
        end
      end
    
      context '文字種' do
        context '通常文字は全てエラー' do
          it "マルチバイト文字" do
            inv.je_number = "３１２３４５"
            is_expected.to be_invalid
          end
          
           it "英字、カナ" do
            inv.je_number = "ＴＥＳＴテス"
            is_expected.to be_invalid
          end
          
          it "カタカナ" do
            inv.je_number = "テストアカウ"
            is_expected.to be_invalid
          end
          
          it "半角カタカナ" do
            inv.je_number = "ﾃｽﾄｱｶｳ"
            is_expected.to be_invalid
          end
        end
      
        context "symbolも全てエラー" do
          it "-=^~¥|" do
            inv.je_number = "-=^~¥|"
            is_expected.to be_invalid
          end
            
          it "_/?.>," do
            inv.je_number = "_/?.>,"
            is_expected.to be_invalid
          end
            
          it "<!¥$%&" do
            inv.je_number = "<!¥$%&"
            is_expected.to be_invalid
          end
            
          it "'()@`[" do
            inv.je_number = "'()@`["
            is_expected.to be_invalid
          end
            
          it "{]}:*;" do
            inv.je_number = "{]}:*;"
            is_expected.to be_invalid
          end
            
          it "+" do
            inv.je_number = "+"
            is_expected.to be_invalid
          end
            
          it "XSSチェック" do
            inv.je_number = %("#)
            is_expected.to be_invalid
          end
        end
      end
    
    end
  end
end
