FactoryBot.define do
  factory :payment_slip do
    company_id {1}
    date {"2019-02-01"}
    state {0}
    created_at {"2019-02-01 08:34:28.562454"}
    create_user_id {1}
    lock_version {0}
    slip_type {1}
  end
end
