FactoryBot.define do
    factory :si_journal, :class => SiJournal do |sjrn|
    sjrn.account_title_id {108}
    sjrn.division_id {5}
    sjrn.amount {1}
    sjrn.remarks {"test"}
    sjrn.dim2_id {1}
    sjrn.sequence(:lineno)
  end
end
