FactoryBot.define do
  factory :vat do
    active { 1 }
    sequence(:tax_code, 1010)
    tax_side { 1 }
    country { 'JP' }
    tax_group { 13 }
    effective_day { '2000-01-01' }
    sequence(:name, 11) { |n| '課税#{n}%'  } 
    sequence(:tax_rate, 11) 
    account_title_id { 1 }
    sequence(:sbo_code, 1010) { |n| 'JP#{n}'}

    trait :purchase do
      tax_side { 1 }
    end

    trait :sales do
      tax_side { 2 }
    end
  end
end
