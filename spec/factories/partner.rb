FactoryBot.define do
  factory :partner, :class => Partner do |ptn|
    ptn.name {"秋原葉テスト空輸(株)"}
    ptn.kana_name {"アキハラバテストクウユ"}
    ptn.sequence(:zaimu_id){ |n| n }
    ptn.remarks {"テスト"}
    ptn.created_at {"2019-01-29 22:14:31.109695" }
    ptn.lock_version {0}
    ptn.hq_zip {"999-9999"}
    ptn.hq_addr {"テスト県テスト市テスト町１－２－３"}
    ptn.industry {"定期航空運送"}
  end
end
