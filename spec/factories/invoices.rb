FactoryBot.define do
  factory :invoice, :class => Invoice do |inv|
    inv.partner_id {1}
    inv.buy_day {"2019-01-27"}
    inv.due_day {"2019-01-27"}
    inv.currency_id {1}
    inv.division_id {1}
    inv.state {1}
    inv.payment_state {0}
    inv.created_at {"2019-01-27 08:34:28.562454"}
    inv.lock_version {0}
    inv.sequence(:invoice_no)
    inv.company_setting_id {1}
    inv.approval {""}
    inv.amount_total {3150}
    inv.je_number {"900001"}
    inv.invoice_details {[
      FactoryBot.build(:invoice_detail)
    ]}
  end
end
