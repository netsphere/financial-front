FactoryBot.define do
  factory :global_setting do
    partner_counter {1}
    id {1}
    bank_charge_account_title_id {101}
    currency_gain_account_title_id {101}
    vat_payment_account_title_id {101}
    currency_limit_day {'2021-02-03'}
    default_division_tree_root_id {1}
  end
end
