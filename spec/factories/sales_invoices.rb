FactoryBot.define do
  factory :sales_invoice, :class => SalesInvoice do |sinv|
    sinv.partner_id {1}
    sinv.zip {9999999}
    sinv.addr1 {"test"}
    sinv.addr2 {"test"}
    sinv.contact_name2 {"test"}
    sinv.due_day {"2019-01-09"}
    sinv.currency_id {1}
    sinv.division_id {5}
    sinv.state {0}
    sinv.created_at {"2019-01-09 09:56:09.416135"}
    sinv.create_user_id {1}
    sinv.lock_version {0}
    sinv.contact_name1 {"test空輸(株"}
    sinv.remarks {"test"}
    sinv.sell_day {"2019-01-09"}
    sinv.delivery {1}
    sinv.disable_due_day {FALSE}
    sinv.company_setting_id {1}
    sinv.receivable_account_id {302}
    sinv.contact_name3 {"test"}
    sinv.amount_total {1}
    sinv.si_details {[
      FactoryBot.build(:si_detail)
    ]}
    sinv.si_journals {[
      FactoryBot.build(:si_journal)
    ]}
  end
end
