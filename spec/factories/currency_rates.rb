FactoryBot.define do
  factory :currency_rate do
    id {1}
    date {'2019-02-03'}
    currency_id {1}
    rate {1000}
  end
end
