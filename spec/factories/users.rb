FactoryBot.define do
  factory :user do
    name {'テストユーザ'}
    kana_name {'テストユーザ'}
    created_at {'2019-01-31 18:05:36.057454'}
    login {'testuser'}
    password {'transnet'}
    password_confirmation {'transnet'}
    email {'testuser@transnet.test'}
    active {1}
    keiri_flag {true}
    marketing_flag {true}
    apar_flag {true}
    hrd_flag {true}
    worker_code {'1003'}
  end
end
