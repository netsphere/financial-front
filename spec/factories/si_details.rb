FactoryBot.define do
  factory :si_detail, :class => SiDetail do |sdtl|
    sdtl.line_no {1}
    sdtl.description {"test"}
    sdtl.unit_price {1}
    sdtl.qty {1}
    sdtl.unit {""}
    sdtl.amount {1}
    sdtl.vat_code {0}
  end
end
