FactoryBot.define do
  factory :invoice_detail, :class => InvoiceDetail do |idtl|
    idtl.account_title_id {101}
    idtl.division_id {1}
    idtl.amount {"3150"}
    idtl.vat_code {10}
    idtl.remarks {"test"}
    idtl.lineno {1}
    idtl.dim2_id {1}
  end
end
