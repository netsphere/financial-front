require "rails_helper"
RSpec.describe PartnersController, type: :controller do

  before do login end

  describe "検索フォーム" do

    context "context入力チェック" do
      it "取引先コードで検索できる" do
        search = {"search" =>{"code" =>"9090909", "text" =>""}}
        get :search, search
        expect(assigns(@partners)[:search].errors.messages[:base]).not_to be_present
        expect(response).to render_template(:search)
      end
    
      it "検索テキストで検索できる" do
        search = {"search" =>{"code" =>"", "text" =>"アキハラバ"}}
        get :search, search
        expect(assigns(@partners)[:search].errors.messages[:base]).not_to be_present
        expect(response).to render_template(:search)
      end

      it "取引先コードと検索テキストを入れないとエラーメッセージが返りindexを描画" do
        search = {"search" =>{"code" =>"", "text" =>""}}
        get :search, search
        expect(response).to be_success
        expect(assigns(@partners)[:search].errors.messages[:base]).to be_present
        expect(response).to render_template(:index)
      end

      it "取引先コードと検索テキスト両方入っていても検索できる" do
        search = {"search" =>{"code" =>"9090909", "text" =>"アキハラバ"}}
        get :search, search
        expect(assigns(@partners)[:search].errors.messages[:base]).not_to be_present
        expect(response).to render_template(:search)
      end
    end

  end
      
  describe "paginate処理" do

    before do
      FactoryBot.create_list(:partner,10) # 10件の一覧を作成
      Partner::PAGINATE_MAX = 8
      Partner::PAGINATE_LIST = 2
    end
    
    context "取得件数" do
      it "paginate処理前は最大8件取得される" do
        search = {"search" =>{"code" =>"", "text" =>"アキハラバ"}}
        get :search, search
        expect(assigns(:result_count)).to eq Partner::PAGINATE_MAX
      end
    
      it "paginate処理後は2件取得される" do
        search = {"search" =>{"code" =>"", "text" =>"アキハラバ"}}
        get :search, search
        expect(assigns(:partners).count).to eq Partner::PAGINATE_LIST
      end
    
      it "2ページ目も2件取得される" do
        search = {"page" => 2 , "search" =>{"code" =>"", "text" =>"アキハラバ"}}
        get :search, search
        expect(assigns(:partners).count).to eq Partner::PAGINATE_LIST
      end

      it "存在しないページ（5ページ目）を取得しようとすると0件で戻る（エラーにならない）" do
        search = {"page" => 5 , "search" =>{"code" =>"", "text" =>"アキハラバ"}}
        get :search, search
        expect(response).to be_success
        expect(assigns(:partners).count).to eq 0
      end

    end

  end
end
