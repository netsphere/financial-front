require "rails_helper"
RSpec.describe InvoicesController, type: :controller do
  fixtures :company
  fixtures :account_title_trees
  fixtures :account_titles
  fixtures :company_settings
  fixtures :controllers_divisions
  fixtures :partners
  fixtures :invoices
  fixtures :invoice_details
  fixtures :control_divisions
  fixtures :users_divisions
  fixtures :divisions
  fixtures :dimensions
  fixtures :our_banks
  fixtures :global_settings
  
  describe "トップ > 過去の請求書一覧（会社=1,取引先全対象）" do
    before do login end
    context "購買請求書" do
      subject{get :list, params}
      before do
        request.env['HTTP_REFERER'] = "http://test.host/invoices/list"
      end
      context "1件抽出" do

        let(:params){{"cost_center"=>"1",
                      "search" =>
                       {"account_titles" => "100",
                        "je_number" => "",
                        "si_number" => "",
                        "remarks" => ""}}}
        it "勘定科目が一致" do
          subject
          expect(assigns(:invoices).size).to eq(1)
        end
        
        let(:params){{"cost_center"=>"1",
                      "search" =>
                       {"account_titles" => "101",
                        "je_number" => "310001",
                        "si_number" => "",
                        "remarks" => ""}}}
        it "勘定科目、JE番号が一致" do
          subject       
          expect(assigns(:invoices).size).to eq(1)
        end
          
        let(:params){{"cost_center"=>"1",
                      "search" =>
                       {"account_titles" => "102",
                        "je_number" => "",
                        "si_number" => "1000000001",
                        "remarks" => ""}}}
        it "勘定科目、請求書番号が一致" do
          subject       
          expect(assigns(:invoices).size).to eq(1)
        end
          
        let(:params){{"cost_center"=>"1",
                      "search" =>
                       {"account_titles" => "103",
                        "je_number" => "",
                        "si_number" => "",
                        "remarks" => "テスト1"}}}
        it "勘定科目、内容テキスト1が一致" do
          subject       
          expect(assigns(:invoices).size).to eq(1)
        end
          
        let(:params){{"cost_center"=>"1",
                      "search" =>
                       {"account_titles" => "104",
                        "je_number" => "310002",
                        "si_number" => "2000000015",
                        "remarks" => ""}}}
        it "勘定科目、JE番号、内容テキスト1が一致" do
          subject       
          expect(assigns(:invoices).size).to eq(1)
        end
          
        let(:params){{"cost_center"=>"1",
                      "search" =>
                       {"account_titles" => "105",
                        "je_number" => "",
                        "si_number" => "2000000016",
                        "remarks" => "テスト2"}}}
        it "勘定科目、請求書番号、内容テキスト1が一致" do
          subject       
          expect(assigns(:invoices).size).to eq(1)
        end
          
        let(:params){{"cost_center"=>"1",
                      "search" =>
                       {"account_titles" => "106",
                        "je_number" => "310003",
                        "si_number" => "2000000017",
                        "remarks" => "テスト3"}}}
        it "勘定科目、JE番号、請求書番号、内容テキスト1が一致" do
          subject       
          expect(assigns(:invoices).size).to eq(1)
        end
      end
 
      context "2件抽出" do
        let(:params){{"cost_center"=>"1",
                      "search" =>
                       {"account_titles" => "0",
                        "je_number" => "310004",
                        "si_number" => "",
                        "remarks" => ""}}}
        it "JE番号が一致" do
          subject       
          expect(assigns(:invoices).size).to eq(2)
        end
        
        let(:params){{"cost_center"=>"1",
                      "search" =>
                       {"account_titles" => "0",
                        "je_number" => "310005",
                        "si_number" => "1000000002",
                        "remarks" => ""}}}
        it "JE番号、請求書番号が一致" do
          subject       
          expect(assigns(:invoices).size).to eq(2)
        end
        
        let(:params){{"cost_center"=>"1",
                      "search" =>
                       {"account_titles" => "0",
                        "je_number" => "310006",
                        "si_number" => "",
                        "remarks" => "テスト4"}}}
        it "JE番号、内容テキスト2が一致" do
          subject       
          expect(assigns(:invoices).size).to eq(2)
        end

        let(:params){{"cost_center"=>"1",
                      "search" =>
                       {"account_titles" => "107",
                        "je_number" => "310007",
                        "si_number" => "",
                        "remarks" => ""}}}
        it "勘定科目、JE番号が一致" do
          subject       
          expect(assigns(:invoices).size).to eq(2)
        end

        let(:params){{"cost_center"=>"1",
                      "search" =>
                       {"account_titles" => "0",
                        "je_number" => "310008",
                        "si_number" => "1000000003",
                        "remarks" => "テスト5"}}}
        it "JE番号、請求書番号、内容テキスト2が一致" do
          subject       
          expect(assigns(:invoices).size).to eq(2)
        end

        let(:params){{"cost_center"=>"1",
                      "search" =>
                       {"account_titles" => "108",
                        "je_number" => "310009",
                        "si_number" => "",
                        "remarks" => "テスト6"}}}
        it "勘定科目、JE番号、内容テキスト2が一致" do
          subject       
          expect(assigns(:invoices).size).to eq(2)
        end

      end
        
    end
  end
  
  describe "トップ > 取引先の検索 > 取引先 > 過去分を一覧表示（会社=2,取引先固定）" do
    before do
      login2
    end
    context "購買請求書" do
      subject{get :listp, params}
      before do
        request.env['HTTP_REFERER'] = "http://test.host/invoices/listp"
      end
      context "1件抽出" do

        let(:params){{"cost_center"=>"2",
                      "search" =>
                       {"account_titles" => "0",
                        "je_number" => "",
                        "si_number" => "1000000004",
                        "remarks" => ""},
                        "partner"=>"2",
                        "company"=>"2"
                        }}
        it "請求書番号が一致" do
          subject       
          expect(assigns(:invoices).size).to eq(1)
        end

        let(:params){{"cost_center"=>"2",
                      "search" =>
                       {"account_titles" => "0",
                        "je_number" => "",
                        "si_number" => "1000000005",
                        "remarks" => "テスト7"},
                        "partner"=>"2",
                        "company"=>"2"
                        }}
        it "請求書番号、内容テキストが一致" do
          subject       
          expect(assigns(:invoices).size).to eq(1)
        end

        let(:params){{"cost_center"=>"2",
                      "search" =>
                       {"account_titles" => "109",
                        "je_number" => "",
                        "si_number" => "1000000006",
                        "remarks" => ""},
                        "partner"=>"2",
                        "company"=>"2"
                        }}
        it "勘定科目、請求書番号が一致" do
          subject       
          expect(assigns(:invoices).size).to eq(1)
        end

        let(:params){{"cost_center"=>"2",
                      "search" =>
                       {"account_titles" => "0",
                        "je_number" => "310010",
                        "si_number" => "1000000007",
                        "remarks" => ""},
                        "partner"=>"2",
                        "company"=>"2"
                        }}
        it "JE番号、請求書番号が一致" do
          subject       
          expect(assigns(:invoices).size).to eq(1)
        end

        let(:params){{"cost_center"=>"2",
                      "search" =>
                       {"account_titles" => "110",
                        "je_number" => "",
                        "si_number" => "1000000008",
                        "remarks" => "テスト8"},
                        "partner"=>"2",
                        "company"=>"2"
                        }}
         it "勘定科目、請求書番号、内容テキストが一致" do
          subject       
          expect(assigns(:invoices).size).to eq(1)
        end

        let(:params){{"cost_center"=>"2",
                      "search" =>
                       {"account_titles" => "111",
                        "je_number" => "310011",
                        "si_number" => "1000000009",
                        "remarks" => ""},
                        "partner"=>"2",
                        "company"=>"2"
                        }}
        it "勘定科目、JE番号、請求書番号が一致" do
          subject       
          expect(assigns(:invoices).size).to eq(1)
        end
      end
      
      context "2件抽出" do

        context do
          let(:params){{"cost_center"=>"2",
                        "search" =>
                         {"account_titles" => "0",
                          "je_number" => "",
                          "si_number" => "",
                          "remarks" => "テスト9"},
                          "partner"=>"4",
                          "company"=>"2"
                          }}
          it "内容テキスト1,2が一致※他項番データ含め7件出力" do
            subject
            expect(assigns(:invoices).size).to eq(7)
          end
        end
          
        context do
          let(:params){{"cost_center"=>"2",
                        "search" =>
                         {"account_titles" => "112",
                          "je_number" => "",
                          "si_number" => "",
                          "remarks" => "テスト10"},
                          "partner"=>"4",
                          "company"=>"2"
                          }}
          it "勘定科目、内容テキストが一致" do
            subject       
            expect(assigns(:invoices).size).to eq(2)
          end
        end
        
        context do
          let(:params){{"cost_center"=>"2",
                        "search" =>
                         {"account_titles" => "0",
                          "je_number" => "310012",
                          "si_number" => "",
                          "remarks" => "テスト11"},
                          "partner"=>"4",
                          "company"=>"2"
                          }}
          it "JE番号、内容テキストが一致" do
            subject
            expect(assigns(:invoices).size).to eq(2)
          end
        end

        context do
          let(:params){{"cost_center"=>"2",
                        "search" =>
                         {"account_titles" => "0",
                          "je_number" => "",
                          "si_number" => "1000000010",
                          "remarks" => "テスト12"},
                          "partner"=>"4",
                          "company"=>"2"
                          }}
          it "請求書番号、内容テキストが一致※テスト想定誤りにより1件一致で正" do
            subject
            expect(assigns(:invoices).size).to eq(1)
          end
        end

        context do
          let(:params){{"cost_center"=>"2",
                        "search" =>
                         {"account_titles" => "113",
                          "je_number" => "310013",
                          "si_number" => "",
                          "remarks" => "テスト13"},
                          "partner"=>"4",
                          "company"=>"2"
                          }}
          it "勘定科目、JE番号、内容テキストが一致" do
            subject       
            expect(assigns(:invoices).size).to eq(2)
          end
        end

        context do
          let(:params){{"cost_center"=>"2",
                        "search" =>
                         {"account_titles" => "0",
                          "je_number" => "310014",
                          "si_number" => "1000000911",
                          "remarks" => "テスト14"},
                          "partner"=>"4",
                          "company"=>"2"
                          }}
          it "JE番号、請求書番号、内容テキストが一致※テスト想定誤りにより1件一致で正" do
            subject       
            expect(assigns(:invoices).size).to eq(1)
          end
        end
      
     end
    end
  end

end
