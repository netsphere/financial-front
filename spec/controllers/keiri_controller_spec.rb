require "rails_helper"

RSpec.describe KeiriController, type: :controller do
  let(:pme){FactoryBot.create(:payment_slip)}
  fixtures :control_divisions
  fixtures :partners
  fixtures :account_titles
  fixtures :users_divisions
  fixtures :divisions
  fixtures :dimensions
  fixtures :our_banks
  fixtures :global_settings

  before do
    login
    FactoryBot.create(:currency_rate)
    session[:entries] = []
    session[:retids] = []
    session[:si_entries] = [] 
    session[:payment_entries]  = []
    session[:payment_entries]  << pme.id #エラー回避用Dummy
  end

  let(:sinv){FactoryBot.create(:sales_invoice)}
  let(:inv){FactoryBot.create(:invoice)}

  describe "get #succeed_export -> #close_documents" do
    before do
      @invno = 90000
    end
    subject {get :succeed_export, params}
      
    context "購買請求書" do
      before do
        session[:si_entries]  << sinv.id #エラー回避用Dummy
      end
      let(:params){{"company" => "1"}}

      context "1件の請求書を投入" do
        before do
          @inv = FactoryBot.create(:invoice, invoice_no: @invno += 1)
          session[:entries] << @inv.id
          session[:retids] << "JEC" + @inv.id.to_s
        end
        it "1件のレコードにJE番号が付与されること" do
          subject
          expect(Invoice.where(je_number: session[:retids]).count).to eq 1
        end
      end
    
      context "5件の請求書を投入" do
        before do
          5.times do
            @inv = FactoryBot.create(:invoice, invoice_no: @invno += 1)
            session[:entries] << @inv.id
            session[:retids] << "JEC" + @inv.id.to_s
          end
        end
        it "5件のレコードにそれぞれ固有のJE番号が付与されること" do
          subject
          expect(Invoice.where(je_number: session[:retids]).count).to eq 5
        end
      end
      
      context "請求書投入後の戻り値にJE番号が付与されてなかった" do
        before do
          @inv = FactoryBot.create(:invoice, invoice_no: @invno += 1)
          session[:entries] << @inv.id
        end
        it "JE番号が空でステータスのみ連動済みに変わっていること" do
          subject
          expect(Invoice.where("id = '?' and je_number is null and state = '20'", session[:entries]).count).to eq 1
        end
      end
    end      
      
    context "販売請求書" do
      before do
        session[:entries]  << inv.id #エラー回避用Dummy
      end
      let(:params){{"company" => "1"}}

      context "1件の請求書を投入" do
        before do
          @sinv = FactoryBot.create(:sales_invoice, si_number: @invno += 1)
          session[:entries] = []
          session[:si_entries] << @sinv.id
          session[:retids] << "JES" + @sinv.id.to_s
        end
        it "購買0件、販売1件のレコードにJE番号が付与されること" do
          subject
          expect(Invoice.where(je_number: session[:retids]).count).to eq 0
          expect(SalesInvoice.where(je_number: session[:retids]).count).to eq 1
        end
      end
    
      context "購買1件、販売5件の請求書を投入" do
        before do
          @inv = FactoryBot.create(:invoice, invoice_no: @invno += 1)
          session[:retids] << "JEI" + @inv.id.to_s

          5.times do
            @sinv = FactoryBot.create(:sales_invoice, si_number: @invno += 1)
            session[:si_entries] << @sinv.id
            session[:retids] << "JEC" + @sinv.id.to_s
          end
        end
        it "それぞれ固有のJE番号が付与されること" do
          subject
          expect(Invoice.where(je_number: session[:retids]).count).to eq 1
          expect(SalesInvoice.where(je_number: session[:retids]).count).to eq 5
        end
      end
      
      context "請求書投入後の戻り値にJE番号が付与されてなかった" do
        before do
          @sinv = FactoryBot.create(:sales_invoice, si_number: @invno += 1)
          session[:si_entries] << @sinv.id
        end
        it "JE番号が空でステータスのみ連動済みに変わっていること" do
          subject
          expect(SalesInvoice.where("id = '?' and je_number is null and state = '20'", session[:si_entries]).count).to eq 1
        end
      end
    end

  end

  describe "Update/Delete keiri_memo" do

    context "購買請求書" do
      context "DB変更" do
        subject {inv.save}
        it "変更した番号が正しくupdateされていること" do
          aft_je_number = "954321"
          inv.je_number = aft_je_number
          subject
          expect(Invoice.find(inv.id).je_number).to eq(aft_je_number)
        end
      end
      
      context "DB削除" do
        subject {inv.destroy}
        it "JE番号カラム追加後も問題なくdeleteできること" do
          bef_size = Invoice.count
          subject
          expect(Invoice.count).to eq bef_size
        end
      end
    end

    context "販売請求書" do
      context "DB変更" do
        subject {sinv.save}
        it "変更した番号が正しくupdateされていること" do
          aft_je_number = "954321"
          sinv.je_number = aft_je_number
          subject
          expect(SalesInvoice.find(sinv.id).je_number).to eq(aft_je_number)
        end
      end
      
      context "DB削除" do
        subject {sinv.destroy}
        it "JE番号カラム追加後も問題なくdeleteできること" do
          bef_size = SalesInvoice.count
          subject
          expect(SalesInvoice.count).to eq bef_size
        end
      end
    end

  end


end
