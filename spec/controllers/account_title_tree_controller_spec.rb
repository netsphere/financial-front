require "rails_helper"

RSpec.describe AccountTitleTreesController, type: :controller do
  fixtures :account_titles
  fixtures :account_title_trees

  describe "POST #update" do
    before do
      request.env['HTTP_REFERER'] = "http://test.host/invoices/list"
    end
    context "子テーブル選択チェック" do
      context "子レコードが選択されている" do
        params = {"parent_id"=>"1000", "account_title_tree"=>{"child_id"=>["102", "103", "106", "23"]}}
        subject{ ->{post :update, params}}
        it "成功のレスポンスが返ること" do
          expect(response).to have_http_status(:success)
        end
        #3件増えていること
        it {is_expected.to change { AccountTitleTree.count }.by(3)}
      end
      context "子レコードが選択されていない" do
        params = {"parent_id"=>"1000"}
        subject{ ->{post :update, params}}
        it "成功のレスポンスが返ること" do
          expect(response).to have_http_status(:success)
        end
        #1件減っていること
        it {is_expected.to change { AccountTitleTree.count }.by(-1)}
      end
    end
  end
end

