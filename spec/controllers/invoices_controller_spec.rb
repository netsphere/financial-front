require "rails_helper"

RSpec.describe InvoicesController, type: :controller do
  fixtures :company
  fixtures :controllers_divisions
  fixtures :control_divisions
  fixtures :partners
  fixtures :account_titles
  fixtures :account_title_trees
  fixtures :users_divisions
  fixtures :divisions
  fixtures :dimensions
  fixtures :our_banks
  fixtures :invoices
  fixtures :invoice_details
  fixtures :partners
  fixtures :global_settings

  before do
    login
    FactoryBot.create(:invoice, division_id: '1', partner_id: '1', invoice_details: [FactoryBot.build(:invoice_detail)])
    FactoryBot.create(:invoice, division_id: '1', partner_id: '1', invoice_details: [FactoryBot.build(:invoice_detail, remarks: "あいうえおかきくけこさしすせそ")])
    FactoryBot.create(:invoice, division_id: '1', partner_id: '1', invoice_no: '1214547870', invoice_details: [FactoryBot.build(:invoice_detail)])
    FactoryBot.create(:partner, id: '6')
    FactoryBot.create(:invoice, division_id: '1', partner_id: '6', invoice_details: [FactoryBot.build(:invoice_detail)])
    FactoryBot.create(:invoice, division_id: '1', partner_id: '6', je_number: '900011')
    FactoryBot.create_list(:invoice_detail, 3, invoice: FactoryBot.create(:invoice, division_id: '1', partner_id: '6', je_number: '900012'))
  end

  describe "GET #list" do
    before do
      request.env['HTTP_REFERER'] = "http://test.host/invoices/list"
    end
    context "正常性チェック" do
      context "検索条件" do
        
        it "条件検索を入力して検索ボタンが押されたら検索結果を返す（1件）" do
          search = {"search"=>{
                     "account_titles"=>"0", 
                     "je_number"=>"", 
                     "si_number"=>"", 
                     "remarks"=>"あいうえおかきくけこさしすせそ"}}
          get :list, search
          expect(assigns(:search).class).to eq InvoiceSearchBox
          expect(assigns(:invoices).size).to eq 1
        end
        
        it "子テーブルが紐付かない場合でもJE番号で検索した結果を返す（1件）" do
          search = {"search"=>{
                     "account_titles"=>"0", 
                     "je_number"=>"900011", 
                     "si_number"=>"", 
                     "remarks"=>""}}
          get :list, search
          expect(assigns(:search).class).to eq InvoiceSearchBox
          expect(assigns(:invoices).size).to eq 1
        end
        
        it "子テーブルが複数紐づく場合でもJE番号で検索した結果を返す（1件）" do
          search = {"search"=>{
                     "account_titles"=>"0", 
                     "je_number"=>"900012", 
                     "si_number"=>"", 
                     "remarks"=>""}}
          get :list, search
          expect(assigns(:search).class).to eq InvoiceSearchBox
          expect(assigns(:invoices).size).to eq 1
        end
        
        it "条件検索を入力せず検索ボタンが押されたらエラーを返しlistにリダイレクトする" do
          search = {"search"=>{
                     "account_titles"=>"0", 
                     "je_number"=>"", 
                     "si_number"=>"", 
                     "remarks"=>""}}
          get :list, search
          expect(assigns(:search).errors.present?).to be true
          expect(response).to redirect_to :action => 'list'
        end

        it "条件検索が成功したら成功のレスポンスが返ること" do
          search = {"search"=>{
                     "account_titles"=>"0", 
                     "je_number"=>"", 
                     "si_number"=>"", 
                     "remarks"=>"あいうえおかきくけこさしすせそ"}}
          get :list, search
          expect(response).to have_http_status(:success)
        end
        
        it "条件検索が0件の場合も成功のレスポンスが返ること。" do
          search = {"search"=>{
                     "account_titles"=>"0", 
                     "je_number"=>"", 
                     "si_number"=>"", 
                     "remarks"=>"どかん"}}
          get :list, search
          expect(response).to have_http_status(:success)
        end
      end
    end
  end

  describe "GET #listp" do
    before do
      request.env['HTTP_REFERER'] = "http://test.host/invoices/listp"
    end
    context "正常性チェック" do
      context "検索条件" do
        it "取引先画面から遷移した場合、選択中の取引先のデータ全件を返す（3件)" do
          search = {"partner" => "6",
                    "company" =>"1"}
          get :listp, search
          expect(assigns(:invoices).count).to eq 3
        end
        
        it "条件検索を入力して検索ボタンが押されたら選択中の条件検索の結果を返す（1件)" do
          search = {"search"=>{
                       "account_titles"=>"0", 
                       "je_number"=>"", 
                       "si_number"=>"1214547870", 
                       "remarks"=>""},
                     "partner" => "1",
                     "company" =>"1"}
          get :listp, search
          expect(assigns(:search).class).to eq InvoiceSearchBox
          expect(assigns(:invoices).size).to eq 1
        end
        
        it "子テーブルが紐付かない場合でもJE番号で検索した結果を返す（1件）" do
          search = {"search"=>{
                     "account_titles"=>"0", 
                     "je_number"=>"900011", 
                     "si_number"=>"", 
                     "remarks"=>""},
                     "partner" => "6",
                     "company" =>"1"}
          get :listp, search
          expect(assigns(:search).class).to eq InvoiceSearchBox
          expect(assigns(:invoices).size).to eq 1
        end
        
        it "子テーブルが複数紐づく場合でもJE番号で検索した結果を返す（1件）" do
          search = {"search"=>{
                     "account_titles"=>"0", 
                     "je_number"=>"900012", 
                     "si_number"=>"", 
                     "remarks"=>""},
                     "partner" => "6",
                     "company" =>"1"}
          get :listp, search
          expect(assigns(:search).class).to eq InvoiceSearchBox
          expect(assigns(:invoices).size).to eq 1
        end
        
        it "条件検索を入力せず検索ボタンが押されたらエラーを返しlistpにリダイレクトする" do
          search = {"search"=>{
                       "account_titles"=>"0", 
                       "je_number"=>"", 
                       "si_number"=>"", 
                       "remarks"=>""},
                     "partner" => "1",
                     "company" =>"1"}
          get :listp, search
          expect(assigns(:search).errors.present?).to be true
          expect(response).to redirect_to "http://test.host/invoices/listp?company=1&partner=1"
        end
      end
    end
  end

  describe "GET #new" do

    context "正常性チェック" do
      let(:params){{"partner" => "1", "company" =>"1"}}
      subject{get :new, params}
      it "勘定科目と会計部門コンボボックス用データ取得が出来ていること" do
        subject
        expect(assigns(:ac_collection).count).to eq 10
        expect(assigns(:div_collection).count).to eq 3
      end
    end
  end
      
      
  describe "GET #edit" do
    let(:params){{"id" => "1"}}
    subject{get :edit, params}
    context "正常性チェック" do
      it "勘定科目と会計部門コンボボックス用データ取得が出来ていること" do
        subject
        expect(assigns(:ac_collection).count).to eq 10
        expect(assigns(:div_collection).count).to eq 3
      end
    end
    context do
      it "勘定科目1件目が想定通りのデータであること" do
        subject
        expect(assigns(:ac_collection).first.account_code).to eq 103
        expect(assigns(:ac_collection).first.name).to eq "TEST3"
      end
    end
    context do
      it "会計科目1件目が想定通りのデータでること" do
        subject
        expect(assigns(:div_collection).reload.first.division_code).to eq "10000"
        expect(assigns(:div_collection).reload.first.name).to eq "テスト1課"
      end       
    end
  end
end

