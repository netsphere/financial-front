require "rails_helper"
RSpec.describe SalesInvoicesController, type: :controller do

  fixtures :users_divisions
  fixtures :divisions
  fixtures :dimensions
  fixtures :our_banks
  fixtures :partners
  fixtures :account_titles
  fixtures :global_settings

  before do
    login
    Division::BUMON_ID = 1
  end

  let(:params){{"partner" => "1", "company" =>"1"}}
  subject{get :new, params}

  describe "異常、イレギュラー時チェック" do
    context "勘定科目テーブルが空" do
      it "エラーにはならないこと" do
        subject
        expect(response).to have_http_status(:success)
      end
      it "勘定科目コンボボックスに0件の変数がセットされる" do
        subject
        expect(assigns(:ac_collection).count).to eq 0
      end
    end
    
    context "会計部門テーブルが空" do
      it "エラーにはならないこと" do
        subject
        expect(response).to have_http_status(:success)
      end
      it "会計部門コンボボックスに0件の変数がセットされる" do
        subject
        expect(assigns(:div_collection).count).to eq 0
      end
    end
  end

end

