require "rails_helper"
RSpec.describe PartnersController, type: :controller do

  before do
    login
  end

  describe "番兵検索" do

    context "縦棒の位置による検索切替" do
      subject{get :search, search}

      context "番兵の位置が先頭" do
        let(:search){{"search" =>{"code" =>"", "text" =>"|TESTANA"}}}
        before do
          FactoryBot.create(:partner, name: 'TESTANAテスト1')
          FactoryBot.create(:partner, kana_name: 'TESTANAテスト2')
          FactoryBot.create(:partner, remarks: 'TESTANAテスト3')
          FactoryBot.create(:partner, name: 'テスト1TESTANA')
          FactoryBot.create(:partner, name: 'テストTESTANA1')
          FactoryBot.create(:partner, name: 'TESTANAテスト1', kana_name: 'TESTANAテスト2')
          FactoryBot.create(:partner, name: 'TESTANAテスト1', remarks: 'TESTANAテスト3')
          FactoryBot.create(:partner, kana_name: 'TESTANAテスト2', remarks: 'TESTANAテスト3')
          FactoryBot.create(:partner, kana_name: 'TESANAテスト4', remarks: 'TESANAテスト5')
        end
        it "先頭に付いていれば前方一致（3件成功）" do
          subject
          expect(assigns(@search)["partners"].map(&:name).grep(/^TESTANA/).size).to eq 3
        end
        it "カナで3件一致すること" do
          subject
          expect(assigns(@search)["partners"].map(&:kana_name).grep(/^TESTANA/).size).to eq 3
        end
        it "remarksで3件一致すること" do
          subject
          expect(assigns(@search)["partners"].map(&:remarks).grep(/^TESTANA/).size).to eq 3
        end
      end

      context "番兵の位置が末尾" do
        let(:search){{"search" =>{"code" =>"", "text" =>"TESTANA｜"}}}
        before do
          FactoryBot.create(:partner, name: 'テスト1TESTANA')
          FactoryBot.create(:partner, kana_name: 'テスト2TESTANA')
          FactoryBot.create(:partner, remarks: 'テスト3TESTANA')
          FactoryBot.create(:partner, name: 'TESTANAテスト1')
          FactoryBot.create(:partner, name: 'テストTESTANA1')
          FactoryBot.create(:partner, name: 'テスト1TESTANA', kana_name: 'テスト2TESTANA')
          FactoryBot.create(:partner, name: 'テスト1TESTANA', remarks: 'テスト3TESTANA')
          FactoryBot.create(:partner, kana_name: 'テスト2TESTANA', remarks: 'テスト3TESTANA')
          FactoryBot.create(:partner, kana_name: 'スト4', remarks: 'TESANAテスト5')
        end
        context "後方一致（番兵全角でも処理）" do
          it "名前で3件一致すること" do
            subject
            expect(assigns(@search)["partners"].map(&:name).grep(/TESTANA$/).size).to eq 3
          end
          it "カナで3件一致すること" do
            subject
            expect(assigns(@search)["partners"].map(&:kana_name).grep(/TESTANA$/).size).to eq 3
          end
          it "remarksで3件一致すること" do
            subject
            expect(assigns(@search)["partners"].map(&:remarks).grep(/TESTANA$/).size).to eq 3
          end
        end
      end
      
      context "番兵なし" do
        let(:search){{"search" =>{"code" =>"", "text" =>"TESTANA"}}}
        before do
          FactoryBot.create(:partner, name: 'テストTESTANA1')
          FactoryBot.create(:partner, kana_name: 'テストTESTANA2')
          FactoryBot.create(:partner, remarks: 'テストTESTANA3')
          FactoryBot.create(:partner, name: 'TESTANAテスト1')
          FactoryBot.create(:partner, name: 'テストTESTANA1')
          FactoryBot.create(:partner, name: 'TESANAテスト1')
          FactoryBot.create(:partner, name: 'テストTESTANA1', kana_name: 'テストTESTANA2')
          FactoryBot.create(:partner, name: 'TESTANAテスト1', remarks: 'TESTANAテスト3')
          FactoryBot.create(:partner, kana_name: 'テストTESTANA2', remarks: 'テストTESTANA3')
          FactoryBot.create(:partner, kana_name: 'TESANAテスト4', remarks: 'TESANAテスト5')
        end
        context "部分一致" do
          it "名前で5件一致すること" do
            subject
            expect(assigns(@search)["partners"].map(&:name).grep(/#{search["search"]["text"]}/).size).to eq 5
          end
          it "カナで3件一致すること" do
            subject
            expect(assigns(@search)["partners"].map(&:kana_name).grep(/#{search["search"]["text"]}/).size).to eq 3
          end
          it "remarksで3件一致すること" do
            subject
            expect(assigns(@search)["partners"].map(&:remarks).grep(/#{search["search"]["text"]}/).size).to eq 3
          end
        end
      end
        
      context "番兵が前後付いている" do
        let(:search){{"search" =>{"code" =>"", "text" =>"|TESTANA|"}}}
        before do
          FactoryBot.create(:partner, name: 'テスト|TESTANA|1')
          FactoryBot.create(:partner, kana_name: 'テスト2|TESTANA|')
          FactoryBot.create(:partner, remarks: '|TESTANA|テスト3')
          FactoryBot.create(:partner, name: '|TESTANAテスト1')
          FactoryBot.create(:partner, name: 'テストTESTANA1|')
          FactoryBot.create(:partner, name: 'テストTEST|ANA1')
          FactoryBot.create(:partner, name: 'テスト|TESTANA|1', kana_name: 'テスト|TESTANA|2')
          FactoryBot.create(:partner, name: '|TESTANA|テスト1', remarks: '|TESTANA|テスト3')
          FactoryBot.create(:partner, kana_name: 'テスト2|TESTANA|', remarks: 'テスト3|TESTANA|')
          FactoryBot.create(:partner, kana_name: '|TESANA|テスト4', remarks: '|TESANA|テスト5')
        end
        context "通常文字として検索" do
          let(:escsrc){search["search"]["text"].insert(0, "\\").insert(9, "\\")} #grep誤検知防止（|をエスケープする）
          it "名前で3件一致すること" do
            subject
            expect(assigns(@search)["partners"].map(&:name).grep(/#{escsrc}/).size).to eq 3
          end
          it "カナで3件一致すること" do
            subject
            expect(assigns(@search)["partners"].map(&:kana_name).grep(/#{escsrc}/).size).to eq 3
          end
          it "remarksで3件一致すること" do
            subject
            expect(assigns(@search)["partners"].map(&:remarks).grep(/#{escsrc}/).size).to eq 3
          end
        end
      end

      context "番兵が文字列内に付いている" do
        let(:search){{"search" =>{"code" =>"", "text" =>"TEST|ANA"}}}
        before do
          FactoryBot.create(:partner, name: 'テストTEST|ANA1テスト')
          FactoryBot.create(:partner, kana_name: 'テスト2TEST|ANA')
          FactoryBot.create(:partner, remarks: 'TEST|ANAテスト3')
          FactoryBot.create(:partner, name: 'TESTANAテスト|1')
        end
        context "通常文字として検索" do
          it "名前で1件一致すること" do
            subject
            expect(assigns(@search)["partners"].map(&:name).grep(/#{search["search"]["text"]}/).size).to eq 1
          end
          it "カナで1件一致すること" do
            subject
            expect(assigns(@search)["partners"].map(&:kana_name).grep(/#{search["search"]["text"]}/).size).to eq 1
          end
          it "remarksで1件一致すること" do
            subject
            expect(assigns(@search)["partners"].map(&:remarks).grep(/#{search["search"]["text"]}/).size).to eq 1
          end
        end
      end

      context "どれにも当てはまらないキーワードで検索" do
        let(:search){{"search" =>{"code" =>"", "text" =>"TE|ST|AN|A"}}}
        before do
          FactoryBot.create(:partner, name: 'テストTEST|ANA1テスト')
          FactoryBot.create(:partner, kana_name: 'テスト2TEST|ANA')
          FactoryBot.create(:partner, remarks: 'TEST|ANAテスト3')
          FactoryBot.create(:partner, name: 'TESTANAテスト|1')
        end
        context "通常文字として検索" do
          it "名前で1件も一致しないこと" do
            subject
            expect(assigns(@search)["partners"].map(&:name).grep(/#{search["search"]["text"]}/).size).to eq 0
          end
          it "カナで1件も一致しないこと" do
            subject
            expect(assigns(@search)["partners"].map(&:kana_name).grep(/#{search["search"]["text"]}/).size).to eq 0
          end
          it "remarksで1件も一致しないこと" do
            subject
            expect(assigns(@search)["partners"].map(&:remarks).grep(/#{search["search"]["text"]}/).size).to eq 0
          end
        end
      end

    end

  end
      
end
