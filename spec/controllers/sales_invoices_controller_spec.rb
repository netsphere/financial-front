require "rails_helper"

RSpec.describe SalesInvoicesController, type: :controller do
  fixtures :company
  fixtures :controllers_divisions
  fixtures :control_divisions
  fixtures :partners
  fixtures :account_titles
  fixtures :users_divisions
  fixtures :divisions
  fixtures :dimensions
  fixtures :our_banks
  fixtures :partners
  fixtures :global_settings

  before do
    login
    FactoryBot.create(:sales_invoice, division_id: '1', partner_id: '1', si_journals: [FactoryBot.build(:si_journal)])
    FactoryBot.create(:sales_invoice, division_id: '1', partner_id: '1', si_journals: [FactoryBot.build(:si_journal, remarks: "あいうえおかきくけこさしすせそ")])
    FactoryBot.create(:sales_invoice, division_id: '1', partner_id: '1', si_number: '1214547870', si_journals: [FactoryBot.build(:si_journal)])
    FactoryBot.create(:partner, id: '6')
    FactoryBot.create(:sales_invoice, division_id: '1', partner_id: '6', si_journals: [FactoryBot.build(:si_journal)])
    FactoryBot.create(:sales_invoice, division_id: '1', partner_id: '6', je_number: '900011')
    FactoryBot.create_list(:si_journal, 3, sales_invoice: FactoryBot.create(:sales_invoice, division_id: '1', partner_id: '6', je_number: '900012'))
  end

  describe "購買・販売請求書 条件検索" do

    describe "GET #list" do
      before do
        request.env['HTTP_REFERER'] = "http://test.host/sales_invoices/list"
      end
      context "検索条件" do
        it "条件検索を入力して検索ボタンが押されたら選択中の条件検索の結果を返す（1件）" do
          search = {"search"=>{
                     "account_titles"=>"0", 
                     "je_number"=>"", 
                     "si_number"=>"", 
                     "remarks"=>"あいうえおかきくけこさしすせそ"}}
          get :list, search
          expect(assigns(:search).class).to eq InvoiceSearchBox
          expect(assigns(:sales_invoices).size).to eq 1
        end
        
        it "子テーブルが紐付かない場合でもJE番号で検索した結果を返す（1件）" do
          search = {"search"=>{
                     "account_titles"=>"0", 
                     "je_number"=>"900011", 
                     "si_number"=>"", 
                     "remarks"=>""}}
          get :list, search
          expect(assigns(:search).class).to eq InvoiceSearchBox
          expect(assigns(:sales_invoices).size).to eq 1
        end
        
        it "子テーブルが複数紐づく場合でもJE番号で検索した結果を返す（1件）" do
          search = {"search"=>{
                     "account_titles"=>"0", 
                     "je_number"=>"900012", 
                     "si_number"=>"", 
                     "remarks"=>""}}
          get :list, search
          expect(assigns(:search).class).to eq InvoiceSearchBox
          expect(assigns(:sales_invoices).size).to eq 1
        end
        
        it "条件検索を入力せず検索ボタンが押されたらエラーを返しlistにリダイレクトする" do
          search = {"search"=>{
                     "account_titles"=>"0", 
                     "je_number"=>"", 
                     "si_number"=>"", 
                     "remarks"=>""}}
          get :list, search
          expect(assigns(:search).errors.present?).to be true
          expect(response).to redirect_to :action => 'list'
        end
      
        it "条件検索が成功したら成功のレスポンスが返ること" do
          search = {"search"=>{
                     "account_titles"=>"0", 
                     "je_number"=>"", 
                     "si_number"=>"", 
                     "remarks"=>"あいうえおかきくけこさしすせそ"}}
          get :list, search
          expect(response).to have_http_status(:success)
        end
        
        it "条件検索が0件の場合も成功のレスポンスが返ること。" do
          search = {"search"=>{
                     "account_titles"=>"0", 
                     "je_number"=>"", 
                     "si_number"=>"", 
                     "remarks"=>"たどん"}}
          get :list, search
          expect(response).to have_http_status(:success)
        end
      end
    end
      
    describe "GET #listp" do
      before do
        request.env['HTTP_REFERER'] = "http://test.host/sales_invoices/listp"
      end
      
      context "正常性チェック" do
        context "検索条件" do
          it "取引先画面から遷移した場合、選択中の取引先のデータ全件を返す（3件)" do
            search = {"partner" => "6",
                      "company" =>"1"}
            get :listp, search
            expect(assigns(:sales_invoices).count).to eq 3
          end
          
          it "条件検索を入力して検索ボタンが押されたら選択中の条件検索の結果を返す（1件)" do
            search = {"search"=>{
                         "account_titles"=>"0", 
                         "je_number"=>"", 
                         "si_number"=>"1214547870",
                         "remarks"=>""},
                       "partner" => "1",
                       "company" =>"1"}
            get :listp, search
            expect(assigns(:search).class).to eq InvoiceSearchBox
            expect(assigns(:sales_invoices).size).to eq 1
          end
            
        it "子テーブルが紐付かない場合でもJE番号で検索した結果を返す（1件）" do
          search = {"search"=>{
                     "account_titles"=>"0", 
                     "je_number"=>"900011", 
                     "si_number"=>"", 
                     "remarks"=>""},
                     "partner" => "6",
                     "company" =>"1"}
          get :listp, search
          expect(assigns(:search).class).to eq InvoiceSearchBox
          expect(assigns(:sales_invoices).size).to eq 1
        end
        
        it "子テーブルが複数紐づく場合でもJE番号で検索した結果を返す（1件）" do
          search = {"search"=>{
                     "account_titles"=>"0", 
                     "je_number"=>"900012", 
                     "si_number"=>"", 
                     "remarks"=>""},
                     "partner" => "6",
                     "company" =>"1"}
          get :listp, search
          expect(assigns(:search).class).to eq InvoiceSearchBox
          expect(assigns(:sales_invoices).size).to eq 1
        end
        
        it "条件検索を入力せず検索ボタンが押されたらエラーを返しlistpにリダイレクトする" do
          search = {"search"=>{
                       "account_titles"=>"0", 
                       "je_number"=>"", 
                       "si_number"=>"", 
                       "remarks"=>""},
                     "partner" => "1",
                     "company" =>"1"}
          get :listp, search
          expect(assigns(:search).errors.present?).to be true
          expect(response).to redirect_to "http://test.host/sales_invoices/listp?company=1&partner=1"
        end
        end
      end
    end

    describe "GET #new" do
      context "正常性チェック" do
        it "勘定科目と会計部門コンボボックス用データ取得が出来ていること" do
          params = {:partner => "1", :company =>"1"}
          get :new, params
          expect(assigns(:ac_collection).count).to eq 3
          expect(assigns(:div_collection).count).to eq 3
        end
      end
    end
    
    describe "POST #create" do
    
      context "正常性チェック" do
        it "勘定科目と会計部門コンボボックス用データ取得が出来ていること" do
          params = {"partner" => "1", "company" =>"1", 
                      "invoice"=>
                        {"lock_version"=>"0", 
                          "company_setting_id"=>"1", 
                          "zip"=>"", 
                          "addr1"=>"", 
                          "addr2"=>"", 
                          "contact_name1"=>"TEST空輸(株)", 
                          "contact_name2"=>"", 
                          "contact_name3"=>"", 
                          "sell_day(1i)"=>"2019", 
                          "sell_day(2i)"=>"1", 
                          "sell_day(3i)"=>"24", 
                          "currency_id"=>"1", 
                          "remarks"=>"", 
                          "due_day(1i)"=>"2019", 
                          "due_day(2i)"=>"1", 
                          "due_day(3i)"=>"24", 
                          "disable_due_day"=>"0", 
                          "delivery"=>"1"}, 
                       "detail_lines"=>"1", 
                       "detail0"=>
                         {"line_no"=>"1", 
                         "description"=>"a", 
                         "unit_price"=>"1", 
                         "qty"=>"1", 
                         "unit"=>"", 
                         "amount"=>"1", 
                         "vat_code"=>"0"},
                       "ba"=>["1"], 
                       "journal_lines"=>"1", 
                       "journal0"=>
                         {"lineno"=>"1", 
                         "account_title_id"=>"108", 
                         "vat_id"=>"-1", 
                         "amount"=>"1", 
                         "division_id"=>"3", 
                         "dim2_id"=>"1", 
                         "remarks"=>"test"}}
          post :create, params
          expect(assigns(:ac_collection).count).to eq 3
          expect(assigns(:div_collection).count).to eq 3
        end
      end
    end
  
    describe "GET #edit_journals" do
    
      context "正常性チェック" do
        it "勘定科目と会計部門コンボボックス用データ取得が出来ていること" do
          params = {"id" => "1"}
          get :edit_journals, params
          expect(assigns(:ac_collection).count).to eq 3
          expect(assigns(:div_collection).count).to eq 3
        end
      end
    end
  
    describe "patch #update_journals" do
    
      context "正常性チェック" do
        it "勘定科目と会計部門コンボボックス用データ取得が出来ていること" do
          params = {"invoice"=>{"lock_version"=>"0"}, 
                    "journal_lines"=>"1", 
                    "journal0"=>
                      {"lineno"=>"1", 
                        "account_title_id"=>"108", 
                        "vat_id"=>"0", 
                        "amount"=>"100", 
                        "division_id"=>"1", 
                        "dim2_id"=>"1", 
                        "remarks"=>"test99"},
                    "id"=>"1"}
          patch :update_journals, params
          expect(assigns(:ac_collection).count).to eq 3
          expect(assigns(:div_collection).count).to eq 3
        end
      end
    end
  end


  describe "経理メモ" do
  
    describe "Update/Delete keiri_memo" do
      let(:sinv){FactoryBot.create(:sales_invoice)}
      subject{sinv}
      context "DB変更" do
        it "update のとき" do
          sinv.keiri_memo = "変更後の経理メモ"
          sinv.save
          sinv.reload
          expect{sinv}.to change { SalesInvoice.count }.by(0)
        end
    
        it "delete のとき" do
          bef_size = SalesInvoice.count
          sinv.destroy
          expect(SalesInvoice.count).to eq bef_size
        end
      end
    
    end
    
    describe "POST #create" do
          params = {"partner" => "1", "company" =>"1", 
                      "invoice"=>
                        {"lock_version"=>"0", 
                          "company_setting_id"=>"1", 
                          "zip"=>"", 
                          "addr1"=>"", 
                          "addr2"=>"", 
                          "contact_name1"=>"TEST空輸(株)", 
                          "contact_name2"=>"", 
                          "contact_name3"=>"", 
                          "sell_day(1i)"=>"2019", 
                          "sell_day(2i)"=>"1", 
                          "sell_day(3i)"=>"24", 
                          "currency_id"=>"1", 
                          "remarks"=>"", 
                          "due_day(1i)"=>"2019", 
                          "due_day(2i)"=>"1", 
                          "due_day(3i)"=>"24", 
                          "disable_due_day"=>"0", 
                          "delivery"=>"1"}, 
                       "detail_lines"=>"1", 
                       "detail0"=>
                         {"line_no"=>"1", 
                         "description"=>"a", 
                         "unit_price"=>"1", 
                         "qty"=>"1", 
                         "unit"=>"", 
                         "amount"=>"1", 
                         "vat_code"=>"0"},
                       "ba"=>["1"], 
                       "journal_lines"=>"1", 
                       "journal0"=>
                         {"lineno"=>"1", 
                         "account_title_id"=>"108", 
                         "vat_id"=>"-1", 
                         "amount"=>"1", 
                         "division_id"=>"5", 
                         "dim2_id"=>"1", 
                         "remarks"=>"test"}}
      subject {post :create, params}
     
      context "正常終了" do
        it "レコード件数が１件増えること" do
          expect{subject}.to change(SalesInvoice, :count).by(1)
        end
     
        it "新規登録した情報を保持していること" do
          subject
          expect(assigns(:invoice)).to be_a(SalesInvoice)
          expect(assigns(:invoice)).to be_persisted
        end
     
        it "showにリダイレクトされること" do
          subject
          expect(response).to redirect_to :action => 'show', :id => assigns(:invoice).id
        end
      end
     
      context "validation にてエラーになるとき" do
        it "新規登録画面を表示すること" do
          params["invoice"].merge!("keiri_memo" => "a" * 201)
          subject
          expect(assigns(:invoice).errors.present?).to be true
          expect(response).to render_template(:new)
        end
      end         
    
    end
  end

end