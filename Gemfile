
# Netsphere Financial Front / ネットスフィア経理フロント
# Copyright (C) 2007-2009,2011-2013 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/bside/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# 次で生成:
# $ rails new KeiriFront --database postgresql --skip-active-storage --javascript=webpack --skip-bundle 


source "https://rubygems.org"

# v3.3.1 では、ピンポイントで bootsnap が動かない.
ruby ">= 3.3.2"

# Bundle edge Rails instead: gem "rails", github: "rails/rails", branch: "main"
gem "rails", "~> 7.2"

# The original asset pipeline for Rails [https://github.com/rails/sprockets-rails]
gem "sprockets-rails"

# Use postgresql as the database for Active Record
gem "pg", "~> 1.1"

# Use the Puma web server [https://github.com/puma/puma]
#gem "puma", ">= 5.0"

# Bundle and transpile JavaScript [https://github.com/rails/jsbundling-rails]
gem "jsbundling-rails"

# Hotwire's SPA-like page accelerator [https://turbo.hotwired.dev]
gem "turbo-rails"

# Hotwire's modest JavaScript framework [https://stimulus.hotwired.dev]
gem "stimulus-rails"

# Build JSON APIs with ease [https://github.com/rails/jbuilder]
gem "jbuilder"

# Use Redis adapter to run Action Cable in production
gem "redis", ">= 4.0.1"

# Use Kredis to get higher-level data types in Redis [https://github.com/rails/kredis]
# gem "kredis"

# Use Active Model has_secure_password [https://guides.rubyonrails.org/active_model_basics.html#securepassword]
# gem "bcrypt", "~> 3.1.7"

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: %i[ windows jruby ]

if RUBY_VERSION != '3.3.1'
  # Reduces boot times through caching; required in config/boot.rb
  gem "bootsnap", require: false
end

group :development, :test do
  # See https://guides.rubyonrails.org/debugging_rails_applications.html#debugging-with-the-debug-gem
  gem "debug", platforms: %i[ mri windows ], require: "debug/prelude"

  # Static analysis for security vulnerabilities [https://brakemanscanner.org/]
  gem "brakeman", require: false

  # Omakase Ruby styling [https://github.com/rails/rubocop-rails-omakase/]
  gem "rubocop-rails-omakase", require: false

  # RSpec
  gem 'rspec-rails'
end

group :development do
  # Use console on exceptions pages [https://github.com/rails/web-console]
  gem "web-console"
end

group :test do
  # Use system testing [https://guides.rubyonrails.org/testing.html#system-testing]
  gem "capybara"
  gem "selenium-webdriver"
end


# rails 4.x のみ対応
#gem 'activerecord-deprecated_finders',
#    require: 'active_record/deprecated_finders'

# will_paginate 3.1.0 で, Model.paginate() という書き方が廃止された.
# 2023 年に v4.0.0 が出ている
#gem "will_paginate", ">= 4.0"

# will_paginate で書き直すのなら, 移行してもよい
gem 'pagy'

# error_messages_for(record)
# 2023 年に v1.3.1 が出ている
gem "dynamic_form"

# 'better_nested_set' は, 大昔に廃れた。
# 後継は 'awesome_nested_set' gem
# コストセンタ (`ControlDivision` クラス) で使用.
# Rails 7.2 needs awesome_nested_set >= 3.7.0  
gem "awesome_nested_set", ">= 3.7.0"

# SFTP for Concur
gem "net-sftp"

# 認証も作り直し
gem "sorcery"
gem "openid_connect"

