#!/bin/sh
# rootになって実行すること
if [ $# -lt 1 ]; then
  echo "usage:"
  echo "  restore_db.sh <<filename>>"
  exit 1
fi
echo "1"
sudo -u postgres dropdb --no-password financial_front_dev
echo "2"
sudo -u postgres createdb --no-password --encoding=UTF-8 --owner=rails financial_front_dev
echo "3"
sudo -u postgres pg_restore --no-password -d financial_front_dev $1
