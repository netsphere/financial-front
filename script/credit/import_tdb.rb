# -*- coding: utf-8 -*-

# 信用調査データのインポート

require_relative '../import_common'
require 'csv'


#############################################################
# 設定

REPORT_DAY = Date.new 2014, 2, 14
FILE_NAME = "COSMS001.TXT"


#############################################################



def import_tdb filename
  data = sjis2utf8 File.read(File.expand_path('../../import_common/'+ filename, __FILE__))

  Partner.transaction {
    lineno = 0
    CSV.parse(data) {|line|
      lineno += 1
      # next if lineno <= 1

      # まずTDBコードで探す
      partner = Partner.find_by_tdb_code line[0] 
      if !partner
        partner = Partner.find_by_zaimu_id(line[153]) 
      end

      if !partner
        raise # 保守的に, エラーにする
        print [line[153], "", line[4]].join("\t"), "\n"

        partner = Partner.new :zaimu_id => line[153],
                        :tdb_code => line[0],
                        :name => normalize_kc(line[4].gsub(/[ 　\t\r\n]/, '')),
                        :kana_name => normalize_kc(line[2]),
                        :hq_zip => "#{line[5][0..2]}-#{line[5][3..6]}",
                        :hq_addr => line[8],
                        :industry => line[136],
                        :remarks => ''
        partner.save!
      else
        # 取引先データあり
        #
        # 取引先名は、社名が古くなってしまうことがなるので、更新しない。
        # あらかじめ compare_bp_names.rb ファイルを使って、目視チェックとマニュアル更新すること
        #
        print [partner.zaimu_id, partner.name, line[4]].join("\t"), "\n"
        raise "partner_id = #{partner.id}" if partner.tdb_code && partner.tdb_code != line[0].to_i

        if partner.tdb_code != line[0] ||
             #  partner.name != normalize_kc(line[4].gsub(/[ 　\t\r\n]/, '')) ||
               partner.hq_zip != "#{line[5][0..2]}-#{line[5][3..6]}" ||
               partner.hq_addr != line[8] ||
               partner.industry != line[136]
          partner.attributes = {
                :tdb_code => line[0],
               # :name => normalize_kc(line[4].gsub(/[ 　\t\r\n]/, '')),
                :hq_zip => "#{line[5][0..2]}-#{line[5][3..6]}",
                :hq_addr => line[8],
                :industry => line[136],
                :updated_by => nil
          }
          partner.save!
        end
      end
    
      # 信用調査を追加
      ci = CreditInquiry.new :partner => partner,
                      :report_date => REPORT_DAY,
                      :score => line[21].to_i != 0 ? line[21] : nil,
                      :remarks => "(一括調査)",
                      :create_user_id => nil
      ci.save!
    }
  } # of transaction
end


import_tdb FILE_NAME
