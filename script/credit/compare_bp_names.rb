# -*- coding: utf-8 -*-

require_relative "../import_common"
require 'csv'


#############################################################
# 設定

FILE_NAME = "COSMS001.TXT"


#############################################################


# CSVファイルと現在のテーブルの名前を並べて出力
def print_pairs filename
  data = sjis2utf8 File.read(File.expand_path('../../import_common/'+ filename, __FILE__))

  print ["ユーザ定義CD", "(local)名前", "本社所在地", "TDBコード",
         "(TDB)名前", "本社所在地", "代表者", "TDBコード"].join("\t"), "\n"

  lineno = 0
  CSV.parse data do |line|
    lineno += 1
    next if lineno <= 1

    tdb_norm_name = norm(line[4]).gsub(/[ 　]/, "")
    # まずTDBコードで探す .. 取引先コードが違う可能性
    partner = Partner.find_by_tdb_code line[0] 
    # 153 = ユーザ定義コード
    partner = Partner.find_by_zaimu_id line[153] if !partner
    # 同名?
    partner = Partner.find_by_name(tdb_norm_name) if !partner

    if !partner
      print [line[153], "", "", "", line[4], line[8], line[66], line[0], "■"].join("\t"), "\n"
    else
      # 何か見つかった
      unmatch = if norm(partner.name).gsub(/[ 　]/, "") != tdb_norm_name ||
                      sprintf("%09d", partner.tdb_code) != line[0]
                  "■"
                else
                  ""
                end

      next if unmatch == ""
      print [partner.zaimu_id, partner.name, partner.hq_addr, partner.tdb_code,
             line[4], line[8], line[66], line[0], unmatch].join("\t"), "\n"
    end
  end
end


print_pairs FILE_NAME
