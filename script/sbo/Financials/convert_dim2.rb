# -*- coding: utf-8 -*-

# 会計部門 vs 次元2マトリクス表 (CSVファイル) から、SBOにインポートするための
# CSVファイルを出力する。


if __FILE__ == $0
  require_relative "../../import_common"
end
require_relative "read_dim2_matrix"


def conv fname
  `iconv --from-code UTF-8 --to-code UTF-16 #{fname} > #{fname}.u16`
end


def main filename
  nest_map, divs, dim2s = read_matrix_file( filename )

  tmpl = File.read("tmpl-oprc-profitcenter.csv")

  # 会計部門+次元2
  File.open("OPRC-ProfitCenter-dim1.txt", "w") do |fp|
    fp.print tmpl
    nest_map.each do |line|
      fp.print line.join("\t"), "\r\n"
    end
  end
  conv("OPRC-ProfitCenter-dim1.txt")

  # 同じものを別形式で。
  File.open("OPRJ-Project.txt", "w") do |fp|
    fp.print File.read("tmpl-OPRJ-project.txt")
    nest_map.each do |line|
      fp.print [line[0], line[1], line[5], line[6], "tYES"].join("\t"), "\r\n"
    end
  end
  conv("OPRJ-Project.txt")

  # 会計部門のみ
  File.open("OPRC-ProfitCenter-dim2.txt", "w") do |fp|
    fp.print tmpl
    divs.each do |div|
      fp.print [
                sprintf("%05d", div.code),
	        div.name, 
                nil,
                2,
                nil,
                20080101,
                nil,
                "tYES", "", ""].join("\t"), "\r\n"
    end  
  end
  conv("OPRC-ProfitCenter-dim2.txt")

  File.open("OPRC-ProfitCenter-dim3.txt", "w") do |fp|
    fp.print tmpl
    dim2s.each do |code, name|
      fp.print [
                code,
                name,
                nil,
                3,
                nil,
                20080101,
                nil,
                "tYES", "", ""].join("\t"), "\r\n"
    end
  end
  conv("OPRC-ProfitCenter-dim3.txt")
end # main


if __FILE__ == $0
  main ARGV[0]
end
