# -*- coding: utf-8 -*-

require "csv"
require "ostruct"


# (会計部門 vs 次元2)マトリクス表を読み込む
def read_matrix_file filename
  data = sjis2utf8 File.read(filename)

  dim2s = {}

  col2code = {}
  codes = nil # tmp

  nest_map = []
  divs = []

  lineno = 0
  CSV.parse(data) do |line|
    lineno += 1

    if lineno <= 6
      next 
    elsif lineno == 7
      raise "file error." if line[5] != "100"
      codes = line
      next
    elsif lineno == 8
      j = 5
      while codes[j].to_s.strip != ""
        col2code[j] = codes[j].to_i
        dim2s[ codes[j].to_i ] = line[j].gsub(/[\r\n]/,"")
        j += 1
      end
      next
    end

    next if line[0].to_i == 1
    if line[1].to_s.strip == "" # 打ち切り
      puts "end lineno = #{lineno}"
      break 
    end

    # map
    div = OpenStruct.new
    div.code = line[1].to_i
    div.name = normalize_kc(line[2].strip)
    div.default = line[4].to_i
    divs << div

    j = 5
    while j < line.length && line[j].to_s.strip != "|"
      t = line[j].to_s.strip 
      if t == "●" || t == "◯" || t == "〇" || t == "○"
        output = [
                  sprintf("%05d%03d", div.code, col2code[j]),
                  "#{div.name} [#{dim2s[col2code[j]]}]",
                  nil,
                  1,
                  nil,
                  20080101,
                  nil,
                  "tYES",
                  sprintf("%05d", div.code),
                  sprintf("%03d", col2code[j])
                 ]
        nest_map << output
      end
      
      j += 1
    end

  end # Reader.parse

  return [nest_map, divs, dim2s]
end # of read_matrix_file
