# -*- coding: utf-8 -*-

# 部門コードの名前, デフォルト次元2を更新する。

if __FILE__ == $0
  require File.dirname(__FILE__) + "/../../import_common"
end

require "./read_dim2_matrix"


def main filename
  nest_map, divs, dim2s = read_matrix_file( filename )

  divs.each do |div|
    r = Division.find_by_division_code sprintf("%05d", div.code)
    if !r
      puts "not found: #{div.code}"
    else
      if r.default_dim2.dim_code != sprintf("%03d", div.default) ||
         r.name != div.name
        puts "#{r.division_code} #{r.name}: #{div.name} #{r.default_dim2.dim_code} #{div.default}"
        r.default_dim2_id = Dimension.find_by_dim_code(sprintf("%03d", div.default)).id
        r.name = div.name
        r.save!
      end
    end
  end

end

if __FILE__ == $0
  Division.transaction do 
    main ARGV[0]
  end
end
