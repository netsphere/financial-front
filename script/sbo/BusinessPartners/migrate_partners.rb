#!/usr/local/rbenv/versions/2.3.3/bin/ruby
# -*- coding:utf-8 -*-

# DTWインポート用の取引先CSVファイルを作る
# 取引マスタファイルと銀行口座マスタファイルの2ファイルを出力する

require_relative "../../import_common"
require "fileutils"

# 設定ファイル
require_relative "migrate_partners-conf"

# 出力先
UPLOAD_DIR = "/home/upload"

LAST_DATE_FILE = File.dirname(__FILE__) + "/LAST_DATE"

TMPL_DIR = File.dirname(__FILE__) + "/tmpl"

#################################################################

AR1 = AccountTitle.find :first, :conditions => ["account_code = 115"]
AR2 = AccountTitle.find :first, :conditions => ["account_code = 168"]

# 社内経理システムから仕訳を送るときに, SUBを付けるかどうかが安定しない (マス
# タ－子の紐付けを変更すると、SUBの有無が変わってしまう
SUB = "" # "SUB"


# 文字の切れ目を考慮しつつ, バイト単位で切り詰める
def strip_bytes str, len
  r = ""
  ary = str.split(//u)
  while !ary.empty? && (r.length + ary.first.length <= len)
    r << ary.shift
  end
  r
end


# SBOでは, 社員を取引先マスタとして投入する
def append_worker_line fp, worker, card_type
  raise TypeError if !worker.is_a?(User)

  # テスト社員は作らない
  return if worker.worker_code.to_i == 0

  # 社員は通貨コードを付けない
  bp_code = sprintf("E%06d", worker.worker_code.to_i) + 
            (card_type == "cCustomer" ? 
                    "C" : 
                    (card_type == "cSupplier" ? "V" : raise))

                               # DTWファイルの2行目
  line = [
    bp_code,                   # CardCode
    worker.name,     # CardName
    card_type,                 # CardType
    (card_type == "cCustomer" ? 103 : 102),            # GroupCode
    nil,  # Address
    nil,            # ZipCode
    nil,                        # MailAddres
    nil,                           # MailZipCod
    nil,                           # Phone1
    nil,                           # Phone2
    nil,                           # Fax
    nil,                           # CntctPrsn
    nil,           # Notes
    nil,                           # GroupNum
    nil,                           # CreditLine
    nil,                           # DebtLine
    nil,                           # Discount
    "vLiable",                 # VatStatus  "vExempted", "vLiable", "vEC"
    nil,                           # LicTradNum
nil, # DdctStatus
nil, # DdctPrcnt
nil, # ValidUntil
nil, # ListNum
nil, # IntrstRate
nil, # Commission
nil, # CommGrCode
    nil, # Free_Text
nil, # SlpCode
    "##",       # Currency  複数通貨は "##". 2014.1.28 取引先マスタは複数通貨にする
    nil, # RateDifAct
    worker.kana_name,  # Cellular
nil, # AvrageLate
nil, # City
nil, # County
nil, # Country
nil, # MailCity
nil, # MailCounty
nil, # MailCountr
nil, # E_Mail
nil, # Picture
nil, # DflAccount
nil, # DflBranch
nil, # BankCode  -- DefaultBank
    nil, # AddID
nil, # Pager
nil, # FatherCard
nil, # CardFName
nil, # FatherType
nil, # DdctOffice
nil, # ExportCode
nil, # MinIntrst
nil, # ECVatGroup
nil, # ShipType
nil, # Password
nil, # Indicator
nil, # IBAN
nil, # CreditCard
nil, # CrCardNum
nil, # CardValid
    (card_type == "cCustomer" ? "1700_000" : "3230_000"),  # DebPayAcct
nil, # validFor
nil, # validFrom
nil, # validTo
nil, # ValidComm
nil, # frozenFor
nil, # frozenFrom
nil, # frozenTo
nil, # FrozenComm
nil, # Block
nil, # State1
nil, # ExemptNo
nil, # Priority
nil, # FormCode
nil, # Box1099
    -1, # PymCode
nil, # BackOrder
nil, # PartDelivr
nil, # BlockDunn
nil, # BankCountr
nil, # HouseBank
nil, # HousBnkCry
nil, # HousBnkAct
nil, # ShipToDef
nil, # CollecAuth
nil, # DME
nil, # InstrucKey
nil, # SinglePaym
nil, # ISRBillId
nil, # PaymBlock
nil, # RefDetails
nil, # HousBnkBrn
nil, # OwnerIdNum
nil, # PyBlckDesc
nil, # LetterNum
nil, # MaxAmount
nil, # FromDate
nil, # ToDate
nil, # ConnBP
nil, # MltMthNum
nil, # 	DeferrTax
nil, # 	Equ
nil, # 	WTLiable
nil, # 	CrtfcateNO	
nil, # ExpireDate
nil, # 	NINum
nil, # 	AccCritria
nil, # 	WTCode
nil, # 	Building	
nil, # DpmClear	
nil, # ChannlBP	
nil, # DfTcnician
nil, # BillToDef
nil, # BoEDiscnt
nil, # Territory
nil, # MailBuildi
nil, # BoEPrsnt
nil, # ProjectCod
    (card_type == "cCustomer" ? "JP6105" : 
            (card_type == "cSupplier" ? "JP1005" : raise)), # VatGroup
nil, # DunTerm
nil, # IntrntSite
nil, # OtrCtlAcct
nil, # BoEOnClct
nil, # CmpPrivate
nil, # LangCode
nil, # UnpaidBoE
nil, # DdgKey
nil, # CDPNum
nil, # Profession
nil, # BCACode
nil, # TaxRndRule
    "tNO", # QryGroup1  取引先プロパティ1
    "tNO", # QryGroup2
    "tNO", # QryGroup3
    "tNO", # QryGroup4
    "tNO", # QryGroup5
    "tNO", # QryGroup6
    "tNO", # QryGroup7
    "tNO", # QryGroup8
    "tNO", # QryGroup9
    "tNO", # QryGroup10
    "tNO", # QryGroup11
    "tNO", # QryGroup12
    "tNO", # QryGroup13
    "tNO", # QryGroup14
    "tNO", # QryGroup15
    "tNO", # QryGroup16
    "tNO", # QryGroup17
    "tNO", # QryGroup18
    "tNO", # QryGroup19
    "tNO", # QryGroup20
    "tNO", # QryGroup21
    "tNO", # QryGroup22
    "tNO", # QryGroup23
    "tNO", # QryGroup24
    "tNO", # QryGroup25
    "tNO", # QryGroup26
    "tNO", # QryGroup27
    "tNO", # QryGroup28
    "tNO", # QryGroup29
    "tNO", # QryGroup30
    "tNO", # QryGroup31
    "tNO", # QryGroup32
    "tNO", # QryGroup33
    "tNO", # QryGroup34
    "tNO", # QryGroup35
    "tNO", # QryGroup36
    "tNO", # QryGroup37
    "tNO", # QryGroup38
    "tNO", # QryGroup39
    "tNO", # QryGroup40
    "tNO", # QryGroup41
    "tNO", # QryGroup42
    "tNO", # QryGroup43
    "tNO", # QryGroup44
    "tNO", # QryGroup45
    "tNO", # QryGroup46
    "tNO", # QryGroup47
    "tNO", # QryGroup48
    "tNO", # QryGroup49
    "tNO", # QryGroup50
    "tNO", # QryGroup51
    "tNO", # QryGroup52
    "tNO", # QryGroup53
    "tNO", # QryGroup54
    "tNO", # QryGroup55
    "tNO", # QryGroup56
    "tNO", # QryGroup57
    "tNO", # QryGroup58
    "tNO", # QryGroup59
    "tNO", # QryGroup60
    "tNO", # QryGroup61
    "tNO", # QryGroup62
    "tNO", # QryGroup63
    "tNO", # QryGroup64
nil, # RegNum
nil, # VerifNum
nil, # DscntObjct
nil, # DscntRel
nil, # TypWTReprt
nil, # ThreshOver
nil, # SurOver
nil, # DpmIntAct
nil, # OpCode347
nil, # InsurOp347
nil, # HierchDdct
nil, # WHShaamGrp
nil, # CertWHT
nil, # CertBKeep
nil, # PlngGroup
nil, # Affiliate
nil, # IndustryC  -- type=long, references OOND
nil, # VatIDNum
nil, # DatevAcct
nil, # DatevFirst
nil, # GTSRegNum
nil, # GTSBankAct
nil, # GTSBilAddr
    sprintf("E%06d", worker.worker_code.to_i), # HsBnkIBAN   -- 代表取引先
    1
  ]

  fp.print line.join("\t"), "\n"
end


# OCRD 取引先マスタ
def append_partner_line fp_ocrd, partner, card_type
  raise TypeError if !partner.is_a?(Partner)

  # 実際の取引通貨で、取引先マスタの統制勘定を設定する
  if card_type == "cSupplier"
    currs = Invoice.find(:all,
                  :select => "DISTINCT currency_id",
                  :conditions => ["partner_id = ?", partner.id]).map do |r|
              Currency.find(r.currency_id).code
            end
  else
    currs = SalesInvoice.find(:all,
                  :select => "DISTINCT currency_id",
                  :conditions => ["partner_id = ?", partner.id]).map do |r| 
              Currency.find(r.currency_id).code
            end
  end

  # 一つだけ、代表として統制勘定を設定
  curr_code = (currs + ["JPY"]).uniq.first
  append_partner_line2 fp_ocrd, partner, curr_code, card_type
end


# 2014.1.28 取引先マスタについては, すべて複数通貨で登録する
def append_partner_line2 fp_ocrd, partner, curr_code, card_type
  raise TypeError if !curr_code.is_a?(String)
  raise ArgumentError if curr_code.length != 3

  if card_type == "cSupplier"
    # 営業未払金/未払金の選択
    details = InvoiceDetail.find :all, 
          :select => "DISTINCT account_title_id",
          :joins => "LEFT JOIN invoices ON invoice_details.invoice_id = invoices.id",
          :conditions => ["invoices.partner_id = ?", partner.id]
    if details.length >= 1
      t = AccountTitle.find(details.first.account_title_id).payable
      ac_code = t ? t.account_code : 305
    else
      ac_code = 305
    end
  else
    # 売掛
    details = SiJournal.find :all,
               :select => "DISTINCT account_title_id",
               :joins => "LEFT JOIN sales_invoices ON si_journals.sales_invoice_id = sales_invoices.id",
               :conditions => ["sales_invoices.partner_id = ?", partner.id]
    if details.length >= 1
      t = AccountTitle.find(details.first.account_title_id).receivable
      ac_code = t ? t.account_code : 115
    else
      ac_code = 115
    end
  end

  # SBO取引先コード
  bp_code = sprintf( "%06d%s", partner.zaimu_id, 
                               (card_type == "cCustomer" ? 
                                  "C" : 
                                  (card_type == "cSupplier" ? "V" : raise)) )

                               # DTWファイルの2行目
  line = [
    bp_code,                   # CardCode
    partner.name,     # CardName
    card_type,                 # CardType
    (card_type == "cCustomer" ? 100 : 101),                   # GroupCode
    nil,            # Address
    nil,            # ZipCode
    nil,                        # MailAddres
    nil,                           # MailZipCod
    nil,                           # Phone1
    nil,                           # Phone2
    nil,                           # Fax
    nil,                           # CntctPrsn
    partner.industry,  # Notes
    nil,                           # GroupNum
    nil,                           # CreditLine
    nil,                           # DebtLine
    nil,                           # Discount
    "vLiable",                 # VatStatus  "vExempted", "vLiable", "vEC"
    nil,                           # LicTradNum
nil, # DdctStatus
nil, # DdctPrcnt
nil, # ValidUntil
nil, # ListNum
nil, # IntrstRate
nil, # Commission
nil, # CommGrCode
    partner.remarks.gsub(/\r?\n/, "/").gsub(/\t/, " "), # Free_Text
nil, # SlpCode
    "##",   # Currency  複数通貨は "##"
nil, # RateDifAct
    partner.kana_name,  # Cellular
nil, # AvrageLate
nil, # City
nil, # County
nil, # Country
nil, # MailCity
nil, # MailCounty
nil, # MailCountr
nil, # E_Mail
nil, # Picture
nil, # DflAccount
nil, # DflBranch
nil, # BankCode  -- DefaultBank
    partner.tdb_code, # AddID
nil, # Pager
nil, # FatherCard
nil, # CardFName
nil, # FatherType
nil, # DdctOffice
nil, # ExportCode
nil, # MinIntrst
nil, # ECVatGroup
nil, # ShipType
nil, # Password
nil, # Indicator
nil, # IBAN
nil, # CreditCard
nil, # CrCardNum
nil, # CardValid
    ("#{ac_code}0_" + curr_code),  # DebPayAcct 債権債務(初期値)は通貨単位
nil, # validFor
nil, # validFrom
nil, # validTo
nil, # ValidComm
nil, # frozenFor
nil, # frozenFrom
nil, # frozenTo
nil, # FrozenComm
nil, # Block
nil, # State1
nil, # ExemptNo
nil, # Priority
nil, # FormCode
nil, # Box1099
    -1, # PymCode
nil, # BackOrder
nil, # PartDelivr
nil, # BlockDunn
nil, # BankCountr
nil, # HouseBank
nil, # HousBnkCry
nil, # HousBnkAct
nil, # ShipToDef
nil, # CollecAuth
nil, # DME
nil, # InstrucKey
nil, # SinglePaym
nil, # ISRBillId
nil, # PaymBlock
nil, # RefDetails
nil, # HousBnkBrn
nil, # OwnerIdNum
nil, # PyBlckDesc
nil, # LetterNum
nil, # MaxAmount
nil, # FromDate
nil, # ToDate
nil, # ConnBP
nil, # MltMthNum
nil, # 	DeferrTax
nil, # 	Equ
nil, # 	WTLiable
nil, # 	CrtfcateNO	
nil, # ExpireDate
nil, # 	NINum
nil, # 	AccCritria
nil, # 	WTCode
nil, # 	Building	
nil, # DpmClear	
nil, # ChannlBP	
nil, # DfTcnician
nil, # BillToDef
nil, # BoEDiscnt
nil, # Territory
nil, # MailBuildi
nil, # BoEPrsnt
nil, # ProjectCod
    curr_code == "JPY" ? 
         (card_type == "cCustomer" ? "JP6105" : 
              (card_type == "cSupplier" ? "JP1005" : raise)) :
         nil, # VatGroup
nil, # DunTerm
nil, # IntrntSite
nil, # OtrCtlAcct
nil, # BoEOnClct
nil, # CmpPrivate
nil, # LangCode
nil, # UnpaidBoE
nil, # DdgKey
nil, # CDPNum
nil, # Profession
nil, # BCACode
nil, # TaxRndRule
    (card_type == "cCustomer" && 
       (bp_code.index("36") == 0 || bp_code.index("37") == 0) ? 
                     "tYES" : "tNO"), # QryGroup1  取引先プロパティ1
    "tNO", # QryGroup2
    "tNO", # QryGroup3
    "tNO", # QryGroup4
    "tNO", # QryGroup5
    "tNO", # QryGroup6
    "tNO", # QryGroup7
    "tNO", # QryGroup8
    "tNO", # QryGroup9
    "tNO", # QryGroup10
    "tNO", # QryGroup11
    "tNO", # QryGroup12
    "tNO", # QryGroup13
    "tNO", # QryGroup14
    "tNO", # QryGroup15
    "tNO", # QryGroup16
    "tNO", # QryGroup17
    "tNO", # QryGroup18
    "tNO", # QryGroup19
    "tNO", # QryGroup20
    "tNO", # QryGroup21
    "tNO", # QryGroup22
    "tNO", # QryGroup23
    "tNO", # QryGroup24
    "tNO", # QryGroup25
    "tNO", # QryGroup26
    "tNO", # QryGroup27
    "tNO", # QryGroup28
    "tNO", # QryGroup29
    "tNO", # QryGroup30
    "tNO", # QryGroup31
    "tNO", # QryGroup32
    "tNO", # QryGroup33
    "tNO", # QryGroup34
    "tNO", # QryGroup35
    "tNO", # QryGroup36
    "tNO", # QryGroup37
    "tNO", # QryGroup38
    "tNO", # QryGroup39
    "tNO", # QryGroup40
    "tNO", # QryGroup41
    "tNO", # QryGroup42
    "tNO", # QryGroup43
    "tNO", # QryGroup44
    "tNO", # QryGroup45
    "tNO", # QryGroup46
    "tNO", # QryGroup47
    "tNO", # QryGroup48
    "tNO", # QryGroup49
    "tNO", # QryGroup50
    "tNO", # QryGroup51
    "tNO", # QryGroup52
    "tNO", # QryGroup53
    "tNO", # QryGroup54
    "tNO", # QryGroup55
    "tNO", # QryGroup56
    "tNO", # QryGroup57
    "tNO", # QryGroup58
    "tNO", # QryGroup59
    "tNO", # QryGroup60
    "tNO", # QryGroup61
    "tNO", # QryGroup62
    "tNO", # QryGroup63
    "tNO", # QryGroup64
nil, # RegNum
nil, # VerifNum
nil, # DscntObjct
nil, # DscntRel
nil, # TypWTReprt
nil, # ThreshOver
nil, # SurOver
nil, # DpmIntAct
nil, # OpCode347
nil, # InsurOp347
nil, # HierchDdct
nil, # WHShaamGrp
nil, # CertWHT
nil, # CertBKeep
nil, # PlngGroup
nil, # Affiliate
nil, # IndustryC  -- type=long, references OOND
nil, # VatIDNum
nil, # DatevAcct
nil, # DatevFirst
nil, # GTSRegNum
nil, # GTSBankAct
nil, # GTSBilAddr
    sprintf("%06d", partner.zaimu_id), # HsBnkIBAN   -- 代表 (BI用) 取引先
    1
  ]

  fp_ocrd.print line.join("\t"), "\n"
  
  # 円建ての仕入先については, 取引先マスタを銀行口座の数だけ増やす (SUB)
  if card_type == "cSupplier"
    bas = BankAccount.find :all, 
                     :conditions => ["partner_id = ? AND zaimu_ac <> ? AND (created_at >= ? OR updated_at >= ?)", 
                                     partner.id, partner.zaimu_id, 
                                     $LAST_DATE, $LAST_DATE]
    bas.each do |ba|
      # まれに, 別取引先と被っていることがある => partner側でC/Vを登録
      if Partner.find(:first, 
                      :conditions => ["zaimu_id = ?", ba.zaimu_ac])
        next
      end
      line[0] = sprintf("%06d", ba.zaimu_ac) + "V" + SUB  
      fp_ocrd.print line.join("\t"), "\n"
    end
  end
end


# 住所 (Address) を出力
# CRD1-BPAddr
def append_address_line fp_crd1, partner, card_type
  return if partner.hq_addr.to_s.strip == ""

  bp_code = sprintf( "%06d%s", 
                    partner.zaimu_id,
                    (card_type == "cCustomer" ? "C" : 
                            (card_type == "cSupplier" ? "V" : raise)) )

  line = [
    bp_code,           # CardCode
    0,                         # LineNum
    "本社",                     # Address"
    partner.hq_addr,  # Street
    nil,             # Block
    partner.hq_zip,  # ZipCode
    nil,             # City
    nil,             # Country 文字列
    "JP",            # Country コード
    nil,             # State
    nil, # LicTradNum
    nil, # TaxCode
    nil, # Building
    "bo_BillTo",    # AddressType "bo_ShipTo" or "bo_BillTo"
    nil, # Address2
    nil, # Address3
    nil, # AddrType
    nil, # StreetNo
  ]

  fp_crd1.print line.join("\t"), "\n"  
end


# 取引先マスタ
# "OCRD" - 取引先
def append_partner fp_ocrd, fp_crd1, partner
  raise TypeError if !partner.is_a?(Partner)

#  isc = GeneralLedger.find :first, 
#               :conditions => ["partner_id = ? AND account_title_id IN (?,?)", 
#                               partner.id, AR1.id, AR2.id]

#  if (isc && isc.id) || 
#              (partner.zaimu_id >= 360000 && partner.zaimu_id <= 379999)

  # 得意先としても全部登録する
  append_partner_line fp_ocrd, partner, "cCustomer"
  append_address_line fp_crd1, partner, "cCustomer"

  # 別の取引先に紐づいている口座で, 同じ取引先コードのものがあれば, 重複しないよう, スキップする
  # こちらの取引先のほうが, おそらく使用不可
  #ba = BankAccount.find :first, 
  #                      :conditions => ["partner_id <> ? AND zaimu_ac = ?",
  #                                      partner.id, partner.zaimu_id]
  #if !ba
  append_partner_line fp_ocrd, partner, "cSupplier"
  append_address_line fp_crd1, partner, "cSupplier"
  #end
end


=begin
# 銀行口座マスタを出力する (英名)
# OCRB
def append_en_banks fp_ocrb, fp_odsc
  # TODO: account_addr1
  #       account_addr2

  # bankcodeがswiftコードでは桁あふれ
  # インポートは諦める
  return 

  swift_codes = {}
  #i = 10001

  bas = EnBankAccount.find :all, 
            :select => "work.*",
            :from => "(SELECT *, ROW_NUMBER() OVER (PARTITION BY partner_id ORDER BY id) AS row_seq FROM en_bank_accounts) AS work",
            :conditions => ["partner_id IS NOT NULL"]

  bas.each do |ba|
    # 銀行マスタ
    if !swift_codes[ba.swift_code]
      line = [
        10000 + ba.id,           # AbsEntry
        ba.swift_code,   # BankCode
        "\"#{ba.bank_name}\"",    # BankName
        ba.swift_code,   # SwiftNum
        nil,              # IBAN
        ba.bank_country, # CountryCod        
        nil,              # PostOffice
        nil,              # DfltActKey
      ]
      fp_odsc.print line.join(","), "\n"
      swift_codes[ba.swift_code] = 1
      # i += 1
    end

    # 取引先銀行口座マスタ
    partner = ba.partner
    line = [
      sprintf("%06d", partner.zaimu_id) + "V",   # CardCode
      ba.row_seq.to_i - 1,         # LineNum  ... 取引先ごとに0始まり
nil, # LogInstanc
nil, # UsrNumber4
nil, # CardCode
      ba.bank_country,          # County
nil, # State
nil, # UsrNumber2
      ba.iban_code,             # IBAN
nil, # ZipCode
      "\"#{ba.bank_addr1}\"",            # City
      "\"#{ba.bank_addr2}\"",            # Block
      nil,                      # Branch
      ba.bank_country,          # Country
nil, # Street
nil, # ControlKey
nil, # UsrNumber3
      ba.swift_code,            # BankCode 
      ba.account_no,            # Account
nil, # UsrNumber1
nil, # AbsEntry
nil, # Building
nil, # BIK
      "\"#{ba.account_name}\"",          # AcctName
nil, # CorresAcct
nil, # Phone
nil, # Fax
nil, # CustIdNum
nil, # ISRBillerI
nil # ISRType
    ]
    fp_ocrb.print line.join(","), "\n"
  end
end
=end


# 相手先の銀行口座によって, 出金元口座の初期値を選択する
# @param bp_code "xxxxxxV" or "xxxxxxVSUB"
def append_ja_payment_our_bank ba, bp_code, company_code, fp_crd2
  raise TypeError if !company_code.is_a?(String)
  raise TypeError if !ba.is_a?(BankAccount)

  bidx = BANK_MAP[company_code][:code_map][ba.bank_branch.bank_id]
  if bidx && bidx >= 0
    line2 = [ bp_code, 0, BANK_MAP[company_code][:our_banks][bidx] ]
    fp_crd2.print line2.join("\t"), "\r\n"
  else    
    # 全部の出金元口座を有効にする
    BANK_MAP[company_code][:our_banks].each_with_index do |bank_code, i|
      line2 = [
        bp_code,    #CardCode
        i,          #LineNum
        bank_code,  #PymCode
      ]
      fp_crd2.print line2.join("\t"), "\r\n"
    end
  end
end


# 銀行口座マスタを出力する (JPのみ)
def append_ja_banks fp_ocrb, fp_crd2s
  raise TypeError if !fp_crd2s.is_a?(Hash)

  bas = BankAccount.find :all, 
             :select => "work.*",
             :from => "(SELECT *, ROW_NUMBER() OVER (PARTITION BY partner_id ORDER BY id) as row_seq FROM bank_accounts) AS work",
             :conditions => ["created_at >= ? OR updated_at >= ?",
                             $LAST_DATE, $LAST_DATE],
             :order => "zaimu_ac"

  bas.each_with_index do |ba, abs_ent|
    bank_branch = ba.bank_branch

    bp_code = sprintf( "%06d%s",
                      ba.zaimu_ac,
                      (ba.partner.zaimu_id == ba.zaimu_ac ? "V" : "V" + SUB) )
    line = [
      bp_code,  # CardCode
      0,        # LineNum 相手先銀行口座ごとに取引先を分けることにした=>単に0
nil, # LogInstanc
nil, # UsrNumber4
nil, # CardCode
nil, # County
nil, # State
      ba.ac_type, # UsrNumber2  科目 普通=1, 当座=2
      ba.kana_name,   # IBAN 取引先口座名義. とにかくこの列に入れる
nil, # ZipCode
nil, # City
nil, # Block
      sprintf("%03d", bank_branch.branch_code),  # Branch   -- N3
      "JP",   # Country
nil, # Street
      1, # ControlKey    取引先ごとに連番
nil, # UsrNumber3
      sprintf("%04d", bank_branch.bank_id),      # BankCode  -- N4
      sprintf("%07d", ba.ac_number),             # Account
      bank_branch.kana_name, # UsrNumber1  支店名カナ
      nil, # from ticket 211. abs_ent + 1,# AbsEntry  ファイル全体で, 1から始まる連番
nil, # Building
nil, # BIK
      ba.kana_name,     # AcctName
nil, # CorresAcct
nil, # Phone
nil, # Fax
nil, # CustIdNum
nil, # ISRBillerI
nil  # ISRType
    ]
    fp_ocrb.print line.join("\t"), "\r\n"

    # 出金元口座
    fp_crd2s.each do |company_code, fp|
      append_ja_payment_our_bank ba, bp_code, company_code, fp
    end
  end # bas.each
end


def main
  if !File.file?(LAST_DATE_FILE)
    puts "Not found LAST_DATE"
    exit 1
  end

  # 最後の行のを使う
  File.open(LAST_DATE_FILE) do |fp|
    while (s = fp.gets.to_s.strip) != ""
      $LAST_DATE = Date.parse(s)
    end
    if !$LAST_DATE
      $LAST_DATE = Date.new(2014, 4, 1) # dmy
    end
  end

  # 取引先マスタ
  File.open "OCRD-BusinessPartners-JP.txt", "w" do |fp_ocrd|
    fp_ocrd.print File.read(TMPL_DIR + "/tmpl-OCRD-BusinessPartners-11.csv")

    File.open "CRD1-BPAddresses.txt", "w" do |fp_crd1|
      fp_crd1.print File.read(TMPL_DIR + "/tmpl-CRD1-BPAddresses1.csv")

      partners = Partner.find :all, 
            :select => "DISTINCT partners.*",
            :joins => "LEFT JOIN bank_accounts ON partners.id = bank_accounts.partner_id",
            :conditions => ["partners.created_at >= ? OR partners.updated_at >= ? OR bank_accounts.created_at >= ? OR bank_accounts.updated_at >= ?",
                            $LAST_DATE, $LAST_DATE, $LAST_DATE, $LAST_DATE],
            :order => "zaimu_id"
      partners.each do |partner|
        # 6桁限定
        next if partner.zaimu_id > 999999

      append_partner fp_ocrd, fp_crd1, partner
    end
  end # CRD1 close
  `iconv --from-code UTF-8 --to-code UTF-16 CRD1-BPAddresses.txt > #{UPLOAD_DIR}/CRD1-BPAddresses.txt`

  # 社員マスタ (取引先マスタの一部として)
  workers = User.find :all, 
                      :conditions => ["created_at >= ? OR updated_at >= ?",
                                      $LAST_DATE, $LAST_DATE],
                      :order => "worker_code"
  workers.each do |worker|
    append_worker_line fp_ocrd, worker, "cCustomer"
    append_worker_line fp_ocrd, worker, "cSupplier"
  end
end # OCRD close
`iconv --from-code UTF-8 --to-code UTF-16 OCRD-BusinessPartners-JP.txt > #{UPLOAD_DIR}/OCRD-BusinessPartners-JP.txt`

  # 取引先の銀行口座
  fp_crd2s = {}
  BANK_MAP.each_key do |company_code|
    fp_crd2s[company_code] = 
                  File.open("CRD2-BPPaymentMethods-#{company_code}.txt", "w")
    fp_crd2s[company_code].print( 
                  File.read(TMPL_DIR + "/tmpl-CRD2 - BPPaymentMethods1.txt") )
  end

  File.open "OCRB-BPBankAccounts.txt", "w" do |fp_ba|
    fp_ba.print File.read(TMPL_DIR + "/tmpl-OCRB-BPBankAccounts1.csv")

    append_ja_banks fp_ba, fp_crd2s
    #append_en_banks fp_ba, fp_odsc
  end
  `iconv --from-code UTF-8 --to-code UTF-16 OCRB-BPBankAccounts.txt > #{UPLOAD_DIR}/OCRB-BPBankAccounts.txt`

  fp_crd2s.each do |company_code, fp_crd2|
    fp_crd2.close
    `iconv --from-code UTF-8 --to-code UTF-16 CRD2-BPPaymentMethods-#{company_code}.txt > #{UPLOAD_DIR}/CRD2-BPPaymentMethods-#{company_code}.txt`
  end

  File.open(LAST_DATE_FILE, "a") do |fp|
    fp.puts Date.today
  end
end # main

main
puts "Finished: #{Time.now}"
