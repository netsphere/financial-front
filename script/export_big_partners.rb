# -*- coding:utf-8 -*-
# 取引金額の大きい相手先を抽出

require_relative 'import_common'
require_relative '../app/controllers/application_controller'


# config ***************************************************************

company = CompanySetting.find 1
sday = Date.new 2010, 4, 1
eday = Date.new 2011, 3, 31

# 2010FY
AVG_RATE = {
  "JPY" => 1.0,
  "USD" => 86.45, "EUR" => 113.83, "CAD" => 84.65, "AUD" => 80.73, 
  "PHP" => 1.93, "SGD" => 64.33, "CNY" => 12.84, "KRW" => 0.0750,
  "HKD" => 11.12,
}

# 2008FY
#AVG_RATE = {
#  "USD" => 100.79, "EUR" => 145.90, "JPY" => 1.00, "KRW" => 0.0882
#}

# 2009FY
#   {"USD" => 93.48, "EUR" => 131.54, "JPY" => 1.00, "KRW" => 0.0759}[currency.code] || 1.00



# 第1ステップ: 取引先の当たりをつける
def collect_partners company, sday, eday
  # USドルなどのこともあるので、しきい値は低めにする。

  partners = Partner.find :all,
      :select => 'partners.*',
      :conditions => [<<EOF, company.id, sday, eday],
(SELECT SUM(invoice_details.amount)
  FROM invoices LEFT JOIN invoice_details ON invoices.id = invoice_details.invoice_id
  WHERE partners.id = invoices.partner_id AND company_setting_id = ? AND
        (invoices.journal_day BETWEEN ? AND ?)
) > 1000000
EOF
      :order => 'partners.id'

  return partners
end


# 金額を調べる。勘定科目、通貨、消費税の別
def collect_journals company, partner, sday, eday
  r = InvoiceDetail.find_by_sql [<<EOF, company.id, partner.id, sday, eday, company.id, partner.id, sday, eday]
SELECT account_title_id, SUM(amount) AS amount, currency_id, vat_code
  FROM
((SELECT invoice_details.account_title_id AS account_title_id,
       invoice_details.amount AS amount, invoices.currency_id AS currency_id,
       invoice_details.vat_code AS vat_code
  FROM invoices LEFT JOIN invoice_details ON invoices.id = invoice_details.invoice_id
  WHERE invoices.company_setting_id = ? AND invoices.partner_id = ? AND
        (invoices.journal_day BETWEEN ? AND ?)
) UNION ALL
(SELECT si_journals.account_title_id, -si_journals.amount, 
        sales_invoices.currency_id, si_journals.vat_code
  FROM sales_invoices LEFT JOIN si_journals
                      ON sales_invoices.id = si_journals.sales_invoice_id
  WHERE sales_invoices.company_setting_id = ? AND sales_invoices.partner_id = ? AND
        (sales_invoices.journal_day BETWEEN ? AND ?)
)) AS t
  GROUP BY account_title_id, currency_id, vat_code
EOF

  return r
end


CURRENCIES = {}

def rate currency_id
  CURRENCIES[currency_id] ||= Currency.find currency_id
  currency = CURRENCIES[currency_id]

  AVG_RATE[currency.code] || (raise "currency missing: #{currency.code}")
end


def bank_account partner_id
  ba = BankAccount.find :first, :conditions => ["partner_id = ?", partner_id]
  if ba
    ba.format_long
  else
    "--"
  end
end


pa = {}
collect_partners(company, sday, eday).each do |partner|
  collect_journals(company, partner, sday, eday).each {|r|
    # 税関とか金額がない
    next if !r.amount 

    pa[partner.id] ||= [partner]
    pa[partner.id] << r
  }
end

puts CURRENCIES.inspect

# 年額1,000,000円以上を抽出
pa.each do |partner_id, ary|
  sum = ary[1..-1].inject 0 do |memo, r| 
    #begin
      amount = r.amount * rate(r.currency_id) / 
                      (10.0 ** CURRENCIES[r.currency_id].exponent)
      amount = amount / 1.05 if r.vat_code.to_i == 10
      memo + amount
    #rescue
    #  raise r.inspect
    #end
  end
  next if sum < 3_500_000

  partner = ary[0]

  print [partner_id, 
         partner.name, 
         partner.hq_addr,
         # bank_account(partner_id),
         sum,
         (ary[1..-1].collect do |r| 
           currency = CURRENCIES[r.currency_id]
           jp_amount = r.amount * rate(r.currency_id) / (10.0 ** currency.exponent)
           jp_amount = jp_amount / 1.05 if r.vat_code.to_i == 10
           
           [ AccountTitle.find(r.account_title_id).name2,
             format_amount(jp_amount, 0),
             currency.name, 
             r.vat_code ].join("\t")
         end )
        ].join("\t") + "\n"
end
