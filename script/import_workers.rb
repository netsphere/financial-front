# -*- coding: utf-8 -*-

# 部門、ユーザ（社員）のインポート
# 財務大将から


require File.dirname(__FILE__) + '/import_common.rb'
require 'csv'


# 社員マスタの取り込み。
# 財務大将の社員マスタでは所属部署がないので、ITFlowから取り込む。
def import_workers
  data = sjis2utf8 File.read('SyainMST.csv')
  
  User.transaction {
    idx = 0
    CSV::Reader.parse(data) {|line|
      idx += 1;
      next if idx <= 4
      raise if line[0].to_i == 0

      r = User.find_by_worker_code sprintf("%06d", line[0].to_i)
      if !r
        next if line[21].to_i != 0 # activeでないのをわざわざ追加しない
        r = User.new(:worker_code => sprintf("%06d", line[0].to_i),
                     :name => line[1],
                     :kana_name => line[2],
                     :email => line[6],
                     :active => (line[21].to_i == 0 ? 1 : 0),
                     :keiri_flag => false,
                     :marketing_flag => false,
                     :apar_flag => false,
                     :hrd_flag => false )
      else
        r.attributes = {
          :name => line[1],
          :kana_name => line[2],
          # :email => line[6],
          :active => (line[21].to_i == 0 ? 1 : 0)
        }
      end

      # 同一人物で社員コードが変わることがある。何だそれ。
      if r.email
        s = User.find :first,
                      :conditions => ['email = ? AND worker_code <> ?',
                                      r.email, r.worker_code]
        if s
          if s.active != 0
            if r.active == 0
              r.email = nil
            else
              puts "skip: (#{r.worker_code} #{r.name}) <=> (#{s.worker_code} #{s.name})"
              next
            end
          else
            # メールアドレスがリサイクルされている。えーっ！
            s.email = nil
            s.save!
          end
        end
      end
      begin
        r.save!
      rescue ActiveRecord::RecordInvalid
        puts "validation error: #{r.inspect}"
        raise
      end
      
      # 部門を登録する。
      if line[3] && line[3].strip != ''
        if !find_division_by_code(line[3])
          raise "unknown division: #{line[3]}"
        end

        ud = UsersDivision.find(:first,
                      :conditions => ['user_id = ? AND division_id = ? AND end_day IS NULL',
                                      r.id, find_division_by_code(line[3]).id])
        if !ud
          # 対応するレコードがないときはおそらく異動.
          # UsersDivision.delete_all ['user_id = ?', r.id]
          ud = UsersDivision.new(:user => r,
                                 :division => find_division_by_code(line[3]),
                                 :effective_day => '1999-01-01',
                                 :default_pair => 0 )
        end

        if ud.default_pair == 0
          # 以前のデフォルトをいったんクリアする
          UsersDivision.find_by_sql [<<EOF, r.id]
UPDATE users_divisions SET default_pair = 0
    WHERE user_id = ? and default_pair <> 0
EOF
          ud.default_pair = 1
          ud.save!
        end
      end
    }
  } # of transaction
end

import_workers()
