#!/usr/bin/ruby
# -*- coding: utf-8 -*-

# 勘定科目のインポート
# 財務大将から


require File.dirname(__FILE__) + '/import_common.rb'
require 'csv'


def import_titles
  data = sjis2utf8(File.read(IMPORT_DATA_DIR + '/kanjo.csv'))
  hojo_data = sjis2utf8(File.read(IMPORT_DATA_DIR + '/hojo_kamoku.csv'))

  title_hash = {}

  idx = 0
  CSV::Reader.parse(data) {|line|
    idx += 1; next if idx == 1
    #next if line[1].to_i > 999  # 科目は3桁
    next if line[0].to_i != 0 && line[0].to_i != 2
    title_hash[line[1].to_i] = {:main => line, :suppl => {}}
  }
  idx = 0
  CSV::Reader.parse(hojo_data) {|line|
    idx += 1; next if idx == 1
    #next if line[1].to_i > 999  # 科目は3桁
    # hojo_code, name, kana_name, start_day, end_day
    title_hash[line[1].to_i][:suppl][line[3].to_i] = line
  }

  AccountTitle.transaction {
    title_hash.each {|code, v|
      if v[:suppl].size > 0
        # 補助コードあり
        v[:suppl].each {|suppl_code, suppl_v|
          r = find_account_title(code, suppl_code) || AccountTitle.new(
                         :account_code => code,
                         :suppl_code => suppl_code,
                         :req_partner => false,
                         :aggregate_flag => false,
                         :vat_code => suppl_v[8],
                         :sign => (v[:main][5].to_i == 0 ? +1 : -1),
                         :description => ''
          )
          r.attributes = {
            :name => "#{v[:main][2]}/#{suppl_v[4]}",
            :kana_name => suppl_v[5],
            :main_name => v[:main][2],
            :end_day => suppl_v[10],
          }
          r.vat_code = suppl_v[8] if suppl_v[8].to_i != 0
          r.save!
        }
      else 
        # 補助コードなし
        r = find_account_title(code, 0) || AccountTitle.new(
                :account_code => code,
                :suppl_code => 0,
                :req_partner => false,
                :aggregate_flag => (v[:main][0].to_i == 2),
                :vat_code => v[:main][9],
                :sign => (v[:main][5].to_i == 0 ? +1 : -1),
                :description => ''
        )
        r.attributes = {
          :name => v[:main][2],
          :kana_name => v[:main][3],
          :main_name => v[:main][2],
          :end_day => v[:main][15],
        }
        r.vat_code = v[:main][9] if v[:main][9].to_i != 0
        r.save!
      end
    }
  }
end

puts "Start at #{Time.now}"
import_titles()
puts "Finished at #{Time.now}"
