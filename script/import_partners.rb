#!/usr/bin/ruby
# -*- coding: utf-8 -*-

# 財務大将から取引先をインポートする。

# 銀行口座の登録はすべて財務大将側で行い、
# 経理システム (仮) では、親コードへの紐付け、個人客の不可視だけおこなう。

# 取引先の名称、コメントなどは購買システムで更新できる（長い正式名など）。インポート時に上書きしない。

# 銀行口座のある相手先は、
#   bank_accounts にレコードがあれば更新 (口座名義カナ)
#   なければ、partner に追加するか、partner と二つレコードを作る
# 銀行口座のない相手先は、
#   無視する


require File.dirname(__FILE__) + '/import_common.rb'
require 'csv'


# 取引先を探す。
# @return
#   見つからないときはnil
def find_partner zaimu_id
  partner = Partner.find_by_zaimu_id(zaimu_id)
  if !partner && (zaimu_id % 10) != 0
    # 末尾0で登録がないか探してみる
    partner = Partner.find_by_zaimu_id((zaimu_id / 10) * 10)
  end
  return partner
end


# 取引先レコードを探す・作る
def create_partner line
  zaimu_id = line[0].to_i
  if !(partner = find_partner(zaimu_id))
    name = norm(line[1].split(/[\:：]/u, 2)[0])
    name = "_" if !name || name == ""
    kana_name = norm(line[2])
    kana_name = "_" if !kana_name || kana_name == ""

    partner = Partner.new(:name => name,
                          :kana_name => kana_name,
                          :zaimu_id => zaimu_id,
                          :hq_zip => '',
                          :hq_addr => '',
                          :remarks => norm(line[1].split(/[\:：]/u, 2)[1] || ""),
                          :industry => '' 
                         )
    partner.save!
  end
  return partner
end


# 銀行を探す。
# line に銀行データが含まれないときはnil, 銀行データが含まれて、銀行マスタにないときはエラー
def find_bank line
  bank_code = line[29].to_i
  branch_code = line[31].to_i
  if bank_code == 0 || branch_code == 0 ||
             line[34].to_i == 0 || # 口座番号
             line[35].to_s.strip == ''
    return nil
  end

  branch = BankBranch.find_by_code bank_code, branch_code
  raise "bank not found in bank master table: (#{bank_code}, #{branch_code})" if !branch
  return branch
end


# 請求書などから参照されていない銀行口座を削除する
def clear_bank_accounts
  BankAccount.find_by_sql <<EOF
DELETE FROM bank_accounts 
  WHERE id IN (SELECT bank_accounts.id FROM bank_accounts 
          LEFT JOIN invoices on invoices.bank_account_id = bank_accounts.id 
          LEFT JOIN our_banks on our_banks.bank_account_id = bank_accounts.id
        WHERE invoices.id is null AND our_banks.id is null);
EOF
end


# 取引先の取り込み
def import_partners filename
  data = sjis2utf8_x( File.read(filename) )
  
  Partner.transaction {
    # clear_bank_accounts

    exists = {}
    idx = 0
    CSV::Reader.parse(data) {|line|
      idx += 1 
      next if idx == 1

      zaimu_id = line[0].to_i
      if zaimu_id == 0 ||
         (zaimu_id >= 120000 && zaimu_id <= 129999 && 
          !(zaimu_id >= 129090 && zaimu_id <= 129092) ) ||  # 個人a
         (zaimu_id >= 170000 && zaimu_id <= 179999) # 個人b
        next
      end
      
      begin
        branch = find_bank line # 銀行口座が同一の取引先をまず探す (取引先コードが違うかも)
      rescue
        puts "#{line[0]} #{line[35]}: #{$!.to_s}"
        next
      end

      if !branch  # インポートデータに銀行口座がない
        partner = Partner.find_by_zaimu_id( zaimu_id )
        if !partner
          puts "warning: partner without bank: #{line[0]} #{line[1]} #{line[2]}"

          # 口座のない取引先は、初回インポートのときのみ作る。
          # 運用開始したらこの処理は不要。
          if zaimu_id >= 360000 && zaimu_id <= 379999
            #create_partner line
            # このセクションだけ, 1の位まで取引先コード
            partner = Partner.new(:name => norm(line[1]),
                                  :kana_name => norm(line[2]),
                                  :zaimu_id => zaimu_id,
                                  :hq_zip => '',
                                  :hq_addr => '',
                                  :remarks => "",
                                  :industry => '' )
            partner.save!
          end
        end

        next 
      end

      # 1) 同じ支払先コードで, 社内経理システムにも登録がある
      bank_ac = BankAccount.find :first,
                             :conditions => ["zaimu_ac = ? AND ac_number = ?",
                                             zaimu_id, line[34].to_i ]
      if bank_ac 
        # 既存のレコード (口座番号のみ一致) の銀行・支店を更新
        changed = false
        if bank_ac.bank_branch_id != branch.id
          bank_ac.bank_branch = branch
          bank_ac.ac_type = line[33]  # 間違えやすいのでここで訂正しておく
          changed = true
          puts "#{line[0]} #{line[35]}: changed bank and branch."
        end

        if bank_ac.kana_name != line[35].strip
          bank_ac.kana_name = line[35].strip
          changed = true
          puts "#{line[0]} #{line[35]}: changed account name."
        end

        bank_ac.save! if changed

        exists[bank_ac.id] = true
        next
      end

      # 2) ほかに、「経理フロント」システム側でその口座があるか
      bank_ac = BankAccount.find :first,
        :conditions => ['bank_branch_id = ? AND ac_type = ? AND ac_number = ?',
                        branch.id, line[33].to_i, line[34].to_i]
      if bank_ac
        # 既存のレコードを更新
        if bank_ac.zaimu_ac != zaimu_id
          puts "dup: #{bank_ac.partner.name} / #{bank_ac.zaimu_ac} <=> #{zaimu_id}"
          exists[bank_ac.id] = true  # 削除したりしない
          next
        end

        if bank_ac.kana_name != line[35].strip
          bank_ac.kana_name = line[35].strip
          bank_ac.save!
          puts "#{line[0]} #{line[35]}: changed account name."
        end
      else
        # 新規登録
        if a = BankAccount.find_by_zaimu_ac(zaimu_id)
          # 同じコードで口座がすでにある
          puts "already other account exists: #{a.partner.name} / #{a.zaimu_ac}"
          exists[a.id] = true
          next
        end
        
        partner = create_partner(line)  # 内部で保存もしている
        bank_ac = BankAccount.new({
                        :zaimu_ac => zaimu_id,
                        :partner => partner,
                        :bank_branch => branch,
                        :ac_type => line[33],
                        :ac_number => line[34],
                        :kana_name => line[35].strip,
                        :active => true
        })
        bank_ac.save!
      end
      exists[bank_ac.id] = true      
    }
=begin
    # インポートデータにない口座は削除する
    BankAccount.find(:all).each {|r|
      if !exists[r.id] &&
             !Invoice.find_by_bank_account_id(r.id) &&
             !OurBank.find_by_bank_account_id(r.id)
        puts "deleted: #{r.partner.name} / #{r.zaimu_ac}"
        r.destroy
      end
    }
=end
  } # of transaction
end


# main
puts "Start at #{Time.now}"
filename = ARGV[0].to_s.strip != "" ? 
               ARGV[0] : (IMPORT_DATA_DIR + '/partners.csv')
import_partners filename
puts "Finished at #{Time.now}"
