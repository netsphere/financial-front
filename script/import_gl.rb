#!/usr/bin/ruby
# -*- coding: utf-8 -*-

# 仕訳の取り込み
# ファイル名は決め打ち。会計システムからエクスポートする。


require File.dirname(__FILE__) + '/import_common.rb'
require 'csv'


ACTUAL_NAME = 'actual'


# 期間を調べる: 月初から月末日に補正する
def get_data_term data
  st = nil; ed = nil
  idx = 0
  CSV::Reader.parse(data) {|line|
    idx += 1; next if idx == 1
    d = Date.parse line[0]
    st = st ? [st, d].min : d
    ed = ed ? [ed, d].max : d
  }
  st = Date.new(st.year, st.mon, 1)
  ed = (ed >> 1); ed = Date.new(ed.year, ed.mon, 1) - 1
  return [st, ed]
end


# 取引先を探す
def get_partner zaimu_id
  return nil if zaimu_id.to_i == 0

  r = Partner.find :first, :conditions => ['zaimu_id = ?', zaimu_id]
  return r if r
  r = BankAccount.find :first, :conditions => ['zaimu_ac = ?', zaimu_id]
  return r ? r.partner : nil
end


# 社員コードがあって、システムに社員データがないときはエラー
def find_worker worker_code
  raise TypeError if !(worker_code.is_a?(Integer) || worker_code.nil?)
  return nil if !worker_code || worker_code == 0

  r = User.find_by_worker_code sprintf("%06d", worker_code)
  raise "no worker: #{worker_code}" if !r
  return r
end


def find_division_or_fallback div_id
  if div_id.to_s.strip != "" 
    div = find_division_by_code(div_id) || raise("not found division #{div_id}")
    if div.company_id != COMPANY.id
      div = COMPANY.company_setting.fallback_division
      puts "div_id = #{div_id} fallbacked."
    end
    return div
  else
    return nil
  end
end


def import_gl
  data = sjis2utf8 File.read(IMPORT_DATA_DIR + '/shiwake.csv')
  
  st, ed = get_data_term data
  puts "start_day = #{st}, end_day = #{ed}"

  GeneralLedger.transaction {
    kubun = Kubun.find :first, :conditions => ["name = ?", ACTUAL_NAME]
    raise if !kubun

    # 元のデータを削除する
    GeneralLedger.delete_all ['kubun_id = ? AND date BETWEEN ? AND ?', kubun.id, st, ed]

    # インポート
    idx = 0
    CSV::Reader.parse(data) {|line|
      idx += 1; next if idx == 1
      (print '.'; STDOUT.flush) if (idx % 100) == 0

      # 借方
      if line[8].to_i != 3999
        r = GeneralLedger.new(
          :date => line[0],  # 伝票日付
          :kubun_id => kubun.id,
          :slip_number => line[1].to_i != 0 ? line[1].to_i : nil, # 伝票番号
          :created_by => find_worker(line[6].to_i), # 作成者
          :created_at => line[5],   # 作成日時
          :account_title => find_account_title(line[8].to_i, line[10].to_i) || (raise "ac#{line[8]}/#{line[10]}"),
          :division => find_division_or_fallback(line[12]),
          :partner => get_partner(line[14]),
          :amount => line[18].to_i,
          :remarks => line[33])
        raise if r.account_title.aggregate_flag
        r.save!
      end

      # 貸方
      if line[20].to_i != 3999
        r = GeneralLedger.new(
          :date => line[0],  # 伝票日付
          :kubun_id => kubun.id,
          :slip_number => line[1].to_i != 0 ? line[1].to_i : nil,
          :created_by => find_worker(line[6].to_i),
          :created_at => line[5],
          :account_title => find_account_title(line[20].to_i, line[22].to_i) || raise("not found title #{line[20]},#{line[22]}"),
          :division => find_division_or_fallback(line[24]),
          :partner => get_partner(line[26]),
          :amount => -(line[30].to_i),
          :remarks => line[33])
        raise if r.account_title.aggregate_flag
        r.save!
      end
    }
  }
  puts ""
end

puts "Start at #{Time.now}"
COMPANY = Company.find(ARGV[1])
import_gl
puts "Finished at #{Time.now}"
