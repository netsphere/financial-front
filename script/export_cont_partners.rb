# -*- coding:utf-8 -*-
# 継続取引先を抽出する

require_relative 'import_common.rb' 

# 1. 比較的最近、取引がある。
# 2. 1年間に2回以上、取引がある。

def export_partners
  limit_day = Date.today - (30 * 12)
  
  # GLも見る。(法人対策)
  # GLは, 発生, 消込み/発生, 消込み/発生, と3回あれば複数回と見る.
  partners = Partner.find :all,
                  :conditions => [<<EOF, limit_day, limit_day, limit_day],
(
  (SELECT count(*) FROM invoices 
       WHERE partner_id = partners.id AND buy_day >= ?) >= 2) OR
(
  (SELECT count(*) FROM sales_invoices 
       WHERE partner_id = partners.id AND sell_day >= ?) >= 2) OR
(
  (SELECT count(DISTINCT date) FROM general_ledger
        WHERE partner_id = partners.id AND date >= ?) >= 3)
EOF
                  :order => 'zaimu_id'
  
  print ["会計コード", "TDBコード", "名前", "フリガナ", "〒", "所在地", "業種", "備考"].join("\t") + "\n"
  partners.each {|partner|
    print [partner.zaimu_id, partner.tdb_code, partner.name, partner.kana_name, 
           partner.hq_zip, partner.hq_addr, partner.industry, partner.remarks.strip.gsub(/\r\n?|\n/, "/")
          ].join("\t")
=begin
    inv = partner.invoices.first
    if inv
      print "\t#{inv.invoice_details.first.remarks}"
    else
      print "\t"
    end
    
    inv = partner.sales_invoices.first
    if inv
      print "\t#{inv.si_journals.first.remarks}"
    else
      print "\t"
    end
=end
    print "\n"
  }
end

export_partners()
