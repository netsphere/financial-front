# -*- coding:utf-8 -*-

# インポートで共通的に使用する関数
# データベースへの接続

ENV["RAILS_ENV"] = "production" if !ENV["RAILS_ENV"]

require_relative "../config/environment" unless defined?(RAILS_ROOT)

require 'nkf'
#DATA_DIR = '/home/upload'


def sjis2utf8 s
  s ? NKF.nkf('-w -S -m0', s) : nil
end


# 全角に変換しない
def sjis2utf8_x s
  s ? NKF.nkf('-w -S -m0 -x', s) : nil
end


# Unicodeの正規化 NFKC
def norm s, form = :kc
  s ? s.mb_chars.normalize(form).to_s : nil
end
alias :normalize_kc :norm


def find_account_title(major, minor)
  raise TypeError if !major.is_a?(Integer) || !minor.is_a?(Integer)
  raise ArgumentError, "major = #{major}" if major == 0
  AccountTitle.find(:first, :conditions => ["account_code = ? and suppl_code = ?", major, minor])
end

def find_division_by_code(division_code)
  if division_code && division_code != ""
    Division.find(:first, :conditions => ["division_code = ?", division_code])
  else
    nil
  end
end

