#!/usr/bin/ruby
# -*- coding:utf-8 -*-

# 部門別・総勘定元帳の出力


require File.dirname(__FILE__) + '/import_common.rb'
require "csv"
require "erubis/helper" # h()


include Erubis::XmlHelper


# 全社計のダウンロード
def dl_output fname, root, term, gl
  fp = File.open File.dirname(__FILE__) + "/../public/download/division_summary-#{fname}.html", "w"

  fp.print <<EOF
<html xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:x="urn:schemas-microsoft-com:office:excel"
      xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja"<
<head>
  <meta http-equiv='Content-Type' content='text/html;charset=utf-8'>
  <meta name='ProgId' content='Excel.Sheet'>
  <xml>
    <o:DocumentProperties>
      <o:Created>#{Time.now.iso8601}</o:Created>
      <o:Company>●</o:Company>
    </o:DocumentProperties>
  </xml>
  <style>
    @page {
        margin:.59in .39in .39in .39in;
        mso-header-margin:.51in;
        mso-footer-margin:.31in;
        mso-page-orientation:landscape;}
    td {
      font-size:9.0pt; }
  </style>
</head>
<body>
    <h1>#{term.sd}-#{term.ed} 実績</h1>
<table border="0" cellpadding="0" cellspacing="0" width="100%"
    style='border-collapse:collapse;table-layout:fixed;'>
<tr>
  <th>伝票日付</th>
  <th>年月</th>
  <th>区分</th>
  <th>伝票NO</th>
  <th>作成日</th>
  <th>作成者コード</th>
  <th>作成者名</th>
  <th>科目コード</th>
  <th>補助コード</th>
  <th>科目名</th>
  <th>科目名2</th>
  <th>会計部門コード</th>
  <th>会計部門名</th>
  <th>次元2コード</th>
  <th>次元2名</th>
  <th>金額</th>
  <th>摘要</th>
</tr>
EOF

  idx = 0
  gl.each {|r|
    next if block_given? && yield(r) 

    idx += 1
    (print "."; STDOUT.flush) if (idx % 100) == 0

    fp.print <<EOF
<tr>
  <td>#{r.date}</td>
  <td>#{sprintf("%04d%02d", r.date.year, r.date.mon)}</td>
  <td>#{Kubun.find(r.kubun_id).long_name}</td>
  <td>#{r.has_attribute?('slip_number') ? r.slip_number : ''}</td>
  <td>#{r.has_attribute?('created_at') && r.created_at ? r.created_at.strftime("%Y/%m/%d") : ''}</td>
  <td>#{r.has_attribute?('created_by') && r.created_by ? r.created_by.worker_code : nil}</td>
  <td>#{r.has_attribute?('created_by') && r.created_by ? r.created_by.name : nil}</td>
  <td>#{r.account_title.account_code}</td>
  <td>#{r.account_title.suppl_code}</td>
  <td>#{r.account_title.name}</td>
  <td>#{r.account_title.account_code}#{r.account_title.main_name}
  <td>#{r.division_id ? r.division.division_code : nil}</td>
  <td>#{r.division_id ? r.division.name : ''}</td>
  <td>#{r.dim2_id ? Dimension.find(r.dim2_id).dim_code : nil}</td>
  <td>#{r.dim2_id ? Dimension.find(r.dim2_id).name : nil}</td>
  <td>#{r.amount * r.account_title.sign}</td>
  <td>#{r.has_attribute?('remarks') ? r.remarks : ''}</td>
</tr>
EOF
  }
  fp.print <<EOF
  </table>
</body></html>
EOF

  fp.close
end


def division_summary pa
  root = pa[:root]
  term = pa[:term]
  kubuns = pa[:kubuns]
  fname = pa[:fname]

  cond = [<<EOF, term.sd, term.ed]
(kubun_id IN (#{kubuns.join(',')})) AND (date BETWEEN ? AND ?)
EOF

  gl = GeneralLedger.find :all,
                    :select => "general_ledger.kubun_id, general_ledger.division_id, general_ledger.dim2_id, general_ledger.account_title_id, general_ledger.date, sum(general_ledger.amount) as amount",
                    :conditions => cond,
                    :group => 'kubun_id, division_id, dim2_id, account_title_id, date'
    
  dl_output(fname, root, term, gl) {|r|
    if r.account_title.account_code > 999 # TODO:fixme!
      true
    else
      false
    end
  }
end


# Marshal.load 対策
ControlDivision
GeneralLedgerController


puts "Started at #{Time.now}"
STDOUT.flush
division_summary Marshal.load([ARGV[0]].pack("H*"))
puts "Finished at #{Time.now}"
