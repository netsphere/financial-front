# -*- coding: utf-8 -*-

# 会計部門のインポート
# 財務大将から


require File.dirname(__FILE__) + '/import_common.rb'
require 'csv'


def import_control_divisions
  data = sjis2utf8(File.read('control_divisions.csv'))
  ControlDivision.transaction {
    idx = 0
    CSV::Reader.parse(data) {|line|
      idx += 1; next if idx == 1
      raise if line[0].to_i == 0
      r = ControlDivision.find(:first, :conditions=>["id = ?", line[0].to_i]) || 
          ControlDivision.new(:id => line[0].to_i)
      r.name = line[1]
      r.save!
    }
  }
end

# 会計部門
def import_divisions
  data = sjis2utf8(File.read(IMPORT_DATA_DIR + '/bumon.csv'))
  Division.transaction {
    idx = 0
    CSV::Reader.parse(data) {|line|
      idx += 1; next if idx == 1
      next if line[0].to_i != 0 && line[0].to_i != 1

      r = find_division_by_code(line[1].strip) ||
              Division.new(
                  :division_code => line[1].strip,
                  :purchase_active => 0)
      r.attributes = {
        :name => line[2],
        :kana_name => (line[3] || ""),
        :end_day => line[5],
      }
      r.save!
    }
  }
end


#import_control_divisions()
import_divisions()
