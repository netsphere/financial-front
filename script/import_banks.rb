# -*- coding: utf-8 -*-

require File.dirname(__FILE__) + '/import_common.rb'
require 'csv'

=begin
require '../app/models/bank'
require '../app/models/bank_branch'
require '../app/models/currency'
require '../app/models/bank_account'
=end

def import_banks filename
  data = sjis2utf8 File.read(filename)

  Bank.transaction {
    idx = 0
    CSV::Reader.parse(data) {|line|
      idx += 1
      (print '.'; STDOUT.flush) if (idx % 100) == 0
      bank_id = line[0].to_i
      b = Bank.find(:first, :conditions => ['id = ?', bank_id])
      if !b
        b = Bank.new(:name => line[3].strip,
                     :kana_name => line[2].strip)
        b.id = bank_id
        b.save!
      end
      branch_code = line[1].to_i
      branch = BankBranch.find(:first,
                  :conditions => ['bank_id = ? AND branch_code = ?', bank_id, branch_code])
      if !branch
        r = BankBranch.new(:bank => b,
                           :branch_code => branch_code,
                           :name => line[5].strip,
                           :kana_name => line[4].strip)
        r.save!
      end
    }
  } # of transaction
end


# http://y-kaku.ddo.jp/ginkokensaku/
# ここで配っているファイルがどの程度使えるか検証してみる。
#    => 証券会社なども登録されており、全部を取り込むとまずい。
# 有効なコードはとびとびになっていて、表だけでは分からない。
=begin
# http://zengin.ajtw.net/
・都市銀行（0001～0017）
・ネット専業銀行等（0033～0037）
   0040 イオン銀行  ok
・地方銀行（0116～0190）
・信託銀行（0288～0326）
   0288 三菱UFJ信託銀行  ok
   0322 新銀行東京
・旧長期信用銀行（397～398）
   0397 新生銀行
・第二地方銀行（0501～0597）
・信用金庫（1001～1996）
・信用組合（2011～2895）
・労働金庫（2951～2997）
・農業協同組合（3056～9375）
・信用農業協同組合連合会（3001～3046）
・信用漁業協同組合連合会（9450～9496）
=end
def update_banks
  data = sjis2utf8_x(File.read('ginkositen.txt'))
  
  bank_hash = {}

  Bank.transaction {
    idx = 0
    CSV::Reader.parse(data) {|line|
      idx += 1
      (print '.'; STDOUT.flush) if (idx % 100) == 0

      b = Bank.find_by_id(line[0])
      next if !b  # 銀行の追加はしない
      
      if line[1].to_i == 0 && line[4].to_i == 1
        # 銀行
        b_code = line[0].to_i
        bank_hash[b_code] = {-1 => line}
        if b.kana_name != line[2].strip
          if b_code <= 597
            # 新銀行東京
            b_name = line[3].index("銀行") ? line[3].strip : line[3].strip + "銀行"
          else
            b_name = line[3].strip
          end
          if b_name.index('・')
            puts b_name
            b_name.gsub!(/・/, '') # 日本トラスティサービス信託銀行
          end
          b.name = norm(b_name)  # UFJとかは半角に
          b.kana_name = Bank.zen2han(line[2].strip)
          b.save!
        end
      elsif line[4].to_i == 2
        # 支店
        if !bank_hash[line[0].to_i]
          puts "data error: #{line}"
          next
        end
        bank_hash[line[0].to_i][line[1].to_i] = line
        br = BankBranch.find :first,
                 :conditions => ['bank_id = ? AND branch_code = ?', line[0], line[1]]
        if !br
          br = BankBranch.new :bank_id => line[0].to_i,
                              :branch_code => line[1].to_i,
                              :name => line[3].strip,
                              :kana_name => line[2].strip
          br.save!
        else
          if br.kana_name != line[2].strip
            br.name = norm(line[3].strip)
            br.kana_name = Bank.zen2han(line[2].strip)
            br.save!
          end
        end
      else
        puts "type error: #{line}"
      end
    }

    # 統廃合などの分を削除していく
    Bank.find(:all).each {|b|
      if !bank_hash[b.id]
        baf = BankAccount.find :first, 
                 :select => 'bank_accounts.*',
                 :joins => 'LEFT JOIN bank_branches ON bank_accounts.bank_branch_id = bank_branches.id',
                 :conditions => ['bank_branches.bank_id = ?', b.id]
        if !baf
          BankBranch.delete_all ['bank_id = ?', b.id]
          b.destroy
        else
          puts "cannot delete: #{b.id} #{b.name} referred by bank_account id=#{baf.id} zaimu_ac=#{baf.zaimu_ac}"
        end
      end
    }
    BankBranch.find(:all).each {|br|
      if bank_hash[br.bank_id] && !bank_hash[br.bank_id][br.branch_code]
        if BankAccount.find_by_bank_branch_id(br.id)
          puts "cannot delete: #{br.bank_id}/#{br.branch_code} #{br.name}"
        else
          br.destroy
        end
      end
    }
  }
end

#import_banks()
update_banks()
