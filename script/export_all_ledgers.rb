#!/usr/bin/ruby
# -*- coding:utf-8 -*-

# 総勘定元帳の出力

require File.dirname(__FILE__) + '/import_common.rb'
require "csv"
require "erubis/helper" # h()

include Erubis::XmlHelper


def export_all_ledgers kubun_id, term_st, term_ed
  kubun = Kubun.find kubun_id
  st = Date.parse term_st
  ed = Date.parse term_ed

  cond = ["kubun_id = ? AND (date BETWEEN ? AND ?)",
          kubun.id, st, ed]

  @gl = GeneralLedger.find :all, :conditions => cond,
                                 :order => "date, id"

  fp = File.open File.dirname(__FILE__) + "/../public/download/exported_ledgers.html", "w"

  fp.print <<EOF
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head></head>
<body>
<table>
  <tr>
    <th>伝票日付</th>
    <th>年月</th>
    <th>区分</th>
    <th>伝票NO</th>
    <th>作成日</th>
    <th>作成者コード</th>
    <th>作成者名</th>
    <th>科目コード</th>
    <th>補助コード</th>
    <th>科目名</th>
    <th>科目名2</th>
    <th>部門コード</th>
    <th>部門名</th>
    <th>取引先コード</th>
    <th>取引先名</th>
    <th>金額</th>
    <th>摘要</th>
  </tr>
EOF

  kubun_cache = {}
  account_title_cache = {}
  user_cache = {}
  division_cache = {}

  idx = 0
  @gl.each {|r| 
    idx += 1
    (print "."; STDOUT.flush) if (idx % 1000) == 0

    created_by = r.created_by_before_type_cast ? 
                  (user_cache[r.created_by_before_type_cast] ||= r.created_by) :
                  nil
    fp.print <<EOF
<tr>
  <td>#{r.date}</td>
  <td>#{sprintf("%04d%02d", r.date.year, r.date.mon)}</td>
  <td>#{h( (kubun_cache[r.kubun_id] ||= r.kubun).long_name )}</td> 
  <td>#{r.slip_number}</td>
  <td>#{r.created_at.strftime("%Y/%m/%d")}</td>
  <td>#{created_by ? created_by.worker_code : nil}</td>
  <td>#{created_by ? h(created_by.name) : nil}</td>
  <td>#{(account_title_cache[r.account_title_id] ||= r.account_title).account_code}</td>
  <td>#{account_title_cache[r.account_title_id].suppl_code}</td>
  <td>#{account_title_cache[r.account_title_id].name}</td>
  <td>#{ "#{account_title_cache[r.account_title_id].account_code}#{account_title_cache[r.account_title_id].main_name}" }
  <td>#{r.division_id ? (division_cache[r.division_id] ||= r.division).division_code : nil}</td>
  <td>#{r.division_id ? h(division_cache[r.division_id].name) : nil}</td>
  <td>#{r.partner ? r.partner.zaimu_id : nil}</td>
  <td>#{r.partner ? h(r.partner.name) : nil}</td>
  <td align="right">#{r.amount}</td>
  <td>#{h r.remarks }</td>
</tr>
EOF
  }

  fp.print <<EOF
</table>
</body>
</html>
EOF

  fp.close
end


puts "Started at #{Time.now}"
STDOUT.flush
export_all_ledgers ARGV[0], ARGV[1], ARGV[2]
puts "Finished at #{Time.now}"

