#!/usr/bin/ruby

# 取引先のインポートを起動

SCRIPT_DIR = File.dirname __FILE__

Process.fork {
  outfname = SCRIPT_DIR + '/import_partners_out.log'
  errfname = SCRIPT_DIR + '/import_partners_err.log'
  
  File.unlink outfname if FileTest.exist?(outfname)
  STDOUT.reopen outfname
  File.unlink errfname if FileTest.exist?(errfname)
  STDERR.reopen errfname
  exec SCRIPT_DIR + '/import_partners.rb'
}
sleep 1
