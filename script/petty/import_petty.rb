# -*- coding: utf-8 -*-

require_relative "../import_common"
require 'csv'
require "bigdecimal"


# 税コード変換
# concurの税コードを社内経理システムの税コードに変換
def convert_tax_code(c_tax_code, account_title, date)
  # 消費税10% start
  jpvat_10_day = Date.new 2019, 10, 1

  case c_tax_code
  when '10'
    return 18
  else
    # 8%以外はConcurのコードと同じ
    return c_tax_code.to_i unless c_tax_code.blank?

    # Concur側で税コードが設定されていない場合
    # 勘定科目が不課税ならば40をセット
    if account_title.tax_side == 0
      return 40
    else
      tax_code = 1010
       # 処理日が10%開始前ならば8%
      if date < jpvat_10_day
        tax_code = 18
      end
      return tax_code
    end
  end
end

# 経費精算リポートの１エントリ
# @return Array [record_count, journal_amount, cc_amt, emp_amt]
#     cc_amt は使用した金額
#     emp_amt は, 現金による場合で、承認された金額
def add_expense_entry report, entry_ary
  cc_amt = BigDecimal.new("0.0")
  emp_amt = BigDecimal.new("0.0")

  line = entry_ary.first
  r = PettyExpenseEntry.new :petty_expense_id => report.id,
           :report_entry_key => line[60],
           :transaction_type => line[61],
           :expense_type_name => line[62],
           :transaction_date => line[63],
           :spend_currency_code => line[64],
           :exchange_rate => line[65],
           :exchange_rate_direction => line[66],
           :is_personal => (line[67] == "Y"),  # TODO: これは不味い。明細化して私費がある
           :description => (line[68] || ""),
           :vendor_name => (line[69] || ""),
           :vendor_description => (line[70] || ""), # 取引先名.
           :receipt_received => (line[71] == "Y"),
           :receipt_type => line[72],
           :employee_attendee_count => line[73].to_i,
           :spouse_attendee_count => line[74].to_i,
           :business_attendee_count => line[75].to_i,
           :spend_amount => line[122],    # 使用した金額(現地通貨) => 法人カードへの支払い
           :approved_amount => line[124], # 承認金額 => 経費の発生
           :payment_code => line[125],
           #:payment_code_name => line[126],
           :reimb_type => line[127].to_i,
           :foreign_or_domestic => line[159]

  if line[128].to_s.strip != ""
    # CCフィード
    r.cc_billing_date = line[128]
    r.cc_masked_cc_number = line[129]
    r.cc_name_on_card = line[130]
    r.cc_jr_key = line[131]
    r.cc_ref_no = line[132]
    r.cc_cct_key = line[133]
    r.cc_cct_type = line[134]
    r.cc_transaction_id = line[135]
    r.cc_transaction_amount = BigDecimal.new(line[136])
    r.cc_tax_amount = BigDecimal.new( line[137] || "0.0" )
    r.cc_currency_code = line[138]
    r.cc_transaction_description = line[143]
    r.cc_mc_code = line[144]
    r.cc_merchant_name = line[145]
    r.cc_merchant_city = line[146]
    r.cc_merchant_state = line[147]
    r.cc_merchant_ctry_code = line[148]
    r.cc_exchange_rate = line[151]
  end

  # 仮払いの返金は, 経費としては追加せず, petty_offsets のみ登録する
  if !(line[184].to_i == 2 && line[186].to_i != 0)
    r.save!
  end

  tax_code = "" # default value（10%）
  journal_amount = BigDecimal.new "0.0000"
  record_count = 0

  # 仮払いからの充当, 明細化や、私費利用などで、複数行になることがある。
  entry_ary.each do |line|
    if line[184].to_i == 2 && line[186].to_i != 0
      # 仮払いからの充当. この金額は col 125 (approved amount) には含まれない
      # この場合, col 163=Employee to col 165=Company になっている。
      ca = PettyCashAdvance.find_by_ca_key(line[186])
      of = PettyOffset.new :expense_id => report.id,
                :cash_advance_id => ca.id,
                :amount => - BigDecimal.new(line[168]),
                :currency_id => ca.reimb_currency_id
      of.save!
      record_count += 1
      journal_amount += BigDecimal.new line[168]
      next
    end

    # 以降, 通常経費. allocation で複数行に分かれていることがある

    # col 163 Payment Type (支払人タイプ)
    # JPにおける経費は、C.TAX, 経費タイプの2行が来る。tax_code を次行の経費に
    # 反映させる
    # JP以外では, C.TAXの行が生成されない
    if line[162] == "C.TAX"
      # 税. 金額は approved amount に含まれないが, この行にしかtax codeが出ない
      # 経費の額がプラスでもマイナスでも、col 163を見ればよい
      tax_code = line[231]
      record_count += 1
      journal_amount += BigDecimal.new line[168] # こちらは含める
      next 
    end
    # 経費行. JPのをぜんぶ課税にするとやりすぎ。
    if line[338].to_s.strip != '' && line[338].strip != 'JP'
      tax_code = 40 # 不課税で固定
    end
    
    # col 163 = 借方, col 165 = 貸方
    if !PettyPayee.find_by_payment_type(line[162]) ||
       !PettyPayee.find_by_payment_type(line[164])
      raise "unexpected payment type: col 163 = #{line[162]}, col 165 = #{line[164]}" 
    end

    # マイナスの経費のときは, (col 163 = Emp, col 165 = Company, col 168 = CR, 
    # col 169 = マイナスの金額) という組み合わせになる。
    #     => プラスでもマイナスでも col 169 そのままでよい.
    #        勘定科目の貸借は、DBに保存する際に入れ替える
    if line[164] == "Employee" || line[162] == "Employee"
      # 現金の経費行
      # col 125は, entryの承認額。明細化されている場合、加算するわけにいかない
      emp_amt += BigDecimal.new(line[168]) # Entry Approved Amount
    end
    if (line[164] != "Company" && line[164] != "Employee") || 
       (line[162] != "Company" && line[162] != "Employee")
      # 法人クレジットカード
      cc_amt = BigDecimal.new(line[123]) # Entry Posted Amount
    end

    if line[83].to_i != 0
      # col 84 => 0:旅費そのほか, 1:訓練, 2:福利費
      case line[83].to_i
      when 1
        ac = AccountTitle.find :first, 
                 :conditions => ["account_code = ? AND suppl_code = ?",
                                 842, 2]  # fixme!!!!
      when 2
        ac = AccountTitle.find :first, 
                 :conditions => ["account_code = ? AND suppl_code = ?",
                                 805, 0]  # TODO: fixme!!!!
      else
        raise "unknown travel type: #{line[83]}"
      end
    elsif line[84] == "Y"
      # col 85 => Y:5000円超
      ac = AccountTitle.find :first, 
                 :conditions => ["account_code = ? AND suppl_code = ?",
                                 854, 0]  # TODO: fixme!!!!
    else
      # 科目コードをそのまま利用
      ac_code, suppl_code = line[166].split "/"
      ac = AccountTitle.find :first, 
                   :conditions => ["account_code = ? AND suppl_code = ?", 
                                   ac_code.to_i, suppl_code.to_i]
    end
    raise "unknown ac: #{line[166]}" if !ac

    # 税コード変換
    tax_code = convert_tax_code(tax_code, ac, r.transaction_date)

    # col 191 (base 1)はblankのことがありうる.
    # => col 48 (base 1): Employee Custom field #8 の値をコピーする。
    div = Division.find_by_division_code( 
                                   line[190].blank? ? line[47] : line[190] )
    raise "division not found: 47:'#{line[47]}', 190:'#{line[190]}'\n#{line.join("/")}" if !div

    # 'CR'のときは貸借入れ替え => 簿記的な貸借ではなく、金額の±で処理する
    case line[167]
    when 'DR'
      payer = line[162]; payee = line[164]
    when 'CR'
      payer = line[164]; payee = line[162]
    else
      raise "unknown debit_or_credit: #{line[167]}"
    end
    al = PettyExpenseAllocation.new :petty_expense_entry_id => r.id,
                :payer_payment_type => payer,
                :payee_payment_type => payee,
                :account_title_id => ac.id,
                #:debit_or_credit => line[167],
                :amount => line[168],
                :division_id => div.id,
                :vat_code => tax_code,
                :allocation_percentage => (line[189] || 0)

    al.save!
    journal_amount += al.amount
    record_count += 1
  end # entry_ary.each

  return [record_count, journal_amount, cc_amt, emp_amt]
end


# 経費精算リポートを一つ.
def add_expense_report record_ary
  line = record_ary.first

  emp = User.find_by_worker_code line[4]
  raise "unknown employee ID: #{line[4]}" if !emp
  group = PettyGroup.find_by_group_code line[8]
  raise "unknown employee group: #{line[8]}" if !group

  report = PettyExpense.new :batch_id => line[1],
           :batch_date => line[2],
           :sequence_number => line[3],
           :user_id => emp.id,
           :petty_group_id => group.id,
           :report_id => line[18].strip,
           :reimb_currency_id => Currency.find_by_code(line[21]).id,
           :country_lang_name => line[22],
           :submit_date => line[23],
           :processing_date => line[25],
           :report_name => line[26],
           :image_required => (line[27] == "Y"),
           :has_vat_entry => (line[28] == "Y"),
           :has_ta_entry => (line[29] == "Y"),
           :total_approved_amount => line[31],
           :emp_amount_total => 0, # 仮. 後で更新する
           :cc_amount_total => 0,  # 後で更新する
           :receivable_amount_total => 0
  report.save!

  # 仮払いのoffsetは, reportに紐づくのに, CSV上, エントリに入っている
  # 経費エントリがCSV上で複数行に複製されてしまっている
  # col 61が重複のときは1行だけイキにする
  rpe_key = nil
  entry_ary = []

  record_count = 0
  journal_amount = BigDecimal.new "0.0000"
  cc_amt = BigDecimal.new("0.0")
  emp_amt = BigDecimal.new("0.0")
  
  record_ary.each do |line|
    if line[60].to_i != rpe_key
      if !entry_ary.empty?
        x = add_expense_entry report, entry_ary 
        record_count += x[0]
        journal_amount += x[1]
        cc_amt += x[2]
        emp_amt += x[3]
      end

      entry_ary = [line]
      rpe_key = line[60].to_i
    else
      entry_ary << line
    end
  end
  if !entry_ary.empty?
    x = add_expense_entry report, entry_ary 
    record_count += x[0]
    journal_amount += x[1]
    cc_amt += x[2]
    emp_amt += x[3]
  end

  puts "emp_amt = #{emp_amt}" # DEBUG
  raise "emp_amt not matched: total_approved_amount = #{report.total_approved_amount}, emp_amt = #{emp_amt}" if report.total_approved_amount != emp_amt + cc_amt

  ca_amount = PettyOffset.where("expense_id = ?", report.id) \
                         .select("SUM(amount) AS amount").order("").first
  ca_amount = ca_amount ? ca_amount.amount.to_i : 0

  # マイナスの経費金額がありうる
  if (report.total_approved_amount - ca_amount - cc_amt) >= 0
    report.emp_amount_total = report.total_approved_amount - ca_amount - cc_amt
  else
    report.receivable_amount_total = ca_amount + cc_amt - 
                                     report.total_approved_amount
  end
  report.cc_amount_total = cc_amt
  report.save!

  return [record_count, journal_amount]
end


# 仮払いの発生
def add_cash_advance record_ary
  raise "multiple line!!" if record_ary.size != 1

  line = record_ary.first
  # 仮払いエラーの暫定対応
  # 常に"172/0"(社員仮払金)で検索
  # ac_code, suppl_code = line[166].split("/")
  ac_code, suppl_code = 172, 0
  ac = AccountTitle.find :first, 
              :conditions => ["account_code = ? AND suppl_code = ?", 
                              ac_code, suppl_code.to_i]
  raise "ac not found: #{ac_code}/#{suppl_code}: #{line}" if !ac

  # TODO: subject, comment

  emp = User.find_by_worker_code line[4]
  raise "unknown employee ID: #{line[4]}" if !emp
  group = PettyGroup.find_by_group_code line[8]
  raise "unknown employee group: #{line[8]}" if !group

  r = PettyCashAdvance.new :batch_id => line[1],
           :batch_date => line[2],
           :sequence_number => line[3],
           :user_id => emp.id,
           :petty_group_id => group.id,
           :payer_payment_type => line[162],
           :payee_payment_type => line[164],
           :account_title_id => ac.id, # not null制約
           :debit_or_credit => line[167],
           :journal_amount => line[168],
           :request_amount => line[176],
           :request_currency_id => Currency.find_by_code(line[177]).id, # not null制約
           :exchange_rate => line[179],
           :reimb_currency_id => Currency.find_by_code(line[180]).id, # not null制約
           :processing_date => line[182],
           :payment_code => line[183],
           :transaction_type => line[184],
           :submit_date => line[185],
           :ca_key => line[186],
           :payment_method => line[187].to_i,
           :name => ""
           #:cash_account_code => line[253],
           #:liability_account_code => line[254]
  r.save!

  return [1, r.journal_amount]
end


def add_record record_ary
  if !record_ary.empty?
    if record_ary.first[18].to_s.strip != ""
      return add_expense_report record_ary 
    else
      return add_cash_advance record_ary
    end
  end
end


# import_petty_driver.rb から呼び出される.
def read_petty_extract_file filename
  # BOMなしUTF-8
  data = File.read filename

  batch_date = nil
  batch_detail_record_count = 0
  batch_journal_amount_total = BigDecimal.new "0.0000"

  report_id = nil
  ca_key = nil
  record_ary = []

  journal_amount = BigDecimal.new "0.0000" # detailの合計

  lineno = 0
  CSV.parse(data, col_sep:'|') do |line|
    lineno += 1

    if lineno == 1
      # header
      raise "unknown file type: #{line[0]}" if line[0] != "EXTRACT"
      batch_date = Date.parse line[1]
      batch_detail_record_count = line[2].to_i
      batch_journal_amount_total = BigDecimal.new( line[3] || "0.0" )
      next
    end

    # detail 
    raise "start column error: #{line[0]}" if line[0] != "DETAIL"
    raise "batch date mismatch: #{line[2]}" if Date.parse(line[2]) != batch_date

    if line[18].to_s.strip != ""
      # 経費精算
      if line[18] != report_id
        if !record_ary.empty?
          x = add_record record_ary 
          journal_amount += x[1]
        end

        record_ary = [line]
        report_id = line[18]
        ca_key = nil
      else
        record_ary << line
      end
    else
      # 仮払い
      raise "cash advance type error: #{line[184]}" if line[184].to_i != 1
      if line[186].to_i != ca_key
        if !record_ary.empty?
          x = add_record record_ary 
          journal_amount += x[1]
        end

        record_ary = [line]
        report_id = nil
        ca_key = line[186].to_i
      else
        record_ary << line
      end      
    end
  end # parse()

  if !record_ary.empty?
    x = add_record record_ary 
    journal_amount += x[1]
  end

  if lineno - 1 != batch_detail_record_count
    raise "record count mismatch: #{filename} lineno = #{lineno}" 
  end
  if journal_amount != batch_journal_amount_total
    raise "journal amount mismatch: #{journal_amount} should be #{batch_journal_amount_total}" 
  end
end


# main
if __FILE__ == $0
  if !ARGV[0] || !File.file?(ARGV[0])
    puts "Usage:"
    puts "    #{$0} filename"
    exit 1
  end

  PettyExpense.transaction do 
    read_petty_extract_file ARGV[0]
  end
end
