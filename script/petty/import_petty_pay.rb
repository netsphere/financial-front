# -*- coding:utf-8 -*-

require_relative "../import_common"
require "csv"
require "bigdecimal"


# 一塊の送金
def payment_demand_batch record_ary
  # header 
  header = record_ary.shift
  demand_batch_id = header[1]
  # 現金払いに変更した場合, このフィールドは空
  demand_batch_close_date = (header[2].to_s.strip != "" ? 
                                                   Date.parse(header[2]) : nil)
  journal_amount_total = BigDecimal.new header[3]  # この金額を戻す

  # details
  total = BigDecimal.new("0.0000")
  record_ary.each do |line|
    emp = User.find_by_worker_code line[1]
    raise "unknown employee ID: #{line[4]}" if !emp
    group = PettyGroup.find_by_group_code line[33]
    raise "unknown employee group: #{line[8]}" if !group

    if line[35]
      cash_advance = PettyCashAdvance.find_by_ca_key line[35]
      raise "unknown ca_key: #{line[35]}" if !cash_advance
      cash_advance.name = line[88]
      cash_advance.save!
    elsif line[51]
      report = PettyExpense.find_by_report_id line[51]
      raise "unknown report_id: #{line[51]}" if !report
    else
      raise "unknown pay type!!"
    end

    r = PettyPay.new :demand_batch_id => demand_batch_id,
                :demand_batch_close_date => demand_batch_close_date,
                :user_id => emp.id,
                :petty_group_id => group.id,
                :transaction_type => line[34],
                :cash_advance_id => (cash_advance ? cash_advance.id : nil),
                :currency_id => Currency.find_by_code(line[42]).id,
                :demand_id => line[43],
                :liability_side => line[47],
                :amount => line[49],
                :expense_id => (report ? report.id : nil)
    r.save!
    total += BigDecimal.new(line[49])
  end

  if total != journal_amount_total
    raise "amount unmatch: should #{journal_amount_total}, but #{total}"
  end

  return [record_ary.length, journal_amount_total]
end


# import_petty_driver.rb から呼び出される
def read_petty_pay_extract_file filename
  # BOMなしUTF-8
  data = File.read filename

  batch_date = nil
  batch_detail_record_count = 0
  batch_journal_amount_total = BigDecimal.new "0.0000"

  lineno = 0
  record_ary = []

  journal_amount = BigDecimal.new "0.0000"  # detailの合計
  detail_count = 0

  CSV.parse(data, col_sep: '|') do |line|
    lineno += 1

    if lineno == 1
      # file header
      raise "unknown file type: #{line[0]}" if line[0].to_i == 0
      batch_date = Date.parse line[1]
      batch_detail_record_count = line[2].to_i
      batch_journal_amount_total = BigDecimal.new( line[3] || "0.0" )
      next
    end

    # header
    # 確実な方法ではないが, 一応, ヘッダのcol 2は, UUID (RFC 4122) になっているみたい。
    if line[1] =~ /[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}/
      if !record_ary.empty?
        x = payment_demand_batch record_ary
        detail_count += x[0]; journal_amount += x[1]
      end

      record_ary = [line]
    else
      record_ary << line
    end
  end # parse()

  if !record_ary.empty?
    x = payment_demand_batch record_ary
    detail_count += x[0]; journal_amount += x[1]
  end

  if detail_count != batch_detail_record_count
    raise "record count mismatch: detail_count = #{detail_count}" 
  end
  if journal_amount != batch_journal_amount_total
    raise "journal amount mismatch: #{journal_amount} should be #{batch_journal_amount_total}" 
  end
end


# main
if __FILE__ == $0
  if !ARGV[0] || !File.file?(ARGV[0])
    puts "Usage:"
    puts "    #{$0} filename"
    exit 1
  end

  PettyPay.transaction do
    read_petty_pay_extract_file ARGV[0]
  end
end
