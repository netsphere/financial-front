#!/usr/local/rbenv/versions/2.3.3/bin/ruby
# -*- coding: utf-8 -*-


require_relative "import_petty")
require_relative "import_petty_pay")
require_relative "concur-conf")
require "fileutils"


SAE_files = nil
pay_files = nil
PettyExpense.transaction do 
  SAE_files = Dir.glob( FETCHED_DIR + "/extract_CES_SAE_*.txt" )
  SAE_files.sort.each do |file|
    read_petty_extract_file file
  end

  pay_files = Dir.glob( FETCHED_DIR + "/extract_cp_*.txt" )
  pay_files.sort.each do |file|
    read_petty_pay_extract_file file
  end
end # transaction

# 全部インポートできてからファイルを移す
SAE_files.each do |file|
  FileUtils.move FETCHED_DIR + "/" + File.basename(file), 
            IMPORTED_DIR + "/" + File.basename(file)
end
pay_files.each do |file|
  FileUtils.move FETCHED_DIR + "/" + File.basename(file), 
            IMPORTED_DIR + "/" + File.basename(file)
end

