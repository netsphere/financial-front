#!/usr/local/bin/ruby
# -*- mode:ruby; coding:utf-8 -*-

# 前日までのextractファイルをダウンロードし, 復号する.

require "net/sftp"
require "openssl"
require "date"
require_relative "concur-conf"

Dir.chdir File.dirname(__FILE__)


# @param files [Array] a list of "file attribute, size, date, and name".
def fetch_files sftp, files
  raise TypeError if !files.is_a?(Array)

  files.each_with_index do |line, idx|
    #next if idx == 0
    d = line.split /[ \t]+/
    if d[-1] && d[-1].index('pgp')
      fname = File.basename(d[-1])
      p fname
      sftp.download!("/out/#{fname}","#{FETCHED_DIR}/#{fname}")
      # cronで回す場合, ttyは使えない
      `gpg --decrypt --no-tty --force-mdc --batch --passphrase #{PGP_PASS} #{FETCHED_DIR}/#{fname} > #{FETCHED_DIR}/#{fname.gsub('.txt.pgp', '.txt')}`
    end
  end
end


date = nil
File.open "#{CONCUR_SAE_ROOT}/FETCHED_DATE", IO::CREAT | IO::RDONLY do |fp| 
  d = fp.read 
  date = d.to_s == "" ? nil : Date.parse(d)
end

option = {
  key_data: [],
  keys: PRIVATE_KEY_ROOT,
  keys_only: true,
  port: 22,
}

files = []
Net::SFTP.start(HOST, USER_NAME, option) do |sftp|
  if date
    ( (date + 1) .. (Date.today - 1) ) .each do |d|
      # パターンに該当するファイルリストを生成する
      sftp.dir.glob("/out","*#{sprintf("%04d%02d%02d", d.year, d.mon, d.day)}*.pgp") do |fn|
        files.push(fn.name) 
      end
      fetch_files sftp, files
    end
  else
    # ファイルリストを生成する
    sftp.dir.glob("/out","*.pgp") do |fn|
      files.push(fn.name) 
    end
    fetch_files sftp, files
  end
end

File.open "#{CONCUR_SAE_ROOT}/FETCHED_DATE", "w" do |fp| 
  fp.puts Date.today - 1 
end
