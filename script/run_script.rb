#!/usr/local/rbenv/versions/2.3.3/bin/ruby
# -*- coding: utf-8 -*-

# 仕訳インポートを起動する

SCRIPT_DIR = File.dirname __FILE__

Process.fork {
  action = ARGV[0] 
  raise SecurityError if action =~ /\.\.|[^a-z0-9_\.]/

  outfname = SCRIPT_DIR + "/" + action + "_out.log"
  errfname = SCRIPT_DIR + "/" + action + "_err.log"
  
  File.unlink outfname if FileTest.exist?(outfname)
  STDOUT.reopen outfname
  File.unlink errfname if FileTest.exist?(errfname)
  STDERR.reopen errfname

  exec SCRIPT_DIR + "/" + action + ".rb", (ARGV[1]||""), 
                                             (ARGV[2] || ""), (ARGV[3] || "")
}
sleep 2
