#!/usr/bin/ruby
# -*- coding: utf-8 -*-

# 仕訳インポートを起動する

SCRIPT_DIR = File.dirname __FILE__

Process.fork {
  outfname = SCRIPT_DIR + '/import_gl_out.log'
  errfname = SCRIPT_DIR + '/import_gl_err.log'
  
  File.unlink outfname if FileTest.exist?(outfname)
  STDOUT.reopen outfname
  File.unlink errfname if FileTest.exist?(errfname)
  STDERR.reopen errfname

  exec SCRIPT_DIR + '/import_gl.rb', "import_gl.rb", ARGV[0]
}
sleep 1
