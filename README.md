
● TODO:
 - 会計部門マスタ作り直し  -> その後, カンパニー - 会計部門 - 社員紐付け
 - 銀行マスタ


# Netsphere Financial Front / ネットスフィア経理フロント

Licence: GPLv3+

Copyright (c) 2007-2009,2011-2013 Hisashi Horikawa, Netsphere Laboratories.
All rights reserved.

概要, 機能, 進捗状況はこちら; https://www.nslabs.jp/bside/



## How to use

```shell
$ createdb --encoding UTF-8 --owner rails keiri-front_development
```

```shell
$ psql -h localhost -U rails keiri-front_development
ユーザー rails のパスワード: 

keiri-front_development=> \i SETUP_SCHEMA.pgsql 
```

 - マイグレーション実行

 - 起動



## Get involved

See https://www.nslabs.jp/contributor-license-agreement.rhtml
