
-- 入金・出金伝票の資金部分
CREATE TABLE payment_banks (
  id              serial PRIMARY KEY,
  payment_slip_id int NOT NULL REFERENCES payment_slips (id),

  our_bank_id     int NOT NULL REFERENCES our_banks (id),

  -- 正規化された金額 [通貨=our_bank]
  -- 出金のときはマイナス
  cash_amount     decimal(12) NOT NULL,

  -- 消し込む債権・債務の金額 [通貨 = 請求書のそれ]
  -- 債権・債務のoffset後の純額
  -- 符号は常に正
  inv_amount      decimal(12) NOT NULL,
  
  created_at      TIMESTAMP NOT NULL
);

