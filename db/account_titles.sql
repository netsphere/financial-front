-- 勘定科目
CREATE TABLE account_titles (
  id              serial PRIMARY KEY,
  aggregate_flag  boolean NOT NULL,    -- 集計科目はTRUE

  account_code    int NOT NULL,
  suppl_code      int NOT NULL,
  main_name       VARCHAR(40) NOT NULL, -- 主コードの名前。ad hocなやり方。
  name            VARCHAR(40) NOT NULL,
  kana_name       VARCHAR(40) NOT NULL,
  description     TEXT NOT NULL,
  end_day         DATE,           -- これがNULL以外で使用不可。

  -- 売掛金、買掛金などのフラグ
  -- 1 = 債権科目, 2 = 債務科目, 3 = 仮払消費税, 0 = その他
  kind            int NOT NULL,

  -- 販売請求書で登録できるか
  enable_si_invoice boolean NOT NULL,

  -- enable_si_invoice = true の場合, 要設定
  -- この科目を計上する際の相手科目となる, 売掛金・未収入金a/c
  receivable_id     int REFERENCES account_titles(id), 

  -- (購買)請求書で登録できるか
  enable_invoice    boolean NOT NULL,

  -- enable_invoice = true の場合, 要設定
  -- 同じく買掛金・未払金a/c
  payable_id        int REFERENCES account_titles(id), 

  sign            int NOT NULL, -- 借方科目 = +1, 貸方科目 = -1
  req_partner     boolean NOT NULL, -- 相手先の指定が必要か

  -- 消費税対象外(不可) = 0, 課税売上 = 1, 課税仕入 = 2
  tax_side         int NOT NULL,

  -- 課税売上または課税仕入の場合の, 税グループの初期値
  -- 税グループはシステムコード. 値の意味は Vat::TAX_TYPES[] を見よ.
  default_tax_type int NOT NULL,

  created_at       TIMESTAMP NOT NULL,
  create_user_id   int NOT NULL REFERENCES users (id),
  updated_at       TIMESTAMP,
  update_user_id   int REFERENCES users (id),

  UNIQUE (account_code, suppl_code)
);
