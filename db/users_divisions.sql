-- -*- coding:utf-8 -*-
-- 所属部門 (の履歴)
CREATE TABLE users_divisions (
  id                 serial PRIMARY KEY,
  user_id            int NOT NULL REFERENCES users (id),
  division_id        int NOT NULL REFERENCES divisions (id),

  -- ヒラはとりあえずNULLで。
  worker_position_id int REFERENCES worker_positions (id),

  default_pair  int NOT NULL,  -- 1でデフォルト

  -- NULLのときは、所属ではなく、取り扱える会計部門のみ加える
  effective_day DATE,

  -- 現在所属しているときは NULL
  end_day       DATE,

  -- 複数の部署を兼務することがあり、また異動によって元の部署に戻ることがある
  UNIQUE (user_id, division_id, effective_day)
);
