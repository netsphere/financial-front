-- 資格マスタ
CREATE TABLE qualifications (
  id        serial PRIMARY KEY,
  name      VARCHAR(100) NOT NULL,
  kana_name VARCHAR(100) NOT NULL,
  active    boolean NOT NULL,

  created_at     TIMESTAMP NOT NULL,
  create_user_id int REFERENCES users (id) NOT NULL,
  updated_at     TIMESTAMP,
  update_user_id int REFERENCES users(id)
);
