-- コストセンタ (control_divisions) = divisions の間で、多対多をつなぐ
CREATE TABLE controllers_divisions (
  id                  serial PRIMARY KEY,
  control_division_id int NOT NULL REFERENCES control_divisions(id),
  division_id         int NOT NULL REFERENCES divisions(id),
  UNIQUE (control_division_id, division_id)
);
