
-- 自社の銀行口座
-- 円建ての口座を日本語・英語の両方で表示することあり。
CREATE TABLE our_banks (
  id                 serial PRIMARY KEY,
  company_setting_id int NOT NULL REFERENCES company_settings (id),

  name               VARCHAR(40) NOT NULL,

  -- いずれか、または両方. 現金の場合は両方ともナル値
  bank_account_id    int UNIQUE REFERENCES bank_accounts(id), 
  en_bank_account_id int UNIQUE REFERENCES en_bank_accounts(id),

  -- 通貨
  currency_id        int NOT NULL REFERENCES currencies (id),

  -- 依頼人コード N10  入金用口座は設定なし
  applicant_code     VARCHAR(10) UNIQUE,

  remarks            VARCHAR(200) NOT NULL,

  -- 現金・預金勘定科目
  account_title_id   int NOT NULL REFERENCES account_titles(id),

  created_at     TIMESTAMP NOT NULL,
  create_user_id int NOT NULL REFERENCES users (id),
  updated_at     TIMESTAMP,
  update_user_id int REFERENCES users(id)
);
