
-- 銀行口座 (英語表示) を表す
-- 自社口座/他社口座
CREATE TABLE en_bank_accounts (
  id             serial PRIMARY KEY,

  -- 取引先の口座の場合 (親). 自社口座のときはナル.
  partner_id     int REFERENCES partners (id),

  -- 受取人取引銀行. IBANがある場合も必須。
  swift_code     VARCHAR(11) NOT NULL, -- SWIFT (BIC) コード. ex. SMBC JP JT

  -- 銀行名/支店名 or 住所. 住所は都市名/州名/国名. 合計でC(140)
  -- ex. 1-3-2 MARUNOUCHI CHIYODA-KU TOKYO 100-0005
  bank_name      VARCHAR(40) NOT NULL, -- ex. SUMITOMO MITSUI BANKING CORPORATION
  bank_addr1     VARCHAR(50) NOT NULL,
  bank_addr2     VARCHAR(80) NOT NULL,

  -- 銀行の国コード. 受取人のではない. ex. JP
  bank_country   CHAR(2) NOT NULL,

  -- 受取人. 合計でC(140)
  account_name   VARCHAR(40) NOT NULL,
  account_addr1  VARCHAR(80) NOT NULL,
  account_addr2  VARCHAR(80) NOT NULL,

  -- IBAN / 口座番号. いずれか一方のみ。
  -- ex. ORDINARY 1234567
  account_no     VARCHAR(34),
  iban_code      VARCHAR(34)

  -- 通貨
  -- currency_id    int NOT NULL REFERENCES currencies (id)
);
