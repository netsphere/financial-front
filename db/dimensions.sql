
-- 次元.
-- 次元1 は会計部門 (divisions) で決め打ち.
-- 会計部門と異なり, カンパニーをまたぐ.
CREATE TABLE dimensions (
  id       serial PRIMARY KEY,

  active   boolean NOT NULL,

  -- 次元2 = 2
  axis     int NOT NULL,

  dim_code VARCHAR(10) NOT NULL UNIQUE,
  name     VARCHAR(40) NOT NULL,

  created_at     TIMESTAMP NOT NULL,
  create_user_id int REFERENCES users (id),
  updated_at     TIMESTAMP,
  update_user_id int REFERENCES users (id)
);
