
-- 部門ごとに (部門に依存して), 次元2以降の初期値を持たせる
CREATE TABLE default_dimensions (
  id          serial PRIMARY KEY,

  division_id  int NOT NULL REFERENCES divisions (id),

  -- 次元2 = 2
  axis         int NOT NULL,

  dimension_id int NOT NULL REFERENCES dimensions (id),

  created_at     TIMESTAMP NOT NULL,
  create_user_id int NOT NULL REFERENCES users (id),

  UNIQUE (division_id, axis)
);
