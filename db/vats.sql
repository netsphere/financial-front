
-- 消費税・付加価値税マスタ
-- カンパニーをまたぐ
CREATE TABLE vats (
  id               serial PRIMARY KEY,

  active           boolean NOT NULL,

  country          CHAR(2) NOT NULL,

  -- 課税売上 = 1, 課税仕入 = 2
  tax_side         int NOT NULL,

  -- 過去との互換性
  --   非課税仕入 / 非課税売上   30
  --   課税仕入10% / 課税売上    1010
  -- 税率が変わるときは, 新たに別コードで作成すること。
  tax_code         VARCHAR(6) NOT NULL,
  -- 新コード
  sbo_code         VARCHAR(6) NOT NULL,
  
  -- 課税(現行税率) = 10, 非課税 = 30, 免税売上 = 60など
  -- 税率違いも区別する => 税のデフォルト値の決定に使う
  tax_group        int NOT NULL,

  -- エラーチェックに使う
  effective_day    DATE NOT NULL,

  -- ex. "課税売上/5%"
  name             VARCHAR(40) NOT NULL,

  tax_rate         NUMERIC(4, 2) NOT NULL,

  account_title_id int NOT NULL REFERENCES account_titles (id),

  created_at       TIMESTAMP NOT NULL,
  create_user_id   int REFERENCES users (id),
  updated_at       TIMESTAMP,
  update_user_id   int REFERENCES users (id),

  UNIQUE (country, tax_side, tax_code)
);

