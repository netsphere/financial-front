
-- 小口グループ
-- ex. カンパニー単位, またはもっと小グループ.
CREATE TABLE petty_groups (
  id          serial PRIMARY KEY,
  company_id  int NOT NULL REFERENCES company_settings (id),

  -- 内部コード  
  group_code  VARCHAR(40) NOT NULL UNIQUE,
  name        VARCHAR(40) NOT NULL,
  description VARCHAR(200) NOT NULL,

  created_at       TIMESTAMP NOT NULL,
  create_user_id   int NOT NULL REFERENCES users (id),
  updated_at       TIMESTAMP,
  update_user_id   int REFERENCES users (id)
);
