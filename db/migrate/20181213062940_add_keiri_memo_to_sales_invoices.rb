class AddKeiriMemoToSalesInvoices < ActiveRecord::Migration
  def change
    add_column :sales_invoices, :keiri_memo, :text, :null => false
  end
end
