class AddColumnToSiDetail < ActiveRecord::Migration
  def change
    add_column :si_details, :advance, :boolean, default: false, null: false
  end
end
