# -*- coding:utf-8 -*-

# 各種マスタにフィールドを追加
class AddFieldsForMat < ActiveRecord::Migration
  def self.up
    execute <<EOF
ALTER TABLE company_settings ADD COLUMN active BOOLEAN NOT NULL DEFAULT true
EOF

=begin
    Company.all.each do |company|
      company.active = true
      company.save!
    end
=end

    execute <<EOF
ALTER TABLE company_settings ALTER active DROP DEFAULT
EOF
    
    execute <<EOF
--ALTER TABLE partners ADD COLUMN vendor_code VARCHAR(20) UNIQUE;
ALTER TABLE invoices ADD COLUMN origin_no   VARCHAR(20) UNIQUE;
ALTER TABLE invoices ALTER created_by DROP NOT NULL
EOF
  end

  def self.down
    raise "cannot down"
  end
end
