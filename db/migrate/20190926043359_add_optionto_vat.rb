class AddOptiontoVat < ActiveRecord::Migration
  def up
    change_column :vats, :sbo_code, :string, limit:6, null:false
  end
end
