# -*- coding:utf-8 -*-

# 請求書の行を維持する
class InvoiceDetailsAddLineno < ActiveRecord::Migration
  class << self
    def up
      execute <<EOF
ALTER TABLE invoice_details
   ADD COLUMN lineno int NOT NULL DEFAULT 0;
ALTER TABLE invoice_details
   ALTER COLUMN lineno DROP DEFAULT
EOF
    end

    def down
      raise ActiveRecord::IrreversibleMigration, "cannot down"
    end
  end # << self
end
