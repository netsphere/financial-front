# -*- coding: utf-8 -*-

# 請求書の金額の桁数を増やす
class SalesInvoicesDigitLength < ActiveRecord::Migration
  class << self
    def up
      execute <<EOF
ALTER TABLE sales_invoices ALTER COLUMN amount_total TYPE NUMERIC(12, 0);
ALTER TABLE si_details     ALTER COLUMN amount       TYPE NUMERIC(12, 0);
ALTER TABLE si_journals    ALTER COLUMN amount       TYPE NUMERIC(12, 0);
EOF
    end

    def down
      raise "cannot down"
    end
  end
end
