# -*- coding:utf-8 -*-

# 入金伝票に金額欄を追加
class ReceivingSlipsAddAmount < ActiveRecord::Migration
  class << self
    def up
      execute <<EOF
ALTER TABLE receiving_slips ADD COLUMN amount decimal(10) NOT NULL DEFAULT 0;
ALTER TABLE receiving_slips ALTER COLUMN amount DROP DEFAULT
EOF

      # 既存のデータは、入金口座の通貨と請求書の通貨は同じ
      ReceivingSlip.find(:all).each do |rs|
        a = 0
        rs.receivings.each do |r| 
          if r.charge != 0
            r.amount += r.charge
            r.save!
          end
          a += r.amount
        end
        rs.amount = a
        rs.save!
      end
    end

    def down
      raise ActiveRecord::IrreversibleMigration, "cannot down"
    end
  end # class << self
end
