# -*- coding:utf-8 -*-

# 名前フィールドを追加
class OurBanksAddName < ActiveRecord::Migration
  class << self
    def up
      # created_atなど: migration では, 互換性のため, NOT NULL は付けない.
      execute <<EOF
ALTER TABLE our_banks ADD COLUMN name           VARCHAR(40);
ALTER TABLE our_banks ADD COLUMN created_at     TIMESTAMP;
ALTER TABLE our_banks ADD COLUMN create_user_id int REFERENCES users (id);
ALTER TABLE our_banks ADD COLUMN updated_at     TIMESTAMP;
ALTER TABLE our_banks ADD COLUMN update_user_id int REFERENCES users(id)
EOF

      OurBank.all.each do |our_bank|
        our_bank.name = our_bank.remarks
        if our_bank.name.blank?
          if our_bank.bank_account
            our_bank.name = our_bank.bank_account.bank_branch.bank.name + "/" + 
                                        our_bank.bank_account.bank_branch.name 
          else
            our_bank.name = "_" # dummy
          end
        end
        our_bank.save!
      end

      execute <<EOF
ALTER TABLE our_banks ALTER COLUMN name SET NOT NULL
EOF
    end


    def down
      raise "cannot down"
    end
  end # class << self
end
