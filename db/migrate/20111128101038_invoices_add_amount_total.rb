# -*- coding:utf-8 -*-

require "application_helper"

# 請求書テーブルに合計金額を追加。
class InvoicesAddAmountTotal < ActiveRecord::Migration
  class << self
    def up
      execute <<EOF
ALTER TABLE invoices ADD COLUMN amount_total int
EOF

      execute <<EOF
UPDATE invoices SET amount_total = coalesce((SELECT sum(amount) FROM invoice_details WHERE invoice_id = invoices.id), 0)
EOF

      ids = ImportDeclaration.find :all, 
                :select => "sum(consumption_tax) AS sum_vat, sum(vat_local) AS sum_local_vat, imports_invoices.invoice_id AS invoice_id",
                :joins => "LEFT JOIN imports_invoices ON import_declarations.id = imports_invoices.import_declaration_id",
                :conditions => ["imports_invoices.invoice_id IS NOT NULL"],
                :group => "invoice_id"

      ids.each do |imd|
        inv = Invoice.find imd.invoice_id
        inv.amount_total += imd.sum_vat.to_i + imd.sum_local_vat.to_i
        inv.save!
      end

      execute <<EOF
ALTER TABLE invoices ALTER COLUMN amount_total SET NOT NULL
EOF
    end

    def down
      raise ActiveRecord::IrreversibleMigration, "cannot down"
    end
  end # << self
end
