# -*- coding:utf-8 -*-

# 部門はカンパニーに所属する。
class DivisionsAddCompany < ActiveRecord::Migration
  class << self
    def up
      company = Company.find 1
      execute <<EOF
ALTER TABLE divisions
     ADD COLUMN company_id int NOT NULL DEFAULT #{company.id} REFERENCES company_settings(id);
ALTER TABLE divisions
     ALTER COLUMN company_id DROP DEFAULT
EOF
    end

    def down
    end
  end # << self
end
