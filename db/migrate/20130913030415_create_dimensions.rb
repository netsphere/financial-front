# -*- coding:utf-8 -*-

# "次元"テーブルを追加する.
class CreateDimensions < ActiveRecord::Migration
  class << self
    def up
      execute <<EOF
CREATE TABLE dimensions (
  id       serial PRIMARY KEY,
  active   boolean NOT NULL,
  -- 次元2 = 2
  axis     int NOT NULL,

  dim_code VARCHAR(10) NOT NULL UNIQUE,
  name     VARCHAR(40) NOT NULL,

  created_at     TIMESTAMP NOT NULL,
  create_user_id int REFERENCES users (id),
  updated_at     TIMESTAMP,
  update_user_id int REFERENCES users (id)
);
EOF

      execute <<EOF
CREATE TABLE default_dimensions (
  id          serial PRIMARY KEY,

  division_id  int NOT NULL REFERENCES divisions (id),

  -- 次元2 = 2
  axis         int NOT NULL,

  dimension_id int NOT NULL REFERENCES dimensions (id),

  created_at     TIMESTAMP NOT NULL,
  create_user_id int NOT NULL REFERENCES users (id),

  UNIQUE (division_id, axis)
);
EOF
    end


    def down
      drop_table :default_dimensions
      drop_table :dimensions
    end
  end # class << self
end
