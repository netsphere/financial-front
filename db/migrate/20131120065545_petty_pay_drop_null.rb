# -*- coding:utf-8 -*-

# petty_paysのNOT NULLを付けすぎ
class PettyPayDropNull < ActiveRecord::Migration
  class << self
    def up
      execute <<EOF
ALTER TABLE petty_pays ALTER COLUMN demand_batch_close_date DROP NOT NULL;
ALTER TABLE petty_pays DROP CONSTRAINT petty_pays_demand_id_key;
EOF
    end

    def down
      raise "cannot down!"
    end
  end
end
