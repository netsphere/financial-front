# -*- coding:utf-8 -*-

# さらにマスタに更新日時などを追加
class MasterAddTime < ActiveRecord::Migration
  class << self
    def up
      [
        ["users",         "create_user_id", "int REFERENCES users (id)", nil],
        ["users",         "update_user_id", "int REFERENCES users (id)", nil],
        # 互換性のため, ナル可
        ["bank_accounts", "created_at",      "TIMESTAMP", nil],
        ["bank_accounts", "create_user_id", "int REFERENCES users (id)", nil],
        ["bank_accounts", "updated_at",      "TIMESTAMP", nil],
        ["bank_accounts", "update_user_id", "int REFERENCES users (id)", nil],
        ["bank_accounts", "lock_version",    "int", "DEFAULT 0"]
      ].each do |pat|
        execute "ALTER TABLE #{pat[0]} ADD COLUMN #{pat[1]} #{pat[2]} #{pat[3]}"
        if pat[3]
          execute "ALTER TABLE #{pat[0]} ALTER COLUMN #{pat[1]} SET NOT NULL"
          execute "ALTER TABLE #{pat[0]} ALTER COLUMN #{pat[1]} DROP DEFAULT"
        end
      end
    end

    def down
      raise "cannot down"
    end

  end # class << self
end
