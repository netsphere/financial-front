# -*- coding:utf-8 -*-

# コストセンタはカンパニーをまたぐのに、カンパニーにフィールドがあるのはおかしい
class MoveDefaultDivisionTreeToGlobal < ActiveRecord::Migration
  class << self
    def up
      column = "default_division_tree_root_id"

      company = Company.find 1
      execute <<EOF
ALTER TABLE global_settings ADD COLUMN #{column} int 
   NOT NULL REFERENCES control_divisions (id) 
   DEFAULT #{company.default_cost_center_id};
ALTER TABLE global_settings ALTER COLUMN #{column} DROP DEFAULT
EOF

      execute <<EOF
ALTER TABLE company_settings DROP COLUMN default_cost_center_id
EOF
    end

    def down
      raise ActiveRecord::IrreversibleMigration, "cannot down"
    end
  end # class << self
end
