class AddJeNumberToInvoices < ActiveRecord::Migration
  def change
    add_column :invoices, :je_number, :string, :limit => 8
    add_index :invoices, :je_number, unique: true
  end
end
