
class DropSboCodeFromVat < ActiveRecord::Migration[7.1]
  def up
    execute <<-SQL
ALTER TABLE vats DROP COLUMN sbo_code;
SQL
  end

  def down
    raise
  end
end
