# -*- coding:utf-8 -*-

require "application_helper"


# 出金伝票を追加
class AddPaymentSlips < ActiveRecord::Migration
  class << self
    def up
      execute <<EOF
CREATE TABLE payment_slips (
  id          serial PRIMARY KEY,
  company_id  int NOT NULL REFERENCES company_settings (id),
  date        DATE NOT NULL,

  journal_export_at TIMESTAMP,
  state             int NOT NULL,

  created_at     TIMESTAMP NOT NULL,
  create_user_id int NOT NULL REFERENCES users(id),
  updated_at     TIMESTAMP,

  lock_version   int NOT NULL
);
CREATE TABLE payment_banks (
  id              serial PRIMARY KEY,
  payment_slip_id int NOT NULL REFERENCES payment_slips (id),

  our_bank_id     int NOT NULL REFERENCES our_banks (id),
  cash_amount     int NOT NULL,
  inv_amount      decimal(12) NOT NULL,
  
  created_at      TIMESTAMP NOT NULL
);
CREATE TABLE payments (
  id              serial PRIMARY KEY,
  payment_slip_id int NOT NULL REFERENCES payment_slips (id),

  invoice_id      int NOT NULL REFERENCES invoices (id),
  account_title_id int NOT NULL REFERENCES account_titles(id),
  amount           int NOT NULL,

  created_at      TIMESTAMP NOT NULL
);
EOF
      
    end # up()


    def down
      raise ActiveRecord::IrreversibleMigration, "cannot down"
    end
  end # << self
end
