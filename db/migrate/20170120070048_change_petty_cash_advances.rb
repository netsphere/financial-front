# -*- coding:utf-8 -*-

# さらに修正.
class ChangePettyCashAdvances < ActiveRecord::Migration
  def self.up
    # batch_id はリサイクルされる
    execute <<EOF
ALTER TABLE petty_cash_advances DROP CONSTRAINT petty_cash_advances_batch_id_sequence_number_key;
ALTER TABLE petty_cash_advances ADD UNIQUE (batch_date, batch_id, sequence_number);
EOF
  end

  
  def self.down
    raise
  end
end
