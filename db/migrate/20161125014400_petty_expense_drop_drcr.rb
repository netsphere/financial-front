# -*- coding:utf-8 -*-

# petty_expense_allocations テーブルから debit_or_credit フィールドを落とす
class PettyExpenseDropDrcr < ActiveRecord::Migration
  def self.up
    PettyExpenseAllocation.transaction do 
      PettyExpenseAllocation.where("debit_or_credit = 'CR'").each do |r|
        orig_payee = r.payee_payment_type
        r.payee_payment_type = r.payer_payment_type
        r.payer_payment_type = orig_payee
        r.save!
      end
    end # transaction
    
    execute <<EOF
ALTER TABLE petty_expense_allocations DROP COLUMN debit_or_credit
EOF
  end
end
