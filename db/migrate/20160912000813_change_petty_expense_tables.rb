# -*- coding:utf-8 -*-


# 'petty_expenses' などを修正する。
class ChangePettyExpenseTables < ActiveRecord::Migration
  def self.up
    # batch_id はリサイクルされる
    execute <<EOF
ALTER TABLE petty_expenses DROP CONSTRAINT petty_expenses_batch_id_sequence_number_key
EOF
    execute <<EOF
ALTER TABLE petty_expenses ADD UNIQUE (batch_date, batch_id, sequence_number)
EOF

    # 桁数が短かったので伸ばす
    execute <<EOF
ALTER TABLE petty_expense_allocations ALTER payee_payment_type TYPE VARCHAR(32)
EOF
  end

  def self.down
    raise
  end
end
