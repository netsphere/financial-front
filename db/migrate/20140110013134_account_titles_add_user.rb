# -*- coding: utf-8 -*-

# 勘定科目マスタにユーザを追加.
class AccountTitlesAddUser < ActiveRecord::Migration
  class << self
    def up
      # 互換性のため, not nullなし
      sql = (["created_at       TIMESTAMP",
              "create_user_id   int REFERENCES users (id)",
              "updated_at       TIMESTAMP",
              "update_user_id   int REFERENCES users (id)"].map do |c| 
                     "ALTER TABLE account_titles ADD COLUMN " + c
             end).join(";")

      execute sql
    end

    def down
      raise "cannot down"
    end

  end # class << self
end
