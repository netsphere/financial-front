# -*- coding:utf-8 -*-

# 販売請求書にフィールドを追加
class SalesInvoicesAddFields < ActiveRecord::Migration
  class << self
    def up
      columns = [
        ["lineno", "int"],
        ["vat_id", "int REFERENCES vats (id)"],
        ["dim2_id", "int REFERENCES dimensions (id)"]
      ]

      columns.each do |col|
        execute <<EOF
ALTER TABLE si_journals ADD COLUMN #{col[0]} #{col[1]}
EOF
      end

      SiJournal.all.each do |r|
        r.lineno = 0
        r.save!
      end

      # 互換性のため, vat_code は NOT NULL だけ落としておく.
      execute <<EOF
ALTER TABLE si_journals ALTER COLUMN vat_code DROP NOT NULL;
ALTER TABLE si_journals ALTER COLUMN lineno SET NOT NULL
EOF
    end


    def down
      raise "cannot down"
    end
  end # class << self
end
