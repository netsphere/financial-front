# -*- coding:utf-8 -*-

# 既存のデータから Partマスタを作る
class CopyParts < ActiveRecord::Migration
  def self.up
    MatReceiving.transaction do 
      MatReceiving.select("DISTINCT part_no, part_desc, part_mat_class, fixed_asset_flag").each do |mat|
        part = mat.part
        if !part
          part = MatPart.new
          part.part_no = mat.part_no
          part.description = mat.part_desc
          part.mat_type = "P"   # 決め打ち
          part.mat_class = mat.part_mat_class
          part.fixed_asset_flag = mat.fixed_asset_flag

          part.save!
        end
      end # of each

      execute <<EOF
ALTER TABLE mat_receivings DROP COLUMN part_desc
EOF
    end
  end


  def self.down
    raise 
  end
end
