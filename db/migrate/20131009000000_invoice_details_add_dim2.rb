# -*- coding:utf-8 -*-

# (購買)請求書の明細行にdim2を追加
class InvoiceDetailsAddDim2 < ActiveRecord::Migration
  class << self
    def up
      # vat_code は, 互換性のため, not null だけ落としておく.
      execute <<EOF
ALTER TABLE invoice_details ADD COLUMN vat_id int REFERENCES vats (id);
ALTER TABLE invoice_details ADD COLUMN dim2_id int REFERENCES dimensions (id);
ALTER TABLE invoice_details ALTER COLUMN vat_code DROP NOT NULL
EOF
    end


    def down
      raise "cannot down"
    end
  end # class << self
end
