# -*- coding:utf-8 -*-

# 元帳データのインポート時に使用するフィールド
class CompanySettingsAddFallbackDivision < ActiveRecord::Migration
  class << self
    def up
      execute <<EOF
ALTER TABLE company_settings2 ADD COLUMN fallback_division_id int REFERENCES divisions (id)
EOF
      dmy = Division.find :first
      CompanySetting.find(:all).each do |r|
        r.fallback_division_id = dmy.id
        r.save!
      end

      execute <<EOF
ALTER TABLE company_settings2 ALTER COLUMN fallback_division_id SET NOT NULL
EOF
    end

    def down
      raise
    end
  end # << self
end
