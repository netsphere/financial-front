# -*- coding:utf-8 -*-

# 総勘定元帳に次元2を追加
class GlAddDim2 < ActiveRecord::Migration
  def self.up
    execute <<EOF
ALTER TABLE general_ledger ADD COLUMN dim2_id int REFERENCES dimensions (id)
EOF
  end

  def self.down
    raise "cannot down"
  end
end
