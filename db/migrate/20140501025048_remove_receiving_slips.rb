# -*- coding:utf-8 -*-

# receiving_slips を削除し, receivings を payment_slips に紐付ける
class RemoveReceivingSlips < ActiveRecord::Migration
  class << self
    def up
      # 既存の出金伝票の金額をすべてマイナスに
      # 列を追加
      execute <<EOF
UPDATE payment_banks SET cash_amount = -cash_amount;
ALTER TABLE receivings 
      ADD COLUMN cash_slip_id int REFERENCES payment_slips (id);
ALTER TABLE payment_banks ALTER COLUMN cash_amount TYPE decimal(12)
EOF

      # receiving_slipを作り直す
      ReceivingSlip.all.each do |receiving_slip|
        inv_amt = Receiving.find :first, 
                  :select => "SUM(amount) AS amount",
                  :conditions => ["receiving_slip_id = ?", receiving_slip.id]
        inv_amt = inv_amt && inv_amt.amount ? inv_amt.amount : 0

        cash_slip_bank = PaymentBank.new
        cash_slip_bank.our_bank_id = receiving_slip.our_bank_id
        cash_slip_bank.cash_amount = receiving_slip.amount
        cash_slip_bank.inv_amount = inv_amt
        cash_slip_bank.created_at = receiving_slip.created_at

        cash_slip = PaymentSlip.new
        cash_slip.company_id = cash_slip_bank.our_bank.company_setting_id
        cash_slip.date = receiving_slip.date
        cash_slip.journal_export_at = receiving_slip.journal_export_at
        cash_slip.state = receiving_slip.state
        cash_slip.created_at = receiving_slip.created_at
        cash_slip.create_user_id = receiving_slip.created_by
        cash_slip.updated_at = receiving_slip.updated_at
        cash_slip.lock_version = 0
        cash_slip.save!

        cash_slip_bank.payment_slip_id = cash_slip.id
        cash_slip_bank.save!

        # 紐付けを変える
        execute <<EOF
UPDATE receivings SET cash_slip_id = #{cash_slip.id} 
    WHERE receiving_slip_id = #{receiving_slip.id};
UPDATE rs_logs SET payment_slip_id = #{cash_slip.id}
    WHERE receiving_slip_id = #{receiving_slip.id}
EOF
      end # of each

      execute <<EOF
ALTER TABLE receivings ALTER COLUMN cash_slip_id SET NOT NULL;
ALTER TABLE receivings DROP COLUMN receiving_slip_id;
ALTER TABLE rs_logs ALTER COLUMN payment_slip_id SET NOT NULL;
ALTER TABLE rs_logs DROP COLUMN receiving_slip_id;
DROP TABLE receiving_slips
EOF
    end


    def down
      raise "cannot down!!"
    end
  end # class << self
end
