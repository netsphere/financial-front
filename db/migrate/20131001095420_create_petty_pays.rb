# -*- coding:utf-8 -*-

# 小口支払い
class CreatePettyPays < ActiveRecord::Migration
  class << self
    def create_petty_pays
      execute <<EOF
CREATE TABLE petty_pays (
  id               serial PRIMARY KEY,

  -- 親. 後から作る?
  cash_slip_id     int REFERENCES payment_slips (id),

  demand_batch_id VARCHAR(36) NOT NULL,  -- header col 2
  demand_batch_close_date DATE NOT NULL, -- header col 3

  -- ex. "pro_test-user"
  user_id          int NOT NULL REFERENCES users (id),  -- col 2
  petty_group_id    int NOT NULL REFERENCES petty_groups (id), -- col 34

  -- "PAYM", "CHNG"
  transaction_type CHAR(4) NOT NULL,  -- col 35
  
  -- cash advance key. 仮払いの支払いのとき
  cash_advance_id  int REFERENCES petty_cash_advances (id),  -- col 36

  currency_id int NOT NULL REFERENCES currencies (id),   -- col 43

  demand_id        int NOT NULL UNIQUE,  -- col 44

  -- skip col 46  入っていないことがある
  -- skip col 47  入っていないことがある

  -- "DR" or "CR". 必ず"DR"のようだが.
  liability_side   CHAR(2) NOT NULL, -- col 48

  -- 払い戻し金額
  amount      NUMERIC(12, 4) NOT NULL,   -- col 50

  -- 経費精算の支払いのとき.
  expense_id        int REFERENCES petty_expenses (id), -- col 52

  -- cash advanceとexpense report が別テーブル. referしない
  -- expense_batch_id int NOT NULL REFERENCES petty_expenses -- col 83

  created_at   TIMESTAMP NOT NULL
);
EOF
    end


    # 小口の入金あり
    def alter_receivings
      execute <<EOF
ALTER TABLE receivings ALTER COLUMN sales_invoice_id DROP NOT NULL;
ALTER TABLE receivings ADD COLUMN petty_expense_id int REFERENCES petty_expenses (id);
ALTER TABLE receivings ADD COLUMN currency_id int REFERENCES currencies (id)
EOF

      Receiving.all.each do |r|
        r.currency_id = r.sales_invoice.currency_id
        r.save!
      end

      execute <<EOF
ALTER TABLE receivings ALTER COLUMN currency_id SET NOT NULL
EOF
    end


    def up
      create_petty_pays
      alter_receivings
    end


    def down
      raise "cannot down"
      drop_table :petty_pays
    end
  end # class << self
end
