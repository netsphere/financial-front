# -*- coding:utf-8 -*-

# 輸入申告にカンパニーを追加
class ImportDeclarationsAddCompany < ActiveRecord::Migration
  class << self
    def up
      company = Company.find 1

      execute <<EOF
ALTER TABLE import_declarations
    ADD COLUMN company_id int NOT NULL REFERENCES company_settings (id) 
                          DEFAULT #{company.id};
ALTER TABLE import_declarations
    ALTER COLUMN company_id DROP DEFAULT   
EOF
    end

    def down
    end
  end # << self
end
