# -*- coding:utf-8 -*-

# 社員コードの型を変更する
class UsersChangeWorkerCode < ActiveRecord::Migration
  class << self
    def up
      execute <<EOF
ALTER TABLE users ADD COLUMN emp_id VARCHAR(48) UNIQUE
EOF
      User.all.each do |user|
        user.emp_id = sprintf("%06d", user.worker_code)
        user.save!
      end

      # テストユーザを作る. hard-coded!!
      u = User.find :first, :conditions => ["emp_id = ?", "22222222"]
      if u
        u.emp_id = "pro_test-user"
        u.save!
      end

      execute <<EOF
ALTER TABLE users DROP COLUMN worker_code;
ALTER TABLE users RENAME emp_id TO worker_code;
ALTER TABLE users ALTER COLUMN worker_code SET NOT NULL;
EOF
    end


    def down
      raise "cannot down"
    end
  end # class << self
end
