# -*- coding:utf-8 -*-

# (1) カンパニーを Company と CompanySetting に分離する。
class NewCompanySettings < ActiveRecord::Migration
  class << self
    def up
      execute <<EOF
CREATE TABLE company_settings2 (
  id          serial PRIMARY KEY,
  company_id  int NOT NULL REFERENCES company_settings(id),

  -- 会計システム連動用
  bank_division_id int NOT NULL REFERENCES divisions (id)
)
EOF
      Company.find(:all).each do |company|
        cs = CompanySetting.new :company_id => company.id,
                              :bank_division_id => company.bank_division_id
        cs.save!
      end

      execute <<EOF
ALTER TABLE company_settings DROP COLUMN bank_division_id
EOF
    end

    def down
      raise ActiveRecord::IrreversibleMigration, "cannot down"
    end
  end
end
