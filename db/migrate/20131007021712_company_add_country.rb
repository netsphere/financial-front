# -*- coding:utf-8 -*-

# カンパニーに国フィールドを追加
class CompanyAddCountry < ActiveRecord::Migration
  class << self
    def up
      execute <<EOF
ALTER TABLE company_settings ADD COLUMN country CHAR(2)
EOF
      Company.all.each do |company|
        case company.functional_currency.code
        when "JPY"
          company.country = "JP"
        when "KRW"
          company.country = "KR"
        else
          raise # TODO:
        end
        company.save!
      end

      execute <<EOF
ALTER TABLE company_settings ALTER COLUMN country SET NOT NULL
EOF
    end
    

    def down
      raise "cannot down"
    end
  end # class << self
end
