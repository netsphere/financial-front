# -*- coding:utf-8 -*-

# 長さが短いフィールドを伸ばす
class AttachFileExtendLengths < ActiveRecord::Migration
  def self.up
    execute <<EOF
ALTER TABLE attach_files
            ALTER COLUMN content_type TYPE character varying(100);
ALTER TABLE attach_files
            ALTER COLUMN original_filename TYPE character varying(260);
EOF
  end
end
