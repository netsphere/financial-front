# -*- coding:utf-8 -*-

# Partマスタを追加
class CreateMatParts < ActiveRecord::Migration
  def self.up
    execute <<EOF
CREATE TABLE mat_parts (
  id        serial PRIMARY KEY,

  part_no   VARCHAR(20) NOT NULL UNIQUE,

  description VARCHAR(36) NOT NULL,

  -- 'P' or 'T'
  mat_type         VARCHAR(2) NOT NULL,

  -- 'C' or 'R'
  mat_class   CHAR(1) NOT NULL,

  -- ときどき変わる
  fixed_asset_flag BOOLEAN NOT NULL,

  created_at      TIMESTAMP NOT NULL,
  create_user_id  int REFERENCES users (id),  -- インポートの場合はナル
  updated_at      TIMESTAMP,
  update_user_id  int REFERENCES users (id),
  lock_version    int NOT NULL
);
EOF
  end


  def self.down
    drop_table :mat_parts
  end
end
