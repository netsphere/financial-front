# -*- coding:utf-8 -*-

# 入金・出金伝票が区別できる属性を追加する
class PaymentSlipsAddType < ActiveRecord::Migration
  class << self
    def up
      execute <<EOF
ALTER TABLE payment_slips ADD COLUMN slip_type int;
ALTER TABLE payment_slips ADD COLUMN update_user_id int REFERENCES users (id)
EOF

      PaymentSlip.all.each do |cash_slip|
        cash_banks = cash_slip.payment_banks
        if cash_banks.empty?
          cash_slip.slip_type = 2 # 過去分で口座がないのは出金伝票.
          cash_slip.save!
          next
        end

        cash_slip.slip_type = cash_banks.first.cash_amount >= 0 ? 1 : 2
        cash_slip.save!
      end

      execute <<EOF
ALTER TABLE payment_slips ALTER COLUMN slip_type SET NOT NULL
EOF
    end

    def down
      raise "cannot down!!"
    end
  end # class << self
end
