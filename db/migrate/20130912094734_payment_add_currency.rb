# -*- coding:utf-8 -*-

# 通貨を支払いレコードにも持たせる
class PaymentAddCurrency < ActiveRecord::Migration
  def self.up
    execute <<EOF
ALTER TABLE payments ADD COLUMN currency_id int REFERENCES currencies (id);
EOF

    Payment.all.each do |payment|
      payment.currency_id = payment.invoice.currency_id
      payment.save!
    end

    execute <<EOF
ALTER TABLE payments ALTER COLUMN currency_id SET NOT NULL
EOF
  end

  def self.down
    raise "cannot down"
  end
end
