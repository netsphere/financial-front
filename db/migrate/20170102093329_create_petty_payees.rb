# -*- coding:utf-8 -*-

# 小口精算のクレジットカード・マスタ
class CreatePettyPayees < ActiveRecord::Migration
  class << self
    # ハードコードしていたのをマスタ化する
    def make_specials
      u = User.first # ダミー
      raise if !u
      
      PettyPayee.create!({:payment_type => "Company",
                         :name => "経費 [System]",
                         :remarks => "",
                         :ar_title_id => nil,
                         :ap_title_id => nil,
                         :bp_code => nil,
                         :create_user_id => u.id,
                         #:created_at => 0,
                         :lock_version => 0})

      #社員未収入金/未払金
      r_ac = AccountTitle.where("account_code = 170").first
      pay_ac = AccountTitle.where("account_code = 323").first
      PettyPayee.create!({:payment_type => "Employee",
                         :name => "社員 [System]",
                         :remarks => "",
                         :ar_title_id => r_ac.id,
                         :ap_title_id => pay_ac.id,
                         :bp_code => nil,
                         :create_user_id => u.id,
                         #:created_at => 0,
                         :lock_version => 0})

      # 未収収入金/未払金 -- 同じ科目を使う
      pay_ac = AccountTitle.where("account_code = 325").first
      PettyPayee.create!({:payment_type => "Corporate_SMBC_VISA_CBCP",
                         :name => "VISAカード",
                         :remarks => "",
                         :ar_title_id => pay_ac.id,
                         :ap_title_id => pay_ac.id,
                         :bp_code => 926993,
                         :create_user_id => u.id,
                         #:created_at => 0,
                         :lock_version => 0})
    end

    
    def up
      execute <<'EOF'
CREATE TABLE petty_payees (
  id     serial PRIMARY KEY,

  -- 1 特殊コード "Company"
  --   経費勘定
  -- 2 特殊コード "Employee"
  --   社員未収入金, 社員未払金
  -- 3 クレジットカード(発行者) + 精算方式
  --   ex) "Corporate_SMBC_VISA_CBCP"
  --   未収入金, 未払金
  payment_type VARCHAR(32) NOT NULL UNIQUE,

  name           VARCHAR(80) NOT NULL,
  remarks        TEXT NOT NULL,

  -- 未収入金a/c
  -- "Company" の場合は未使用
  ar_title_id    int REFERENCES account_titles (id),

  -- 未払金a/c
  -- "Company" の場合は未使用	
  ap_title_id    int REFERENCES account_titles (id),
  
  -- 取引先コード
  -- "Company", "Employee" の場合は未使用
  bp_code        int ,
  
  created_at     TIMESTAMP NOT NULL,
  create_user_id int NOT NULL REFERENCES users (id),
  updated_at     TIMESTAMP,
  update_user_id int REFERENCES users (id),
  lock_version   int NOT NULL,

  UNIQUE (ap_title_id, bp_code)
);
EOF

      make_specials()

      # 参照制約を付ける
      tbls = ["petty_expense_allocations", "petty_cash_advances"]
      cols = ["payer_payment_type", "payee_payment_type"]
      tbls.each do |t|
        cols.each do |c|
          execute <<EOF
ALTER TABLE #{t}
    ADD CONSTRAINT #{t}_#{c}_fkey FOREIGN KEY (#{c}) REFERENCES petty_payees (payment_type);
EOF
        end
      end
      
    end # def up
    
  end # class << self
end
