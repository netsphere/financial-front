class AddJeNumberToSalesInvoices < ActiveRecord::Migration
  def change
    add_column :sales_invoices, :je_number, :string, :limit => 8
    add_index :sales_invoices, :je_number, unique: true
  end
end
