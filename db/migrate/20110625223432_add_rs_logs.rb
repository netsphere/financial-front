# -*- coding:utf-8 -*-

# 入金伝票にログを追加
class AddRsLogs < ActiveRecord::Migration
  class << self
    def up
      execute <<EOF
CREATE TABLE rs_logs (
  id                serial PRIMARY KEY,
  receiving_slip_id int REFERENCES receiving_slips (id),
  payment_slip_id   int REFERENCES payment_slips (id),
  kind              int NOT NULL,    -- InvoiceLog::MODIFIED, InvoiceLog::SIGNED_OFF
  remarks           VARCHAR(200) NOT NULL,
  done_by           int NOT NULL REFERENCES users(id),
  done_at           TIMESTAMP NOT NULL
);
EOF
    end

    def down
    end
  end
end
