# -*- coding:utf-8 -*-

# 請求書はすべて支払い済み
class InvoicesAllPaid < ActiveRecord::Migration
  class << self
    def up
      # 全部、この科目を使う
      fallback = AccountTitle.find_by_account_code(321)  # TODO: special

      # cut-off day  TODO: special
      day = Date.new 2011, 12, 28
      slip_day = Date.new 2011, 1, 1

      PaymentSlip.transaction do 
        Company.find(:all).each do |company|
          Currency.find(:all).each do |currency|
            invs = Invoice.find :all, 
                  :conditions => ["company_setting_id = ? AND (bank_account_id IS NULL) AND due_day < ? AND currency_id = ? AND (state = 20 AND payment_state != 20)", company.id, day, currency.id]

            slip = PaymentSlip.new :company => company,
                                 :date => slip_day,
                                 :journal_export_at => Time.now,
                                 :state => 20,
                                 :created_by => User.find(:first)
            slip.save!

            ttl = 0
            invs.each do |inv|
              # inv.remain_ac_amounts.each do |acid, amount|
                # raise "inv_id = #{inv.id}, #{inv.remain_ac_amounts.inspect}" if acid.to_i == 0

              pay = Payment.new :payment_slip => slip,
                            :invoice => inv,
                            :account_title_id => fallback.id,
                            :amount => inv.amount_total
              pay.save!
              ttl += pay.amount
            # end
            end
            pb = PaymentBank.new :payment_slip => slip,
                          :our_bank => OurBank.find(:first, 
                                     :conditions => ["company_setting_id = ?", 
                                                     company.id]),
                          :cash_amount => 100, # 手抜き
                          :inv_amount => ttl
            pb.save!
          end # currency
        end # company
      end # transaction
    end # method up()


    def down
      raise ActiveRecord::IrreversibleMigration, "cannot down"
    end
  end # << self
end
