class AddColumnToVat < ActiveRecord::Migration
  def change
    add_column :vats, :sbo_code, :string, limit:6
  end
end
