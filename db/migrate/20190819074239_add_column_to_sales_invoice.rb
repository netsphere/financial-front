class AddColumnToSalesInvoice < ActiveRecord::Migration
  def change
    add_column :sales_invoices, :including_tax, :boolean, default: false, null: false
  end
end
