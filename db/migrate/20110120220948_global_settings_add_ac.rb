# -*- coding: utf-8 -*-

require "application_helper"

# 大域設定をデータベースに保存するようにする。
class GlobalSettingsAddAc < ActiveRecord::Migration
  class << self
    def add_ac_instances
      r = []
      r << ( AccountTitle.find_by_account_code(852) ||
             AccountTitle.new(:aggregate_flag => false,
                      :account_code => 8852,
                      :suppl_code => 0,
                      :main_name => "支払手数料",
                      :name => "支払手数料",
                      :kana_name => "シハライテスウリョウ",
                      :description => "",
                      :kind => 0,
                      :sign => +1,
                      :req_partner => false,
                      :vat_code => 10) )

      r << ( AccountTitle.find_by_account_code(905) ||
             AccountTitle.new(:aggregate_flag => false,
                      :account_code => 9905,
                      :suppl_code => 0,
                      :main_name => "為替差損益",
                      :name => "為替差損益",
                      :kana_name => "カワセサソンエキ",
                      :description => "",
                      :kind => 0,
                      :sign => -1,
                      :req_partner => false,
                      :vat_code => 40) )

      r << ( AccountTitle.find_by_account_code(173) ||
             AccountTitle.new(:aggregate_flag => false,
                      :account_code => 1173,
                      :suppl_code => 0,
                      :main_name => "仮払JP消費税等",
                      :name => "仮払JP消費税等",
                      :kana_name => "カリバライショウヒゼイ",
                      :description => "",
                      :kind => 0,
                      :sign => +1,
                      :req_partner => false,
                      :vat_code => 40) )
      r.each do |ac| ac.save! end
      r
    end

    def up
      columns = ["bank_charge_account_title_id", 
                 "currency_gain_account_title_id",
                 "vat_payment_account_title_id"]

      acs = add_ac_instances
      columns.each_with_index do |c, i|
        execute <<EOF
ALTER TABLE global_settings ADD COLUMN #{c} int NOT NULL REFERENCES account_titles(id) DEFAULT #{acs[i].id};
ALTER TABLE global_settings ALTER COLUMN #{c} DROP DEFAULT
EOF
      end
    end

    def down
      raise ActiveRecord::IrreversibleMigration, "cannot down"
    end
  end # class << self
end
