# -*- coding:utf-8 -*-

# 売掛金、買掛金などのフラグ欄を追加
class AccountTitlesAddKind < ActiveRecord::Migration
  class << self
    def up
      execute <<EOF
ALTER TABLE account_titles ADD COLUMN kind int NOT NULL DEFAULT 0;
ALTER TABLE account_titles ALTER COLUMN kind DROP DEFAULT
EOF
    end

    def down
      raise ActiveRecord::IrreversibleMigration, "cannot down"
    end
  end # class << self
end
