# -*- coding:utf-8 -*-

# 材料仕入
class CreateMatReceivings < ActiveRecord::Migration
  def self.up
execute <<EOF
CREATE TABLE mat_receivings (
  id            serial PRIMARY KEY,

  -- 親
  invoice_id    int NOT NULL REFERENCES invoices (id),
  vendor_code   VARCHAR(20) NOT NULL,

  -- debit = 'D', credit = 'C'
  booking_type  CHAR(1) NOT NULL,

  -- こちらもデータが入っていない
  -- account_number

  delivery_date DATE NOT NULL,

  cost_center   VARCHAR(8) NOT NULL,

  -- <orderInformation>
  order_type    VARCHAR(2) NOT NULL,
  order_no      VARCHAR(12) NOT NULL,
  order_item_no int NOT NULL,
  
  -- <partInformation>
  part_no        VARCHAR(20) NOT NULL,
  -- P/Nの名前. 冗長だが...
  part_desc      VARCHAR(36) NOT NULL,
  part_mat_class CHAR(1) NOT NULL,

  -- mat_class とは一致しない
  fixed_asset_flag BOOLEAN NOT NULL,

  -- 'Not_Available'のときはナル値
  part_serial    VARCHAR(20),

  receiving_no   int NOT NULL,

  currency_code  VARCHAR(3) NOT NULL,

  -- <detailAmount>
  -- 税抜価格の合計
  total_amount   DECIMAL(12, 4) NOT NULL,

  quantity       int NOT NULL,

  -- 'JP8', 'NA', ...
  tax_code       VARCHAR(6) NOT NULL,

  tax_amount     DECIMAL(12, 4) NOT NULL,

  created_at     TIMESTAMP NOT NULL,
  updated_at     TIMESTAMP
);
CREATE TABLE mat_receiving_charges (
  id     serial PRIMARY KEY,

  -- 親
  mat_receiving_id int NOT NULL REFERENCES mat_receivings (id),

  charge_code      VARCHAR(6) NOT NULL,

  -- charge_codeの名前. 冗長だが...
  charge_code_desc VARCHAR(36) NOT NULL,

  -- <chargeAmount>
  -- こちらも税抜き価格
  total_amount     DECIMAL(12, 4) NOT NULL,

  tax_code         VARCHAR(6) NOT NULL,

  tax_amount       DECIMAL(12, 4) NOT NULL,

  created_at     TIMESTAMP NOT NULL,
  updated_at     TIMESTAMP
);
EOF
  end


  def self.down
    drop_table :mat_receiving_charges
    drop_table :mat_receivings
  end
end
