class AddColumnToCompany < ActiveRecord::Migration
  def change
    add_column :company_settings, :tax_reg_number, :string, limit:14
  end
end
