
class UsersDivisionsDropNull < ActiveRecord::Migration
  class << self
    def up
      execute <<EOF
ALTER TABLE users_divisions ALTER COLUMN effective_day DROP NOT NULL
EOF
    end

    def down
      raise ActiveRecord::IrreversibleMigration, "cannot down"
    end
  end # class << self
end
