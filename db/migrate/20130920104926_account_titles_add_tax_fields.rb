# -*- coding:utf-8 -*-

# 勘定科目マスタに, 
#     (1) 消費税フィールドを追加する
#     (2) 請求書で登録可能かのフラグ
class AccountTitlesAddTaxFields < ActiveRecord::Migration
  class << self
    # hard-coded. # TODO: fixme
    def tax_side account_code
      # 資産
      if [159, 
          201, 203, 205, 207, 209, 211, 213, 215, 218, 221, 222, 223, 224, 228,
          229, 231, 232, 233, 234, 235, 236, 249].include?(account_code)
        return 2
      end

      # 売上
      if account_code >= 600 && account_code <= 699 ||
                            account_code >= 901 && account_code <= 909 ||
                            account_code >= 921 && account_code <= 929
        if [905, 906, 907, 922, 926].include?(account_code)
          return 0
        end

        return 1
      end

      # 費用
      if account_code >= 701 && account_code <= 899 ||
                      account_code >= 911 && account_code <= 919 ||
                      account_code >= 931 && account_code <= 999
        if [722, 732, 734, 738, 781, 783, 801, 802, 803, 807, 808, 809, 
            814, 815, 816, 817, 831, 832, 833, 835, 836, 837, 838, 839, 840,
            856, 857, 858, 860, 863, 
            911, 914, 915, 916, 932, 933, 934, 951, 952].include?(account_code)
          return 0
        end
        if [739, 819, 865].include?(account_code)
          return 1
        end

        return 2
      end

      return 0
    end


    def up
      fields = [
        ["enable_si_invoice", "boolean"],
        ["enable_invoice", "boolean"],
        ["tax_side", "int"],
        # ["default_tax_type", "int"]
      ]

      fields.each do |pair|
        execute <<EOF
ALTER TABLE account_titles ADD COLUMN #{pair[0]} #{pair[1]} 
EOF
      end

      execute <<EOF
ALTER TABLE account_titles RENAME vat_code TO default_tax_type
EOF

      AccountTitle.find(:all).each do |ac|
        ac.enable_si_invoice = ac.receivable_id ? true : 0
        ac.enable_invoice = ac.payable_id ? true : 0

        ac.tax_side = tax_side(ac.account_code)
        if ac.tax_side == 0
          ac.default_tax_type = 0
        else
          ac.default_tax_type = 0 if ac.default_tax_type == 40 # hard-coded!
        end

        ac.kana_name = "_" if ac.kana_name.blank?  # workaround
        ac.save!
      end

      fields.each do |pair|
        execute <<EOF
ALTER TABLE account_titles ALTER COLUMN #{pair[0]} SET NOT NULL
EOF
      end
    end


    def down
      raise
    end
  end # class << self
end
