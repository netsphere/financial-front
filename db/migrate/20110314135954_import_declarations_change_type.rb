# -*- coding:utf-8 -*-

require "application_helper"


# 申告番号にはアルファベットも含まれる
class ImportDeclarationsChangeType < ActiveRecord::Migration
  class << self
    def up
      execute <<EOF
ALTER TABLE import_declarations 
    ADD COLUMN declaration_number2 VARCHAR(11) UNIQUE
EOF
      ImportDeclaration.find(:all).each do |r|
        r.declaration_number2 = r.declaration_number.to_s
        r.bl_number = "xxx" if !r.bl_number || r.bl_number.length < 3  # 異常データ対策
        if r.declaration_number2.length < 6
          r.declaration_number2 = r.declaration_number2 + "xxxxxx"
        end
        r.save!
      end

      execute <<EOF
ALTER TABLE import_declarations
    ALTER COLUMN declaration_number2 SET NOT NULL;
ALTER TABLE import_declarations
    DROP COLUMN declaration_number;
ALTER TABLE import_declarations
    RENAME declaration_number2 TO declaration_number
EOF
    end

    def down
    end
  end # << self
end
