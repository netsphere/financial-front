# -*- coding: utf-8 -*-

# 小口経費精算関係のテーブルを追加
class AddPettyTables < ActiveRecord::Migration
  class << self

    def create_petty_cash_advances
      execute <<EOF
CREATE TABLE petty_cash_advances (
  id                    serial PRIMARY KEY,

  ----------------------------------------------------
  -- Batch Data

  batch_id              int,      -- col 2

  batch_date            DATE,     -- col 3

  -- BatchId のなかでの行番号. batchごとに1始まり
  sequence_number       int,      -- col 4

  ----------------------------------------------------
  -- Employee Data

  -- employee ID
  -- ex. "pro_test-user"
  user_id           int NOT NULL REFERENCES users (id),
  petty_group_id    int NOT NULL REFERENCES petty_groups (id), -- col 9

  ----------------------------------------------------
  -- Journal Entry Data

  -- "Company" or "Employee"
  -- 仮払いの場合、"Company"固定、か。
  payer_payment_type VARCHAR(16) NOT NULL, -- col 163

  -- 仮払いの場合, "Employee"固定、か。
  payee_payment_type VARCHAR(16) NOT NULL, -- coi 165

  -- ex. "849/2"   col 167
  account_title_id   int NOT NULL REFERENCES account_titles (id), 

  -- "DR" or "CR"
  -- 仮払いの場合, "DR"
  debit_or_credit    CHAR(2) NOT NULL,  -- col 168

  -- debit_or_creditとは関係なく, 正値が借方
  -- 通貨は reimbursement currency
  journal_amount     NUMERIC(12, 4) NOT NULL, -- col 169

  -----------------------------------------------------
  -- Cash Advance Data

  -- 要求金額
  request_amount        NUMERIC(12, 4) NOT NULL, -- col 177

  request_currency_id   int NOT NULL REFERENCES currencies (id), -- col 178

  -- col 179 skip.

  exchange_rate         NUMERIC(10, 4) NOT NULL, -- col 180

  -- col 181
  reimb_currency_id int NOT NULL REFERENCES currencies (id),

  -- col 182 skip.

  -- 経理承認日
  processing_date       DATE,  -- col 183

  -- ex. "Cash", "AMEX"
  payment_code          VARCHAR(10) NOT NULL, -- col 184

  -- 1 = issue, 3 = system
  transaction_type      int NOT NULL,  -- col 185

  -- 本人が提出した日
  submit_date           DATE NOT NULL,  -- col 186

  -- 仮払申請のキー
  ca_key                int NOT NULL UNIQUE, -- col 187

  payment_method        int NOT NULL,  -- col 188
  name VARCHAR(40) NOT NULL,

  -- 仕訳出力日時
  journal_export_at TIMESTAMP,

  UNIQUE (batch_id, sequence_number)
);
EOF
    end


    def create_petty_expenses
      execute <<EOF
CREATE TABLE petty_expenses (
  id              serial PRIMARY KEY,

  ----------------------------------------------------
  -- Batch Data

  batch_id        int,    -- col 2

  batch_date      DATE,   -- col 3

  -- BatchId のなかでの行番号. batchごとに1始まり
  sequence_number int,    -- col 4

  ----------------------------------------------------
  -- Employee Data

  -- employee ID
  -- ex. "pro_test-user"
  user_id           int NOT NULL REFERENCES users (id),  
  petty_group_id    int NOT NULL REFERENCES petty_groups (id), -- col 9

  ----------------------------------------------------
  -- Report Data

  -- 実データは20桁ぐらい
  report_id                 VARCHAR(32) NOT NULL UNIQUE,  -- col 19

  -- ex. "JPY"  col 22
  reimb_currency_id int NOT NULL REFERENCES currencies (id),

  -- ex. "JAPAN"
  country_lang_name   VARCHAR(64) NOT NULL,  -- col 23

  -- 本人が提出した日
  submit_date         DATE NOT NULL,  -- col 24

  -- 経理承認日
  processing_date     DATE, -- col 26

  -- ex. "TEST_REPORT_02"
  report_name    VARCHAR(40) NOT NULL,  -- col 27

  image_required BOOLEAN NOT NULL,  -- col 28

  has_vat_entry  BOOLEAN NOT NULL,  -- col 29

  has_ta_entry   BOOLEAN NOT NULL,  -- col 30

  -- skip col 31

  -- 承認された合計額
  total_approved_amount NUMERIC(12, 4) NOT NULL,  -- col 32

  -- 合計金額 (消し込みのため.)
  emp_amount_total  NUMERIC(12, 4) NOT NULL,
  cc_amount_total   NUMERIC(12, 4) NOT NULL,
  receivable_amount_total NUMERIC(12, 4) NOT NULL,

  -- 仕訳出力日時
  journal_export_at TIMESTAMP,

  UNIQUE (batch_id, sequence_number)
);
EOF
    end


    def create_petty_expense_entries
      execute <<EOF
CREATE TABLE petty_expense_entries (
  id                  serial PRIMARY KEY,

  -- 親
  petty_expense_id    int NOT NULL REFERENCES petty_expenses(id),

  ---------------------------------------------------------------------------
  -- Report Entry Data

  -- エントリのユニークキー. 
  -- 1エントリの仕訳に対して複数部門 (allocations) が発生することに注意.
  report_entry_key    int NOT NULL UNIQUE,  -- col 61

  -- "REG" or "CHD"
  transaction_type    CHAR(3) NOT NULL,  -- col 62

  -- 経費タイプ名
  -- ex. "Advertising"
  expense_type_name   VARCHAR(64) NOT NULL, -- col 63

  -- 取引日(本人の支出した日)
  transaction_date    DATE NOT NULL,  -- col 64

  -- 変な通貨のことがある. テキストで保存
  spend_currency_code CHAR(3) NOT NULL, -- col 65

  -- from spend currency to reimbursement currency
  exchange_rate       NUMERIC(10, 4) NOT NULL,  -- col 66

  -- "M" = multiply
  exchange_rate_direction CHAR(1) NOT NULL,   -- col 67

  is_personal             BOOLEAN NOT NULL, -- col 68

  description             VARCHAR(64) NOT NULL, -- col 69

  vendor_name             VARCHAR(64) NOT NULL, -- col 70

  -- ex. "広告代理店"
  vendor_description      VARCHAR(64) NOT NULL, -- col 71

  receipt_received        BOOLEAN NOT NULL,  -- col 72

  -- "T", "R" or "N"
  receipt_type            CHAR(1) NOT NULL, -- col 73

  -- 本人を含む同席人数
  employee_attendee_count int NOT NULL,  -- col 74

  spouse_attendee_count   int NOT NULL,  -- col 75

  business_attendee_count int NOT NULL,  -- col 76

  -- in spend currency
  spend_amount            NUMERIC(12, 4) NOT NULL, -- col 123

  -- skip 124

  -- 承認された金額 in reimbursement currency
  approved_amount         NUMERIC(12, 4) NOT NULL, -- col 125

  -- ex. "CASH",  カードフィード => "IBCP"
  payment_code            VARCHAR(4) NOT NULL,  -- col 126

  --payment_code_name       VARCHAR(40) NOT NULL,  -- col 127

  -- 本人に支払う（振込・現金）の場合 => 1. 現金渡しずみでも1が来る
  -- カードフィードの場合 blank
  reimb_type              int NOT NULL,  -- col 128

  ---------------------------------------------------
  -- Credit Card Data

  cc_billing_date      DATE,    -- col 129

  -- 頭6桁 + 下4桁がunmasked
  cc_masked_cc_number  VARCHAR(20),  -- col 130

  cc_name_on_card      VARCHAR(40),  -- col 131

  -- フィードIDか?
  cc_jr_key               VARCHAR(13),   -- col 132

  cc_ref_no               VARCHAR(64) UNIQUE,   -- col 133

  cc_cct_key              VARCHAR(64) UNIQUE,   -- col 134

  -- "CAV" => Cash Advance
  -- "RPE" => 購入
  cc_cct_type             CHAR(3),  -- col 135

  cc_transaction_id       VARCHAR(32) UNIQUE,  -- col 136

  -- in spend amount
  cc_transaction_amount   NUMERIC(12, 4),   -- col 137

  -- amount of tax on the transaction amount
  cc_tax_amount           NUMERIC(12, 4),   -- col 138

  -- spend currency  => テキストで保存
  cc_currency_code        CHAR(3),     -- col 139

  cc_transaction_description VARCHAR(42),    -- col 144

  cc_mc_code            int,  -- col 145
  cc_merchant_name      VARCHAR(50),  -- col 146
  cc_merchant_city      VARCHAR(40),  -- col 147
  cc_merchant_state     VARCHAR(32),  -- col 148

  -- ex. "AU"
  cc_merchant_ctry_code VARCHAR(2),  -- col 149

  -- ex. "63.3241"
  cc_exchange_rate      NUMERIC(10, 4), -- col 152

  ----------------------------------------------------
  -- Entry Location Data

  -- "FRGN", "HOME" or "OOSD"
  foreign_or_domestic CHAR(4) NOT NULL -- col 160

  -----------------------------------------------------
  -- Cash Advance Data

  -- 仮払引当(使用)時に埋める. col 187    => reportのほうに紐付ける
  -- petty_cash_advance_id int REFERENCES petty_cash_advances (id),

  -- 仮払いでないときも埋まっている
  -- payment_method      int NOT NULL
);
EOF
    end


    def create_petty_expense_allocations
      execute <<EOF
CREATE TABLE petty_expense_allocations (
  id                     serial PRIMARY KEY,

  -- 親
  petty_expense_entry_id int NOT NULL REFERENCES petty_expense_entries (id),

  ----------------------------------------------------
  -- Journal Entry Data

  -- "Company" or "Employee"
  -- 1エントリに対して複数行、異なる値のことあり.
  payer_payment_type VARCHAR(16) NOT NULL, -- col 163

  payee_payment_type VARCHAR(16) NOT NULL, -- coi 165

  -- ex. "849/2"  col 167
  account_title_id   int NOT NULL REFERENCES account_titles (id),

  debit_or_credit    CHAR(2) NOT NULL,  -- col 168

  -- debit_or_creditとは関係なく, 正値が借方
  -- reimbursement_currency での金額
  -- col 125 (approved amount) を col 190 (allocation percentage) で按分して計算
  amount             NUMERIC(12, 4) NOT NULL, -- col 169

  -- col 191 を変換
  division_id        int NOT NULL REFERENCES divisions (id),

  vat_code           int NOT NULL,

  -- 1～100
  allocation_percentage  NUMERIC(5, 2) NOT NULL   -- col 190
);
EOF
    end


    def create_petty_offsets
      execute <<EOF
CREATE TABLE petty_offsets (
  id              serial PRIMARY KEY,

  -- 親
  expense_id      int NOT NULL REFERENCES petty_expenses (id),

  cash_advance_id int NOT NULL REFERENCES petty_cash_advances (id),  

  -- 今回消し込む額. cash advance の通貨での金額 => 一部使用がありえる
  -- 正の金額
  amount          NUMERIC(12, 4) NOT NULL,

  -- 消し込む金額の通貨
  currency_id     int NOT NULL REFERENCES currencies (id),

  created_at      TIMESTAMP NOT NULL
);
EOF
    end


    def create_petty_groups
      execute <<EOF
CREATE TABLE petty_groups (
  id         serial PRIMARY KEY,
  company_id int NOT NULL REFERENCES company_settings (id),

  -- 内部コード  
  group_code  VARCHAR(40) NOT NULL UNIQUE,
  name        VARCHAR(40) NOT NULL,
  description VARCHAR(200) NOT NULL,
  created_at       TIMESTAMP NOT NULL,
  create_user_id   int NOT NULL REFERENCES users (id),
  updated_at       TIMESTAMP,
  update_user_id   int REFERENCES users (id)
);
EOF
    end


    def up
      create_petty_groups
      create_petty_cash_advances
      create_petty_expenses
      create_petty_expense_entries
      create_petty_expense_allocations
      create_petty_offsets
      #create_petty_pays
    end

    def down
      #drop_table :petty_pays
      drop_table :petty_offsets
      drop_table :petty_expense_allocations
      drop_table :petty_expense_entries
      drop_table :petty_expenses
      drop_table :petty_cash_advances
      drop_table :petty_groups
    end
  end # class << self
end
