# -*- coding:utf-8 -*-

# 法人カード対応
class PettyExtendPaymentType < ActiveRecord::Migration
  def self.up
    # ついでに, petty_expense_allocations の直し漏れも対策。
    execute <<EOF
ALTER TABLE petty_cash_advances
   ALTER COLUMN payer_payment_type TYPE VARCHAR(32);
ALTER TABLE petty_cash_advances
   ALTER COLUMN payee_payment_type TYPE VARCHAR(32);
ALTER TABLE petty_cash_advances
   ALTER COLUMN payment_code TYPE VARCHAR(32);

ALTER TABLE petty_expense_allocations
   ALTER COLUMN payer_payment_type TYPE VARCHAR(32);
EOF
  end

  def self.down
    raise
  end
end
