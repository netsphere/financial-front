# -*- coding:utf-8 -*-

# 自社銀行口座に通貨フィールドを追加
class OurBanksCurrency < ActiveRecord::Migration
  class << self
    def up
      execute <<EOF
ALTER TABLE our_banks ADD COLUMN currency_id int REFERENCES currencies (id)
EOF

      jpy = Currency.find_by_code "JPY"
      OurBank.find(:all).each do |our_bank|
        if our_bank.bank_account_id 
          our_bank.currency_id = jpy.id
        elsif our_bank.en_bank_account_id
          our_bank.currency_id = our_bank.en_bank_account.currency_id
        else
          our_bank.currency_id = jpy.id # 現金
        end
        our_bank.save!
      end

      execute <<EOF
ALTER TABLE our_banks ALTER COLUMN currency_id SET NOT NULL;
ALTER TABLE en_bank_accounts DROP COLUMN currency_id
EOF
    end

    def down
      raise ActiveRecord::IrreversibleMigration, "cannot down"
    end
  end # << self
end
