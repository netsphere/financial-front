# -*- coding:utf-8 -*-

require "csv"
require "application_helper"

def sjis2utf8 s
  require "nkf"
  if s
    NKF.nkf('-w -S -m0', s)
  else
    nil
  end
end


# 会計部門マスタに, 次元2(初期値) を追加.
class DivisionsAddDefaultDim2 < ActiveRecord::Migration
  class << self
    # 初期値を設定する
    def import_dim2
      require "../../../../local-repos/local-repo/financial-front-extensions/sbo_migrate/convert_dim2"
      nest_map, divs, dim2s = read_matrix_file("dim2.csv")

      # 次元2
      dim2s.each do |code, name|
        dim2 = Dimension.new :active => true, :axis => 2, :dim_code => code,
                             :name => name
        dim2.save!
      end

      # 初期値
      divs.each do |line|
        div = Division.find_by_division_code(line.code.to_s)
        div.default_dim2_id = Dimension.find_by_dim_code(line.default.to_s).id
        div.save!
      end
    end


    def up
      # migration では, 互換性のため, NOT NULL は付けない.
      execute <<EOF
ALTER TABLE divisions ADD COLUMN default_dim2_id int REFERENCES dimensions (id);
ALTER TABLE divisions ADD COLUMN created_at      TIMESTAMP;
ALTER TABLE divisions ADD COLUMN create_user_id  int REFERENCES users (id);
ALTER TABLE divisions ADD COLUMN updated_at      TIMESTAMP;
ALTER TABLE divisions ADD COLUMN update_user_id  int REFERENCES users(id)
EOF

      import_dim2()

      # 少なくとも, 生きているのは埋めていること
      errs = Division.find :all, 
           :conditions => ["(end_day IS NULL AND purchase_active <> 0) AND default_dim2_id IS NULL"]
      if !errs.empty?
        print "DIVISION ERROR!!\n"
        errs.each do |r|
          print "#{r.division_code} #{r.name}\n"
        end
        raise "migration error"
      end

    end


    def down
      raise "cannot down"
    end
  end # class << self
end
