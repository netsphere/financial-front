# -*- mode:ruby; coding:utf-8 -*-

# 消費税・付加価値税マスタ
# ハードコードからテーブルに移す
class CreateVats < ActiveRecord::Migration
  class << self
    def up
      execute <<EOF
CREATE TABLE vats (
  id               serial PRIMARY KEY,
  country          CHAR(2) NOT NULL,
  tax_side         int NOT NULL,
  tax_group        int NOT NULL,
  tax_code         VARCHAR(6) NOT NULL,
  effective_day    DATE NOT NULL,
  name             VARCHAR(40) NOT NULL,
  tax_rate         NUMERIC(4, 2) NOT NULL,
  active           boolean NOT NULL,
  account_title_id int NOT NULL REFERENCES account_titles (id),
  created_at       TIMESTAMP NOT NULL,
  create_user_id   int REFERENCES users (id),
  updated_at       TIMESTAMP,
  update_user_id   int REFERENCES users (id),
  UNIQUE (country, tax_side, tax_code)
);
EOF
      day = Date.new 2008, 1, 1 # dummy
      ac1 = AccountTitle.find_by_account_code(173)  # TODO: fixme
      raise if !ac1
      ac2 = AccountTitle.find_by_account_code(337)
      raise if !ac2

      # いくつか登録しておく
      [{:country => "JP", 
                    :tax_side => 2, 
                    :tax_group => 10,
                    :tax_code => "10",
                    :effective_day => day, 
                    :name => "JP課税仕入5%(課税売上対応)",
                    :tax_rate => 5.0, 
                    :active => true, 
                    :account_title_id => ac1.id},
       {:country => "JP", 
                    :tax_side => 2, 
                    :tax_group => 30,
                    :tax_code => "30",
                    :effective_day => day, 
                    :name => "JP非課税仕入",
                    :tax_rate => 0.0, 
                    :active => true, 
                    :account_title_id => ac1.id},
       {:country => "JP", 
                    :tax_side => 1, 
                    :tax_group => 10,
                    :tax_code => "10",
                    :effective_day => day, 
                    :name => "JP課税売上5%",
                    :tax_rate => 5.0, 
                    :active => true, 
                    :account_title_id => ac2.id},
       {:country => "JP", 
                    :tax_side => 1, 
                    :tax_group => 30,
                    :tax_code => "30",
                    :effective_day => day, 
                    :name => "JP非課税売上",
                    :tax_rate => 0.0, 
                    :active => true, 
                    :account_title_id => ac2.id},
       {:country => "JP", 
                    :tax_side => 1, 
                    :tax_group => 60,
                    :tax_code => "60",
                    :effective_day => day, 
                    :name => "JP売上/輸出免税",
                    :tax_rate => 0.0, 
                    :active => true, 
                    :account_title_id => ac2.id}
      ].each do |params|
        vat = Vat.new
        # assign_attributes() は v3.2以降
        params.each do |k, v|
          vat.send "#{k}=", v
        end
        vat.save!
      end
    end


    def down
      drop_table :vats
    end
  end # class << self
end
