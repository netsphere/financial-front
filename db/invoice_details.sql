
-- 請求書の明細 => 仕訳と一致
CREATE TABLE invoice_details (
  id               serial PRIMARY KEY,
  invoice_id       int NOT NULL REFERENCES invoices(id),

  -- 行番号. 1始まり
  lineno           int NOT NULL,

  account_title_id int NOT NULL REFERENCES account_titles(id),
  division_id      int NOT NULL REFERENCES divisions(id),

  -- 日本以外では null.
  vat_id           int REFERENCES vats (id),

  -- 互換性のため, 過去のデータはナル値のまま.
  dim2_id          int REFERENCES dimensions (id),

  -- 請求書の通貨での金額. 正規化される.
  amount           int NOT NULL,

  -- 過去との互換性のため, migration では, 列を削除しない. [v2.0 -> v3.0]
  -- vat_code         int NOT NULL,   -- 10=税込5%, 30=非課税, 40=対象外

  remarks          VARCHAR(200) NOT NULL
);
