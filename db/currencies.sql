-- 通貨
CREATE TABLE currencies (
  id         serial PRIMARY KEY,
  name       VARCHAR(20) NOT NULL,

  -- ISO 4217 Currency Code
  code       CHAR(3) NOT NULL UNIQUE,  
  exponent   int NOT NULL   -- 小数点以下の桁数
);
