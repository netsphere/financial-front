-- 日本の銀行マスタ
CREATE TABLE banks (
  id        int PRIMARY KEY,      -- 銀行コード N 4  さすがにこれは変更されない。
  name      VARCHAR(15) NOT NULL, -- 銀行名
  kana_name VARCHAR(15) NOT NULL  -- 銀行名（カナ） C 15
);
