
-- 元帳区分
CREATE TABLE kubuns (
  id        serial PRIMARY KEY,
  name      VARCHAR(40) NOT NULL UNIQUE,   -- システム内部で検索するのに使う名前
  long_name VARCHAR(40) NOT NULL,
  active    int NOT NULL    -- 各部門で参照できるか
);
