
-- 仮払い
CREATE TABLE petty_cash_advances (
  id                    serial PRIMARY KEY,

  ----------------------------------------------------
  -- Batch Data

  -- 番号はリサイクルされる.
  batch_id              int,      -- col 2

  batch_date            DATE,     -- col 3

  -- BatchId のなかでの行番号. batchごとに1始まり
  sequence_number       int,      -- col 4

  ----------------------------------------------------
  -- Employee Data

  -- employee ID
  -- ex. "pro_test-user"
  user_id           int NOT NULL REFERENCES users (id),  -- col 5

  petty_group_id    int NOT NULL REFERENCES petty_groups (id), -- col 9

  ----------------------------------------------------
  -- Journal Entry Data

  -- col 163
  -- "Company" or "Employee"
  -- 仮払いの場合、"Company"固定、か。
  payer_payment_type VARCHAR(32) NOT NULL REFERENCES petty_payees (payment_type), 

  -- col 165
  -- 仮払いの場合, "Employee"固定、か。
  -- 法人カードのキャッシングの場合, 'Corporate_SMBC_VISA_CBCP'
  payee_payment_type VARCHAR(32) NOT NULL REFERENCES petty_payees (payment_type),

  -- ex. "849/2"   col 167
  account_title_id   int NOT NULL REFERENCES account_titles (id), 

  -- "DR" or "CR"
  -- 仮払いの場合, "DR"
  debit_or_credit    CHAR(2) NOT NULL,  -- col 168

  -- debit_or_creditとは関係なく, 正値が借方
  -- 通貨は reimbursement currency
  journal_amount     NUMERIC(12, 4) NOT NULL, -- col 169

  -----------------------------------------------------
  -- Cash Advance Data

  -- 要求金額
  request_amount        NUMERIC(12, 4) NOT NULL, -- col 177

  request_currency_id   int NOT NULL REFERENCES currencies (id), -- col 178

  -- col 179 skip.

  exchange_rate         NUMERIC(10, 4) NOT NULL, -- col 180

  -- col 181
  reimb_currency_id int NOT NULL REFERENCES currencies (id),

  -- col 182 skip.

  -- 経理承認日
  processing_date       DATE,  -- col 183

  -- ex. "Cash", "AMEX"
  --     'Company Billed/Company Paid' => 法人カードでキャッシング
  payment_code          VARCHAR(32) NOT NULL, -- col 184

  -- 1 = issue, 3 = system
  transaction_type      int NOT NULL,  -- col 185

  -- 本人が提出した日
  submit_date           DATE NOT NULL,  -- col 186

  -- 仮払申請のキー
  ca_key                int NOT NULL UNIQUE, -- col 187

  payment_method        int NOT NULL,  -- col 188

  -----------------------------------------------------
  -- Expense Pay Data

  --cash_account_code      VARCHAR(14) NOT NULL, -- col 254

  --liability_account_code VARCHAR(14) NOT NULL,

  -- payデータのほうに入っている。
  name   VARCHAR(40) NOT NULL, -- pay col 89

  -- 仕訳出力日時
  journal_export_at TIMESTAMP,

  -- batch_id はリサイクルされる。
  UNIQUE (batch_date, batch_id, sequence_number)
);
