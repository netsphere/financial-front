
-- 小口 (経費精算 or 仮払い) の支払い
CREATE TABLE petty_pays (
  id               serial PRIMARY KEY,

  -- 親. インポート後に作る
  cash_slip_id            int REFERENCES payment_slips (id),

  demand_batch_id         VARCHAR(36) NOT NULL,  -- header col 2
  
  -- 現金払いにした場合, ナル.
  demand_batch_close_date DATE, -- header col 3

  -- ex. "pro_test-user"
  user_id          int NOT NULL REFERENCES users (id),  -- col 2

  petty_group_id    int NOT NULL REFERENCES petty_groups (id), -- col 34

  -- "PAYM", "CHNG"
  transaction_type CHAR(4) NOT NULL,  -- col 35
  
  -- cash advance key. 仮払いの支払いのとき
  cash_advance_id  int REFERENCES petty_cash_advances (id),  -- col 36

  currency_id      int NOT NULL REFERENCES currencies (id),   -- col 43

  demand_id        int NOT NULL,  -- col 44

  -- skip col 46  入っていないことがある
  -- skip col 47  入っていないことがある

  -- "DR" or "CR". 必ず"DR"のようだが.
  liability_side   CHAR(2) NOT NULL, -- col 48

  -- 払い戻し金額
  amount      NUMERIC(12, 4) NOT NULL,   -- col 50

  -- 経費精算の支払いのとき.
  expense_id        int REFERENCES petty_expenses (id), -- col 52

  -- cash advanceとexpense report が別テーブル. referしない
  -- expense_batch_id int NOT NULL REFERENCES petty_expenses -- col 83

  created_at   TIMESTAMP NOT NULL
);
