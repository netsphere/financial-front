
-- 経費精算リポート (ヘダー)
CREATE TABLE petty_expenses (
  id              serial PRIMARY KEY,

  ----------------------------------------------------
  -- Batch Data

  -- 番号はリサイクルされる。
  batch_id        int,    -- col 2

  batch_date      DATE,   -- col 3

  -- BatchId のなかでの行番号. batchごとに1始まり
  sequence_number int,    -- col 4

  ----------------------------------------------------
  -- Employee Data

  -- employee ID
  -- ex. "pro_test-user"
  user_id         int NOT NULL REFERENCES users (id),  -- col 5

  petty_group_id    int NOT NULL REFERENCES petty_groups (id), -- col 9

  ----------------------------------------------------
  -- Report Data

  -- 実データは20桁ぐらい
  report_id           VARCHAR(32) NOT NULL UNIQUE,  -- col 19

  -- ex. "JPY"  col 22
  reimb_currency_id   int NOT NULL REFERENCES currencies (id),

  -- ex. "JAPAN"
  country_lang_name   VARCHAR(64) NOT NULL,  -- col 23

  -- 本人が提出した日
  submit_date         DATE NOT NULL,  -- col 24

  -- 経理承認日
  processing_date     DATE, -- col 26

  -- ex. "TEST_REPORT_02"
  report_name    VARCHAR(40) NOT NULL,  -- col 27

  image_required BOOLEAN NOT NULL,  -- col 28

  has_vat_entry  BOOLEAN NOT NULL,  -- col 29

  has_ta_entry   BOOLEAN NOT NULL,  -- col 30

  -- skip col 31

  -- 承認された経費の合計額
  total_approved_amount NUMERIC(12, 4) NOT NULL,  -- col 32

  ----------------
  -- 債権・債務の金額 (消し込みのため.)

  -- 社員に対する支払い -> 社員未払金
  emp_amount_total  NUMERIC(12, 4) NOT NULL,

  -- クレジットカード会社に対する支払い -> 未払金
  cc_amount_total   NUMERIC(12, 4) NOT NULL,

  -- 社員からの回収 -> 社員未収入金
  receivable_amount_total NUMERIC(12, 4) NOT NULL,


  -- 仕訳出力日時
  journal_export_at TIMESTAMP,

  -- batch_id はリサイクルされる。
  UNIQUE (batch_date, batch_id, sequence_number)
);

