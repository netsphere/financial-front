
-- 場所ごとのコード
CREATE TABLE bp_addresses (
  id           serial PRIMARY KEY,

  -- 親
  partner_id   int NOT NULL REFERENCES partners (id),

  vendor_code  VARCHAR(20) NOT NULL UNIQUE,

  name         VARCHAR(80) NOT NULL,

  created_at   TIMESTAMP NOT NULL,
  created_by   int NOT NULL REFERENCES users (id),
  updated_at   TIMESTAMP,
  updated_by   int REFERENCES users (id),
  lock_version int NOT NULL
);
