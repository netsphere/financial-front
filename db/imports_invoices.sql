
-- 輸入報告（申告・許可）と請求書の多対多
CREATE TABLE imports_invoices (
  id                    serial PRIMARY KEY,
  import_declaration_id int NOT NULL REFERENCES import_declarations (id),
  invoice_id            int NOT NULL REFERENCES invoices (id),
  create_user_id        int NOT NULL REFERENCES users (id),
  created_at            TIMESTAMP NOT NULL,
  UNIQUE (import_declaration_id, invoice_id)
);
