
-- エントリ
CREATE TABLE petty_expense_entries (
  id                  serial PRIMARY KEY,

  -- 親
  petty_expense_id    int NOT NULL REFERENCES petty_expenses(id),

  ---------------------------------------------------------------------------
  -- Report Entry Data

  -- エントリのユニークキー. 
  -- 1エントリの仕訳に対して複数部門 (allocations) が発生することに注意.
  report_entry_key    int NOT NULL UNIQUE,  -- col 61

  -- "REG" or "CHD"
  transaction_type    CHAR(3) NOT NULL,  -- col 62

  -- 経費タイプ名
  -- ex. "Advertising"
  expense_type_name   VARCHAR(64) NOT NULL, -- col 63

  -- 取引日(本人の支出した日)
  transaction_date    DATE NOT NULL,  -- col 64

  -- 変な通貨のことがある. テキストで保存
  spend_currency_code CHAR(3) NOT NULL, -- col 65

  -- from spend currency to reimbursement currency
  exchange_rate       NUMERIC(10, 4) NOT NULL,  -- col 66

  -- "M" = multiply
  exchange_rate_direction CHAR(1) NOT NULL,   -- col 67

  is_personal             BOOLEAN NOT NULL, -- col 68

  description             VARCHAR(64) NOT NULL, -- col 69

  -- 実際のデータでは、いつも空.
  -- TODO: 列を削除する
  vendor_name             VARCHAR(64) NOT NULL, -- col 70

  -- 取引先名. ex) "xxx商店"
  vendor_description      VARCHAR(64) NOT NULL, -- col 71

  receipt_received        BOOLEAN NOT NULL,  -- col 72

  -- "T", "R" or "N"
  receipt_type            CHAR(1) NOT NULL, -- col 73

  -- 本人を含む同席人数
  employee_attendee_count int NOT NULL,  -- col 74

  spouse_attendee_count   int NOT NULL,  -- col 75

  business_attendee_count int NOT NULL,  -- col 76

  -- in spend currency
  spend_amount            NUMERIC(12, 4) NOT NULL, -- col 123

  -- skip 124

  -- 承認された金額 in reimbursement currency
  approved_amount         NUMERIC(12, 4) NOT NULL, -- col 125

  -- ex. "CASH",  カードフィード => "IBCP"
  payment_code            VARCHAR(4) NOT NULL,  -- col 126

  --payment_code_name       VARCHAR(40) NOT NULL,  -- col 127

  -- 本人に支払う（振込・現金）の場合 => 1. 現金渡しずみでも1が来る
  -- カードフィードの場合 blank
  reimb_type              int NOT NULL,  -- col 128

  ---------------------------------------------------
  -- Credit Card Data

  cc_billing_date      DATE,    -- col 129

  -- 頭6桁 + 下4桁がunmasked
  cc_masked_cc_number  VARCHAR(20),  -- col 130

  cc_name_on_card      VARCHAR(40),  -- col 131

  -- フィードIDか?
  cc_jr_key               VARCHAR(13),   -- col 132

  cc_ref_no               VARCHAR(64) UNIQUE,   -- col 133

  cc_cct_key              VARCHAR(64) UNIQUE,   -- col 134

  -- "CAV" => Cash Advance
  -- "RPE" => 購入
  cc_cct_type             CHAR(3),  -- col 135

  cc_transaction_id       VARCHAR(32) UNIQUE,  -- col 136

  -- in spend amount
  cc_transaction_amount   NUMERIC(12, 4),   -- col 137

  -- amount of tax on the transaction amount
  cc_tax_amount           NUMERIC(12, 4),   -- col 138

  -- spend currency  => テキストで保存
  cc_currency_code        CHAR(3),     -- col 139

  cc_transaction_description VARCHAR(42),    -- col 144

  cc_mc_code            int,  -- col 145
  cc_merchant_name      VARCHAR(50),  -- col 146
  cc_merchant_city      VARCHAR(40),  -- col 147
  cc_merchant_state     VARCHAR(32),  -- col 148

  -- ex. "AU"
  cc_merchant_ctry_code VARCHAR(2),  -- col 149

  -- ex. "63.3241"
  cc_exchange_rate      NUMERIC(10, 4), -- col 152

  ----------------------------------------------------
  -- Entry Location Data

  -- "FRGN", "HOME" or "OOSD"
  foreign_or_domestic CHAR(4) NOT NULL -- col 160

  -----------------------------------------------------
  -- Cash Advance Data

  -- 仮払引当(使用)時に埋める. col 187    => reportのほうに紐付ける
  -- petty_cash_advance_id int REFERENCES petty_cash_advances (id),

  -- 仮払いでないときも埋まっている
  -- payment_method      int NOT NULL
);
