-- 顧客あて請求書に載せる自社銀行口座
CREATE TABLE si_banks (
  id               serial PRIMARY KEY,
  sales_invoice_id int NOT NULL REFERENCES sales_invoices (id),
  our_bank_id      int NOT NULL REFERENCES our_banks (id),
  UNIQUE (sales_invoice_id, our_bank_id)
);

