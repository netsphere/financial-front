
-- 小口精算の仕訳

-- 例) CCの私費
--       Payer    Payee     Debit/Credit  Amount
--       -------  --------- ------------  ------
--    1. Company  VISA_CBCP DR             799.0
--    2. Employee Company   CR            -799.0

--    現金値引き
--       Payer    Payee     Debit/Credit  Amount
--       -------  --------- ------------  ------
--    1. Company  Employee  DR            30000.0
--    2. Employee Company   CR              -14.0

-- 仕訳を生成するには, 'CR' のときは, 次のいずれか (結果は同じ);
--    a) Payer = 借方, Payee = 貸方として, 金額の符号反転
--    b) 貸借を逆にして, 金額はマイナスのまま.
CREATE TABLE petty_expense_allocations (
  id                     serial PRIMARY KEY,

  -- 親
  petty_expense_entry_id int NOT NULL REFERENCES petty_expense_entries (id),

  ----------------------------------------------------
  -- Journal Entry Data

  -- col 163   
  -- "Company" or "Employee"
  -- 1エントリに対して複数行、異なる値のことあり.
  payer_payment_type VARCHAR(32) NOT NULL REFERENCES petty_payees (payment_type), 

  -- col 165
  -- コーポレートカードの場合, "Corporate_SMBC_VISA_CBCP"
  payee_payment_type VARCHAR(32) NOT NULL REFERENCES petty_payees (payment_type), 

  -- ex. "849/2"  col 167
  account_title_id   int NOT NULL REFERENCES account_titles (id),

  -- 'DR' or 'CR'
  -- 金額がマイナスの時は 'CR'
  -- このフィールドは廃止::
  --     インポート時に, 'CR' だった場合は, payer (借方) と payee (貸方) を
  --     入れ替える
  --debit_or_credit    CHAR(2) NOT NULL,  -- col 168

  -- debit_or_creditとは関係なく, 正値が借方
  -- reimbursement_currency での金額
  -- col 125 (approved amount) を col 190 (allocation percentage) で按分して計算
  amount             NUMERIC(12, 4) NOT NULL, -- col 169

  -- col 191 を変換
  division_id        int NOT NULL REFERENCES divisions (id),

  vat_code           int NOT NULL,

  -- 1～100
  allocation_percentage  NUMERIC(5, 2) NOT NULL   -- col 190
);

