-- 請求書の更新日時、承認日時を記録
CREATE TABLE invoice_logs (
  id         serial PRIMARY KEY,
  invoice_id int NOT NULL REFERENCES invoices(id),
  kind       int NOT NULL,            -- InvoiceLog::MODIFIED, InvoiceLog::SIGNED_OFF
  remarks    VARCHAR(200) NOT NULL,
  done_by    int NOT NULL REFERENCES users(id),
  done_at    TIMESTAMP NOT NULL
);
