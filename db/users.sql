-- 社員
CREATE TABLE users (
  id            serial PRIMARY KEY,

  -- 社員コード. 変わることがある
  worker_code   VARCHAR(48) NOT NULL UNIQUE,

  name          VARCHAR(40) NOT NULL,
  kana_name     VARCHAR(40) NOT NULL,
  active        int NOT NULL,  -- 1でアクティブ

  -- ロール (役割)
  keiri_flag     boolean NOT NULL,  -- 経理承認ができる
  marketing_flag boolean NOT NULL,  -- 売上情報を閲覧できる
  apar_flag      boolean NOT NULL,  -- (購買)請求書・販売請求書
  hrd_flag       boolean NOT NULL,  -- 人事

  -- ログインする人のみ設定
  login            VARCHAR(40) UNIQUE,
  email            VARCHAR(40) UNIQUE,
  crypted_password VARCHAR(40),
  salt             VARCHAR(40),
  remember_token            VARCHAR(40),
  remember_token_expires_at TIMESTAMP,

  created_at     TIMESTAMP NOT NULL,
  -- インポートで流し込むため, ナル可
  create_user_id int REFERENCES users (id),
  updated_at     TIMESTAMP,
  update_user_id int REFERENCES users (id)
);
