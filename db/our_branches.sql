
-- 自社店舗
CREATE TABLE our_branches (
  id          serial PRIMARY KEY,
  branch_code int NOT NULL UNIQUE,  -- 店舗コード
  name        VARCHAR(40) NOT NULL
);
