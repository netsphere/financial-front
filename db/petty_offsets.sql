
-- 仮払い伝票と経費精算伝票との紐付け
CREATE TABLE petty_offsets (
  id              serial PRIMARY KEY,

  -- 親 = リポートに紐付け
  expense_id      int NOT NULL REFERENCES petty_expenses (id),

  -- 一つの経費リポートに, 複数紐付け可能
  cash_advance_id int NOT NULL REFERENCES petty_cash_advances (id),  

  -- 今回消し込む額. cash advance の通貨での金額 => 一部使用がありえる
  -- 正の金額
  amount          NUMERIC(12, 4) NOT NULL,

  -- 消し込む金額の通貨
  currency_id     int NOT NULL REFERENCES currencies (id),

  created_at      TIMESTAMP NOT NULL
);
