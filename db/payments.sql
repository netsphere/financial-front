
-- 債務(取引先に限る.)の支払い (1行)
CREATE TABLE payments (
  id               serial PRIMARY KEY,
  -- 親
  payment_slip_id  int NOT NULL REFERENCES payment_slips (id),

  invoice_id       int NOT NULL REFERENCES invoices (id),

  -- => petty_pays table へ移動.
  --petty_expense_id int REFERENCES petty_expenses (id),
  --petty_cash_advance_id int REFERENCES petty_cash_advances (id),

  -- 買掛金科目。買掛金/未払金の区別は、行の生成時にしかできない
  account_title_id int NOT NULL REFERENCES account_titles(id),

  -- 消し込む債務の金額.
  -- 正規化された金額 [通貨=invoiceの]
  amount           int NOT NULL,

  -- 消し込む金額の通貨 (invoiceのと同じ)
  currency_id      int NOT NULL REFERENCES currencies (id),

  created_at       TIMESTAMP NOT NULL
);
