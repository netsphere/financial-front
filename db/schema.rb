# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2024_08_15_092407) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "account_title_trees", id: :serial, force: :cascade do |t|
    t.integer "parent_id", null: false
    t.integer "child_id", null: false

    t.unique_constraint ["parent_id", "child_id"], name: "account_title_trees_parent_id_child_id_key"
  end

  create_table "account_titles", id: :serial, force: :cascade do |t|
    t.boolean "aggregate_flag", null: false
    t.integer "account_code", null: false
    t.integer "suppl_code", null: false
    t.string "main_name", limit: 40, null: false
    t.string "name", limit: 40, null: false
    t.string "kana_name", limit: 40, null: false
    t.text "description", null: false
    t.date "end_day"
    t.integer "kind", null: false
    t.boolean "enable_si_invoice", null: false
    t.integer "receivable_id"
    t.boolean "enable_invoice", null: false
    t.integer "payable_id"
    t.integer "sign", null: false
    t.boolean "req_partner", null: false
    t.integer "tax_side", null: false
    t.integer "default_tax_type", null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "create_user_id", null: false
    t.datetime "updated_at", precision: nil
    t.integer "update_user_id"

    t.unique_constraint ["account_code", "suppl_code"], name: "account_titles_account_code_suppl_code_key"
  end

  create_table "action_groups", id: :serial, force: :cascade do |t|
    t.string "name", limit: 100, null: false
  end

  create_table "attach_file_refs", id: :serial, force: :cascade do |t|
    t.integer "attach_file_id", null: false
    t.integer "partner_id"
    t.integer "invoice_id"
    t.datetime "created_at", precision: nil

    t.unique_constraint ["attach_file_id", "partner_id", "invoice_id"], name: "attach_file_refs_attach_file_id_partner_id_invoice_id_key"
  end

  create_table "attach_files", id: :serial, force: :cascade do |t|
    t.binary "data", null: false
    t.string "original_filename", limit: 260, null: false
    t.string "content_type", limit: 100, null: false
    t.integer "attach_kind_id", null: false
    t.string "remarks", limit: 200, null: false
    t.integer "division_id", null: false
    t.integer "create_user_id", null: false
    t.datetime "created_at", precision: nil
  end

  create_table "attach_kinds", id: :serial, force: :cascade do |t|
    t.string "name", limit: 60, null: false
    t.integer "sort", null: false
    t.integer "enable_invoice", null: false
  end

  create_table "bank_accounts", id: :serial, force: :cascade do |t|
    t.integer "partner_id", null: false
    t.integer "zaimu_ac", null: false
    t.boolean "active", null: false
    t.integer "bank_branch_id", null: false
    t.integer "ac_type", null: false
    t.integer "ac_number", null: false
    t.string "kana_name", limit: 30, null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "create_user_id"
    t.datetime "updated_at", precision: nil
    t.integer "update_user_id"
    t.integer "lock_version", null: false

    t.unique_constraint ["bank_branch_id", "ac_type", "ac_number"], name: "bank_accounts_bank_branch_id_ac_type_ac_number_key"
    t.unique_constraint ["zaimu_ac"], name: "bank_accounts_zaimu_ac_key"
  end

  create_table "bank_branches", id: :serial, force: :cascade do |t|
    t.integer "bank_id", null: false
    t.integer "branch_code", null: false
    t.string "name", limit: 15, null: false
    t.string "kana_name", limit: 15, null: false

    t.unique_constraint ["bank_id", "branch_code"], name: "bank_branches_bank_id_branch_code_key"
  end

  create_table "banks", id: :integer, default: nil, force: :cascade do |t|
    t.string "name", limit: 15, null: false
    t.string "kana_name", limit: 15, null: false
  end

  create_table "bp_addresses", id: :serial, force: :cascade do |t|
    t.integer "partner_id", null: false
    t.string "vendor_code", limit: 20, null: false
    t.string "name", limit: 80, null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "created_by", null: false
    t.datetime "updated_at", precision: nil
    t.integer "updated_by"
    t.integer "lock_version", null: false

    t.unique_constraint ["vendor_code"], name: "bp_addresses_vendor_code_key"
  end

  create_table "company_settings", id: :serial, force: :cascade do |t|
    t.boolean "active", null: false
    t.string "tax_reg_number", limit: 14
    t.string "name", limit: 60, null: false
    t.string "name_en", limit: 60, null: false
    t.date "fixed_day", null: false
    t.date "fixed_gl_day", null: false
    t.string "country", limit: 2, null: false
    t.integer "currency_id", null: false
    t.integer "si_counter", null: false
    t.text "si_footer", null: false
    t.binary "logo_image"
  end

  create_table "company_settings2", id: :serial, force: :cascade do |t|
    t.integer "company_id", null: false
    t.integer "bank_division_id", null: false
    t.integer "fallback_division_id", null: false
  end

  create_table "control_divisions", id: :serial, force: :cascade do |t|
    t.string "name", limit: 40, null: false
    t.integer "parent_id"
    t.integer "lft", null: false
    t.integer "rgt", null: false
  end

  create_table "controllers_divisions", id: :serial, force: :cascade do |t|
    t.integer "control_division_id", null: false
    t.integer "division_id", null: false

    t.unique_constraint ["control_division_id", "division_id"], name: "controllers_divisions_control_division_id_division_id_key"
  end

  create_table "credit_inquiries", id: :serial, force: :cascade do |t|
    t.integer "partner_id", null: false
    t.date "report_date", null: false
    t.integer "score"
    t.text "remarks", null: false
    t.integer "create_user_id"
    t.datetime "created_at", precision: nil, null: false
  end

  create_table "currencies", id: :serial, force: :cascade do |t|
    t.string "name", limit: 20, null: false
    t.string "code", limit: 3, null: false
    t.integer "exponent", null: false

    t.unique_constraint ["code"], name: "currencies_code_key"
  end

  create_table "currency_rates", id: :serial, force: :cascade do |t|
    t.date "date", null: false
    t.integer "currency_id", null: false
    t.decimal "rate", precision: 8, scale: 4, null: false

    t.unique_constraint ["date", "currency_id"], name: "currency_rates_date_currency_id_key"
  end

  create_table "default_dimensions", id: :serial, force: :cascade do |t|
    t.integer "division_id", null: false
    t.integer "axis", null: false
    t.integer "dimension_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "create_user_id", null: false

    t.unique_constraint ["division_id", "axis"], name: "default_dimensions_division_id_axis_key"
  end

  create_table "dimensions", id: :serial, force: :cascade do |t|
    t.boolean "active", null: false
    t.integer "axis", null: false
    t.string "dim_code", limit: 10, null: false
    t.string "name", limit: 40, null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "create_user_id"
    t.datetime "updated_at", precision: nil
    t.integer "update_user_id"

    t.unique_constraint ["dim_code"], name: "dimensions_dim_code_key"
  end

  create_table "divisions", id: :serial, force: :cascade do |t|
    t.string "division_code", limit: 20, null: false
    t.integer "company_id", null: false
    t.string "name", limit: 40, null: false
    t.string "kana_name", limit: 40, null: false
    t.integer "default_dim2_id"
    t.date "end_day"
    t.integer "purchase_active", null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "create_user_id", null: false
    t.datetime "updated_at", precision: nil
    t.integer "update_user_id"

    t.unique_constraint ["division_code"], name: "divisions_division_code_key"
  end

  create_table "en_bank_accounts", id: :serial, force: :cascade do |t|
    t.integer "partner_id"
    t.string "swift_code", limit: 11, null: false
    t.string "bank_name", limit: 40, null: false
    t.string "bank_addr1", limit: 50, null: false
    t.string "bank_addr2", limit: 80, null: false
    t.string "bank_country", limit: 2, null: false
    t.string "account_name", limit: 40, null: false
    t.string "account_addr1", limit: 80, null: false
    t.string "account_addr2", limit: 80, null: false
    t.string "account_no", limit: 34
    t.string "iban_code", limit: 34
  end

  create_table "general_ledger", id: :serial, force: :cascade do |t|
    t.date "date", null: false
    t.integer "slip_number"
    t.integer "kubun_id", null: false
    t.integer "division_id"
    t.integer "dim2_id"
    t.integer "account_title_id", null: false
    t.integer "partner_id"
    t.integer "work_action_id"
    t.bigint "amount", null: false
    t.string "remarks", limit: 250, null: false
    t.integer "created_by"
    t.datetime "created_at", precision: nil
    t.index ["partner_id"], name: "fki_general_ledger_partner_id_fkey"
    t.index ["work_action_id"], name: "fki_general_ledger_work_action_id_fkey"
  end

  create_table "global_settings", id: :integer, default: nil, force: :cascade do |t|
    t.integer "partner_counter", null: false
    t.date "currency_limit_day", null: false
    t.integer "bank_charge_account_title_id", null: false
    t.integer "currency_gain_account_title_id", null: false
    t.integer "vat_payment_account_title_id", null: false
    t.integer "default_division_tree_root_id", null: false
  end

  create_table "import_declarations", id: :serial, force: :cascade do |t|
    t.integer "company_id", null: false
    t.string "declaration_number", limit: 11, null: false
    t.string "exporter_name", limit: 100, null: false
    t.string "bl_number", limit: 18, null: false
    t.decimal "invoice_price", precision: 10, null: false
    t.integer "invoice_currency_id", null: false
    t.integer "invoice_incoterm_id", null: false
    t.integer "customs_tariff", null: false
    t.integer "consumption_tax", null: false
    t.integer "vat_local", null: false
    t.text "description", null: false
    t.date "permit_date", null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "create_user_id", null: false
    t.datetime "updated_at", precision: nil

    t.unique_constraint ["declaration_number"], name: "import_declarations_declaration_number_key"
  end

  create_table "imports_invoices", id: :serial, force: :cascade do |t|
    t.integer "import_declaration_id", null: false
    t.integer "invoice_id", null: false
    t.integer "create_user_id", null: false
    t.datetime "created_at", precision: nil, null: false

    t.unique_constraint ["import_declaration_id", "invoice_id"], name: "imports_invoices_import_declaration_id_invoice_id_key"
  end

  create_table "invoice_details", id: :serial, force: :cascade do |t|
    t.integer "invoice_id", null: false
    t.integer "lineno", null: false
    t.integer "account_title_id", null: false
    t.integer "division_id", null: false
    t.integer "vat_id"
    t.integer "dim2_id"
    t.integer "amount", null: false
    t.string "remarks", limit: 200, null: false
  end

  create_table "invoice_logs", id: :serial, force: :cascade do |t|
    t.integer "invoice_id", null: false
    t.integer "kind", null: false
    t.string "remarks", limit: 200, null: false
    t.integer "done_by", null: false
    t.datetime "done_at", precision: nil, null: false
  end

  create_table "invoices", id: :serial, force: :cascade do |t|
    t.integer "company_setting_id", null: false
    t.string "origin_no", limit: 20
    t.string "approval", limit: 20
    t.integer "partner_id", null: false
    t.string "invoice_no", limit: 40
    t.integer "bank_account_id"
    t.integer "en_bank_account_id"
    t.date "buy_day", null: false
    t.date "due_day", null: false
    t.integer "currency_id", null: false
    t.integer "amount_total", null: false
    t.integer "division_id", null: false
    t.date "journal_day"
    t.datetime "journal_export_at", precision: nil
    t.string "je_number", limit: 8
    t.integer "state", null: false
    t.integer "payment_state", null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "created_by"
    t.datetime "updated_at", precision: nil
    t.integer "lock_version", null: false

    t.unique_constraint ["je_number"], name: "invoices_je_number_key"
    t.unique_constraint ["origin_no"], name: "invoices_origin_no_key"
    t.unique_constraint ["partner_id", "invoice_no"], name: "invoices_partner_id_invoice_no_key"
  end

  create_table "kubuns", id: :serial, force: :cascade do |t|
    t.string "name", limit: 40, null: false
    t.string "long_name", limit: 40, null: false
    t.integer "active", null: false

    t.unique_constraint ["name"], name: "kubuns_name_key"
  end

  create_table "mat_parts", id: :serial, force: :cascade do |t|
    t.string "part_no", limit: 20, null: false
    t.string "description", limit: 36, null: false
    t.string "mat_type", limit: 2, null: false
    t.string "mat_class", limit: 1, null: false
    t.boolean "fixed_asset_flag", null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "create_user_id"
    t.datetime "updated_at", precision: nil
    t.integer "update_user_id"
    t.integer "lock_version", null: false

    t.unique_constraint ["part_no"], name: "mat_parts_part_no_key"
  end

  create_table "mat_receiving_charges", id: :serial, force: :cascade do |t|
    t.integer "mat_receiving_id", null: false
    t.string "charge_code", limit: 6, null: false
    t.string "charge_code_desc", limit: 36, null: false
    t.decimal "total_amount", precision: 12, scale: 4, null: false
    t.string "tax_code", limit: 6, null: false
    t.decimal "tax_amount", precision: 12, scale: 4, null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil
  end

  create_table "mat_receivings", id: :serial, force: :cascade do |t|
    t.integer "invoice_id", null: false
    t.string "vendor_code", limit: 20, null: false
    t.string "booking_type", limit: 1, null: false
    t.date "delivery_date", null: false
    t.string "cost_center", limit: 8, null: false
    t.string "order_type", limit: 2, null: false
    t.string "order_no", limit: 12, null: false
    t.integer "order_item_no", null: false
    t.string "part_no", limit: 20, null: false
    t.string "part_mat_class", limit: 1, null: false
    t.boolean "fixed_asset_flag", null: false
    t.string "part_serial", limit: 20
    t.integer "receiving_no", null: false
    t.string "currency_code", limit: 3, null: false
    t.decimal "total_amount", precision: 12, scale: 4, null: false
    t.integer "quantity", null: false
    t.string "tax_code", limit: 6, null: false
    t.decimal "tax_amount", precision: 12, scale: 4, null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil
  end

  create_table "our_banks", id: :serial, force: :cascade do |t|
    t.integer "company_setting_id", null: false
    t.string "name", limit: 40, null: false
    t.integer "bank_account_id"
    t.integer "en_bank_account_id"
    t.integer "currency_id", null: false
    t.string "applicant_code", limit: 10
    t.string "remarks", limit: 200, null: false
    t.integer "account_title_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "create_user_id", null: false
    t.datetime "updated_at", precision: nil
    t.integer "update_user_id"

    t.unique_constraint ["applicant_code"], name: "our_banks_applicant_code_key"
    t.unique_constraint ["bank_account_id"], name: "our_banks_bank_account_id_key"
    t.unique_constraint ["en_bank_account_id"], name: "our_banks_en_bank_account_id_key"
  end

  create_table "our_branches", id: :serial, force: :cascade do |t|
    t.integer "branch_code", null: false
    t.string "name", limit: 40, null: false

    t.unique_constraint ["branch_code"], name: "our_branches_branch_code_key"
  end

  create_table "our_contacts", id: :serial, force: :cascade do |t|
    t.integer "company_setting_id", null: false
    t.string "lang", limit: 2, null: false
    t.string "zip", limit: 10, null: false
    t.string "addr1", limit: 100, null: false
    t.string "addr2", limit: 100, null: false
    t.string "addr3", limit: 100, null: false
    t.string "name", limit: 100, null: false
    t.string "tel", limit: 20, null: false
  end

  create_table "partners", id: :serial, force: :cascade do |t|
    t.integer "zaimu_id", null: false
    t.integer "tdb_code"
    t.string "name", limit: 80, null: false
    t.string "kana_name", limit: 60, null: false
    t.string "hq_zip", limit: 8, null: false
    t.string "hq_addr", limit: 100, null: false
    t.string "industry", limit: 60, null: false
    t.text "remarks", null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "created_by"
    t.datetime "updated_at", precision: nil
    t.integer "updated_by"
    t.integer "lock_version", null: false

    t.unique_constraint ["tdb_code"], name: "partners_tdb_code_key"
    t.unique_constraint ["zaimu_id"], name: "partners_zaimu_id_key"
  end

  create_table "payment_banks", id: :serial, force: :cascade do |t|
    t.integer "payment_slip_id", null: false
    t.integer "our_bank_id", null: false
    t.decimal "cash_amount", precision: 12, null: false
    t.decimal "inv_amount", precision: 12, null: false
    t.datetime "created_at", precision: nil, null: false
  end

  create_table "payment_slips", id: :serial, force: :cascade do |t|
    t.integer "slip_type", null: false
    t.integer "company_id", null: false
    t.date "date", null: false
    t.datetime "journal_export_at", precision: nil
    t.integer "state", null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "create_user_id", null: false
    t.datetime "updated_at", precision: nil
    t.integer "update_user_id"
    t.integer "lock_version", null: false
  end

  create_table "payments", id: :serial, force: :cascade do |t|
    t.integer "payment_slip_id", null: false
    t.integer "invoice_id", null: false
    t.integer "account_title_id", null: false
    t.integer "amount", null: false
    t.integer "currency_id", null: false
    t.datetime "created_at", precision: nil, null: false
  end

  create_table "petty_cash_advances", id: :serial, force: :cascade do |t|
    t.integer "batch_id"
    t.date "batch_date"
    t.integer "sequence_number"
    t.integer "user_id", null: false
    t.integer "petty_group_id", null: false
    t.string "payer_payment_type", limit: 32, null: false
    t.string "payee_payment_type", limit: 32, null: false
    t.integer "account_title_id", null: false
    t.string "debit_or_credit", limit: 2, null: false
    t.decimal "journal_amount", precision: 12, scale: 4, null: false
    t.decimal "request_amount", precision: 12, scale: 4, null: false
    t.integer "request_currency_id", null: false
    t.decimal "exchange_rate", precision: 10, scale: 4, null: false
    t.integer "reimb_currency_id", null: false
    t.date "processing_date"
    t.string "payment_code", limit: 32, null: false
    t.integer "transaction_type", null: false
    t.date "submit_date", null: false
    t.integer "ca_key", null: false
    t.integer "payment_method", null: false
    t.string "name", limit: 40, null: false
    t.datetime "journal_export_at", precision: nil

    t.unique_constraint ["batch_date", "batch_id", "sequence_number"], name: "petty_cash_advances_batch_date_batch_id_sequence_number_key"
    t.unique_constraint ["ca_key"], name: "petty_cash_advances_ca_key_key"
  end

  create_table "petty_expense_allocations", id: :serial, force: :cascade do |t|
    t.integer "petty_expense_entry_id", null: false
    t.string "payer_payment_type", limit: 32, null: false
    t.string "payee_payment_type", limit: 32, null: false
    t.integer "account_title_id", null: false
    t.decimal "amount", precision: 12, scale: 4, null: false
    t.integer "division_id", null: false
    t.integer "vat_code", null: false
    t.decimal "allocation_percentage", precision: 5, scale: 2, null: false
  end

  create_table "petty_expense_entries", id: :serial, force: :cascade do |t|
    t.integer "petty_expense_id", null: false
    t.integer "report_entry_key", null: false
    t.string "transaction_type", limit: 3, null: false
    t.string "expense_type_name", limit: 64, null: false
    t.date "transaction_date", null: false
    t.string "spend_currency_code", limit: 3, null: false
    t.decimal "exchange_rate", precision: 10, scale: 4, null: false
    t.string "exchange_rate_direction", limit: 1, null: false
    t.boolean "is_personal", null: false
    t.string "description", limit: 64, null: false
    t.string "vendor_name", limit: 64, null: false
    t.string "vendor_description", limit: 64, null: false
    t.boolean "receipt_received", null: false
    t.string "receipt_type", limit: 1, null: false
    t.integer "employee_attendee_count", null: false
    t.integer "spouse_attendee_count", null: false
    t.integer "business_attendee_count", null: false
    t.decimal "spend_amount", precision: 12, scale: 4, null: false
    t.decimal "approved_amount", precision: 12, scale: 4, null: false
    t.string "payment_code", limit: 4, null: false
    t.integer "reimb_type", null: false
    t.date "cc_billing_date"
    t.string "cc_masked_cc_number", limit: 20
    t.string "cc_name_on_card", limit: 40
    t.string "cc_jr_key", limit: 13
    t.string "cc_ref_no", limit: 64
    t.string "cc_cct_key", limit: 64
    t.string "cc_cct_type", limit: 3
    t.string "cc_transaction_id", limit: 32
    t.decimal "cc_transaction_amount", precision: 12, scale: 4
    t.decimal "cc_tax_amount", precision: 12, scale: 4
    t.string "cc_currency_code", limit: 3
    t.string "cc_transaction_description", limit: 42
    t.integer "cc_mc_code"
    t.string "cc_merchant_name", limit: 50
    t.string "cc_merchant_city", limit: 40
    t.string "cc_merchant_state", limit: 32
    t.string "cc_merchant_ctry_code", limit: 2
    t.decimal "cc_exchange_rate", precision: 10, scale: 4
    t.string "foreign_or_domestic", limit: 4, null: false

    t.unique_constraint ["cc_cct_key"], name: "petty_expense_entries_cc_cct_key_key"
    t.unique_constraint ["cc_ref_no"], name: "petty_expense_entries_cc_ref_no_key"
    t.unique_constraint ["cc_transaction_id"], name: "petty_expense_entries_cc_transaction_id_key"
    t.unique_constraint ["report_entry_key"], name: "petty_expense_entries_report_entry_key_key"
  end

  create_table "petty_expenses", id: :serial, force: :cascade do |t|
    t.integer "batch_id"
    t.date "batch_date"
    t.integer "sequence_number"
    t.integer "user_id", null: false
    t.integer "petty_group_id", null: false
    t.string "report_id", limit: 32, null: false
    t.integer "reimb_currency_id", null: false
    t.string "country_lang_name", limit: 64, null: false
    t.date "submit_date", null: false
    t.date "processing_date"
    t.string "report_name", limit: 40, null: false
    t.boolean "image_required", null: false
    t.boolean "has_vat_entry", null: false
    t.boolean "has_ta_entry", null: false
    t.decimal "total_approved_amount", precision: 12, scale: 4, null: false
    t.decimal "emp_amount_total", precision: 12, scale: 4, null: false
    t.decimal "cc_amount_total", precision: 12, scale: 4, null: false
    t.decimal "receivable_amount_total", precision: 12, scale: 4, null: false
    t.datetime "journal_export_at", precision: nil

    t.unique_constraint ["batch_date", "batch_id", "sequence_number"], name: "petty_expenses_batch_date_batch_id_sequence_number_key"
    t.unique_constraint ["report_id"], name: "petty_expenses_report_id_key"
  end

  create_table "petty_groups", id: :serial, force: :cascade do |t|
    t.integer "company_id", null: false
    t.string "group_code", limit: 40, null: false
    t.string "name", limit: 40, null: false
    t.string "description", limit: 200, null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "create_user_id", null: false
    t.datetime "updated_at", precision: nil
    t.integer "update_user_id"

    t.unique_constraint ["group_code"], name: "petty_groups_group_code_key"
  end

  create_table "petty_offsets", id: :serial, force: :cascade do |t|
    t.integer "expense_id", null: false
    t.integer "cash_advance_id", null: false
    t.decimal "amount", precision: 12, scale: 4, null: false
    t.integer "currency_id", null: false
    t.datetime "created_at", precision: nil, null: false
  end

  create_table "petty_payees", id: :serial, force: :cascade do |t|
    t.string "payment_type", limit: 32, null: false
    t.string "name", limit: 80, null: false
    t.text "remarks", null: false
    t.integer "ar_title_id"
    t.integer "ap_title_id"
    t.integer "bp_code"
    t.datetime "created_at", precision: nil, null: false
    t.integer "create_user_id", null: false
    t.datetime "updated_at", precision: nil
    t.integer "update_user_id"
    t.integer "lock_version", null: false

    t.unique_constraint ["ap_title_id", "bp_code"], name: "petty_payees_ap_title_id_bp_code_key"
    t.unique_constraint ["payment_type"], name: "petty_payees_payment_type_key"
  end

  create_table "petty_pays", id: :serial, force: :cascade do |t|
    t.integer "cash_slip_id"
    t.string "demand_batch_id", limit: 36, null: false
    t.date "demand_batch_close_date"
    t.integer "user_id", null: false
    t.integer "petty_group_id", null: false
    t.string "transaction_type", limit: 4, null: false
    t.integer "cash_advance_id"
    t.integer "currency_id", null: false
    t.integer "demand_id", null: false
    t.string "liability_side", limit: 2, null: false
    t.decimal "amount", precision: 12, scale: 4, null: false
    t.integer "expense_id"
    t.datetime "created_at", precision: nil, null: false
  end

  create_table "qualifications", id: :serial, force: :cascade do |t|
    t.string "name", limit: 100, null: false
    t.string "kana_name", limit: 100, null: false
    t.boolean "active", null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "create_user_id", null: false
    t.datetime "updated_at", precision: nil
    t.integer "update_user_id"
  end

  create_table "receivings", id: :serial, force: :cascade do |t|
    t.integer "cash_slip_id", null: false
    t.integer "sales_invoice_id"
    t.integer "petty_expense_id"
    t.integer "amount", null: false
    t.integer "currency_id", null: false
    t.integer "charge", null: false
    t.datetime "created_at", precision: nil, null: false
  end

  create_table "reporting_titles", id: :serial, force: :cascade do |t|
    t.integer "report_id", null: false
    t.integer "position", null: false
    t.integer "account_title_id", null: false

    t.unique_constraint ["report_id", "position"], name: "reporting_titles_report_id_position_key"
  end

  create_table "reports", id: :serial, force: :cascade do |t|
    t.string "name", limit: 40, null: false
  end

  create_table "rs_logs", id: :serial, force: :cascade do |t|
    t.integer "payment_slip_id", null: false
    t.integer "kind", null: false
    t.string "remarks", limit: 200, null: false
    t.integer "done_by", null: false
    t.datetime "done_at", precision: nil, null: false
  end

  create_table "sales_invoices", id: :serial, force: :cascade do |t|
    t.integer "company_setting_id", null: false
    t.integer "si_number"
    t.integer "partner_id", null: false
    t.string "zip", limit: 10, null: false
    t.string "addr1", limit: 100, null: false
    t.string "addr2", limit: 100, null: false
    t.string "contact_name1", limit: 100, null: false
    t.string "contact_name2", limit: 100, null: false
    t.string "contact_name3", limit: 100, null: false
    t.date "sell_day", null: false
    t.date "due_day", null: false
    t.boolean "disable_due_day", null: false
    t.integer "division_id", null: false
    t.date "journal_day"
    t.datetime "journal_export_at", precision: nil
    t.string "je_number", limit: 8
    t.integer "receivable_account_id", null: false
    t.decimal "amount_total", precision: 12, null: false
    t.integer "currency_id", null: false
    t.boolean "including_tax", null: false
    t.integer "state", null: false
    t.text "remarks", null: false
    t.integer "delivery", null: false
    t.integer "issued_by"
    t.text "keiri_memo", null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "create_user_id", null: false
    t.datetime "updated_at", precision: nil
    t.integer "lock_version", null: false

    t.unique_constraint ["company_setting_id", "si_number"], name: "sales_invoices_company_setting_id_si_number_key"
    t.unique_constraint ["je_number"], name: "sales_invoices_je_number_key"
  end

  create_table "si_banks", id: :serial, force: :cascade do |t|
    t.integer "sales_invoice_id", null: false
    t.integer "our_bank_id", null: false

    t.unique_constraint ["sales_invoice_id", "our_bank_id"], name: "si_banks_sales_invoice_id_our_bank_id_key"
  end

  create_table "si_details", id: :serial, force: :cascade do |t|
    t.integer "sales_invoice_id", null: false
    t.integer "line_no", null: false
    t.integer "unit_price"
    t.integer "qty"
    t.string "unit", limit: 10
    t.decimal "amount", precision: 12, null: false
    t.integer "vat_code", null: false
    t.string "description", limit: 200, null: false
    t.boolean "advance", null: false

    t.unique_constraint ["sales_invoice_id", "line_no"], name: "si_details_sales_invoice_id_line_no_key"
  end

  create_table "si_journals", id: :serial, force: :cascade do |t|
    t.integer "sales_invoice_id", null: false
    t.integer "lineno", null: false
    t.integer "account_title_id", null: false
    t.integer "division_id", null: false
    t.integer "vat_id"
    t.integer "dim2_id"
    t.decimal "amount", precision: 12, null: false
    t.string "remarks", limit: 200, null: false
  end

  create_table "si_logs", id: :serial, force: :cascade do |t|
    t.integer "sales_invoice_id", null: false
    t.integer "kind", null: false
    t.string "remarks", limit: 200, null: false
    t.integer "done_by", null: false
    t.datetime "done_at", precision: nil, null: false
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "worker_code", limit: 48, null: false
    t.string "name", limit: 40, null: false
    t.string "kana_name", limit: 40, null: false
    t.integer "active", null: false
    t.boolean "keiri_flag", null: false
    t.boolean "marketing_flag", null: false
    t.boolean "apar_flag", null: false
    t.boolean "hrd_flag", null: false
    t.string "login", limit: 40
    t.string "email", limit: 40
    t.string "crypted_password", limit: 40
    t.string "salt", limit: 40
    t.string "remember_token", limit: 40
    t.datetime "remember_token_expires_at", precision: nil
    t.datetime "created_at", precision: nil, null: false
    t.integer "create_user_id"
    t.datetime "updated_at", precision: nil
    t.integer "update_user_id"

    t.unique_constraint ["email"], name: "users_email_key"
    t.unique_constraint ["login"], name: "users_login_key"
    t.unique_constraint ["worker_code"], name: "users_worker_code_key"
  end

  create_table "users_divisions", id: :serial, force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "division_id", null: false
    t.integer "worker_position_id"
    t.integer "default_pair", null: false
    t.date "effective_day"
    t.date "end_day"

    t.unique_constraint ["user_id", "division_id", "effective_day"], name: "users_divisions_user_id_division_id_effective_day_key"
  end

  create_table "users_qualifications", id: :serial, force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "qualification_id", null: false
    t.date "effective_day", null: false
    t.date "expire_day"

    t.unique_constraint ["user_id", "qualification_id", "effective_day"], name: "users_qualifications_user_id_qualification_id_effective_day_key"
  end

  create_table "vats", id: :serial, force: :cascade do |t|
    t.boolean "active", null: false
    t.string "country", limit: 2, null: false
    t.integer "tax_side", null: false
    t.string "tax_code", limit: 6, null: false
    t.integer "tax_group", null: false
    t.date "effective_day", null: false
    t.string "name", limit: 40, null: false
    t.decimal "tax_rate", precision: 4, scale: 2, null: false
    t.integer "account_title_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "create_user_id"
    t.datetime "updated_at", precision: nil
    t.integer "update_user_id"

    t.unique_constraint ["country", "tax_side", "tax_code"], name: "vats_country_tax_side_tax_code_key"
  end

  create_table "work_actions", id: :serial, force: :cascade do |t|
    t.integer "action_group_id"
    t.string "name", limit: 100, null: false
    t.string "partners", limit: 100, null: false
    t.integer "division_id", null: false
  end

  create_table "worker_positions", id: :serial, force: :cascade do |t|
    t.string "name", limit: 100, null: false
    t.boolean "active", null: false
  end

  add_foreign_key "account_title_trees", "account_titles", column: "child_id", name: "account_title_trees_child_id_fkey"
  add_foreign_key "account_title_trees", "account_titles", column: "parent_id", name: "account_title_trees_parent_id_fkey"
  add_foreign_key "account_titles", "account_titles", column: "payable_id", name: "account_titles_payable_id_fkey"
  add_foreign_key "account_titles", "account_titles", column: "receivable_id", name: "account_titles_receivable_id_fkey"
  add_foreign_key "account_titles", "users", column: "create_user_id", name: "account_titles_create_user_id_fkey"
  add_foreign_key "account_titles", "users", column: "update_user_id", name: "account_titles_update_user_id_fkey"
  add_foreign_key "attach_file_refs", "attach_files", name: "attach_file_refs_attach_file_id_fkey"
  add_foreign_key "attach_file_refs", "invoices", name: "attach_file_refs_invoice_id_fkey"
  add_foreign_key "attach_file_refs", "partners", name: "attach_file_refs_partner_id_fkey"
  add_foreign_key "attach_files", "attach_kinds", name: "attach_files_attach_kind_id_fkey"
  add_foreign_key "attach_files", "divisions", name: "attach_files_division_id_fkey"
  add_foreign_key "attach_files", "users", column: "create_user_id", name: "attach_files_create_user_id_fkey"
  add_foreign_key "bank_accounts", "bank_branches", name: "bank_accounts_bank_branch_id_fkey"
  add_foreign_key "bank_accounts", "partners", name: "bank_accounts_partner_id_fkey"
  add_foreign_key "bank_accounts", "users", column: "create_user_id", name: "bank_accounts_create_user_id_fkey"
  add_foreign_key "bank_accounts", "users", column: "update_user_id", name: "bank_accounts_update_user_id_fkey"
  add_foreign_key "bank_branches", "banks", name: "bank_branches_bank_id_fkey"
  add_foreign_key "bp_addresses", "partners", name: "bp_addresses_partner_id_fkey"
  add_foreign_key "bp_addresses", "users", column: "created_by", name: "bp_addresses_created_by_fkey"
  add_foreign_key "bp_addresses", "users", column: "updated_by", name: "bp_addresses_updated_by_fkey"
  add_foreign_key "company_settings", "currencies", name: "company_settings_currency_id_fkey"
  add_foreign_key "company_settings2", "company_settings", column: "company_id", name: "company_settings2_company_id_fkey"
  add_foreign_key "company_settings2", "divisions", column: "bank_division_id", name: "company_settings2_bank_division_id_fkey"
  add_foreign_key "company_settings2", "divisions", column: "fallback_division_id", name: "company_settings2_fallback_division_id_fkey"
  add_foreign_key "control_divisions", "control_divisions", column: "parent_id", name: "control_divisions_parent_id_fkey"
  add_foreign_key "controllers_divisions", "control_divisions", name: "controllers_divisions_control_division_id_fkey"
  add_foreign_key "controllers_divisions", "divisions", name: "controllers_divisions_division_id_fkey"
  add_foreign_key "credit_inquiries", "partners", name: "credit_inquiries_partner_id_fkey"
  add_foreign_key "credit_inquiries", "users", column: "create_user_id", name: "credit_inquiries_create_user_id_fkey"
  add_foreign_key "currency_rates", "currencies", name: "currency_rates_currency_id_fkey"
  add_foreign_key "default_dimensions", "dimensions", name: "default_dimensions_dimension_id_fkey"
  add_foreign_key "default_dimensions", "divisions", name: "default_dimensions_division_id_fkey"
  add_foreign_key "default_dimensions", "users", column: "create_user_id", name: "default_dimensions_create_user_id_fkey"
  add_foreign_key "dimensions", "users", column: "create_user_id", name: "dimensions_create_user_id_fkey"
  add_foreign_key "dimensions", "users", column: "update_user_id", name: "dimensions_update_user_id_fkey"
  add_foreign_key "divisions", "company_settings", column: "company_id", name: "divisions_company_id_fkey"
  add_foreign_key "divisions", "dimensions", column: "default_dim2_id", name: "divisions_default_dim2_id_fkey"
  add_foreign_key "divisions", "users", column: "create_user_id", name: "divisions_create_user_id_fkey"
  add_foreign_key "divisions", "users", column: "update_user_id", name: "divisions_update_user_id_fkey"
  add_foreign_key "en_bank_accounts", "partners", name: "en_bank_accounts_partner_id_fkey"
  add_foreign_key "general_ledger", "account_titles", name: "general_ledger_account_title_id_fkey"
  add_foreign_key "general_ledger", "dimensions", column: "dim2_id", name: "general_ledger_dim2_id_fkey"
  add_foreign_key "general_ledger", "divisions", name: "general_ledger_division_id_fkey"
  add_foreign_key "general_ledger", "kubuns", name: "general_ledger_kubun_id_fkey"
  add_foreign_key "general_ledger", "partners", name: "general_ledger_partner_id_fkey"
  add_foreign_key "general_ledger", "users", column: "created_by", name: "general_ledger_created_by_fkey"
  add_foreign_key "general_ledger", "work_actions", name: "general_ledger_work_action_id_fkey"
  add_foreign_key "global_settings", "account_titles", column: "bank_charge_account_title_id", name: "global_settings_bank_charge_account_title_id_fkey"
  add_foreign_key "global_settings", "account_titles", column: "currency_gain_account_title_id", name: "global_settings_currency_gain_account_title_id_fkey"
  add_foreign_key "global_settings", "account_titles", column: "vat_payment_account_title_id", name: "global_settings_vat_payment_account_title_id_fkey"
  add_foreign_key "global_settings", "control_divisions", column: "default_division_tree_root_id", name: "global_settings_default_division_tree_root_id_fkey"
  add_foreign_key "import_declarations", "company_settings", column: "company_id", name: "import_declarations_company_id_fkey"
  add_foreign_key "import_declarations", "currencies", column: "invoice_currency_id", name: "import_declarations_invoice_currency_id_fkey"
  add_foreign_key "import_declarations", "users", column: "create_user_id", name: "import_declarations_create_user_id_fkey"
  add_foreign_key "imports_invoices", "import_declarations", name: "imports_invoices_import_declaration_id_fkey"
  add_foreign_key "imports_invoices", "invoices", name: "imports_invoices_invoice_id_fkey"
  add_foreign_key "imports_invoices", "users", column: "create_user_id", name: "imports_invoices_create_user_id_fkey"
  add_foreign_key "invoice_details", "account_titles", name: "invoice_details_account_title_id_fkey"
  add_foreign_key "invoice_details", "dimensions", column: "dim2_id", name: "invoice_details_dim2_id_fkey"
  add_foreign_key "invoice_details", "divisions", name: "invoice_details_division_id_fkey"
  add_foreign_key "invoice_details", "invoices", name: "invoice_details_invoice_id_fkey"
  add_foreign_key "invoice_details", "vats", name: "invoice_details_vat_id_fkey"
  add_foreign_key "invoice_logs", "invoices", name: "invoice_logs_invoice_id_fkey"
  add_foreign_key "invoice_logs", "users", column: "done_by", name: "invoice_logs_done_by_fkey"
  add_foreign_key "invoices", "bank_accounts", name: "invoices_bank_account_id_fkey"
  add_foreign_key "invoices", "company_settings", name: "invoices_company_setting_id_fkey"
  add_foreign_key "invoices", "currencies", name: "invoices_currency_id_fkey"
  add_foreign_key "invoices", "divisions", name: "invoices_division_id_fkey"
  add_foreign_key "invoices", "en_bank_accounts", name: "invoices_en_bank_account_id_fkey"
  add_foreign_key "invoices", "partners", name: "invoices_partner_id_fkey"
  add_foreign_key "invoices", "users", column: "created_by", name: "invoices_created_by_fkey"
  add_foreign_key "mat_parts", "users", column: "create_user_id", name: "mat_parts_create_user_id_fkey"
  add_foreign_key "mat_parts", "users", column: "update_user_id", name: "mat_parts_update_user_id_fkey"
  add_foreign_key "mat_receiving_charges", "mat_receivings", name: "mat_receiving_charges_mat_receiving_id_fkey"
  add_foreign_key "mat_receivings", "invoices", name: "mat_receivings_invoice_id_fkey"
  add_foreign_key "our_banks", "account_titles", name: "our_banks_account_title_id_fkey"
  add_foreign_key "our_banks", "bank_accounts", name: "our_banks_bank_account_id_fkey"
  add_foreign_key "our_banks", "company_settings", name: "our_banks_company_setting_id_fkey"
  add_foreign_key "our_banks", "currencies", name: "our_banks_currency_id_fkey"
  add_foreign_key "our_banks", "en_bank_accounts", name: "our_banks_en_bank_account_id_fkey"
  add_foreign_key "our_banks", "users", column: "create_user_id", name: "our_banks_create_user_id_fkey"
  add_foreign_key "our_banks", "users", column: "update_user_id", name: "our_banks_update_user_id_fkey"
  add_foreign_key "our_contacts", "company_settings", name: "our_contacts_company_setting_id_fkey"
  add_foreign_key "partners", "users", column: "created_by", name: "partners_created_by_fkey"
  add_foreign_key "partners", "users", column: "updated_by", name: "partners_updated_by_fkey"
  add_foreign_key "payment_banks", "our_banks", name: "payment_banks_our_bank_id_fkey"
  add_foreign_key "payment_banks", "payment_slips", name: "payment_banks_payment_slip_id_fkey"
  add_foreign_key "payment_slips", "company_settings", column: "company_id", name: "payment_slips_company_id_fkey"
  add_foreign_key "payment_slips", "users", column: "create_user_id", name: "payment_slips_create_user_id_fkey"
  add_foreign_key "payment_slips", "users", column: "update_user_id", name: "payment_slips_update_user_id_fkey"
  add_foreign_key "payments", "account_titles", name: "payments_account_title_id_fkey"
  add_foreign_key "payments", "currencies", name: "payments_currency_id_fkey"
  add_foreign_key "payments", "invoices", name: "payments_invoice_id_fkey"
  add_foreign_key "payments", "payment_slips", name: "payments_payment_slip_id_fkey"
  add_foreign_key "petty_cash_advances", "account_titles", name: "petty_cash_advances_account_title_id_fkey"
  add_foreign_key "petty_cash_advances", "currencies", column: "reimb_currency_id", name: "petty_cash_advances_reimb_currency_id_fkey"
  add_foreign_key "petty_cash_advances", "currencies", column: "request_currency_id", name: "petty_cash_advances_request_currency_id_fkey"
  add_foreign_key "petty_cash_advances", "petty_groups", name: "petty_cash_advances_petty_group_id_fkey"
  add_foreign_key "petty_cash_advances", "petty_payees", column: "payee_payment_type", primary_key: "payment_type", name: "petty_cash_advances_payee_payment_type_fkey"
  add_foreign_key "petty_cash_advances", "petty_payees", column: "payer_payment_type", primary_key: "payment_type", name: "petty_cash_advances_payer_payment_type_fkey"
  add_foreign_key "petty_cash_advances", "users", name: "petty_cash_advances_user_id_fkey"
  add_foreign_key "petty_expense_allocations", "account_titles", name: "petty_expense_allocations_account_title_id_fkey"
  add_foreign_key "petty_expense_allocations", "divisions", name: "petty_expense_allocations_division_id_fkey"
  add_foreign_key "petty_expense_allocations", "petty_expense_entries", name: "petty_expense_allocations_petty_expense_entry_id_fkey"
  add_foreign_key "petty_expense_allocations", "petty_payees", column: "payee_payment_type", primary_key: "payment_type", name: "petty_expense_allocations_payee_payment_type_fkey"
  add_foreign_key "petty_expense_allocations", "petty_payees", column: "payer_payment_type", primary_key: "payment_type", name: "petty_expense_allocations_payer_payment_type_fkey"
  add_foreign_key "petty_expense_entries", "petty_expenses", name: "petty_expense_entries_petty_expense_id_fkey"
  add_foreign_key "petty_expenses", "currencies", column: "reimb_currency_id", name: "petty_expenses_reimb_currency_id_fkey"
  add_foreign_key "petty_expenses", "petty_groups", name: "petty_expenses_petty_group_id_fkey"
  add_foreign_key "petty_expenses", "users", name: "petty_expenses_user_id_fkey"
  add_foreign_key "petty_groups", "company_settings", column: "company_id", name: "petty_groups_company_id_fkey"
  add_foreign_key "petty_groups", "users", column: "create_user_id", name: "petty_groups_create_user_id_fkey"
  add_foreign_key "petty_groups", "users", column: "update_user_id", name: "petty_groups_update_user_id_fkey"
  add_foreign_key "petty_offsets", "currencies", name: "petty_offsets_currency_id_fkey"
  add_foreign_key "petty_offsets", "petty_cash_advances", column: "cash_advance_id", name: "petty_offsets_cash_advance_id_fkey"
  add_foreign_key "petty_offsets", "petty_expenses", column: "expense_id", name: "petty_offsets_expense_id_fkey"
  add_foreign_key "petty_payees", "account_titles", column: "ap_title_id", name: "petty_payees_ap_title_id_fkey"
  add_foreign_key "petty_payees", "account_titles", column: "ar_title_id", name: "petty_payees_ar_title_id_fkey"
  add_foreign_key "petty_payees", "users", column: "create_user_id", name: "petty_payees_create_user_id_fkey"
  add_foreign_key "petty_payees", "users", column: "update_user_id", name: "petty_payees_update_user_id_fkey"
  add_foreign_key "petty_pays", "currencies", name: "petty_pays_currency_id_fkey"
  add_foreign_key "petty_pays", "payment_slips", column: "cash_slip_id", name: "petty_pays_cash_slip_id_fkey"
  add_foreign_key "petty_pays", "petty_cash_advances", column: "cash_advance_id", name: "petty_pays_cash_advance_id_fkey"
  add_foreign_key "petty_pays", "petty_expenses", column: "expense_id", name: "petty_pays_expense_id_fkey"
  add_foreign_key "petty_pays", "petty_groups", name: "petty_pays_petty_group_id_fkey"
  add_foreign_key "petty_pays", "users", name: "petty_pays_user_id_fkey"
  add_foreign_key "qualifications", "users", column: "create_user_id", name: "qualifications_create_user_id_fkey"
  add_foreign_key "qualifications", "users", column: "update_user_id", name: "qualifications_update_user_id_fkey"
  add_foreign_key "receivings", "currencies", name: "receivings_currency_id_fkey"
  add_foreign_key "receivings", "payment_slips", column: "cash_slip_id", name: "receivings_cash_slip_id_fkey"
  add_foreign_key "receivings", "petty_expenses", name: "receivings_petty_expense_id_fkey"
  add_foreign_key "receivings", "sales_invoices", name: "receivings_sales_invoice_id_fkey"
  add_foreign_key "reporting_titles", "account_titles", name: "reporting_titles_account_title_id_fkey"
  add_foreign_key "reporting_titles", "reports", name: "reporting_titles_report_id_fkey"
  add_foreign_key "rs_logs", "payment_slips", name: "rs_logs_payment_slip_id_fkey"
  add_foreign_key "rs_logs", "users", column: "done_by", name: "rs_logs_done_by_fkey"
  add_foreign_key "sales_invoices", "account_titles", column: "receivable_account_id", name: "sales_invoices_receivable_account_id_fkey"
  add_foreign_key "sales_invoices", "company_settings", name: "sales_invoices_company_setting_id_fkey"
  add_foreign_key "sales_invoices", "currencies", name: "sales_invoices_currency_id_fkey"
  add_foreign_key "sales_invoices", "divisions", name: "sales_invoices_division_id_fkey"
  add_foreign_key "sales_invoices", "partners", name: "sales_invoices_partner_id_fkey"
  add_foreign_key "sales_invoices", "users", column: "create_user_id", name: "sales_invoices_create_user_id_fkey"
  add_foreign_key "sales_invoices", "users", column: "issued_by", name: "sales_invoices_issued_by_fkey"
  add_foreign_key "si_banks", "our_banks", name: "si_banks_our_bank_id_fkey"
  add_foreign_key "si_banks", "sales_invoices", name: "si_banks_sales_invoice_id_fkey"
  add_foreign_key "si_details", "sales_invoices", name: "si_details_sales_invoice_id_fkey"
  add_foreign_key "si_journals", "account_titles", name: "si_journals_account_title_id_fkey"
  add_foreign_key "si_journals", "dimensions", column: "dim2_id", name: "si_journals_dim2_id_fkey"
  add_foreign_key "si_journals", "divisions", name: "si_journals_division_id_fkey"
  add_foreign_key "si_journals", "sales_invoices", name: "si_journals_sales_invoice_id_fkey"
  add_foreign_key "si_journals", "vats", name: "si_journals_vat_id_fkey"
  add_foreign_key "si_logs", "sales_invoices", name: "si_logs_sales_invoice_id_fkey"
  add_foreign_key "si_logs", "users", column: "done_by", name: "si_logs_done_by_fkey"
  add_foreign_key "users", "users", column: "create_user_id", name: "users_create_user_id_fkey"
  add_foreign_key "users", "users", column: "update_user_id", name: "users_update_user_id_fkey"
  add_foreign_key "users_divisions", "divisions", name: "users_divisions_division_id_fkey"
  add_foreign_key "users_divisions", "users", name: "users_divisions_user_id_fkey"
  add_foreign_key "users_divisions", "worker_positions", name: "users_divisions_worker_position_id_fkey"
  add_foreign_key "users_qualifications", "qualifications", name: "users_qualifications_qualification_id_fkey"
  add_foreign_key "users_qualifications", "users", name: "users_qualifications_user_id_fkey"
  add_foreign_key "vats", "account_titles", name: "vats_account_title_id_fkey"
  add_foreign_key "vats", "users", column: "create_user_id", name: "vats_create_user_id_fkey"
  add_foreign_key "vats", "users", column: "update_user_id", name: "vats_update_user_id_fkey"
  add_foreign_key "work_actions", "action_groups", name: "work_actions_action_group_id_fkey"
  add_foreign_key "work_actions", "divisions", name: "work_actions_division_id_fkey"
end
