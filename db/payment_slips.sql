
-- 入金/出金伝票
CREATE TABLE payment_slips (
  id          serial PRIMARY KEY,

  -- 入金伝票 = 1, 出金伝票 = 2
  slip_type   int NOT NULL,

  company_id  int NOT NULL REFERENCES company_settings (id),
  date        DATE NOT NULL,

  journal_export_at TIMESTAMP,

  -- 0 = 初期状態, 10 = 経理承認済み, 20 = 会計システム連動済み, 30 = キャンセル
  state             int NOT NULL,

  created_at     TIMESTAMP NOT NULL,
  create_user_id int NOT NULL REFERENCES users (id),
  updated_at     TIMESTAMP,
  update_user_id int REFERENCES users (id),

  lock_version   int NOT NULL
);
