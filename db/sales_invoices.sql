
-- 顧客に対する請求書.
-- 非定形かつ B2B 前提
CREATE TABLE sales_invoices (
  id                 serial PRIMARY KEY,

  -- シェアードサービス会社が所管することがある。カンパニーは所管部門と別に持つ。
  company_setting_id int NOT NULL REFERENCES company_settings (id),

  -- 請求書番号
  -- 印刷時に採番する
  si_number          int,

  partner_id   int NOT NULL REFERENCES partners (id),

  -- 宛先... つど記入する
  zip           VARCHAR(10) NOT NULL,
  addr1         VARCHAR(100) NOT NULL,
  addr2         VARCHAR(100) NOT NULL,
  contact_name1 VARCHAR(100) NOT NULL,
  contact_name2 VARCHAR(100) NOT NULL,
  contact_name3 VARCHAR(100) NOT NULL,

  -- 販売日
  sell_day     DATE NOT NULL, 

  due_day      DATE NOT NULL,
  disable_due_day boolean NOT NULL,   -- 請求書に期日を表示しないときにtrue

  division_id  int NOT NULL REFERENCES divisions (id), -- 所管部門

  -- 会計システム連携
  journal_day           DATE,            -- 会計伝票日. 出力するまでナル値.
  journal_export_at     TIMESTAMP,
  je_number             VARCHAR(8) UNIQUE,
  receivable_account_id int NOT NULL REFERENCES account_titles (id), -- 債権科目
  
  -- 合計金額(税込み). これがないと消し込みが難しい
  amount_total          NUMERIC(12, 0) NOT NULL, 

  currency_id           int NOT NULL REFERENCES currencies (id),
  -- 税込価額で表示 = TRUE
  including_tax BOOLEAN NOT NULL,
  
  -- 0 = 初期状態, 10 = 経理承認, 20 = 会計システム連動, 30 = キャンセル
  state        int NOT NULL,    
  -- 請求書に表示するメモ
  remarks      TEXT NOT NULL,
  
  delivery     int NOT NULL,  -- 送付方法。1 = 担当者持参、2 = 郵送
  issued_by    int REFERENCES users (id),  -- 発行したときの担当者

  -- 社内メモ
  keiri_memo     TEXT NOT NULL,
  
  created_at     TIMESTAMP NOT NULL,
  create_user_id int NOT NULL REFERENCES users (id),
  updated_at     TIMESTAMP,
  lock_version   int NOT NULL,

  UNIQUE (company_setting_id, si_number)
);

