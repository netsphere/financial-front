-- �̔��������̍X�V�����A���F�������L�^
CREATE TABLE si_logs (
  id               serial PRIMARY KEY,
  sales_invoice_id int NOT NULL REFERENCES sales_invoices(id),
  kind             int NOT NULL,    -- InvoiceLog::MODIFIED, InvoiceLog::SIGNED_OFF
  remarks          VARCHAR(200) NOT NULL,
  done_by          int NOT NULL REFERENCES users(id),
  done_at          TIMESTAMP NOT NULL
);
