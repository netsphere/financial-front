
-- 勘定科目の集計木
-- 集計木そのものは、帳票をまたいで共有される
-- 集計先を複数にできるようにする。... 多対多
--     ex. 営業収益 = 物販売上 + サービス売上
--         営業収入 = 営業収益 - 売掛金 + 前受金
--         流動資産 = 売掛金 + ...
CREATE TABLE account_title_trees (
  id        serial PRIMARY KEY,
  parent_id int NOT NULL REFERENCES account_titles(id),
  child_id  int NOT NULL REFERENCES account_titles(id),
  UNIQUE (parent_id, child_id)
);
