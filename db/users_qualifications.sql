
-- 社員が持っている資格.
-- 有効期限がある場合がある。
CREATE TABLE users_qualifications (
  id               serial PRIMARY KEY,
  user_id          int NOT NULL REFERENCES users (id),
  qualification_id int NOT NULL REFERENCES qualifications (id),

  effective_day    DATE NOT NULL,
  expire_day       DATE,

  UNIQUE (user_id, qualification_id, effective_day)
);
