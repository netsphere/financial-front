
-- 輸入申告
-- http://ec.europa.eu/agriculture/markets/export_refunds/forms/jp.pdf
-- http://help.sap.com/erp2005_ehp_03/helpdata/EN/2b/274550995111d1b4e30000e82d81b0/frameset.htm
CREATE TABLE import_declarations (
  id                  serial PRIMARY KEY,
  company_id          int NOT NULL REFERENCES company_settings (id),

  -- 申告番号 N11. 手書きの場合アルファベットが含まれることがある。
  declaration_number  VARCHAR(11) NOT NULL UNIQUE,

  exporter_name       VARCHAR(100) NOT NULL, -- 輸出者名

  -- AWB (B/L) 番号. 船便の場合えらく長いことがある
  bl_number           VARCHAR(18) NOT NULL,   

  -- 仕入書価格. ドル建て等の場合は10^n倍する
  invoice_price       DECIMAL(10) NOT NULL,          
  invoice_currency_id int NOT NULL REFERENCES currencies(id), -- invoiceの通貨
  invoice_incoterm_id int NOT NULL, 

  customs_tariff      int NOT NULL,          -- D 関税
  consumption_tax     int NOT NULL,          -- F 消費税
  vat_local           int NOT NULL,          -- A 地方消費税
  description         TEXT NOT NULL,      -- 内容など
  permit_date         DATE NOT NULL,      -- 輸入許可日

  created_at          TIMESTAMP NOT NULL,
  create_user_id      int NOT NULL REFERENCES users (id),
  updated_at          TIMESTAMP
);
