-- -*- coding:utf-8 -*-
-- TODO: impl. このテーブルは未完成

-- 注文書
CREATE TABLE purchase_orders (
  id         serial PRIMARY KEY,
  po_date    DATE NOT NULL,

  partner_id int NOT NULL REFERENCES partners (id),
  to_name    VARCHAR(100) NOT NULL,
  to_phone   VARCHAR(40) NOT NULL,

  ship_to_name  VARCHAR(100) NOT NULL,
  ship_to_addr1 VARCHAR(100) NOT NULL,

  our reference number

  payment terms VARCHAR(100) NOT NULL,   -- 支払条件

freight terms
incoterms
forwarder

authorized_by int NOT NULL REFERENCES users (id), -- authorized signature

-- 以下はdetail表へ
description
unit_price
qty
unit
amount





);
