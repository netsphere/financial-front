
-- 明細のほうに付いているチャージ
CREATE TABLE mat_receiving_charges (
  id     serial PRIMARY KEY,

  -- 親
  mat_receiving_id int NOT NULL REFERENCES mat_receivings (id),

  charge_code      VARCHAR(6) NOT NULL,

  -- charge_codeの名前. 冗長だが...
  charge_code_desc VARCHAR(36) NOT NULL,

  -- <chargeAmount>
  -- こちらも税抜き価格
  total_amount     DECIMAL(12, 4) NOT NULL,

  tax_code         VARCHAR(6) NOT NULL,

  tax_amount       DECIMAL(12, 4) NOT NULL,

  created_at     TIMESTAMP NOT NULL,
  updated_at     TIMESTAMP
);
