# -*- coding:utf-8 -*-

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


# global_setting に必要な勘定科目
# @return [Array of AccountTitle] 
def add_ac_instances user
  r = []
  r << AccountTitle.new(:aggregate_flag => false,
                      :account_code => 8852,
                      :suppl_code => 0,
                      :main_name => "支払手数料",
                      :name => "支払手数料",
                      :kana_name => "シハライテスウリョウ",
                      :description => "",
                      :kind => 0,
                      :enable_si_invoice => false,
                      :enable_invoice => false,
                      :sign => +1,
                      :req_partner => false,
                      :tax_side => 2,
                        :default_tax_type => 10)

  r << AccountTitle.new(:aggregate_flag => false,
                      :account_code => 9905,
                      :suppl_code => 0,
                      :main_name => "為替差損益",
                      :name => "為替差損益",
                      :kana_name => "カワセサソンエキ",
                      :description => "",
                      :kind => 0,
                      :enable_si_invoice => false,
                      :enable_invoice => false,
                      :sign => -1,
                      :req_partner => false,
                      :tax_side => 0,
                        :default_tax_type => 0) 

  r << AccountTitle.new(:aggregate_flag => false,
                      :account_code => 1173,
                      :suppl_code => 0,
                      :main_name => "仮払JP消費税(Manual)",
                      :name => "仮払JP消費税(Manual)",
                      :kana_name => "カリバライショウヒゼイ",
                      :description => "",
                      :kind => 3,
                      :enable_si_invoice => false,
                      :enable_invoice => false,
                      :sign => +1,
                      :req_partner => false,
                      :tax_side => 0,
                        :default_tax_type => 0)
  r.each do |ac|
    ac.create_user_id = user.id
    ac.update_user_id = user.id
    ac.save!
  end
  return r
end


GlobalSetting.transaction do
  user = User.new worker_code: 'S1',
                  name:        'admin',
                  kana_name:   'アドミン',
                  active:      true,
                  keiri_flag:  true,
                  marketing_flag: true,
                  apar_flag:      true,
                  hrd_flag:       true,
                  login:          'admin',
                  email:          'admin@localhost',
                  password: 'admin',
                  password_confirmation: 'admin'
                  #salt:             'XXX'
  user.save!
  
  root = ControlDivision.new :name => "(変更してください)部門集計木"
  root.save!

  acs = add_ac_instances user
  global = GlobalSetting.new :partner_counter => 400001, 
                           :currency_limit_day => Date.today,
                           :bank_charge_account_title_id => acs[0].id,
                           :currency_gain_account_title_id => acs[1].id,
                           :vat_payment_account_title_id => acs[2].id,
                           :default_division_tree_root_id => root.id
  global[:id] = 1
  global.save!
end

