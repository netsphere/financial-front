
-- カンパニー
-- 参照が循環するため、テーブルを分ける
CREATE TABLE company_settings2 (
  id                serial PRIMARY KEY,
  company_id        int NOT NULL REFERENCES company_settings(id),

  -- 会計システム連動用. 送金手数料、為替差損益
  bank_division_id  int NOT NULL REFERENCES divisions (id),

  -- 元帳データのインポート時に使用
  fallback_division_id int NOT NULL REFERENCES divisions (id)
);
