
-- サプライヤからの請求書
CREATE TABLE invoices (
  id                 serial PRIMARY KEY,
  company_setting_id int NOT NULL REFERENCES company_settings (id),

  -- 転記元
  origin_no       VARCHAR(20) UNIQUE,

  -- 稟議書#
  approval        VARCHAR(20),

  -- 銀行口座は会社分割等で紐付けを変更することがあり、取引先を優先する
  partner_id      int NOT NULL REFERENCES partners(id),

  -- ベンダの請求書番号
  invoice_no      VARCHAR(40), 

  -- 銀行口座がない相手もある
  -- 外貨建ても、複数の口座を持つ相手先がある。例えば通貨違いなど。
  bank_account_id    int REFERENCES bank_accounts(id),   
  en_bank_account_id int REFERENCES en_bank_accounts(id),

  buy_day         DATE NOT NULL,
  due_day         DATE NOT NULL,

  -- 請求通貨
  currency_id     int NOT NULL REFERENCES currencies(id),

  -- 合計金額 (VAT含む。)
  -- 明細を足し上げたもの。キャッシュ.
  amount_total    int NOT NULL,

  -- 所管部門
  -- シェアードサービス会社が所管することがある。カンパニーとは別に持つ。
  division_id     int NOT NULL REFERENCES divisions(id),

  -- 会計伝票日
  journal_day       DATE,    
  journal_export_at TIMESTAMP,
  je_number         VARCHAR(8) UNIQUE,
  
  -- 0 = 初期状態, 10 = 経理承認、20 = 会計システム連動済
  state           int NOT NULL,  

  -- 0 = 初期状態, 10 = 支払依頼作成 (印刷) 済み, 20 = キャンセル
  payment_state   int NOT NULL,  

  created_at      TIMESTAMP NOT NULL,
  created_by      int REFERENCES users(id),  -- インポートの場合はナル
  updated_at      TIMESTAMP,
  lock_version    int NOT NULL,

  UNIQUE(partner_id, invoice_no)
);

