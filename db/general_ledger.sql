-- 総勘定元帳
CREATE TABLE general_ledger (
  id               serial PRIMARY KEY,
  date             DATE NOT NULL,    -- 伝票日
  slip_number      int,              -- 伝票番号
  -- 実績, 予算年度
  kubun_id         int NOT NULL REFERENCES kubuns (id),

  -- 会計部門. B/S科目は部門がない。
  division_id      int REFERENCES divisions (id),

  dim2_id          int REFERENCES dimensions (id),

  -- belongs_to()は、複数外部キーに対応していない。
  account_title_id int NOT NULL REFERENCES account_titles (id),

  -- 取引先
  partner_id       int REFERENCES partners (id),

  -- 予算項目
  work_action_id   int REFERENCES work_actions (id), 

  amount           int8 NOT NULL,
  remarks          VARCHAR(250) NOT NULL,

  created_by       int REFERENCES users (id),
  created_at       timestamp
);
CREATE INDEX fki_general_ledger_work_action_id_fkey
   ON general_ledger (work_action_id);
CREATE INDEX fki_general_ledger_partner_id_fkey
   ON general_ledger (partner_id);
