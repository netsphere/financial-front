
-- 販売請求書にもとづいて生成する仕訳
-- 基本的に, invoice_details と同じ形.
CREATE TABLE si_journals (
  id               serial PRIMARY KEY,
  sales_invoice_id int NOT NULL REFERENCES sales_invoices(id),

  lineno           int NOT NULL,

  account_title_id int NOT NULL REFERENCES account_titles(id),
  division_id      int NOT NULL REFERENCES divisions(id),

  vat_id           int REFERENCES vats (id),

  dim2_id          int REFERENCES dimensions (id),

  -- 請求通貨での金額. 正規化される.
  amount           NUMERIC(12, 0) NOT NULL,

  -- 過去との互換性のため, migration では, 列を削除しない. [v2.0 -> v3.0]
  -- vat_code         int NOT NULL,

  remarks          VARCHAR(200) NOT NULL
);
