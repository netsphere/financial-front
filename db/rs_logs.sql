-- -*- coding:utf-8 -*-
-- 入金・出金伝票の更新日時、承認日時を記録
CREATE TABLE rs_logs (
  id                serial PRIMARY KEY,

  -- どちらかを埋める
  -- receiving_slip_id int REFERENCES receiving_slips (id),
  payment_slip_id   int NOT NULL REFERENCES payment_slips (id),

  -- InvoiceLog::MODIFIED, InvoiceLog::SIGNED_OFF
  kind              int NOT NULL,    

  remarks           VARCHAR(200) NOT NULL,
  done_by           int NOT NULL REFERENCES users(id),
  done_at           TIMESTAMP NOT NULL
);
