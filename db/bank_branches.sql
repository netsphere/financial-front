-- 銀行支店マスタ
CREATE TABLE bank_branches (
  id          serial PRIMARY KEY,
  -- 銀行コード
  bank_id     int NOT NULL REFERENCES banks(id), 
  branch_code int NOT NULL,   -- 支店コード N 3   支店コードは銀行合併などで変更される。
  name        VARCHAR(15) NOT NULL,
  kana_name   VARCHAR(15) NOT NULL, -- 支店名（カナ） C 15
  UNIQUE (bank_id, branch_code)
);
