
-- カンパニー
CREATE TABLE company_settings (
  id              serial PRIMARY KEY,
  active          BOOLEAN NOT NULL,
  -- 登録番号(JP)
  -- Legal entity より細かく company を作ることある. UNIQUE 付けない
  tax_reg_number  VARCHAR(14), 
  
  name            VARCHAR(60) NOT NULL,
  name_en         VARCHAR(60) NOT NULL,

  fixed_day       DATE NOT NULL,   -- 締め日 (支払依頼の印刷用)
  fixed_gl_day    DATE NOT NULL,   -- 会計システム側で締め済みの決算日

  country         CHAR(2) NOT NULL,
  currency_id     int NOT NULL REFERENCES currencies(id), -- 機能通貨

  -- 販売請求書
  si_counter      int NOT NULL,    -- 販売請求書の連番
  si_footer       text NOT NULL,
  logo_image      BYTEA
);
