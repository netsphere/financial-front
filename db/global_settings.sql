
-- 企業グループ全体の設定
CREATE TABLE global_settings (
  id                 int PRIMARY KEY,

  -- 取引先コードカウンタ
  partner_counter    int NOT NULL,

  -- 通貨の有効期限
  currency_limit_day DATE NOT NULL,

  -- 送金手数料
  bank_charge_account_title_id   int NOT NULL REFERENCES account_titles(id),

  -- 為替差損益
  currency_gain_account_title_id int NOT NULL REFERENCES account_titles(id),

  -- 仮払消費税
  vat_payment_account_title_id   int NOT NULL REFERENCES account_titles(id),

  -- デフォルト部門集計木ルート
  -- 通常は「所属」視点
  default_division_tree_root_id int NOT NULL REFERENCES control_divisions (id)
);
