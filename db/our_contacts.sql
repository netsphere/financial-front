
CREATE TABLE our_contacts (
  id     serial PRIMARY KEY,
  company_setting_id int NOT NULL REFERENCES company_settings (id),
  lang   CHAR(2) NOT NULL,  -- 言語コード ja, en
  zip    VARCHAR(10) NOT NULL,
  addr1  VARCHAR(100) NOT NULL,
  addr2  VARCHAR(100) NOT NULL,
  addr3  VARCHAR(100) NOT NULL,
  name   VARCHAR(100) NOT NULL,  -- 役職 氏名
  -- contact_name2 VARCHAR(200) NOT NULL,
  tel    VARCHAR(20) NOT NULL
);
