
-- 小口精算で用いる (相手先 + 精算コード)
--
-- TODO: インポート時に正規化したうえで、クレジットカードのみをこのテーブルに
--       格納するとよい.
CREATE TABLE petty_payees (
  id     serial PRIMARY KEY,

  -- 1 特殊コード "Company"
  --   経費勘定
  -- 2 特殊コード "Employee"
  --   社員未収入金, 社員未払金
  -- 3 クレジットカード(発行者) + 精算方式
  --   ex) "Corporate_SMBC_VISA_CBCP"
  --   未収入金, 未払金
  payment_type VARCHAR(32) NOT NULL UNIQUE,

  name           VARCHAR(80) NOT NULL,
  remarks        TEXT NOT NULL,

  -- 未収入金a/c
  -- "Company" の場合は未使用
  ar_title_id    int REFERENCES account_titles (id),

  -- 未払金a/c
  -- "Company" の場合は未使用	
  ap_title_id    int REFERENCES account_titles (id),
  
  -- 取引先コード
  -- "Company", "Employee" の場合は未使用
  bp_code        int ,
  
  created_at     TIMESTAMP NOT NULL,
  create_user_id int NOT NULL REFERENCES users (id),
  updated_at     TIMESTAMP,
  update_user_id int REFERENCES users (id),
  lock_version   int NOT NULL,

  UNIQUE (ap_title_id, bp_code)
);
