-- -*- coding:utf-8 -*-

-- 添付ファイルの種別
CREATE TABLE attach_kinds (
  id              serial PRIMARY KEY,
  name            VARCHAR(60) NOT NULL,
  sort            int NOT NULL,     -- 表示順
  enable_invoice  int NOT NULL    -- 請求書にアタッチできる種別か？ 非0で請求書にattach
);
