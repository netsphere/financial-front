-- �בփ��[�g
-- 1����ʉݓ�����̉~�̊z��o�^����B
CREATE TABLE currency_rates (
  id          serial PRIMARY KEY,
  date        DATE NOT NULL,
  currency_id int NOT NULL REFERENCES currencies(id),
  rate        decimal(8, 4) NOT NULL,
  UNIQUE (date, currency_id)
);
