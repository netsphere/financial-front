
-- 次元1 = 会計部門
CREATE TABLE divisions (
  id                  serial PRIMARY KEY,
  division_code       VARCHAR(20) NOT NULL UNIQUE,

  -- いずれかのカンパニーに属する
  company_id          int NOT NULL REFERENCES company_settings(id),

  name                VARCHAR(40) NOT NULL,
  kana_name           VARCHAR(40) NOT NULL,

  -- 次元2を使わない場合は, ナル値
  default_dim2_id     int REFERENCES dimensions (id),

  -- ナル値ではないとき, 非アクティブ
  end_day             DATE,

  purchase_active     int NOT NULL,    -- 追加可能か。0以外で追加可能。

  created_at     TIMESTAMP NOT NULL,
  create_user_id int NOT NULL REFERENCES users (id),
  updated_at     TIMESTAMP,
  update_user_id int REFERENCES users(id)
);
