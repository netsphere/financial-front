-- 添付ファイル
CREATE TABLE attach_files (
  id                serial PRIMARY KEY,
  data              BYTEA NOT NULL,

  -- エクスプローラでも 260 文字までの長さのファイルを作れる
  original_filename VARCHAR(260) NOT NULL,

  -- Excel (.xlsx) がやたらと長い
  -- application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
  content_type      VARCHAR(100) NOT NULL,

  attach_kind_id    int NOT NULL REFERENCES attach_kinds(id),
  remarks           VARCHAR(200) NOT NULL,
  division_id       int NOT NULL REFERENCES divisions(id),  -- 所管部門

  create_user_id    int NOT NULL REFERENCES users(id),
  created_at        TIMESTAMP
);
