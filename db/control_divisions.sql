-- コストセンタ (会計部門の集合)
CREATE TABLE control_divisions (
  id        serial PRIMARY KEY,
  name      VARCHAR(40) NOT NULL,

  -- nested setを適用する。root は parent_id = NULL (複数持てる)
  parent_id int REFERENCES control_divisions (id),
  lft       int NOT NULL,
  rgt       int NOT NULL
);
