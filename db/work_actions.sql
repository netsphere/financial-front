
-- 計画 (の1行)
CREATE TABLE work_actions (
  id          serial PRIMARY KEY,
  action_group_id int REFERENCES action_groups(id), -- NOT NULLにしないのはcompatibilityのため
  name        VARCHAR(100) NOT NULL,   -- 内容
  partners    VARCHAR(100) NOT NULL,   -- 支払先など
  division_id int NOT NULL REFERENCES divisions (id)   -- 申請部門
);
