
-- 役職. 部長とか課長とか
-- (社員, 部門) と紐付けされる
CREATE TABLE worker_positions (
  id     serial PRIMARY KEY,
  name   VARCHAR(100) NOT NULL,
  active boolean NOT NULL
);
