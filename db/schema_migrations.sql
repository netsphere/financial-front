
CREATE TABLE schema_migrations (
  "version" VARCHAR(255) NOT NULL UNIQUE
);
INSERT INTO schema_migrations (version) VALUES ('20110110215857');
INSERT INTO schema_migrations (version) VALUES ('20110120220948');
INSERT INTO schema_migrations (version) VALUES ('20110206223427');
INSERT INTO schema_migrations (version) VALUES ('20110214124627');
INSERT INTO schema_migrations (version) VALUES ('20110223021402');
INSERT INTO schema_migrations (version) VALUES ('20110314135954');
INSERT INTO schema_migrations (version) VALUES ('20110322121934');
INSERT INTO schema_migrations (version) VALUES ('20110329032801');
INSERT INTO schema_migrations (version) VALUES ('20110329071044');
INSERT INTO schema_migrations (version) VALUES ('20110620120547');
INSERT INTO schema_migrations (version) VALUES ('20110625223431');
INSERT INTO schema_migrations (version) VALUES ('20110625223432');
INSERT INTO schema_migrations (version) VALUES ('20111115234604');
INSERT INTO schema_migrations (version) VALUES ('20111128101038');
INSERT INTO schema_migrations (version) VALUES ('20111205155735');
INSERT INTO schema_migrations (version) VALUES ('20111222102949');
-- ここまで v2.0.0
INSERT INTO schema_migrations (version) VALUES ('20120409042037'); -- create_vats
INSERT INTO schema_migrations (version) VALUES ('20130830062944'); -- petty
INSERT INTO schema_migrations (version) VALUES ('20130912094734'); -- payment curr.
INSERT INTO schema_migrations (version) VALUES ('20130913030415'); -- dim.

INSERT INTO schema_migrations (version) VALUES ('20130920104926'); -- ac-tax
INSERT INTO schema_migrations (version) VALUES ('20130927042227'); -- worker
INSERT INTO schema_migrations (version) VALUES ('20131001095420'); -- petty pay
INSERT INTO schema_migrations (version) VALUES ('20131007021712');
INSERT INTO schema_migrations (version) VALUES ('20131009000000');
INSERT INTO schema_migrations (version) VALUES ('20131015041650');
INSERT INTO schema_migrations (version) VALUES ('20131017023340');
INSERT INTO schema_migrations (version) VALUES ('20131017044322');
-- v2.99.0 ここまで
INSERT INTO schema_migrations (version) VALUES ('20131120065545');
INSERT INTO schema_migrations (version) VALUES ('20140108014702');
INSERT INTO schema_migrations (version) VALUES ('20140110013134');
INSERT INTO schema_migrations (version) VALUES ('20140319080159');
INSERT INTO schema_migrations (version) VALUES ('20140410063317');
-- v3.0系列ここまで
INSERT INTO schema_migrations (version) VALUES ('20140501025048');
INSERT INTO schema_migrations (version) VALUES ('20140502040405');
INSERT INTO schema_migrations (version) VALUES ('20140925015324');
INSERT INTO schema_migrations (version) VALUES ('20140925051616');
INSERT INTO schema_migrations (version) VALUES ('20140926041148');
INSERT INTO schema_migrations (version) VALUES ('20150331072845');
INSERT INTO schema_migrations (version) VALUES ('20150401001915');
INSERT INTO schema_migrations (version) VALUES ('20160912000813'); -- change_petty_expense_tables
INSERT INTO schema_migrations (version) VALUES ('20161104062306'); -- Petty extend payment type
INSERT INTO schema_migrations (version) VALUES ('20161125014400'); -- Petty expense drop drcr
INSERT INTO schema_migrations (version) VALUES ('20170102093329'); -- Create petty payees
INSERT INTO schema_migrations (version) VALUES ('20170120070048'); -- Change petty cash advances

-- Reboot -----------------------------------
INSERT INTO schema_migrations (version) VALUES ('20170416101429'); -- attach_file_extend_lengths.rb
INSERT INTO schema_migrations (version) VALUES ('20181213062940'); -- add_keiri_memo_to_sales_invoices.rb
INSERT INTO schema_migrations (version) VALUES ('20190108021226'); -- add_je_number_to_sales_invoices.rb
INSERT INTO schema_migrations (version) VALUES ('20190108022727'); -- add_je_number_to_invoices.rb
INSERT INTO schema_migrations (version) VALUES ('20190805234504'); -- add_column_to_vat.rb
INSERT INTO schema_migrations (version) VALUES ('20190819074134'); -- add_column_to_si_detail.rb
INSERT INTO schema_migrations (version) VALUES ('20190819074239'); -- add_column_to_sales_invoice.rb
INSERT INTO schema_migrations (version) VALUES ('20190819105656'); -- add_column_to_company.rb
INSERT INTO schema_migrations (version) VALUES ('20190926043359'); -- add_optionto_vat.rb


