
-- 勘定科目の表示順
CREATE TABLE reporting_titles (
  id               serial PRIMARY KEY,
  report_id        int NOT NULL REFERENCES reports(id),
  position         int NOT NULL,   -- for acts_as_list
  account_title_id int NOT NULL REFERENCES account_titles(id),

  -- 科目は複数回, 表示可.
  UNIQUE (report_id, position)
);
