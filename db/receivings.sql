
-- 入金伝票の, 1行の債権
-- 債権残高は請求書と突合して求める。
CREATE TABLE receivings (
  id                serial PRIMARY KEY,

  -- 親
  cash_slip_id      int NOT NULL REFERENCES payment_slips (id),
  -- receiving_slip_id int NOT NULL REFERENCES receiving_slips (id),

  -- sales_invoice または petty_expense のいずれかを埋める
  -- 販売請求書. 分割入金あり.
  sales_invoice_id  int REFERENCES sales_invoices(id),

  -- 小口の未収入金
  petty_expense_id  int REFERENCES petty_expenses (id),

  -- 消し込む債権の金額. 一部入金がある
  -- 通貨は債権の通貨.
  amount            int NOT NULL,
  currency_id       int NOT NULL REFERENCES currencies (id),

  charge            int NOT NULL, -- 手数料当方負担

  created_at        TIMESTAMP NOT NULL
);
