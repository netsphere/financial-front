
-- 取引先（販売先、仕入先の両方をかねる）
-- リーガルエンティティを表す。
CREATE TABLE partners (
  id           serial PRIMARY KEY,

  -- (代表)取引先コード
  -- 振込先口座 (bank_accountsレコード) がないときがあるので、こちらからも参照。
  zaimu_id     int    NOT NULL UNIQUE, 

  tdb_code     int UNIQUE,

  name         VARCHAR(80) NOT NULL,
  kana_name    VARCHAR(60) NOT NULL,
  hq_zip       VARCHAR(8) NOT NULL,   -- 本社郵便番号
  hq_addr      VARCHAR(100) NOT NULL,
  industry     VARCHAR(60) NOT NULL,  -- 主業

  remarks      TEXT NOT NULL,       -- コメント

  created_at   TIMESTAMP NOT NULL,
  created_by   int REFERENCES users (id), -- インポートでは設定しない
  updated_at   TIMESTAMP,
  updated_by   int REFERENCES users (id),
  lock_version int NOT NULL
);
