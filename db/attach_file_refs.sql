-- 添付ファイルと参照アイテムとの繋ぎ
CREATE TABLE attach_file_refs (
  id             serial PRIMARY KEY,
  attach_file_id int NOT NULL REFERENCES attach_files(id),
  partner_id     int REFERENCES partners(id),
  invoice_id     int REFERENCES invoices(id),
  created_at     TIMESTAMP,
  UNIQUE (attach_file_id, partner_id, invoice_id)
);
