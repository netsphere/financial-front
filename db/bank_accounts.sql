-- 取引先 (仕入先) の銀行口座 (JP) を表す
-- 財務大将から取り込む。経理システム (仮) では登録・更新できない
CREATE TABLE bank_accounts (
  id             serial PRIMARY KEY,      

  -- 親
  partner_id     int NOT NULL REFERENCES partners (id),

  -- 財務大将での支払先コード。取引先コードとは違うことがある。
  zaimu_ac       int NOT NULL UNIQUE,    
  active         boolean NOT NULL,

  bank_branch_id int NOT NULL REFERENCES bank_branches(id),
  ac_type        int NOT NULL,  -- 科目     N 1  1=普通 2=当座 4=貯蓄 9=その他
  ac_number      int NOT NULL,  -- 口座番号 N 7
  kana_name      VARCHAR(30) NOT NULL, -- 受取人名（半角カナ） C 30

  created_at      TIMESTAMP NOT NULL,
  -- インポートでは設定しない
  create_user_id int REFERENCES users (id), 
  updated_at      TIMESTAMP,
  update_user_id int REFERENCES users (id),
  lock_version    int NOT NULL,

  UNIQUE (bank_branch_id, ac_type, ac_number)
);
