
-- 顧客に対する請求書の明細
CREATE TABLE si_details (
  id               serial PRIMARY KEY,
  sales_invoice_id int NOT NULL REFERENCES sales_invoices (id),

  -- 行番号。1開始
  line_no          int NOT NULL,  

  unit_price       int,            -- 単価 (参考). 正規化される
  qty              int,            -- 数量 (参考)
  unit             VARCHAR(10),    -- 単位 (参考)。とりあえず文字列で。
  amount           NUMERIC(12, 0) NOT NULL,   -- 金額 (必須). 正規化される

  -- 外税かどうか. 0=税なし, 20=外税5% (for compatibility), 18=外税8%
  vat_code         int NOT NULL,

  description      VARCHAR(200) NOT NULL,

  -- 立替金 = TRUE
  advance   BOOLEAN NOT NULL,
  
  UNIQUE (sales_invoice_id, line_no)
);
