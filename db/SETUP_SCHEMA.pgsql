
\i users.sql

-- 基本マスタ
\i schema_migrations.sql
\i account_titles.sql
\i account_title_trees.sql
\i control_divisions.sql
-- \i roles.sql
\i currencies.sql
\i company_settings.sql

\i dimensions.sql
\i divisions.sql
\i controllers_divisions.sql

\i action_groups.sql
\i work_actions.sql
\i kubuns.sql

\i banks.sql
\i bank_branches.sql

-- 取引先
\i partners.sql
\i credit_inquiries.sql
\i bank_accounts.sql
\i en_bank_accounts.sql
\i bp_addresses.sql

\i general_ledger.sql

\i currency_rates.sql

\i global_settings.sql
\i company_settings2.sql
\i our_banks.sql
\i our_contacts.sql
\i our_branches.sql

\i vats.sql

-- (購買)請求書
\i invoices.sql
\i invoice_details.sql
\i invoice_log.sql

\i attach_kinds.sql
\i attach_files.sql
\i attach_file_refs.sql

-- 入金/出金伝票
\i payment_slips.sql
\i payments.sql
\i payment_banks.sql

-- 販売請求書
\i sales_invoices.sql
\i si_details.sql
\i si_banks.sql
\i si_logs.sql

\i worker_positions.sql
\i users_divisions.sql
\i si_journals.sql
\i import_declarations.sql
\i reports.sql
\i reporting_titles.sql
\i imports_invoices.sql
-- \i receiving_slips.sql
\i rs_logs.sql

\i qualifications.sql
\i users_qualifications.sql
-- purchase_orders.sql

-- 小口精算
\i petty_groups.sql
\i petty_payees.sql
\i petty_cash_advances.sql
\i petty_expenses.sql
\i petty_expense_entries.sql
\i petty_expense_allocations.sql
\i petty_offsets.sql
\i petty_pays.sql
-- 入金伝票
\i receivings.sql

\i default_dimensions.sql

-- MM => (購買)請求書
\i mat_parts.sql
\i mat_receivings.sql
\i mat_receiving_charges.sql

