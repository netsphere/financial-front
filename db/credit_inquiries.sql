-- 信用調査
-- 継続記録する（評点の推移を見る）
CREATE TABLE credit_inquiries (
  id             serial PRIMARY KEY,
  partner_id     int NOT NULL REFERENCES partners(id),

  report_date    DATE NOT NULL,
  -- 評点なしの場合がある
  score          int,
  remarks        TEXT NOT NULL,

  -- 一括登録のときはナル
  create_user_id int REFERENCES users(id),
  created_at     TIMESTAMP NOT NULL
);
